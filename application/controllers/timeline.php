
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Timeline extends CI_Controller {
	private $tokenError = array(
							'undefined_rest_api' => array('Error 1.3' => 'A url passada não corresponde a um método válido!'), 
							'missed_token' => array('Error 2' => 'Token de acesso não informado.'), 
							'unknown_token' => array('Error 2.1' => 'Token de acesso desconhecido.',), 
							'error_troll' => array('Error 666' => 'too bad! Informe um número seu mané.'),
							'unknown_post' => array('Content not found' => 'Este conteúdo não existe ou foi deletado pelo usuário.')
			);

	function __construct()
	{
		parent::__construct();
		$this->load->library('authorization');
	}


	/*
	* ajax_set_post()
	* Set post
	* @param 
	* return void
	*/
	function ajax_set_post(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();
		// Pega os dados vindo do formulário
		$args['filter'] = POST();
		// Converte id para id de banco
		$decryptedID = decrypt($args['filter']['id'], TOKENHASH);
		// Verifica se bate com o da sessão
		if(sessionID() == $decryptedID){

			$arrPost['token'] 		= sessionID();
			$arrPost['conteudo'] 	= ext($args['filter'], 'conteudo');
			$arrPost['data_postagem']= date('Y-m-d H:i:s');
			// Insere na tabela
			$insert = $this->system_model->insert('sep_timeline', $arrPost);
			// Retorna o resultado
			if($this->system_model->affected_rows() == 1){
				echo 1;
			} else {
				echo 0;
			}

		} else {
			Authorization::ajaxCuspe();
		}

	}



	function mongo(){


		$m = new MongoClient();

		$db = $m->timeline;

		$collection = $db->posts;

		$cursor = $collection->find();

		foreach ($cursor as $document) {

			
				echo $document["component"] . "<br>";
			

		}
	}

	function timeline_modal_image()
	{
		error_reporting(0);
		// Pega tipo de arquivo
		$format = end(explode('.', $_FILES['file_upload']['name']));

		// data e hora
		$data = date('Y-m-d');
		$hour = date('H:i:s');
		// Explode
		$date = explode('-', $data);
		$hora = explode(':', $hour);
		
		// Catch data coming from ajax
		$args['filter'] = $this->input->post();

		/*
		* Ordem dos arrays passados
		* string + dia + mes + ano + hora + min + seg 
		*/
		$tokenMD5 = md5($_FILES['file_upload']['name']);
		$file =  string_aleatoria(10) . '_' . $tokenMD5 . '_' .  $data[2] . '_' . $data[1] . '_' . $data[0] . '_' . $hora[0] . '_' . $hora[1] . '_' . $hora[2] . '.' . $format;		


		generateLOG($this->session->userdata('token'), 'TIMELINE/UPLOAD_IMAGEM');
			
		// Move anexo para pasta
		$upload = move_uploaded_file($_FILES['file_upload']['tmp_name'], UPLOAD . 'timeline/' . $file);

		if(file_exists(UPLOAD . 'timeline/' . $file)){
			
			$link = UPLOADLINK .  'timeline/' .  $file;
			echo $link;
		} else {
			echo "Erro interno ao realizar upload :(";
		}
	}

	function public_post($token = null){
		if($token != null){
			
			$tokenReverse = decrypt($token, tokenTIMELINE);			
				
			$m = new MongoClient();
			$db = $m->timeline; 
			$collection = $db->posts;
			$args['cursor'] = $collection->findOne( array('_id' => new MongoId($tokenReverse) ) );


			// Get Json
			$user = restApiReturnJson($args['cursor']['token']);
			
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['date'] 		= $user[0]->{'data_registro'};
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['email'] 		= $user[0]->{'email'};
			$args['type'] 		= $user[0]->{'tipo'};
			$args['level'] 		= $user[0]->{'nivel'};
			$args['name'] 		= $user[0]->{'nome'};
			$args['surname'] 	= $user[0]->{'sobrenome'};
			$args['cover'] 		= $user[0]->{'foto_capa'};
			$args['profile'] 	= $user[0]->{'foto_perfil'};
			$args['account'] 	= $user[0]->{'plano'};


			if($this->session->userdata('logado'))
			{			
				$userLOGGED = restApiReturnJson($this->session->userdata('token'));
				$args['useridLOGGED'] 	= $userLOGGED[0]->{'usuarioid'};
				$args['dateLOGGED'] 		= $userLOGGED[0]->{'data_registro'};
				$args['useridLOGGED'] 	= $userLOGGED[0]->{'usuarioid'};
				$args['emailLOGGED'] 		= $userLOGGED[0]->{'email'};
				$args['typeLOGGED'] 		= $userLOGGED[0]->{'tipo'};
				$args['levelLOGGED'] 		= $userLOGGED[0]->{'nivel'};
				$args['nameLOGGED'] 		= $userLOGGED[0]->{'nome'};
				$args['surnameLOGGED'] 	= $userLOGGED[0]->{'sobrenome'};
				$args['coverLOGGED'] 		= $userLOGGED[0]->{'foto_capa'};
				$args['profileLOGGED'] 	= $userLOGGED[0]->{'foto_perfil'};
				$args['accountLOGGED'] 	= $userLOGGED[0]->{'plano'};
			}

			VIEW('public/timeline/public_post', $args);
	
		
		} else {
			header('Content-Type: text/json; charset=utf-8'); 
			echo json_encode($this->tokenError['unknown_post'], JSON_UNESCAPED_UNICODE);
		}
	}


}

/* End of file timeline.php */
/* Location: ./application/controllers/timeline.php */

