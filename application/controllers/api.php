
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(0);
class Api extends CI_Controller {
	
	private $token = 'f3161a2765a1ecce55077385f3ca0bdcba7ca9cf';
	private $tokenError = array(
							'undefined_rest_api' => array('Error 1.3' => 'A url passada não corresponde a um método válido!'), 
							'missed_token' => array('Error 2' => 'Token de acesso não informado.'), 
							'unknown_token' => array('Error 2.1' => 'Token de acesso desconhecido.',), 
							'error_troll' => array('Error 666' => 'too bad! Informe um número seu mané.')
			);
	/*
	* Index()
	* Area do admin
	* @param 
	* return void
	*/
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);
	}

	function api_following($token, $tokenUser)
	{
		//echo SERVER . 'application/assets/internaljson/following/' . $tokenUser . '.json';
		if($token == $this->token ){
			if(file_exists(SERVER . 'application/assets/internaljson/following/' . $tokenUser . '.json')){
								
				$handle = fopen(SERVER . 'application/assets/internaljson/following/' . $tokenUser . '.json', "r+");
				$json = stream_get_contents($handle);
				fclose($handle);			
				
				header('Content-Type: text/json; charset=utf-8'); 
				// Return json
				echo $json;
			} else {
				header('Content-Type: text/html; charset=utf-8'); 
				echo json_encode($this->tokenError['undefined_rest_api'], JSON_UNESCAPED_UNICODE);
			}

		} else {
			echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);
		}
	}
	function get_questions_count($token)
	{
		
		if($token == $this->token)
		{
			// Deve ler de .json
			MODEL('api_model');
			$json = $this->api_model->get_questions_count();
			
			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($json[0], JSON_UNESCAPED_UNICODE);

		} 
		else if(!$token)
		{

			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['missed_token'], JSON_UNESCAPED_UNICODE);
		}
		else {
			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);
		}
	}


	function get_questions_recorded($token)
	{
		
		if($token == $this->token)
		{
			// Deve ler de .json
			MODEL('api_model');
			$json = $this->api_model->get_questions_recorded();
			
			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($json[0], JSON_UNESCAPED_UNICODE);

		} 
		else if(!$token)
		{

			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['missed_token'], JSON_UNESCAPED_UNICODE);
		}
		else {
			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);
		}
	}



	function get_questions_by_discipline($token)
	{
		
		if($token == $this->token)
		{
			// Deve ler de .json
			MODEL('api_model');
			$json = $this->api_model->get_questions_by_discipline();
			
			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($json, JSON_UNESCAPED_UNICODE);

		} 
		else if(!$token)
		{

			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['missed_token'], JSON_UNESCAPED_UNICODE);
		}
		else {
			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);
		}
	}


	function get_questions_selecteds($token)
	{
		if($token == $this->token)
		{
			// Deve ler de .json
			MODEL('api_model');
			$json = $this->api_model->get_questions_selecteds();
			
			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($json, JSON_UNESCAPED_UNICODE);

		} 
		else if(!$token)
		{
			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['missed_token'], JSON_UNESCAPED_UNICODE);
		}
		else {

			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);
		}
	}




	/********************************
	********* API USERS *************
	*********************************/
	function get_user_data($token)
	{
		if($token)
		{
			
			if(file_exists(SERVER . 'application/assets/internaljson/' . $token . '.json')){
								
				$handle = fopen(SERVER . 'application/assets/internaljson/' . $token . '.json', "r+");
				$json = stream_get_contents($handle);
				fclose($handle);			
				
				header('Content-Type: text/json; charset=utf-8'); 
				// Return json
				echo $json;
			}
		} 
		else if(!$token)
		{
			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['missed_token'], JSON_UNESCAPED_UNICODE);
		}
		else {

			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);
		}
	}

	function get_timeline_normal($token)
	{
		if($token)
		{
			
			if(file_exists(SERVER . 'application/assets/internaljson/' . $token . '.json')){
								
				$handle = fopen(SERVER . 'application/assets/internaljson/' . $token . '.json', "r+");
				$json = stream_get_contents($handle);
				fclose($handle);			
				
				header('Content-Type: text/json; charset=utf-8'); 
				// Return json
				echo $json;
			}
		} 
	}


	function get_question_info($qcid)
	{

		if($qcid)
		{
			
			MODEL('api_model');
			$json = $this->api_model->get_question_info($qcid);
			
			header('Content-Type: text/html; charset=utf-8'); 
			// Return json
			//echo json_encode($json[0], JSON_UNESCAPED_UNICODE);
			//var_dump($json[0]);
			echo 'Prova: <b><h3>' .ext($json[0], 'prova') . '</h3></b><hr/>';
			echo 'Banca: <b><h3>' .ext($json[0], 'banca') . '</h3></b><hr/>';
			echo 'Orgao: <b><h3>' .ext($json[0], 'orgao') . '</h3></b><hr/>';
			echo 'Disciplina: <b><h3>' .ext($json[0], 'disciplina') . '</h3></b><hr/>';
			echo 'Assunto: <b><h3>' .ext($json[0], 'assunto1') . '</h3></b><hr/>';
			echo 'Questão: <b><h3>' .ext($json[0], 'questao') . '</h3></b><hr/>';
		} else {
			header('Content-Type: application/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['error_troll'], JSON_UNESCAPED_UNICODE);
		}
	}



	function get_notifications($token)
	{

		if($token != null){

			MODEL('api_model');

			$tokenReverse = decrypt($token, TOKENHASH);

			$updates = $this->api_model->get_notifications($tokenReverse);

			$arrayTranslate = array(
				'LOGIN' => 'Fez login no sistema',
				'LOGOUT' => 'Saiu do sistema',
				'SIMULADO/CRIADO' => 'Iniciou um novo simulado',
				'ALTERAR/FOTO_PERFIL' => 'Alterou a foto de perfil',
				'ALTERAR/FOTO_CAPA' => 'Alterou a foto de capa',
				'SIMULADO/CRIADO/VIMEO' => 'Iniciou um novo simulado versão assinante',
				'FORUM/RESPONDER' => 'Respondeu um tópico no fórum',
				'FORUM/DELETAR' => 'Deleteu uma resposta no fórum',
				'SEGUIR/USUARIO' => 'Está seguindo mais uma pessoa',
				'DADOS/EDITAR' => 'Editou seus dados',
				'DEFAULT' => 'Está descobrindo as funcionalidades do Simule & Passe - Descubra também!!',
				'NOTICIA/CRIADA' => 'Postou uma nova notícia',
				'NOTICIA/BLOG' => 'Postou no blog',
				'NOTICIA/EDITAL' => 'Postou sobre algum edital'
			);
			$html = '';
			foreach($updates as $notification):
			$notificationList = ($arrayTranslate[$notification['evento']] == '') ? $arrayTranslate['DEFAULT'] : $arrayTranslate[$notification['evento']];
	
				$html .= '<div class="media border-bottom innerAll margin-none">';
				$html .= '<img src="'.$notification['foto_perfil'].'" class="pull-left media-object" width="40"/>';
				$html .= '<div class="media-body">';
				$html .= $notification['nome'];
				$html .= '<h5 class="margin-none"><a href="" class="text-inverse">'.$notificationList.'</a></h5>';
				$html .= '<small>'.timestamp_decode_date($notification['data_registro']) .' '.timestamp_decode_time($notification['data_registro']).'</small>';
				$html .= '</div>';
				$html .= '</div>';
			endforeach;

			echo $html;

		} else {
			echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);	
		}
	}


	function resend_mail(){

		MODEL('system_model');

		$where = ' ativo LIKE "N"';
		$usuarios = $this->system_model->select_where('sis_usuario', $where);

		foreach ($usuarios as $dados):
			
			$message = '<strong>Bem-vindo ao Simule & Passe! Libere seu cadastro.</strong><br/>';
			$message .= 'Você cadastrou seu email com sucesso!!!<br/>';
			$message .= 'Aproveite agora mesmo para acessar o sistema e começar seus estudos!';
			$message .= '<br /><br />';
			$message .= 'Para acessar seu cadastro utilize o link abaixo:<br/>';
			$message .= 'http://www.simuleepasse.com.br/login/validar_auto_cadastro/' . $dados['token'];
			$arrMail = array(
				'to_name' => $dados['nome'],
				'to_mail' => $dados['email'],
				'subject' => 'Ative seu cadastro no Simule & Passe',
				'message' => $message
			);
			send_mailgun($arrMail);
			echo "email enviado ". $dados['email'] . " e ativo =". $dados['ativo']  ." <br />";

			//sleep(5);
		endforeach;

	}

	function vimeo_test($video, $user)
	{	
		generateLOG($user, 'VIMEO/TEST');
		echo '<iframe src="//player.vimeo.com/video/'. $video .'" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
	}

}

/* End of file api.php */
/* Location: ./application/controllers/api.php */


