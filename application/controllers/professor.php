<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Professor extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		$arrayTokens = array(
			'cf0452315167af2e8c3494d2535', '7fdbce88888a75e493b71e6dc04','6bf292d07a038b6b178b0e66687', 
			'c0f2800ad21e9e3b40e4adcb1c0','0afc14557631fafc47de49ff133', 'b5b2e20eb2c0e80e99b4a0a7449',
			'c0566a905dc32d6d27768605701', '0f9f486709135f5d0906eb36f59','5ede98094458f938863ac126791', 
			'e0b6403481cd302008886887e38','68b8a3d7605bfdd58e2d52a6486', '893428ea62bdb8d21bada9bb363',
			'482015ea01c617a37eadf91ce09','aa8388a5a045debf166b60a9ed4', 'aacd2a381e6570a7031b0a7d21e',
			'bff775219196c0d3018e45f701d','3910590908c323f11aea42fb46f', '81b1e121d118fc5ccfc3e59c3c3',
			'3f747c6f5313ea9cb335bd4fd62','4df9e6da71b00314e65c5f3ddd4', '7a1f0d3d91a8e34a774beb0c6f2',
			'e58eecdde1e7ce7fce9521094da','4d071c43c32373f5b066ec39591', '7a3f95dcd67d6cd662ce3df3440',
			'6557dfa10c16d454151c73f6086','df6cabe908827aba7f1b7ea5f00', '961daa97b3a09c1f80eec05bb45',
			'384d36c58d9c8ebf907b7a02f10', 'f57442dfb075acbfc0679fd5256', '583daa670e4dd907b8ecf2cdc30',
			'c689a80984bea6965156eda228f', 'f85a689a876f9d59ad5fc3be8b0'
		);


		/*
		$count = count($arrayTokens);
		$x = 0;
		$contain = 0;
		while($x < $count){
			
			if($this->session->userdata('token') == $arrayTokens[$x]){
				$contain =1;
			}
			$x++;
		}
		if($contain == 0){
			redirect('usuario/');
		}
		*/

	}


	function questao($qcid)
	{
		if($this->session->userdata('logado') == true && $this->session->userdata('nivel') == 2 || $this->session->userdata('nivel') == 1)
		{
			
			MODEL('professor_model');
			$args['data'] = $this->professor_model->get_question_by_qcid($qcid);
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.html')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.htm')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.htm'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.jpg')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.png')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-A.html')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'-A.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-A.jpg')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'-A.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-A.png')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'-A.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-B.html')){ $args['imagem'][1] = UPLOADLINK . 'complementar/Q'.$qcid.'-B.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-B.jpg')){ $args['imagem'][1] = UPLOADLINK . 'complementar/Q'.$qcid.'-B.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-B.png')){ $args['imagem'][1] = UPLOADLINK . 'complementar/Q'.$qcid.'-B.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-C.html')){ $args['imagem'][2] = UPLOADLINK . 'complementar/Q'.$qcid.'-C.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-C.jpg')){ $args['imagem'][2] = UPLOADLINK . 'complementar/Q'.$qcid.'-C.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-C.png')){ $args['imagem'][2] = UPLOADLINK . 'complementar/Q'.$qcid.'-C.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-D.html')){ $args['imagem'][3] = UPLOADLINK . 'complementar/Q'.$qcid.'-D.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-D.jpg')){ $args['imagem'][3] = UPLOADLINK . 'complementar/Q'.$qcid.'-D.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-D.png')){ $args['imagem'][3] = UPLOADLINK . 'complementar/Q'.$qcid.'-D.png'; }

			// Carrega view de professor
			VIEW('dashboard/professor/questao', $args);
		} else {
			// Redireciona
			redirect('login/logout');
		}
	}

	function ajax_get_questionsByDiscipline()
	{
	$args['filtro'] = $this->input->post();
		$discipline = ext($args['filtro'], 'discipline');

		MODEL('professor_model');
		$args['data'] = $this->professor_model->ajax_get_questionsByDiscipline($discipline);

		VIEW('/dashboard/professor/ajax_get_questionsByDiscipline', $args);


	}


	function ajax_professor_set_question()
	{
		$args['filtro'] = $this->input->post();

		$arrParam['usuarioid'] 	= ext($args['filtro'], 'usuarioid');
		$arrParam['professor'] 	= ext($args['filtro'], 'professor');
		$arrParam['questaoid'] 	= ext($args['filtro'], 'questaoid');
		$arrParam['qcid'] 		= ext($args['filtro'], 'qcid');
		$arrParam['prova'] 		= ext($args['filtro'], 'prova');
		$arrParam['disciplina'] = ext($args['filtro'], 'disciplina');

		MODEL('system_model');
		MODEL('professor_model');


		error_reporting(0);
		if(count($this->professor_model->get_question_teacher(ext($args['filtro'], 'qcid'))) > 0){
			echo "Esta questão não pode ser salva pois já foi escolhida por alguém :/";
		} else {

			if($this->system_model->insert('sep_professores_prova', $arrParam)){
				echo "Questão salva com sucesso!";
			} else {
				echo "Esta questão não pode ser salva pois já foi escolhida por alguém :/";
			}
		}

			
		
	}
	function questoes_selecionadas()
	{
		if($this->session->userdata('logado') == true && $this->session->userdata('nivel') == 2 || $this->session->userdata('nivel') == 1)
		{
			$user = restApiReturnJson($this->session->userdata('token'));
		

			MODEL('professor_model');
			$args['data'] = $this->professor_model->get_question_teacher_selected($this->session->userdata('idusuario'));
			$args['data_count'] = $this->professor_model->get_count_question($this->session->userdata('idusuario'));


			// Carrega view de professor
			VIEW('dashboard/professor/questoes_selecionadas', $args);
		} else {
			// Redireciona
			redirect('login/logout');
		}
	}


	function ver_questao_selecionada($qcid)
	{
		if($this->session->userdata('logado') == true && $this->session->userdata('nivel') == 2  || $this->session->userdata('nivel') == 1)
		{
			
			MODEL('usuario_model');
			MODEL('professor_model');

			$args['usuario'] = $this->usuario_model->get_user_dados($this->session->userdata('idusuario'));
			$args['data'] = $this->professor_model->get_question_teacher_selected_by_id($qcid);

			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.html')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.htm')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.htm'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.jpg')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.png')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-A.html')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'-A.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-A.jpg')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'-A.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-A.png')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'-A.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-B.html')){ $args['imagem'][1] = UPLOADLINK . 'complementar/Q'.$qcid.'-B.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-B.jpg')){ $args['imagem'][1] = UPLOADLINK . 'complementar/Q'.$qcid.'-B.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-B.png')){ $args['imagem'][1] = UPLOADLINK . 'complementar/Q'.$qcid.'-B.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-C.html')){ $args['imagem'][2] = UPLOADLINK . 'complementar/Q'.$qcid.'-C.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-C.jpg')){ $args['imagem'][2] = UPLOADLINK . 'complementar/Q'.$qcid.'-C.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-C.png')){ $args['imagem'][2] = UPLOADLINK . 'complementar/Q'.$qcid.'-C.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-D.html')){ $args['imagem'][3] = UPLOADLINK . 'complementar/Q'.$qcid.'-D.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-D.jpg')){ $args['imagem'][3] = UPLOADLINK . 'complementar/Q'.$qcid.'-D.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-D.png')){ $args['imagem'][3] = UPLOADLINK . 'complementar/Q'.$qcid.'-D.png'; }


			// Carrega view de professor
			VIEW('dashboard/professor/questao', $args);
		} else {
			// Redireciona
			redirect('login/logout');
		}
	}

	function questoes_selecionadas_gravar(){
		if($this->session->userdata('logado') == true && $this->session->userdata('nivel') == 2  || $this->session->userdata('nivel') == 1)
		{
			
			MODEL('usuario_model');
			MODEL('professor_model');

			$args['usuario'] = $this->usuario_model->get_user_dados($this->session->userdata('idusuario'));
			$args['data'] = $this->professor_model->get_question_teacher_selected($this->session->userdata('idusuario'));

			VIEW('/dashboard/professor/questoes_selecionadas_gravar', $args);
		} else {
			// Redireciona
			redirect('login/logout');		
		}
	}



	function teste()
	{
		if($this->session->userdata('logado') == true && $this->session->userdata('nivel') == 2)
		{
			
			MODEL('usuario_model');
			MODEL('professor_model');

			$args['usuario'] = $this->usuario_model->get_user_dados($this->session->userdata('idusuario'));

			// Carrega view de professor
			VIEW('usuario/default2', $args);
		} else {
			// Redireciona
			redirect('login/logout');
		}
	}


	/*****************************************************
	******************************************************
	************  NOVA AREA DE PROFESSORES  **************
	******************************************************
	******************************************************/
	function index()
	{		
		if($this->session->userdata('logado') == true && $this->session->userdata('nivel') == 2  || $this->session->userdata('nivel') == 1)
		{			
			MODEL('professor_model');
			$args['discipline'] = $this->professor_model->get_unique_discipline();

			// get teacher view
			VIEW('dashboard/professor/novo_default', $args);
		} else {
			// if user is not logged or don't have permission
			redirect('login/logout');
		}
	}

	function listar_questoes()
	{
		if($this->session->userdata('logado') == true && $this->session->userdata('nivel') == 2  || $this->session->userdata('nivel') == 1)
		{	
			$args['filtro'] = $this->input->post();
			$discipline = ext($args['filtro'], 'disciplina');
			$args['disciplina'] = $discipline;
			// Load teacher model
			MODEL('professor_model');
			// Get disciplines
			$args['discipline'] = $this->professor_model->get_unique_discipline();
			// Get questions by discipline
			$args['data'] = $this->professor_model->ajax_get_questionsByDiscipline($discipline);
			
			// get teacher view
			VIEW('dashboard/professor/listar_questoes', $args);
		} else {
			// if user is not logged or don't have permission
			redirect('login/logout');
		}
	}

	function gravar_questoes()
	{
		if($this->session->userdata('logado') == true && $this->session->userdata('nivel') == 2  || $this->session->userdata('nivel') == 1)
		{	
			$args['filtro'] = $this->input->post();
			$discipline = ext($args['filtro'], 'disciplina');
			$args['disciplina'] = $discipline;
			// Load teacher model
			MODEL('professor_model');
			// Get disciplines
			$args['discipline'] = $this->professor_model->get_unique_discipline();
			// Get questions by discipline
			$args['data'] = $this->professor_model->ajax_get_questionsByDiscipline($discipline);
			
			// get teacher view
			VIEW('dashboard/professor/listar_questoes', $args);
		} else {
			// if user is not logged or don't have permission
			redirect('login/logout');
		}
	}




	function gravar($qcid = null)
	{
		if($this->session->userdata('logado') == true && $this->session->userdata('nivel') == 2  || $this->session->userdata('nivel') == 1)
		{
			
			MODEL('usuario_model');
			MODEL('professor_model');

			$args['usuario'] = $this->usuario_model->get_user_dados($this->session->userdata('idusuario'));
			$args['data'] = $this->professor_model->get_question_teacher_selected_by_id($qcid);

			
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.html')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.htm')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.htm'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.jpg')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '.png')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-A.html')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'-A.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-A.jpg')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'-A.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-A.png')){ $args['imagem'][0] = UPLOADLINK . 'complementar/Q'.$qcid.'-A.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-B.html')){ $args['imagem'][1] = UPLOADLINK . 'complementar/Q'.$qcid.'-B.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-B.jpg')){ $args['imagem'][1] = UPLOADLINK . 'complementar/Q'.$qcid.'-B.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-B.png')){ $args['imagem'][1] = UPLOADLINK . 'complementar/Q'.$qcid.'-B.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-C.html')){ $args['imagem'][2] = UPLOADLINK . 'complementar/Q'.$qcid.'-C.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-C.jpg')){ $args['imagem'][2] = UPLOADLINK . 'complementar/Q'.$qcid.'-C.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-C.png')){ $args['imagem'][2] = UPLOADLINK . 'complementar/Q'.$qcid.'-C.png'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-D.html')){ $args['imagem'][3] = UPLOADLINK . 'complementar/Q'.$qcid.'-D.html'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-D.jpg')){ $args['imagem'][3] = UPLOADLINK . 'complementar/Q'.$qcid.'-D.jpg'; }
			if(file_exists(UPLOAD . 'complementar/Q'. $qcid . '-D.png')){ $args['imagem'][3] = UPLOADLINK . 'complementar/Q'.$qcid.'-D.png'; }

			// Carrega view de professor
			VIEW('dashboard/professor/gravar', $args);
		} else {
			// Redireciona
			redirect('login/logout');
		}
	}

	function ajax_set_gravadaOkay()
	{
		$args['filtro'] = $this->input->post();
		$qcid = ext($args['filtro'], 'qcid');

		$arrParam['gravada'] = 'SIM';
		$where = array('qcid' => $qcid);
		MODEL('system_model');

		error_reporting(0);
		if($this->system_model->update('sep_professores_prova', $arrParam, $where) > 0)
		{
			echo 1;
		} else {
			echo 0;
		}

	}


	function question_delete()
	{
		$args['filtro'] = $this->input->post();

		$qcid = ext($args['filtro'], 'qcid');

		$where = array('qcid' => $qcid);
		MODEL('system_model');
		error_reporting(0);
		if($this->system_model->delete('sep_professores_prova', $where) > 0)
		{
			echo 1;
		} else {
			echo 0;
		}
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

