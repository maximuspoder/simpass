<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questoes extends CI_Controller {

	
	/*
	* Index()
	* Area administrativa inicial de questoes
	* @param 
	* return void
	*/
	function index($notify = null)
	{


			MODEL('questoes_model');
			
			$args['questoes'] = $this->questoes_model->get_questoes_imagem_dentro();
			$args['total'] = $this->questoes_model->get_questoes_count();

			//var_dump($args['questoes']);
			//die();
			
			if($notify != null && $notify == 1 || $notify ==0){
				$args['notify'] = $notify;	
			}
			
			// Carrega view de login
			VIEW('dashboard/questoes/default', $args);
	

	}

	/*
	* editar_imagem()
	* Edita imagem dentro da questão
	* @param 
	* return void
	*/
	function editar_imagem($qcid)
	{




			MODEL('questoes_model');
			
			$args['questao'] = $this->questoes_model->get_questoes_imagem_dentro_byQCID($qcid);
			$args['qcid'] = $qcid;

			// Carrega view de login
			VIEW('dashboard/questoes/editar_imagem', $args);

	}

	function editar_imagem_upload()
	{
	
		// Pega tipo de arquivo
		$format = end(explode('.', $_FILES['file_upload']['name']));
		// data e hora
		$data = date('Y-m-d');
		$hour = date('H:i:s');
		// Explode
		$date = explode('-', $data);
		$hora = explode(':', $hour);

		/*
		* Ordem dos arrays passados
		* string + dia + mes + ano + hora + min + seg 
		*/
		$file =  string_aleatoria(10) . '_' . $data[2] . '_' . $data[1] . '_' . $data[0] . '_' . $hora[0] . '_' . $hora[1] . '_' . $hora[2] . '.' . $format;

		
		// Move anexo para pasta
		$upload = move_uploaded_file($_FILES['file_upload']['tmp_name'], UPLOAD . '/anexo/' . $file);
	
		if(file_exists(UPLOAD . 'anexo/' . $file)){
				
			echo UPLOADLINK . 'anexo/' .$file;
	
		} else {
			echo "Erro interno ao realizar upload :(";
		}
	}

	function editar_imagem_process()
	{

			$args['filtro'] = $this->input->post();
			MODEL('system_model');

			$arrParam['questao'] =   trim(ext($args['filtro'], 'questao'));
			$arrParam['resposta1'] = str_replace('<br /><br />', '<br />', trim(ext($args['filtro'], 'resposta1')));
			$arrParam['resposta2'] = str_replace('<br /><br />', '<br />', trim(ext($args['filtro'], 'resposta2')));
			$arrParam['resposta3'] = str_replace('<br /><br />', '<br />', trim(ext($args['filtro'], 'resposta3')));
			$arrParam['resposta4'] = str_replace('<br /><br />', '<br />', trim(ext($args['filtro'], 'resposta4')));
			$arrParam['resposta5'] = str_replace('<br /><br />', '<br />', trim(ext($args['filtro'], 'resposta5')));
			$arrParam['editado'] = 's';



			$where = array('qcid' => ext($args['filtro'], 'qcid'));
		
			error_reporting(0);
			if($this->system_model->update('sep_questoes', $arrParam, $where)){

				redirect('questoes/index/1');
			} else {
				redirect('questoes/index/0');
			}

			// Carrega view de login
			VIEW('dashboard/questoes/editar_imagem', $args);

	}

	function questao($qcid)
	{
	
			
			MODEL('usuario_model');
			MODEL('professor_model');

			$args['usuario'] = $this->usuario_model->get_user_dados($this->session->userdata('idusuario'));
			$args['data'] = $this->professor_model->get_question_by_qcid($qcid);

			// Carrega view de professor
			VIEW('dashboard/questoes/questao', $args);

	}

	function buscar()
	{
	
			$args['filtro'] = $this->input->post();
			$qcid = (ext($args['filtro'], 'questao') != "") ? str_replace("Q", "", ext($args['filtro'], 'questao')) : 0;
			
			MODEL('usuario_model');
			MODEL('questoes_model');

			$args['usuario'] = $this->usuario_model->get_user_dados($this->session->userdata('idusuario'));
			$args['questoes'] = $this->questoes_model->get_question_by_qcid($qcid);

			// Carrega view de professor
			VIEW('dashboard/questoes/buscar', $args);

	}


	

}# end