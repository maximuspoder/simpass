<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Professores extends CI_Controller {

	public function __construct(){
		parent::__construct();
		MODEL('professores_model');

		if($this->session->userdata('nivel') != 2)
		{
			redirect('home');
		}
	}

	public function index(){
		redirect('professores/show');
	}

	public function show($page = 0)
	{
		if($this->session->userdata('logado') == true)
		{

            $args['filtro'] = $this->input->post();
            //print_r($args) . "<br>";

            $resultArray = $this->professores_model->get_questoes_listagem($args['filtro']);
            $this->load->library('array_table');
            $this->array_table->page_link = BASEURL . 'professores/show';
            $this->array_table->set_id('module_table');
            $this->array_table->set_data($resultArray);
            $this->array_table->set_actual_page($page);
            $this->array_table->set_items_per_page(50);
            $this->array_table->add_columnid('ID(#)');
            $this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Questão\', \'' . BASEURL . 'professores/ajax_show_questao/{0}\', \''. IMG.'icon_view.png\', 400, 600);"><i class="fa fa-search" title="Ver"></i></a>');
            
            $this->array_table->add_column('<a href="javascript:void(0);" onclick="redirect_to(\'editar/{0}\')"><i class="fa fa-edit" title="Editar"> </a>');
            $this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_professor_set_question_eliminate_div('.$this->session->userdata('idusuario').','. '\''.$this->session->userdata("nome").'\','. '{6}, {0},\'{5}\',\'{3}\')"><i class="fa fa-save" title="Salvar"></a>');
            $this->array_table->set_columns(array('ID(#)', 'Órgão', 'Banca', 'Disciplina'));
            // Processa tabela do modulo
            $args['module_table'] = $this->array_table->get_html();
            

			VIEW('dashboard/professores/default', $args);
		} else {
			redirect('home');
		}
	}

	function editar($questao){
		if($this->session->userdata('logado') == true)
		{

			$qcid = (int)$questao;
			$args['questao'] = $this->professores_model->get_questao_by_id($qcid);

			VIEW('dashboard/professores/editar', $args);
		} else {
			redirect('home');
		}
	}

	function editar_process($questao){
		if($this->session->userdata('logado') == true)
		{

			$qcid = (int)$questao;
			$args['filtro'] = $this->input->post();

			$arrParam['disciplina'] = ext($args['filtro'], 'disciplina');
			$arrParam['assunto1'] = ext($args['filtro'], 'assunto1');
			$arrParam['assunto2'] = ext($args['filtro'], 'assunto2');
			$arrParam['assunto3'] = ext($args['filtro'], 'assunto3');
			$arrParam['questao'] = ext($args['filtro'], 'questao');
			$arrParam['resposta1'] = ext($args['filtro'], 'resposta1');
			$arrParam['resposta2'] = ext($args['filtro'], 'resposta2');
			$arrParam['resposta3'] = ext($args['filtro'], 'resposta3');
			$arrParam['resposta4'] = ext($args['filtro'], 'resposta4');
			$arrParam['resposta5'] = ext($args['filtro'], 'resposta5');

			$where = array('qcid' => $qcid);
			$update = $this->system_model->update('sep_questoes', $arrParam, $where);

			if($this->system_model->affected_rows() > 0){
				$args['type'] = 'success';
				$args['info'] = 'Sucesso!';
				$args['link'] = 'professores/show';
				$args['message'] = 'A questão foi editada com sucesso.';
				VIEW('dashboard/message', $args);
			} else {
				$args['type'] = 'info';
				$args['info'] = 'Opa';
				$args['link'] = 'professores/show';
				$args['message'] = 'Nada aconteceu, veja se você realmente mudou alguma coisa na questão.';
				VIEW('dashboard/message', $args);
			}
		} else {
			redirect('home');
		}
	}

	/*
	* Exibe a questão em modal, na listagem
	*/
	function ajax_show_questao($questao){
		$qcid = (int)$questao;

		$where = ' qcid='. $qcid;
		$args['questao'] = $this->system_model->select_where('sep_questoes',  $where);

		echo "<h3>Questão:</h3> <br>";
		echo ext($args['questao'][0], 'questao');

		switch ($args['questao'][0]['resposta_correta']) {
			case 'A':
				$resposta = $args['questao'][0]['resposta1'];
				break;
			case 'B':
				$resposta = $args['questao'][0]['resposta2'];
				break;
			case 'C':
				$resposta = $args['questao'][0]['resposta3'];
				break;
			case 'D':
				$resposta = $args['questao'][0]['resposta4'];
				break;
			case 'E':
				$resposta = $args['questao'][0]['resposta5'];
				break;

			default:
				$resposta = 'SEM RESPOSTA ???';
				break;
		}
		echo "<br/><h5>Respostas:</h5>";
		echo $args['questao'][0]['resposta1'] . "<br/><br/>";
		echo $args['questao'][0]['resposta2'] . "<br/><br/>";
		echo $args['questao'][0]['resposta3'] . "<br/><br/>";
		echo $args['questao'][0]['resposta4'] . "<br/><br/>";
		echo $args['questao'][0]['resposta5'] . "<br/>";

		echo "<br/><h5>Resposta Correta:</h5>";
		echo '<div style="color:green; font-weight:bold">'.$resposta.'</div><br/><button onclick="parent.close_modal()" class="btn">Ok, fechar</button>';
	}



	/*
	* Salva a questão selecionada no filtro
	*/
	function ajax_professor_set_question()
	{
		$args['filtro'] = $this->input->post();

		$arrParam['usuarioid'] 	= ext($args['filtro'], 'usuarioid');
		$arrParam['professor'] 	= ext($args['filtro'], 'professor');
		$arrParam['questaoid'] 	= ext($args['filtro'], 'questaoid');
		$arrParam['qcid'] 		= ext($args['filtro'], 'qcid');
		$arrParam['prova'] 		= ext($args['filtro'], 'prova');
		$arrParam['disciplina'] = ext($args['filtro'], 'disciplina');

		MODEL('system_model');
		MODEL('professor_model');


		error_reporting(0);
		if(count($this->professor_model->get_question_teacher(ext($args['filtro'], 'qcid'))) > 0){
			echo "Esta questão não pode ser salva pois já foi escolhida por alguém :/";
		} else {

			if($this->system_model->insert('sep_professores_prova', $arrParam)){
				echo "Questão salva com sucesso!";
			} else {
				echo "Esta questão não pode ser salva pois já foi escolhida por alguém :/";
			}
		}


	}


	function questoes($page = 0){

		if($this->session->userdata('logado') == true)
		{

            $args['filtro'] = $this->input->post();


            $resultArray = $this->professores_model->get_question_teacher_selected($this->session->userdata('idusuario'), $args['filtro']);
            $this->load->library('array_table');
            $this->array_table->page_link = BASEURL . 'professores/show';
            $this->array_table->set_id('module_table');
            $this->array_table->set_data($resultArray);
            $this->array_table->set_actual_page($page);
            $this->array_table->set_items_per_page(50);
            $this->array_table->add_columnid('ID(#)');
            $this->array_table->add_column('<a href="javascript:void(0);" onclick="redirect_to(\'../professor/gravar/{0}\')"><i class="fa fa-video-camera" title="Gravar"> </a>');
            //$this->array_table->add_column('<a href="javascript:void(0);" onclick="ajax_professor_set_question_eliminate_div('.$this->session->userdata('idusuario').','. '\''.$this->session->userdata("nome").'\','. '{6}, {0},\'{5}\',\'{3}\')"><i class="fa fa-save" title="Salvar"></a>');
            $this->array_table->set_columns(array('ID(#)', 'Órgão', 'Banca', 'Disciplina'));
            // Processa tabela do modulo
            $args['module_table'] = $this->array_table->get_html();

			VIEW('dashboard/professores/questoes', $args);
		} else {
			redirect('home');
		}
	}
}
