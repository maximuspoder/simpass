<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	function index()
	{
		if($this->session->userdata('logado') == true)
		{
			header('location: ' . BASEURL . 'usuario/');
		} else {
			VIEW('frente/login2');
		}
	}

	public function login_process()
	{
		if($this->session->userdata('logado') == true)
		{
			header('location: ' . BASEURL . 'usuario/');
		}
		// Coleta dados passado no post
		$args['dados'] = $this->input->post();

		// Pega dado por dado
		$usuario 	= addslashes($args['dados']['email']);
		$senha 		= addslashes(md5($args['dados']['senha']));
		// carrega camada model
		MODEL('login_model');
		// Verifica se existe usuário
		if($this->login_model->user_exists( $usuario ) > 0)
		{

			// Usuário existe
			// Processa se usuário e senha são iguais
			if($this->login_model->user_pass_exists( $usuario, $senha ))
			{
				// Coleta os dados do usuário
				$args['data'] = $this->login_model->get_user_dados($usuario, $senha);
				// Carrega library
				$this->load->library('session');
				header('Content-Type: application/json; charset=utf-8');
				$fp = fopen(SERVER . 'application/assets/internaljson/' . ext($args['data'][0], 'token') . '.json', 'w');
				fwrite($fp, json_encode($args['data'], JSON_UNESCAPED_UNICODE));
				fclose($fp);

				MODEL('api_model');
				$args['following'] = $this->api_model->api_following(ext($args['data'][0], 'token'));

				header('Content-Type: application/json; charset=utf-8');
				$fp = fopen(SERVER . 'application/assets/internaljson/seguindo/' . ext($args['data'][0], 'token') . '.json', 'w');
				fwrite($fp, json_encode($args['following'], JSON_UNESCAPED_UNICODE));
				fclose($fp);

				MODEL('system_model');
				$arrLOG['evento'] = 'LOGIN';
				$arrLOG['token'] = ext($args['data'][0], 'token');
				$log = $this->system_model->insert('sep_logs', $arrLOG);


				$userSession = array(
					'logado' 		=> true,
					'nivel' 		=> ext($args['data'][0], 'nivel'),
					'token' 		=> ext($args['data'][0], 'token'),
					'idusuario' 	=> ext($args['data'][0], 'usuarioid'),
					'usuariovinculo'=> ext($args['data'][0], 'usuariovinculo'),
					'data_registro' => ext($args['data'][0], 'data_registro'),
					'email' 		=> ext($args['data'][0], 'email'),
					'email2' 		=> ext($args['data'][0], 'email2'),
					'nome' 			=> ext($args['data'][0], 'nome'),
					'sobrenome' 	=> ext($args['data'][0], 'sobrenome'),
					'sexo' 			=> ext($args['data'][0], 'sexo'),
					'dia' 			=> ext($args['data'][0], 'dia'),
					'mes' 			=> ext($args['data'][0], 'mes'),
					'ano' 			=> ext($args['data'][0], 'ano'),
					'cpf' 			=> ext($args['data'][0], 'cpf'),
					'rg' 			=> ext($args['data'][0], 'rg'),
					'estado' 		=> ext($args['data'][0], 'estado'),
					'cidade' 		=> ext($args['data'][0], 'cidade'),
					'bairro' 		=> ext($args['data'][0], 'bairro'),
					'telefone' 		=> ext($args['data'][0], 'telefone'),
					'foto_capa' 	=> ext($args['data'][0], 'foto_capa'),
					'foto_perfil' 	=> ext($args['data'][0], 'foto_perfil'),
					'plano' 		=> ext($args['data'][0], 'plano'),
					'facebook' 		=> ext($args['data'][0], 'facebook'),
					'twitter' 		=> ext($args['data'][0], 'twitter'),
					'youtube' 		=> ext($args['data'][0], 'youtube'),
					'site' 			=> ext($args['data'][0], 'site'),
					'cargo' 		=> ext($args['data'][0], 'cargo'),
					'aniversario' 	=> ext($args['data'][0], 'aniversario'),
					'tipo' 			=> ext($args['data'][0], 'tipo'),
					'sobre' 		=> ext($args['data'][0], 'sobre'),
				);
				// Cria sessão
				$this->session->set_userdata($userSession);
				// Send to user
				header('location: ' . BASEURL . 'usuario/');
			} else {
				$args['error'] = 1;
				// Senha errada
				VIEW('frente/login2', $args);
			}

		} else {
			$args['error'] = 1;
			// erro
			VIEW('frente/login2', $args);
		}
	}


	function logout()
	{
		generateLOG($this->session->userdata('token'), 'LOGOUT');
		// Array de login
     	$arr_dados = array(
			'logado' 			=> $this->session->userdata('logado'),
			'idusuario'     	=> $this->session->userdata('idusuario'),
			'nivel'				=> $this->session->userdata('nivel'),
			'login'				=> $this->session->userdata('login'),
			'nome'				=> $this->session->userdata('nome'),
			'sobrenome'			=> $this->session->userdata('sobrenome')
     	);
     	$this->session->unset_userdata($arr_dados);

     	if($this->session->userdata('logado') == FALSE)
     	{
     		redirect('home/');
     	}
	}

	function new_register()
	{
		if($this->input->post()){
			$args['filter'] = $this->input->post();

			$arrParamDados['foto_capa'] = 'http://simuleepasse.com.br/application/assets/uploads/cover/default.png';
			$arrParamDados['foto_perfil'] = 'http://simuleepasse.com.br/application/assets/uploads/profile/default.png';
			$arrParamDados['token'] = substr(generate_token_unique( ext($args['filter'], 'mail') ), 0, 27);

			error_reporting(0);
			MODEL('system_model');
			if($this->system_model->insert('sis_usuario_dados', $arrParamDados)){

				$arrParam['token'] 		= $arrParamDados['token'];
				$arrParam['nome'] 		= ext($args['filter'], 'name');
				$arrParam['sobrenome'] 	= ext($args['filter'], 'surname');
				$arrParam['email'] 		= ext($args['filter'], 'mail');
				$arrParam['senha'] 		= MD5(ext($args['filter'], 'password'));
				$arrParam['sexo'] 		= ext($args['filter'], 'sex');
				$arrParam['dia'] 		= ext($args['filter'], 'day');
				$arrParam['mes'] 		= ext($args['filter'], 'month');
				$arrParam['ano'] 		= ext($args['filter'], 'year');
				$arrParam['ativo'] 		= 'N';
				$arrParam['usuariotipoid'] = 4;

				if($this->system_model->insert('sis_usuario', $arrParam)){
					$message = '<strong>Bem-vindo ao Simule & Passe!</strong><br/>';
					$message .= 'Você cadastrou seu email com sucesso!!!<br/>';
					$message .= 'Aproveite agora mesmo para acessar o sistema e começar seus estudos!';
					$message .= '<br /><br />';
					$message .= 'Para acessar seu cadastro utilize o link abaixo:<br/>';
					$message .= 'http://www.simuleepasse.com.br/login/validar_auto_cadastro/' . $arrParam['token'];
					$arrMail = array(
						'to_name' => $arrParam['nome'],
						'to_mail' => $arrParam['email'],
						'subject' => 'Ative seu cadastro no Simule & Passe',
						'message' => $message
					);
					send_mailgun($arrMail);
					echo 2; // Gravou usuário
				} else {
					echo 1; // Erro ao inserir em usuario
				}
			} else {
				echo 0; // Erro em inserir em dados
			}
		}
	}

	function validate_mail()
	{
		if($this->input->post()){
			$args['filter'] = $this->input->post();

			MODEL('system_model');
			$where = 'email LIKE "'. ext($args['filter'], 'mail') . '" ';

			$select = $this->system_model->select_where('sis_usuario', $where);
			if(count($select) > 0){
				echo 1;
			} else {
				echo 0;
			}

		}
	}

	function register_success()
	{
		VIEW('frente/success');
	}

	function validar_auto_cadastro($token = null)
	{
		if($token){

			$setToken = addslashes($token);
			MODEL('system_model');
			$where = ' token LIKE "'. $setToken .'" AND ativo LIKE "N"';

			$get_where = $this->system_model->select_where('sis_usuario', $where);
			if(count($get_where) > 0){
				$updateWhere = array('token' => $setToken);
				$arrUpdate['ativo'] = 'S';
				$update = $this->system_model->update('sis_usuario', $arrUpdate, $updateWhere);
				VIEW('frente/validar_auto_cadastro');
			} else {
				$args['message'] = 'Erro ao ativar este cadastro!';
				$args['solution'] = "- Verifique seu email ou entre em contato conosco;<br/>
				- Verifique se esta contata já não está ativa.";
				VIEW('error_page', $args);
			}
		} else {
			$args['message'] = 'O que está acontecendo aqui?';
			$args['solution'] = "- Tentei entender mais não cheguei a uma conclusão, entre em contato conosco por email para solucionarmos este problema";
			VIEW('error_page', $args);
		}
	}

	function recuperar_senha()
	{
		VIEW('frente/recuperar_senha');
	}

	function recuperar_senha_process()
	{

		$args['filter'] = $this->input->post();
		$email = ext($args['filter'], 'email');

		if($email == ""){
			redirect('home/');
		} else {
			MODEL('system_model');

			$where = ' email LIKE "'.$email.'" ';
			$select  = $this->system_model->select_where('sis_usuario', $where);

			if(count($select) > 0){
				$message = '<strong>Acesse o link abaixo para recuperar sua senha!</strong><br/>';
				$message .= 'http://www.simuleepasse.com.br/login/nova_senha/' . encrypt($select[0]['token'], TOKENHASH);
				$arrMail = array(
					'to_name' => 'Usuári Simule & Passe',
					'to_mail' => $email,
					'subject' => 'Recuperar Senha - Simule & Passe',
					'message' => $message
				);
				send_mailgun($arrMail);

				$args['link'] = 'home/';
				$args['title'] = 'Recuperando sua senha!';
				$args['message'] = 'Enviamos um email com um link para alterar sua senha de acesso.';
				VIEW('success_page', $args);
			} else {
				$args['message'] = "OK, você estava tentando recuperar sua senha e o email informado não existe em nossa base de dados!";
				$args['solution'] = "
				 - Verifique novamente o email informado.";
				VIEW('error_page', $args);
			}
		}
	}

	function nova_senha($token = null)
	{
		if($token == null){
			redirect('home/');
		} else {
			MODEL('system_model');

			$tokenDecrypt = decrypt($token, TOKENHASH);
			$where = ' token LIKE "'.$tokenDecrypt.'" ';
			$select  = $this->system_model->select_where('sis_usuario', $where);

			if(count($select) > 0){
				$args['token'] = $token;
				VIEW('frente/nova_senha', $args);
			} else {
				$args['message'] = "OK, você estava tentando recuperar sua senha e os parâmetros passados não correspondem e um cadastro em nosso sistema!";
				$args['solution'] = "
				 - Verifique novamente o link recebido ou email cadastrado.";
				VIEW('error_page', $args);
			}

		}
	}

	function nova_senha_process()
	{
		if($this->input->post()){

			MODEL('system_model');

			$args['filter'] = $this->input->post();
			$arrParam['senha'] = md5(ext($args['filter'], 'senha'));
			$arrParam['ativo'] = 'S';

			$token = decrypt(ext($args['filter'], 'hash'), TOKENHASH);
			error_reporting(0);
			$where = array('token' => $token);
			$select  = $this->system_model->update('sis_usuario',$arrParam, $where);

			if(count($select) >  0){
				$args['link'] = 'home/';
				$args['title'] = 'Senha alterada com sucesso!';
				$args['message'] = 'Agora você pode realizar seu login.';
				VIEW('success_page', $args);
			}
		}
	}
}