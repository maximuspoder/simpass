<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if($this->session->userdata('logado') == true)
		{
			header('location: ' . BASEURL . 'usuario/');
		}
	}
	function index()
	{
		VIEW('frente/default');
	}

	function error_404()
	{
		$args['message'] = 'Essa página não existe!';
		$args['solution'] = "- Não sei onde você está tentando chegar para ajudá-lo! Desculpe-me :(";
		VIEW('error_page', $args);
	}
}

