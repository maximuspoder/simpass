<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Contato extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		redirect('home/');
	}

	function enviar_mensagem()
	{
		if($this->input->post()){
			$args['filter'] = $this->input->post();

			$arrParam['nome'] 	= ext($args['filter'], 'name_contact');
			$arrParam['email'] 	= ext($args['filter'], 'email_contact');
			$arrParam['duvida'] = ext($args['filter'], 'message_contact');

			MODEL('system_model');
			error_reporting(0);
			$insert = $this->system_model->insert('sep_contato', $arrParam);

			$message = '<strong>Contato via formulário.</strong><br/>';
			$message .= 'Nome: '.$arrParam['nome'].'<br/>';
			$message .= 'Email:'.$arrParam['email'].'<br/><br />';
			$message .= 'Mensagem:'.$arrParam['duvida'].'<br /><br />';
			$arrMail1 = array(
				'to_name' => $arrParam['nome'],
				'to_mail' => 'administrador@simuleepasse.com.br',
				'subject' => 'Contato via formulário Simule & Passe',
				'message' => $message
			);
			send_mailgun($arrMail1);
			$message = '<strong>Contato via formulário.</strong><br/>';
			$message .= 'Nome: '.$arrParam['nome'].'<br/>';
			$message .= 'Email:'.$arrParam['email'].'<br/><br />';
			$message .= 'Mensagem:'.$arrParam['duvida'].'<br /><br />';
			$arrMail2 = array(
				'to_name' => $arrParam['nome'],
				'to_mail' => 'fernando.alves@simuleepasse.com.br',
				'subject' => 'Contato via formulário Simule & Passe',
				'message' => $message
			);
			send_mailgun($arrMail2);

			$args['title'] = 'Mensagem enviada!';
			$args['message'] = 'Sua mensagem foi enviada com sucesso, aguarde que logo retornaremos.';
			$args['link']= 'home/';
			VIEW('success_page', $args);

		} else {
			redirect('home/');
		}
	}

}/* End of file contato.php */
/* Location: ./application/controllers/contato.php */
