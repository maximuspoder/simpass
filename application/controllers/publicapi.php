
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Publicapi extends CI_Controller {
	
	private $masterToken = '34cbc5c592c69e9a10c9c110914';

	private $tokenError = array(
				'undefined_rest_api' => array('Error 1.3' => 'This URL not exists in this server!'), 
				'missed_token' => array('Error 2' => 'You must to use some information if want to get something.'), 
				'unknown_token' => array('Error 2.1' => 'unknown Token.'),
				'unknown_method' => array('Error 5.0' => 'unknown method.')
	);

	private $methods = array(
		'userinfo', 'count_user', 'count_question', 'followers_data', 'promo_mail', 'count_questions',
		'count_questions_recorded', 'user_logs', 'user_last_log'
	);


	function __construct()
	{
		parent::__construct();
		MODEL('system_model');
		MODEL('api_model');
	}

	function user($token){

		if(file_exists(SERVER . 'application/assets/internaljson/' . $token . '.json')){
							
			$handle = fopen(SERVER . 'application/assets/internaljson/' . $token . '.json', "r+");
			$json = stream_get_contents($handle);
			fclose($handle);			
			
			header('Content-Type: text/json; charset=utf-8'); 
			// Return json
			echo $json;
		} else {
			echo 1;
		}
	}

	function rest($method, $token = null){	
		switch ($method) {
			case $this->methods[0]:
				// userinfo
				$this->userinfo($token);
			break;

			case $this->methods[1]:
				// count_user
				$this->count_user($token);
			break;

			case $this->methods[2]:
				
			break;

			case $this->methods[3]:
				// followers_data
				$this->followers_data($token);
			break;

			case $this->methods[4]:
				// followers_data
				$this->promo_mail($token);
			break;

			case $this->methods[5]:
				$this->count_questions();
			break;

			case $this->methods[6]:
				$this->count_questions_recorded();
			break;

			case $this->methods[7]:
				$this->user_logs($token);
			break;

			case $this->methods[8]:
				$this->user_last_log($token);
			break;
			
			default:
				header('Content-Type: text/json; charset=utf-8'); 
				// Return json
				echo json_encode($this->tokenError['unknown_method'], JSON_UNESCAPED_UNICODE);
			break;
		}
	}


	/*************
	METHODS
	*************/

	function userinfo($token){
		//$json = LoadJSON()
	}

	function count_user(){
		generateLOG('null reference', 'API/METHOD/COUNT_USERS');
		$users = $this->api_model->count_user();
		header('Content-Type: text/json; charset=utf-8'); 
		echo json_encode($users, JSON_PRETTY_PRINT);
	}

	function followers_data($token){
	
		//$tokenReverse = decrypt
		$where = ' token LIKE "'.$token.'" ';
		$selectByToken = $this->system_model->select_where('sis_usuario', $where);
		
		//generateLOG($token, 'API/METHOD/FOLLOWERS_DATA');
		if(count($selectByToken) > 0){
			$followers = $this->api_model->followers_data($token);
			header('Content-Type: text/json; charset=utf-8'); 
			// Return json
			echo json_encode($followers, JSON_PRETTY_PRINT);
		} else {
			header('Content-Type: text/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);
		}
	}

	function promo_mail(){
		$Where = ' usuarioid > 51 AND usuariotipoid = 4';
		$select_where = $this->system_model->select_where('sis_usuario', $Where);
	
		foreach ($select_where as $data):
		
			$message = '<strong>Olá'. $data['nome'] .'</strong>.<br/>';
			$message .= 'Hoje é o grande dia de você experimentar 3 simulados <strong>totalmente FREE com videos</strong>.<br/>';
			$message .= 'Aproveite os links abaixo para turbinar seus estudos e conhecer mais o Simule & Passe!';
			$message .= '<br /><br />';
			$message .= 'Para acessar os simulados free utilize os links abaixo, mas lembre-se que você deve estar cadastrado e logado em nosso sitema.<br/><br/>';
			$message .= 'Link 1(Simulado com vídeos de várias disciplinas):<br/> http://simuleepasse.com.br/simulado/free/n2a5k7Cqz2ePY7CrpZePkn6lz2u5Yn6lpWOK/1﻿ <br/><br/>';
			$message .= 'Link 2(Simulado com vídeos de Direito do Trabalho):<br/> http://simuleepasse.com.br/simulado/free/o5i6k7DapGqOaYXWo2e7ZYaozmmHloPZz2KK/1﻿ <br/><br/>';
			$message .= 'Link 3(Simulado com vídeos de Direito Processual do Trabalho):<br/> http://simuleepasse.com.br/simulado/free/z2K8Y4WlnJO6Y37apZeJk4Gl0Wa3lbDXnZWG/2 <br/><br/>';
			$arrMail = array(
				'to_name' => $data['nome'],
				'to_mail' => $data['email'],
				'subject' => '3 simulados com vídeos totalmente FREE para você - Simule & Passe',
				'message' => $message
			);
			send_mailgun($arrMail);

		endforeach;
	}

	function count_questions(){
		$where = ' editado is not null';
		$fields = ' count(1) as count_questions ';
		$select = $this->system_model->select_fields_where('sep_questoes', $fields, $where);

		header('Content-Type: text/json; charset=utf-8'); 
		// Return json
		echo json_encode($select, JSON_PRETTY_PRINT);
	}

	function count_questions_recorded(){

		$select = $this->api_model->count_questions_recorded();

		header('Content-Type: text/json; charset=utf-8'); 
		// Return json
		echo json_encode($select, JSON_PRETTY_PRINT);
	}

	function user_logs($token){

		if($token == $this->masterToken){
			$select = $this->api_model->user_logs();

			header('Content-Type: text/json; charset=utf-8'); 
			// Return json
			echo json_encode($select, JSON_PRETTY_PRINT);
		} else {
			header('Content-Type: text/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);
		}
	}

	function user_last_log($token){

		if($token == $this->masterToken){

			$select = $this->api_model->user_last_log();
			header('Content-Type: text/json; charset=utf-8'); 
			// Return json
			echo json_encode($select, JSON_PRETTY_PRINT);
		} else {
			header('Content-Type: text/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);
		}
	}

	function dashboard($token = null){

		//if($token == $this->masterToken){
			
			$select = $this->api_model->user_last_log();

			$count_return = 0;
			$count_no_return = 0;
			error_reporting(0);
			foreach ($select as $data):
				if(timestamp_decode_day($data['data_evento']) > timestamp_decode_day($data['cadastro']) && $data['data_evento'] != null){
					$count_return++;
				} else {
					$count_no_return++;
				}
			endforeach;
			$args['count_return'] = $count_return;
			$args['count_no_return'] = $count_no_return;
			VIEW('public/dashboard/default', $args);
			/*
		} else {
			header('Content-Type: text/json; charset=utf-8'); 
			// Return json
			echo json_encode($this->tokenError['unknown_token'], JSON_UNESCAPED_UNICODE);
		}
		*/
	}



}

/* End of file publicapi.php */
/* Location: ./application/controllers/publicapi.php */

