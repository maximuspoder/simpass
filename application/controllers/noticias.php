<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Noticias extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		MODEL('noticias_model');
	}

	function index()
	{
		if($this->session->userdata('logado'))
		{			
			$user = restApiReturnJson($this->session->userdata('token'));
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['date'] 		= $user[0]->{'data_registro'};
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['email'] 		= $user[0]->{'email'};
			$args['type'] 		= $user[0]->{'tipo'};
			$args['level'] 		= $user[0]->{'nivel'};
			$args['name'] 		= $user[0]->{'nome'};
			$args['surname'] 	= $user[0]->{'sobrenome'};
			$args['cover'] 		= $user[0]->{'foto_capa'};
			$args['profile'] 	= $user[0]->{'foto_perfil'};
			$args['account'] 	= $user[0]->{'plano'};
		}

		$args['sepNews'] = $this->noticias_model->get_last_news(30);
		$args['sepBlog'] = $this->noticias_model->get_last_blog();
		// do something
		VIEW('noticias/default', $args);
	}

	function nova()
	{
		if($this->session->userdata('logado') && $this->session->userdata('nivel') == 1 || $this->session->userdata('nivel') == 3 || $this->session->userdata('nivel') == 2)
		{
			// Get Json
			$user = restApiReturnJson($this->session->userdata('token'));
			
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['date'] 		= $user[0]->{'data_registro'};
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['email'] 		= $user[0]->{'email'};
			$args['type'] 		= $user[0]->{'tipo'};
			$args['level'] 		= $user[0]->{'nivel'};
			$args['name'] 		= $user[0]->{'nome'};
			$args['surname'] 	= $user[0]->{'sobrenome'};
			$args['cover'] 		= $user[0]->{'foto_capa'};
			$args['profile'] 	= $user[0]->{'foto_perfil'};
			$args['account'] 	= $user[0]->{'plano'};
			// do something
			VIEW('noticias/nova', $args);
		} else {
			// Redireciona
			redirect('noticias/');
		}
	}

	function nova_process()
	{

		if($this->session->userdata('logado') && $this->session->userdata('nivel') == 1 || $this->session->userdata('nivel') == 3 || $this->session->userdata('nivel') == 2)
		{
			
			$args['filter'] = $this->input->post();
			

			// Pega tipo de arquivo
			$format = end(explode('.', $_FILES['imagem']['name']));

				// data e hora
				$data = date('Y-m-d');
				$hour = date('H:i:s');
				// Explode
				$date = explode('-', $data);
				$hora = explode(':', $hour);

				$tokenMD5 = md5($this->session->userdata('token'));
				$file =  string_aleatoria(10) . '_' . $tokenMD5 . '_' .  $data[2] . '_' . $data[1] . '_' . $data[0] . '_' . $hora[0] . '_' . $hora[1] . '_' . $hora[2] . '.' . $format;		

				// Move anexo para pasta
				$upload = move_uploaded_file($_FILES['imagem']['tmp_name'], UPLOAD . 'noticias/' . $file);


				$this->load->library('m2brimagem');

				$this->m2brimagem->m2brimagem(UPLOAD . 'noticias/' . $file);
				$valida = $this->m2brimagem->valida();
				
				if ($valida == 'OK') {
					$this->m2brimagem->redimensiona(503, 377);
				    $this->m2brimagem->grava(UPLOAD . 'noticias/thumb/' . $file);
				} else {
					die($valida);
				}




				if(file_exists(UPLOAD . 'noticias/' . $file)){

					MODEL('system_model');
					$arrParam['titulo'] = ext($args['filter'], 'titulo');
					$arrParam['token'] = $this->session->userdata('token');
					$arrParam['categoria'] = ext($args['filter'], 'categoria');
					$arrParam['imagem'] = $file;
					$arrParam['conteudo'] = ext($args['filter'], 'content');
					error_reporting(0);
					$insert = $this->system_model->insert('sep_blog', $arrParam);
						if(count($insert) > 0){
							if(ext($args['filter'], 'categoria') == 'concursos'){
								generateLOG($this->session->userdata('token'), 'NOTICIA/CRIADA');
							}
							else if(ext($args['filter'], 'categoria') == 'blog'){
								generateLOG($this->session->userdata('token'), 'NOTICIA/BLOG');
							}
							else if(ext($args['filter'], 'categoria') == 'edital'){
								generateLOG($this->session->userdata('token'), 'NOTICIA/EDITAL');
							}

							
							redirect('noticias/nova_success');
						} else {
							$args['message'] = "OK, você estava criando uma nova notícia e recebeu um erro inesperado,
							mas ok, erros acontecem em todos os lugares, erga a cabeça!";
							$args['solution'] = "
							 - O título já existe em nosso banco de dados;<br />
							 - A imagem não foi carregada.";
							VIEW('error_page', $args);
						}
				}

			

		} else {
			// Redireciona
			redirect('noticias/');
		}
	}

	function nova_success()
	{

		// Get Json
		$user = restApiReturnJson($this->session->userdata('token'));
		
		$args['userid'] 	= $user[0]->{'usuarioid'};
		$args['date'] 		= $user[0]->{'data_registro'};
		$args['userid'] 	= $user[0]->{'usuarioid'};
		$args['email'] 		= $user[0]->{'email'};
		$args['type'] 		= $user[0]->{'tipo'};
		$args['level'] 		= $user[0]->{'nivel'};
		$args['name'] 		= $user[0]->{'nome'};
		$args['surname'] 	= $user[0]->{'sobrenome'};
		$args['cover'] 		= $user[0]->{'foto_capa'};
		$args['profile'] 	= $user[0]->{'foto_perfil'};
		$args['account'] 	= $user[0]->{'plano'};

		$args['message'] = 'Sua noticía foi criada com sucesso!';
		VIEW('noticias/nova_success', $args);
	}



	function conteudo($category, $blogid, $urlName)
	{
		if($this->session->userdata('logado'))
		{			
			$user = restApiReturnJson($this->session->userdata('token'));
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['date'] 		= $user[0]->{'data_registro'};
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['email'] 		= $user[0]->{'email'};
			$args['type'] 		= $user[0]->{'tipo'};
			$args['level'] 		= $user[0]->{'nivel'};
			$args['name'] 		= $user[0]->{'nome'};
			$args['surname'] 	= $user[0]->{'sobrenome'};
			$args['cover'] 		= $user[0]->{'foto_capa'};
			$args['profile'] 	= $user[0]->{'foto_perfil'};
			$args['account'] 	= $user[0]->{'plano'};
		}
		$args['url'] = $category.'/'.$blogid.'/'.$urlName;
		$args['sepNews'] = $this->noticias_model->get_last_news(7);
		$args['news'] = $this->noticias_model->get_news_by_id($blogid);

		VIEW('noticias/conteudo', $args);
	}

	function postadas_por_mim()
	{
		if($this->session->userdata('logado') && $this->session->userdata('nivel') == 1 || $this->session->userdata('nivel') == 3 || $this->session->userdata('nivel') == 2)
		{
			// Get Json
			$user = restApiReturnJson($this->session->userdata('token'));
			
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['date'] 		= $user[0]->{'data_registro'};
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['email'] 		= $user[0]->{'email'};
			$args['type'] 		= $user[0]->{'tipo'};
			$args['level'] 		= $user[0]->{'nivel'};
			$args['name'] 		= $user[0]->{'nome'};
			$args['surname'] 	= $user[0]->{'sobrenome'};
			$args['cover'] 		= $user[0]->{'foto_capa'};
			$args['profile'] 	= $user[0]->{'foto_perfil'};
			$args['account'] 	= $user[0]->{'plano'};

			$args['news'] = $this->noticias_model->postadas_por_mim($this->session->userdata('token'));
			// do something
			VIEW('noticias/postadas_por_mim', $args);
		} else {
			// Redireciona
			redirect('noticias/');
		}
	}

	function deletar($id)
	{
		if($this->session->userdata('logado') && $this->session->userdata('nivel') == 1 || $this->session->userdata('nivel') == 3 || $this->session->userdata('nivel') == 2)
		{
			MODEL('system_model');
			$where = array('token' => $this->session->userdata('token'), 'blogid' => $id);
			if($this->system_model->delete('sep_blog', $where)){
				redirect('noticias/postadas_por_mim');
			}
		} else {
			// Redireciona
			redirect('noticias/');
		}
	}

	function editar($id){
		if($this->session->userdata('logado') && $this->session->userdata('nivel') == 1 || $this->session->userdata('nivel') == 3 || $this->session->userdata('nivel') == 2)
		{
			MODEL('system_model');
			$where = ' token LIKE "'.$this->session->userdata('token').'"  AND blogid='.$id;
			$select = $this->system_model->select_where('sep_blog', $where);
			$args['noticia'] = $select;
			if(count($select) > 0){
				// Get Json
				$user = restApiReturnJson($this->session->userdata('token'));
				
				$args['userid'] 	= $user[0]->{'usuarioid'};
				$args['date'] 		= $user[0]->{'data_registro'};
				$args['userid'] 	= $user[0]->{'usuarioid'};
				$args['email'] 		= $user[0]->{'email'};
				$args['type'] 		= $user[0]->{'tipo'};
				$args['level'] 		= $user[0]->{'nivel'};
				$args['name'] 		= $user[0]->{'nome'};
				$args['surname'] 	= $user[0]->{'sobrenome'};
				$args['cover'] 		= $user[0]->{'foto_capa'};
				$args['profile'] 	= $user[0]->{'foto_perfil'};
				$args['account'] 	= $user[0]->{'plano'};
				$args['idnoticia'] = $id;
				VIEW('noticias/editar', $args);
			} else {
				redirect('noticias/postadas_por_mim');
			}
		} else {
			// Redireciona
			redirect('noticias/');
		}
	}

	function editar_process(){
		if($this->session->userdata('logado') && $this->session->userdata('nivel') == 1 || $this->session->userdata('nivel') == 3 || $this->session->userdata('nivel') == 2)
		{
			
		$args['filter'] = $this->input->post();
		// Get Json
		$user = restApiReturnJson($this->session->userdata('token'));
		$args['userid'] 	= $user[0]->{'usuarioid'};
		$args['date'] 		= $user[0]->{'data_registro'};
		$args['userid'] 	= $user[0]->{'usuarioid'};
		$args['email'] 		= $user[0]->{'email'};
		$args['type'] 		= $user[0]->{'tipo'};
		$args['level'] 		= $user[0]->{'nivel'};
		$args['name'] 		= $user[0]->{'nome'};
		$args['surname'] 	= $user[0]->{'sobrenome'};
		$args['cover'] 		= $user[0]->{'foto_capa'};
		$args['profile'] 	= $user[0]->{'foto_perfil'};
		$args['account'] 	= $user[0]->{'plano'};
			

		if($_FILES['imagem']['name'] != ""){
			// Pega tipo de arquivo
			$format = end(explode('.', $_FILES['imagem']['name']));
			// data e hora
			$data = date('Y-m-d');
			$hour = date('H:i:s');
			// Explode
			$date = explode('-', $data);
			$hora = explode(':', $hour);
			$tokenMD5 = md5($this->session->userdata('token'));
			$file =  string_aleatoria(10) . '_' . $tokenMD5 . '_' .  $data[2] . '_' . $data[1] . '_' . $data[0] . '_' . $hora[0] . '_' . $hora[1] . '_' . $hora[2] . '.' . $format;
			// Move anexo para pasta
			$upload = move_uploaded_file($_FILES['imagem']['tmp_name'], UPLOAD . 'noticias/' . $file);

			$this->load->library('m2brimagem');
			$this->m2brimagem->m2brimagem(UPLOAD . 'noticias/' . $file);
			$valida = $this->m2brimagem->valida();
			if ($valida == 'OK') {
				$this->m2brimagem->redimensiona(503, 377);
			    $this->m2brimagem->grava(UPLOAD . 'noticias/thumb/' . $file);
			} else {
				die($valida);
			}
			if(file_exists(UPLOAD . 'noticias/' . $file)){

				MODEL('system_model');
				$arrParam['titulo'] = ext($args['filter'], 'titulo');
				$arrParam['categoria'] = ext($args['filter'], 'categoria');
				$arrParam['imagem'] = $file;
				$arrParam['conteudo'] = ext($args['filter'], 'content');
				$idnoticia = ext($args['filter'], 'idnoticia');
				$where = array('blogid' => $idnoticia, 'token' => $this->session->userdata('token'));
				error_reporting(0);
				$update = $this->system_model->update('sep_blog', $arrParam, $where);
				if(count($update) > 0){
					if(ext($args['filter'], 'categoria') == 'concursos'){
						generateLOG($this->session->userdata('token'), 'NOTICIA/EDITADA');
					}
					else if(ext($args['filter'], 'categoria') == 'blog'){
						generateLOG($this->session->userdata('token'), 'NOTICIA/BLOG/EDITADA');
					}
					else if(ext($args['filter'], 'categoria') == 'edital'){
						generateLOG($this->session->userdata('token'), 'NOTICIA/EDITAL/EDITADA');
					}
					
					$user = restApiReturnJson($this->session->userdata('token'));

					$args['message'] = 'Sua noticía foi editada com sucesso!';
					VIEW('noticias/editar_success',$args);
				} else {
					$args['message'] = "OK, você estava editando uma nova notícia e recebeu um erro inesperado,
					mas ok, erros acontecem em todos os lugares, erga a cabeça!";
					$args['solution'] = "
					 - Tente novamente.<br />";
					VIEW('error_page', $args);
				}
			}
			} else {

				MODEL('system_model');
				$arrParam['titulo'] = ext($args['filter'], 'titulo');
				$arrParam['token'] = $this->session->userdata('token');
				$arrParam['categoria'] = ext($args['filter'], 'categoria');
				$arrParam['conteudo'] = ext($args['filter'], 'content');
				$idnoticia = ext($args['filter'], 'idnoticia');
				$where = array('blogid' => $idnoticia, 'token' => $this->session->userdata('token'));
				error_reporting(0);
				$update = $this->system_model->update('sep_blog', $arrParam, $where);
				if(count($update) > 0){
					if(ext($args['filter'], 'categoria') == 'concursos'){
						generateLOG($this->session->userdata('token'), 'NOTICIA/EDITADA');
					}
					else if(ext($args['filter'], 'categoria') == 'blog'){
						generateLOG($this->session->userdata('token'), 'NOTICIA/BLOG/EDITADA');
					}
					else if(ext($args['filter'], 'categoria') == 'edital'){
						generateLOG($this->session->userdata('token'), 'NOTICIA/EDITAL/EDITADA');
					}
					$args['message'] = 'Sua noticía foi editada com sucesso!';
					VIEW('noticias/editar_success',$args);
				} else {
					$args['message'] = "OK, você estava editando uma nova notícia e recebeu um erro inesperado,
					mas ok, erros acontecem em todos os lugares, erga a cabeça!";
					$args['solution'] = "
					 - Tente novamente.<br />";
					VIEW('error_page', $args);
				}
			}	

		} else {
			// Redireciona
			redirect('noticias/');
		}
	}



}/* End of file noticias.php */
/* Location: ./application/controllers/noticias.php */

