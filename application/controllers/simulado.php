<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Simulado extends CI_Controller {
	private $tokenError = array(
				'undefined_rest_api' => array('Error 1.3' => 'This URL not exists in this server!'), 
				'missed_token' => array('Error 2' => 'You must to use some information if want to get something.'), 
				'unknown_token' => array('Error 2.1' => 'unknown Token.'),
				'unknown_method' => array('Error 5.0' => 'unknown method.')
	);

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('authorization');
		MODEL('simulado_model');
	}

	function index()
	{
		if($this->session->userdata('logado') == true)
		{
			redirect('simulado/novo');
		} else {
			// redirect
			redirect('home/');
		}
	}


	function novo(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		// IF USER IS PRO LOAD FILTER WITH VIDEOS
		if($args['account'] != null){
			// Load json from bancas
			$args['bancas_vimeo'] = LoadJSON('simulados', 'bancas_vimeo');
			// Load json from anos
			$args['anos_vimeo'] 	= LoadJSON('simulados', 'anos_vimeo');
			// Load json from cargos
			$args['cargos_vimeo'] = LoadJSON('simulados', 'cargos_vimeo');
			// Load json from orgaos
			$args['orgaos_vimeo'] = LoadJSON('simulados', 'orgaos_vimeo');
			// Load json from orgaos
			$args['disciplinas_vimeo'] = LoadJSON('simulados', 'disciplinas_vimeo');
			// Load json from assuntos
			$args['assuntos_vimeo'] = LoadJSON('simulados', 'assuntos_vimeo');
		}
		// Load json from bancas
		$args['bancas'] = LoadJSON('simulados', 'bancas');
		// Load json from anos
		$args['anos'] 	= LoadJSON('simulados', 'anos');
		// Load json from cargos
		$args['cargos'] = LoadJSON('simulados', 'cargos');
		// Load json from provas
		$args['provas'] = LoadJSON('simulados', 'provas');
		// Load json from orgaos
		$args['orgaos'] = LoadJSON('simulados', 'orgaos');
		// Load json from orgaos
		$args['disciplinas'] = LoadJSON('simulados', 'disciplinas');
		// Load json from assuntos
		$args['assuntos'] = LoadJSON('simulados', 'assuntos');


		MODEL('usuario_model');
		$args['followers_count'] = $this->usuario_model->data_view_profile_get_followers($this->session->userdata('token'));
		$args['following_count'] = $this->usuario_model->data_view_profile_get_following($this->session->userdata('token'));
		// Carrega view de login
		VIEW('simulado/default', $args);
	}

	/*
	* deletar()
	* deletar simulado
	*/
	function deletar($id){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$where = array('token' => $this->session->userdata('token'), 'provaid' => $id);
		if($this->system_model->delete('sep_simulado', $where)){
			redirect('simulado/historico');
		}
	}

	function limpar($historico)
	{
		if($this->session->userdata('logado') == true && $historico == "historico")
		{
			MODEL('system_model');
			$where = array('token' => $this->session->userdata('token'));
			if($this->system_model->delete('sep_simulado', $where)){
				redirect('simulado/historico');
			}
		} else {
			// Redireciona
			redirect('home/');
		}
	}

	function ajax_restSimuladoEspelho(){
		// Filter to ajax post
		$args['filter'] = $this->input->post();
		// Load json from bancas
		//$args['banca'] = LoadJSON('simulados', $args['filter']['banca']);
		$args['banca'] = LoadJSON('simulados','geral');

		$banca  = ext($args['filter'], 'banca');
		$json = $args['banca']->{"$banca"};

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($json, JSON_UNESCAPED_UNICODE);
	}

	function create(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();
		generateLOG($this->session->userdata('token'), 'SIMULADO/CRIADO');

		// Filter to ajax post
		$args['filter'] = POST();

		// GET Simulado
		$args['simulado'] = $this->simulado_model->get_prova_by_name(ext($args['filter'], 'prova'));
		$args['last'] = $this->simulado_model->get_last_simulado( $this->session->userdata('token'));
		$last_provaid = (ext($args['last'][0], 'provaid') > 0) ? ext($args['last'][0], 'provaid') + 1 : 1;

		$x = 0;
		$count = count($args['simulado']);

		$sql = 'INSERT INTO sep_simulado (simuladoid, provaid, token, prova, qcid, resposta, resposta_correta, data_registro) VALUES';
		while($x < $count){
			if($x + 1 == $count){
				$sql .= " ('', ". $last_provaid .", '". $this->session->userdata('token') ."', '". ext($args['simulado'][$x], 'prova') ."', ". ext($args['simulado'][$x], 'qcid') .", null, '". ext($args['simulado'][$x], 'resposta_correta') ."', now());";
			} else {
				$sql .= " ('', ". $last_provaid .", '". $this->session->userdata('token') ."', '". ext($args['simulado'][$x], 'prova') ."', ". ext($args['simulado'][$x], 'qcid') .", null, '". ext($args['simulado'][$x], 'resposta_correta') ."', now()),";
			}
			$x++;
		}
		$insert = $this->simulado_model->create_simulado($sql);
		while($insert < 1){

			$insert = $this->simulado_model->create_simulado($sql);
		}

		$args['sim'] = $this->simulado_model->get_user_simulado_respostas( $this->session->userdata('token'), $last_provaid );

		header('Content-Type: application/json; charset=utf-8');
		// Generate results
		$fp = fopen(SERVER . 'application/assets/internaljson/prova/'. $this->session->userdata('token') . '_'. $last_provaid . '.json', 'w');
		fwrite($fp, json_encode($args['sim'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		redirect('simulado/prova/'. $last_provaid);
	}

	function finalizar_prova($idprova){
		error_reporting(0);
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$where = ' provaid = '. $idprova.' AND token LIKE "'.$this->session->userdata('token').'" ';
		generateLOG($this->session->userdata('token'), 'SIMULADO/FINALIZADO_FORCA');
		$select = $this->system_model->select_where('sep_simulado', $where);

		if(count($select) > 0){
			$arrParam['resposta'] = '';
			$whereUpdate = array('resposta' => null, 'token' => $this->session->userdata('token'), 'provaid' => $idprova);
			$update = $this->system_model->update('sep_simulado', $arrParam, $whereUpdate);
			if(count($update) > 0){
				redirect('simulado/prova/'. $idprova);
			} else {
				redirect('simulado/finalizar_prova/'. $idprova);
			}
		}
	}

	function prova($idprova, $itemsPerPage = 5, $questaoID = null){
		error_reporting(0);
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$itemsPerPage = ($itemsPerPage > 20) ? 20 : $itemsPerPage;
		$args['itemsPerPage'] = $itemsPerPage;

		$idprova = addslashes($idprova);

		$questaoID = ($questaoID == null) ? null : decrypt($questaoID, QCIDHASH);

		// Count
		$count = count($this->simulado_model->get_user_simulado($this->session->userdata('token'), $idprova));
		// Data
		$args['simulado'] 		= $this->simulado_model->get_user_simulado_limit($this->session->userdata('token'), $idprova, $itemsPerPage, $questaoID);
		if(count($args['simulado']) == 0){
			$args['simulado'] 		= $this->simulado_model->get_user_simulado_limit($this->session->userdata('token'), $idprova, $itemsPerPage, null);
		}
		$args['itemsPerPage'] 	= $itemsPerPage;
		$args['provaid'] 		= $idprova;

		if($count > 0){
			VIEW('simulado/prova', $args);
		} else {
			generateLOG($this->session->userdata('token'), 'SIMULADO/FINALIZADO');
			$args['simulado_final'] = $this->simulado_model->get_simulado_numeros($this->session->userdata('token'), $idprova);
			VIEW('simulado/prova_finalizada', $args);
		}
	}

	function prova_set_result(){
		if(POST()){
		// Filter to ajax post
		$args['filter'] = $this->input->post();

		$token 		= ext($args['filter'], 'token');
		$provaid 	= ext($args['filter'], 'provaid');
		$qcid 		= ext($args['filter'], 'id');
		$resposta	= ext($args['filter'], 'resposta');

		$sql = "UPDATE sep_simulado SET resposta = '$resposta' 
				WHERE qcid = $qcid
				AND provaid = $provaid 
				AND token = '$token'";

		$update = $this->simulado_model->update_simulado($sql);

		while($update < 1)
		{

			$update = $this->simulado_model->update_simulado($sql);

		}// end while

		if($update > 0)
		{
			$provaJSON = LoadJSON('prova', $token . '_' . $provaid);
			$x = 0;
			$count = count($provaJSON);
			while($x < $count)
			{
				//print_r($provaJSON[$x]);
				if($provaJSON[$x]->{'qcid'} == $qcid){
					echo $provaJSON[$x]->{'resposta_correta'};
					break;
				} 
				$x++;
			}
		}// end if
		} else {
			redirect('usuario/');
		}

	}

	function historico(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$args['history'] = $this->simulado_model->get_user_history($this->session->userdata('token'));
		VIEW('simulado/historico', $args);
	}

	function desempenho(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		generateLOG($this->session->userdata('token'), 'SIMULADO/MEU_DESEMPENHO');
		$args['history'] = $this->simulado_model->get_desempenho($this->session->userdata('token'));
		VIEW('simulado/desempenho', $args);	
	}

	function public_performance($token){
		if($token != null){
			$args['token'] = $token;
			$tokenReverse = decrypt($token, tokenTIMELINE);			

			// Get Json
			$user = restApiReturnJson($tokenReverse);
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['date'] 		= $user[0]->{'data_registro'};
			$args['userid'] 	= $user[0]->{'usuarioid'};
			$args['email'] 		= $user[0]->{'email'};
			$args['type'] 		= $user[0]->{'tipo'};
			$args['level'] 		= $user[0]->{'nivel'};
			$args['name'] 		= $user[0]->{'nome'};
			$args['surname'] 	= $user[0]->{'sobrenome'};
			$args['cover'] 		= $user[0]->{'foto_capa'};
			$args['profile'] 	= $user[0]->{'foto_perfil'};
			$args['account'] 	= $user[0]->{'plano'};

			generateLOG('null', 'SIMULADO/MEU_DESEMPENHO/PUBLIC_VIEW');
			if($this->session->userdata('logado'))
			{
				$userLOGGED = restApiReturnJson($this->session->userdata('token'));
				$args['useridLOGGED'] 	= $userLOGGED[0]->{'usuarioid'};
				$args['dateLOGGED'] 	= $userLOGGED[0]->{'data_registro'};
				$args['useridLOGGED'] 	= $userLOGGED[0]->{'usuarioid'};
				$args['emailLOGGED'] 	= $userLOGGED[0]->{'email'};
				$args['typeLOGGED'] 	= $userLOGGED[0]->{'tipo'};
				$args['levelLOGGED'] 	= $userLOGGED[0]->{'nivel'};
				$args['nameLOGGED'] 	= $userLOGGED[0]->{'nome'};
				$args['surnameLOGGED'] 	= $userLOGGED[0]->{'sobrenome'};
				$args['coverLOGGED'] 	= $userLOGGED[0]->{'foto_capa'};
				$args['profileLOGGED'] 	= $userLOGGED[0]->{'foto_perfil'};
				$args['accountLOGGED'] 	= $userLOGGED[0]->{'plano'};
			}

			MODEL('simulado_model');
			$args['history'] = $this->simulado_model->get_desempenho($tokenReverse);

			VIEW('public/simulado/public_performance', $args);
		} else {
			header('Content-Type: text/json; charset=utf-8'); 
			echo json_encode($this->tokenError['unknown_post'], JSON_UNESCAPED_UNICODE);
		}
	}



	/*
	************************************************
	***********  filter AJAX session   *************
	***********************************************/

	function ajax_simulado_filter($id = null){
	if($this->input->post() && $id == null){


		$args['filter'] = $this->input->post();

		$banca 		= (ext($args['filter'], 'banca') != '') ? " banca LIKE '". ext($args['filter'], 'banca') ."' ": '';
        $orgao 		= (ext($args['filter'], 'orgao') != '') ? " orgao LIKE '". ext($args['filter'], 'orgao') ."' ": '';
        $cargo 		= (ext($args['filter'], 'cargo') != '') ? " cargo LIKE '". ext($args['filter'], 'cargo') ."' ": '';
        $disciplina = (ext($args['filter'], 'disciplina') != '') ? " disciplina LIKE '". ext($args['filter'], 'disciplina') ."' ": '';
        $assunto 	= (ext($args['filter'], 'assunto') != '') ? " assunto1 LIKE '". ext($args['filter'], 'assunto') ."' ": '';
        $nivel 		= (ext($args['filter'], 'nivel') != '') ? " escolaridade LIKE '". ext($args['filter'], 'nivel') ."' ": '';
        $ano 		= (ext($args['filter'], 'ano') != '') ? " ano = ". ext($args['filter'], 'ano') .' ': '';

        MODEL('simulado_model');

		$fieldWhere = '';
		$AND = '';
		if($banca != ''){
			$fieldWhere = 'banca';
			$whereParam = ' WHERE ' . $banca;

			$AND .= ($orgao != '') ? ' AND ' . $orgao : '';
			$AND .= ($cargo != '') ? ' AND ' . $cargo : '';
			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		} 
		else if($orgao != ''){
			$fieldWhere = 'orgao';
			$whereParam = ' WHERE ' . $orgao;

			$AND .= ($cargo != '') ? ' AND ' . $cargo : '';
			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($cargo != ''){
			$fieldWhere = 'cargo';
			$whereParam = ' WHERE ' . $cargo;

			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($disciplina != ''){
			$fieldWhere = 'disciplina';
			$whereParam = ' WHERE ' . $disciplina;

			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($assunto != ''){
			$fieldWhere = 'assunto';
			$whereParam = ' WHERE ' . $assunto;
			$where = 1;
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';

		}
		else if($nivel != ''){
			$fieldWhere = 'nivel';
			$whereParam = ' WHERE ' .  $nivel;

			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($ano != ''){
			$fieldWhere = 'ano';
			$whereParam = ' WHERE ' . $ano;

		} else {
			$fieldWhere = null;
			$whereParam = 'WHERE ano = 2014';

		}



        $args['result'] = $this->simulado_model->ajax_simulado_filter($whereParam, $AND);
echo 1; die();
        $count = count($args['result']);
       	if($count > 0){
       		


       		$filterBarWithParams_OBJECT = array(
       			0 => array('label' => 'Questões', 'input_name' => '', 'value' => '', 'icon' => ''),
       			1 => array('label' => '', 'input_name' => '', 'value' => '', 'icon' => ''),
       			2 => array('label' => '', 'input_name' => '', 'value' => '', 'icon' => '')
       		);

       		$limit = (ext($args['result'][0], 'questoes') > 200) ? 201 : ext($args['result'][0], 'questoes');

       		$combo = '<select id="total_questoes" class="form-control">';
       		$combo .= number_options($limit);
       		$combo .= '</select>';

       		$html = '<div class="col-md-12" style="margin-bottom:20px;">';
       		$html .= '<div class="col-md-6">';
       		$html .= ' <label for="textDescription">Total de Questões:</label>';
       		$html .= $combo;
       		$html .= '</div>';
       		$html .= '<div class="col-md-12" style="margin-top:10px;">';
       		$html .= ' <label for="textDescription"></label>';
       		$html .= '<button onclick="ajax_simulado_filter_set_simulado()" class="btn btn-primary">Iniciar Simulado</button>';
       		$html .= '</div>';
       		$html .= '</div>';
       		


       		//$filterBarWithParams($filterBarWithParams_OBJECT);

       		// Result Object Info
       		$percentInfo_OBJECT = array(
       			0 => array('icon' => 'icon-document-bar', 'label' => 'Questões', 	'number' => ext($args['result'][0], 'questoes')),
       			1 => array('icon' => 'icon-document-bar', 'label' => 'Disciplinas', 'number' => ext($args['result'][0], 'disciplina')),
       			2 => array('icon' => 'icon-document-bar', 'label' => 'Assuntos', 	'number' => ext($args['result'][0], 'assunto')),
       			3 => array('icon' => 'icon-document-bar', 'label' => 'Cargos', 		'number' => ext($args['result'][0], 'cargo')),
       		);       	
       		percentInfo($percentInfo_OBJECT);

       		echo $html;


       	} else {
       		
       		$html = '<div class="alert alert-info">';
       		$html .= 'Seu critérios de pesquisa não retornam resultados :/ <strong>Pesquise novamente!</strong>';
       		$html .= '</div>';

       		echo $html;
       	}
     } else if($id != null){


     	$args['filter'] = $this->input->post();

		$banca 		= (ext($args['filter'], 'banca') != '') ? " banca LIKE '". ext($args['filter'], 'banca') ."' ": '';
        $orgao 		= (ext($args['filter'], 'orgao') != '') ? " orgao LIKE '". ext($args['filter'], 'orgao') ."' ": '';
        $cargo 		= (ext($args['filter'], 'cargo') != '') ? " cargo LIKE '". ext($args['filter'], 'cargo') ."' ": '';
        $disciplina = (ext($args['filter'], 'disciplina') != '') ? " q.disciplina LIKE '". ext($args['filter'], 'disciplina') ."' ": '';
        $assunto 	= (ext($args['filter'], 'assunto') != '') ? " assunto1 LIKE '". ext($args['filter'], 'assunto') ."' ": '';
        $nivel 		= (ext($args['filter'], 'nivel') != '') ? " escolaridade LIKE '". ext($args['filter'], 'nivel') ."' ": '';
        $ano 		= (ext($args['filter'], 'ano') != '') ? " ano = ". ext($args['filter'], 'ano') .' ': '';

        MODEL('simulado_model');

		$fieldWhere = '';
		$AND = '';
		if($banca != ''){
			$fieldWhere = 'banca';
			$whereParam = ' WHERE ' . $banca;

			$AND .= ($orgao != '') ? ' AND ' . $orgao : '';
			$AND .= ($cargo != '') ? ' AND ' . $cargo : '';
			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		} 
		else if($orgao != ''){
			$fieldWhere = 'orgao';
			$whereParam = ' WHERE ' . $orgao;

			$AND .= ($cargo != '') ? ' AND ' . $cargo : '';
			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($cargo != ''){
			$fieldWhere = 'cargo';
			$whereParam = ' WHERE ' . $cargo;

			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($disciplina != ''){
			$fieldWhere = 'disciplina';
			$whereParam = ' WHERE ' . $disciplina;

			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($assunto != ''){
			$fieldWhere = 'assunto';
			$whereParam = ' WHERE ' . $assunto;
			$where = 1;
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';

		}
		else if($nivel != ''){
			$fieldWhere = 'nivel';
			$whereParam = ' WHERE ' .  $nivel;

			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($ano != ''){
			$fieldWhere = 'ano';
			$whereParam = ' WHERE ' . $ano;

		} else {
			$fieldWhere = null;
			$whereParam = 'WHERE ano = 2014';

		}



        $args['result'] = $this->simulado_model->ajax_simulado_filter_vimeo($whereParam, $AND);

        $count = count($args['result']);
       	if($count > 0){
       		


       		$filterBarWithParams_OBJECT = array(
       			0 => array('label' => 'Questões', 'input_name' => '', 'value' => '', 'icon' => ''),
       			1 => array('label' => '', 'input_name' => '', 'value' => '', 'icon' => ''),
       			2 => array('label' => '', 'input_name' => '', 'value' => '', 'icon' => '')
       		);

       		$limit = (ext($args['result'][0], 'questoes') > 200) ? 201 : ext($args['result'][0], 'questoes');

       		$combo = '<select id="total_questoes" class="form-control">';
       		$combo .= number_options($limit);
       		$combo .= '</select>';

       		$html = '<div class="col-md-12" style="margin-bottom:20px;">';
       		$html .= '<div class="col-md-6">';
       		$html .= ' <label for="textDescription">Total de Questões:</label>';
       		$html .= $combo;
       		$html .= '</div>';
       		$html .= '<div class="col-md-12" style="margin-top:10px;">';
       		$html .= ' <label for="textDescription"></label>';
       		$html .= '<button onclick="ajax_simulado_filter_set_simulado_video()" class="btn btn-primary">Iniciar Simulado</button>';
       		$html .= '</div>';
       		$html .= '</div>';
       		


       		//$filterBarWithParams($filterBarWithParams_OBJECT);

       		// Result Object Info
       		$percentInfo_OBJECT = array(
       			0 => array('icon' => 'icon-document-bar', 'label' => 'Questões', 	'number' => ext($args['result'][0], 'questoes')),
       			1 => array('icon' => 'icon-document-bar', 'label' => 'Disciplinas', 'number' => ext($args['result'][0], 'disciplina')),
       			2 => array('icon' => 'icon-document-bar', 'label' => 'Assuntos', 	'number' => ext($args['result'][0], 'assunto')),
       			3 => array('icon' => 'icon-document-bar', 'label' => 'Cargos', 		'number' => ext($args['result'][0], 'cargo')),
       		);       	
       		percentInfo($percentInfo_OBJECT);

       		echo $html;


       	} else {
       		
       		$html = '<div class="alert alert-info">';
       		$html .= 'Seu critérios de pesquisa não retornam resultados :/ <strong>Pesquise novamente!</strong>';
       		$html .= '</div>';

       		echo $html;
       	}



     } else {
     	redirect('home/');
     }
	}

	function ajax_simulado_filter_set_simulado($id = null)
	{
		if($this->input->post() && $id == null){
		generateLOG($this->session->userdata('token'), 'SIMULADO/CRIADO');
		
		$args['filter'] = $this->input->post();

		$banca 		= (ext($args['filter'], 'banca') != '') ? " banca LIKE '". ext($args['filter'], 'banca') ."' ": '';
        $orgao 		= (ext($args['filter'], 'orgao') != '') ? " orgao LIKE '". ext($args['filter'], 'orgao') ."' ": '';
        $cargo 		= (ext($args['filter'], 'cargo') != '') ? " cargo LIKE '". ext($args['filter'], 'cargo') ."' ": '';
        $disciplina = (ext($args['filter'], 'disciplina') != '') ? " disciplina LIKE '". ext($args['filter'], 'disciplina') ."' ": '';
        $assunto 	= (ext($args['filter'], 'assunto') != '') ? " assunto1 LIKE '". ext($args['filter'], 'assunto') ."' ": '';
        $nivel 		= (ext($args['filter'], 'nivel') != '') ? " escolaridade LIKE '". ext($args['filter'], 'nivel') ."' ": '';
        $ano 		= (ext($args['filter'], 'ano') != '') ? " ano = ". ext($args['filter'], 'ano') .' ': '';
        $limit 		= ext($args['filter'], 'total_questoes');
        $token 		= ext($args['filter'], 'token');

        MODEL('simulado_model');


		$fieldWhere = '';
		$AND = '';
		if($banca != ''){
			$fieldWhere = 'banca';
			$whereParam = ' WHERE ' . $banca;

			$AND .= ($orgao != '') ? ' AND ' . $orgao : '';
			$AND .= ($cargo != '') ? ' AND ' . $cargo : '';
			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		} 
		else if($orgao != ''){
			$fieldWhere = 'orgao';
			$whereParam = ' WHERE ' . $orgao;

			$AND .= ($cargo != '') ? ' AND ' . $cargo : '';
			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($cargo != ''){
			$fieldWhere = 'cargo';
			$whereParam = ' WHERE ' . $cargo;

			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($disciplina != ''){
			$fieldWhere = 'disciplina';
			$whereParam = ' WHERE ' . $disciplina;

			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($assunto != ''){
			$fieldWhere = 'assunto';
			$whereParam = ' WHERE ' . $assunto;
			$where = 1;
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';

		}
		else if($nivel != ''){
			$fieldWhere = 'nivel';
			$whereParam = ' WHERE ' .  $nivel;

			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($ano != ''){
			$fieldWhere = 'ano';
			$whereParam = ' WHERE ' . $ano;

		} else {
			$fieldWhere = null;
			$whereParam = 'WHERE ano = 2014';
		}


		
		$args['simulado'] = $this->simulado_model->ajax_simulado_filter_set_simulado($whereParam, $AND, $limit);

		$args['last'] = $this->simulado_model->get_last_simulado( $token );
			
		$last_provaid = (ext($args['last'][0], 'provaid') > 0) ? ext($args['last'][0], 'provaid') + 1 : 1;

		$x = 0;

		$count = count($args['simulado']);


		$sql = 'INSERT INTO sep_simulado (simuladoid, provaid, token, prova, qcid, resposta, resposta_correta, data_registro) VALUES';
		while($x < $count){
			if($x + 1 == $count){
				$sql .= " ('', ". $last_provaid .", '". $token ."', '0', ". ext($args['simulado'][$x], 'qcid') .", null, '". ext($args['simulado'][$x], 'resposta_correta') ."', now());";
			} else {
				$sql .= " ('', ". $last_provaid .", '". $token ."', '0', ". ext($args['simulado'][$x], 'qcid') .", null, '". ext($args['simulado'][$x], 'resposta_correta') ."', now()),";
			}
			$x++;
		}

		$insert = $this->simulado_model->create_simulado($sql);
		while($insert < 1){

			$insert = $this->simulado_model->create_simulado($sql);
		}

		$args['sim'] = $this->simulado_model->get_user_simulado_respostas( $token, $last_provaid );

		header('Content-Type: application/json; charset=utf-8');
		// Generate results
		$fp = fopen(SERVER . 'application/assets/internaljson/prova/'. $token . '_'. $last_provaid . '.json', 'w');
		fwrite($fp, json_encode($args['sim'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		echo $last_provaid;
		

	 	} else if($id != null){

	 		generateLOG($this->session->userdata('token'), 'SIMULADO/CRIADO/VIDEO');
			
		$args['filter'] = $this->input->post();

		$banca 		= (ext($args['filter'], 'banca') != '') ? " banca LIKE '". ext($args['filter'], 'banca') ."' ": '';
        $orgao 		= (ext($args['filter'], 'orgao') != '') ? " orgao LIKE '". ext($args['filter'], 'orgao') ."' ": '';
        $cargo 		= (ext($args['filter'], 'cargo') != '') ? " cargo LIKE '". ext($args['filter'], 'cargo') ."' ": '';
        $disciplina = (ext($args['filter'], 'disciplina') != '') ? " q.disciplina LIKE '". ext($args['filter'], 'disciplina') ."' ": '';
        $assunto 	= (ext($args['filter'], 'assunto') != '') ? " assunto1 LIKE '". ext($args['filter'], 'assunto') ."' ": '';
        $nivel 		= (ext($args['filter'], 'nivel') != '') ? " escolaridade LIKE '". ext($args['filter'], 'nivel') ."' ": '';
        $ano 		= (ext($args['filter'], 'ano') != '') ? " ano = ". ext($args['filter'], 'ano') .' ': '';
        $limit 		= ext($args['filter'], 'total_questoes');
        $token 		= ext($args['filter'], 'token');

        MODEL('simulado_model');

		$fieldWhere = '';
		$AND = '';
		if($banca != ''){
			$fieldWhere = 'banca';
			$whereParam = ' WHERE ' . $banca;

			$AND .= ($orgao != '') ? ' AND ' . $orgao : '';
			$AND .= ($cargo != '') ? ' AND ' . $cargo : '';
			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		} 
		else if($orgao != ''){
			$fieldWhere = 'orgao';
			$whereParam = ' WHERE ' . $orgao;

			$AND .= ($cargo != '') ? ' AND ' . $cargo : '';
			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($cargo != ''){
			$fieldWhere = 'cargo';
			$whereParam = ' WHERE ' . $cargo;

			$AND .= ($disciplina != '') ? ' AND ' . $disciplina : '';
			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($disciplina != ''){
			$fieldWhere = 'disciplina';
			$whereParam = ' WHERE ' . $disciplina;

			$AND .= ($assunto != '') ? ' AND ' . $assunto : '';
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($assunto != ''){
			$fieldWhere = 'assunto';
			$whereParam = ' WHERE ' . $assunto;
			$where = 1;
			$AND .= ($nivel != '') ? ' AND ' . $nivel : '';
			$AND .= ($ano != '') ? ' AND ' . $ano : '';

		}
		else if($nivel != ''){
			$fieldWhere = 'nivel';
			$whereParam = ' WHERE ' .  $nivel;

			$AND .= ($ano != '') ? ' AND ' . $ano : '';
		}
		else if($ano != ''){
			$fieldWhere = 'ano';
			$whereParam = ' WHERE ' . $ano;

		} else {
			$fieldWhere = null;
			$whereParam = 'WHERE ano = 2014';
		}



		$args['simulado'] = $this->simulado_model->ajax_simulado_filter_set_simulado_video($whereParam, $AND, $limit);


		$args['last'] = $this->simulado_model->get_last_simulado( $token );
			
		$last_provaid = (ext($args['last'][0], 'provaid') > 0) ? ext($args['last'][0], 'provaid') + 1 : 1;

		$x = 0;

		$count = count($args['simulado']);


		$sql = 'INSERT INTO sep_simulado (simuladoid, provaid, token, prova, qcid, resposta, resposta_correta, data_registro) VALUES';
		while($x < $count){
			if($x + 1 == $count){
				$sql .= " ('', ". $last_provaid .", '". $token ."', '0', ". ext($args['simulado'][$x], 'qcid') .", null, '". ext($args['simulado'][$x], 'resposta_correta') ."', now());";
			} else {
				$sql .= " ('', ". $last_provaid .", '". $token ."', '0', ". ext($args['simulado'][$x], 'qcid') .", null, '". ext($args['simulado'][$x], 'resposta_correta') ."', now()),";
			}
			$x++;
		}

		$insert = $this->simulado_model->create_simulado($sql);
		while($insert < 1){

			$insert = $this->simulado_model->create_simulado($sql);
		}

		$args['sim'] = $this->simulado_model->get_user_simulado_respostas( $token, $last_provaid );

		header('Content-Type: application/json; charset=utf-8');
		// Generate results
		$fp = fopen(SERVER . 'application/assets/internaljson/prova/'. $token . '_'. $last_provaid . '.json', 'w');
		fwrite($fp, json_encode($args['sim'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		echo $last_provaid;



	 	} else {
	 		redirect('home/');
	 	}
	}

	function criar_simulado(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();
		if($this->session->userdata('nivel') == 1 || $this->session->userdata('nivel') == 2 || $this->session->userdata('nivel') == 3){


			$args['meus_simulados'] = $this->simulado_model->meus_simulados_criados( $this->session->userdata('token'));

			$args['provaid'] = $this->simulado_model->get_last_simulado_montado( $this->session->userdata('token'));
			error_reporting(0);
			$count = count(ext($args['provaid'], 'idprova'));
			$args['id'] = ($count > 0) ? ext($args['provaid'][0], 'idprova') + 1 : 1;

			$args['disciplina'] = $this->simulado_model->get_questoes_vimeo_disciplinas();

			VIEW('usuario/geral/criar_simulado', $args);
		} else {
			redirect('usuario/');
		}
	}


	function get_teacher_disciplinas_vimeo()
	{

			$args['filter'] = $this->input->post();
			$args['token'] = ext($args['filter'], 'token');
			$args['provaid'] =ext($args['filter'], 'provaid');

			$disciplina = ext($args['filter'], 'disciplina');

			MODEL('simulado_model');
			$args['data'] = $this->simulado_model->get_questoes_vimeo($disciplina);

			VIEW('simulado/get_teacher_disciplinas_vimeo', $args);
	}

	function set_teacher_disciplinas_vimeo()
	{
		$args['filter'] = $this->input->post();
		$arrParam['token'] = ext($args['filter'], 'token');
		$arrParam['idprova'] = ext($args['filter'], 'provaid');
		$arrParam['qcid'] = ext($args['filter'], 'questaoid');
		$arrParam['data_inicio'] = date('Y-m-d H:i:s');

		MODEL('system_model');

		$getWhereCount = ' token LIKE "'. $arrParam['token'] .'" AND idprova = '. $arrParam['idprova'];
		if(count($this->system_model->select_where('sep_simulado_montado', $getWhereCount)) > 9){
			echo 3;
			return false;
		}

		$where = ' token LIKE "'. $arrParam['token'] .'" AND qcid = '. $arrParam['qcid'] . ' AND idprova = '. $arrParam['idprova'];

		if(count($this->system_model->select_where('sep_simulado_montado', $where)) > 0){
			echo 0;
			return false;
		}
		error_reporting(0);
		$insert = $this->system_model->insert('sep_simulado_montado', $arrParam);
		if($insert){
			echo 1;
		} else {
			echo 0;
		}
	}

	function set_simulado_criado()
	{

		$args['filter'] = $this->input->post();
		$arrParam['token'] = ext($args['filter'], 'token');
		$arrParam['idprova'] =ext($args['filter'], 'provaid');

		MODEL('system_model');
		$where = ' token LIKE "'. $arrParam['token'] . '" and idprova = '. $arrParam['idprova'] . ' and finalizado is null';

		error_reporting(0);
		if(count($this->system_model->select_where('sep_simulado_montado', $where)) > 0){
			$updateWhere = array('token' => $arrParam['token'], 'idprova' => $arrParam['idprova']);
			$arrUpdate['finalizado'] = 'S';

			if($this->system_model->update('sep_simulado_montado', $arrUpdate, $updateWhere)){
				$link =  BASEURL. 'simulado/free/'. encrypt($arrParam['token'], TOKENHASH).'/'.$arrParam['idprova'];
				echo message('success', 'Copie o link para compartilhar com seus amigos e alunos! Link: '.$link);
			}
		}
	}

	function free($token = null, $provaid = null)
	{
		if($token == null || $provaid == null){
			$args['message'] = "OK, você estava tentando acessar o simulado e não deu, verifique se realmente o link passado está correo!";
							$args['solution'] = "
							 - Pode ser que este simulado tenha expirado.";
							VIEW('error_page', $args);
		}
		else if($token != null && $provaid != null && $this->session->userdata('logado')){
			$tokenDecrypt =  decrypt($token, TOKENHASH);
			MODEL('simulado_model');

			$args['simulado'] = $this->simulado_model->get_last_simulado_montado_mount($tokenDecrypt, $provaid);


			generateLOG($this->session->userdata('token'), 'SIMULADO/CRIADO/FREE');

			$args['last'] = $this->simulado_model->get_last_simulado( $this->session->userdata('token'));
			
			$last_provaid = (ext($args['last'][0], 'provaid') > 0) ? ext($args['last'][0], 'provaid') + 1 : 1;

			$x = 0;

			$count = count($args['simulado']);

			$sql = 'INSERT INTO sep_simulado (simuladoid, provaid, token, prova, qcid, resposta, resposta_correta, data_registro) VALUES';
			while($x < $count){
				if($x + 1 == $count){
					$sql .= " ('', ". $last_provaid .", '". $this->session->userdata('token') ."', '". ext($args['simulado'][$x], 'prova') ."', ". ext($args['simulado'][$x], 'qcid') .", null, '". ext($args['simulado'][$x], 'resposta_correta') ."', now());";
				} else {
					$sql .= " ('', ". $last_provaid .", '". $this->session->userdata('token') ."', '". ext($args['simulado'][$x], 'prova') ."', ". ext($args['simulado'][$x], 'qcid') .", null, '". ext($args['simulado'][$x], 'resposta_correta') ."', now()),";
				}
				$x++;
			}

			
			$insert = $this->simulado_model->create_simulado($sql);
			while($insert < 1){

				$insert = $this->simulado_model->create_simulado($sql);
			}

			$args['sim'] = $this->simulado_model->get_user_simulado_respostas( $this->session->userdata('token'), $last_provaid );

			header('Content-Type: application/json; charset=utf-8');
			// Generate results
			$fp = fopen(SERVER . 'application/assets/internaljson/prova/'. $this->session->userdata('token') . '_'. $last_provaid . '.json', 'w');
			fwrite($fp, json_encode($args['sim'], JSON_UNESCAPED_UNICODE));
			fclose($fp);

			
			redirect('simulado/prova_free/'. $last_provaid);



		} else {
			$args['title'] = "Hey, faça seu login para acessar este simulado";
			$args['message'] = "Caso não tenha uma conta ainda cadastre-se.";
			$args['link'] = 'home/';
			VIEW('success_page', $args);
		}
	}

	function prova_free($idprova, $itemsPerPage = 5){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$itemsPerPage = ($itemsPerPage > 10) ? 10 : $itemsPerPage;
		$args['itemsPerPage'] = $itemsPerPage;

		$idprova = addslashes($idprova);
		// Count
		$count = count($this->simulado_model->get_user_simulado($this->session->userdata('token'), $idprova));

		// Data
		$args['simulado'] 		= $this->simulado_model->get_user_simulado_limit($this->session->userdata('token'), $idprova, $itemsPerPage, null);
		//$args['pagination'] 	= ceil($count / $itemsPerPage);
		$args['itemsPerPage'] 	= $itemsPerPage;
		$args['provaid'] 		= $idprova;


		if($count > 0){
			// Load view
			VIEW('simulado/prova_free', $args);
		} else {

			$args['simulado_final'] = $this->simulado_model->get_simulado_numeros($this->session->userdata('token'), $idprova);
			// Load view
			VIEW('simulado/prova_finalizada', $args);
		}	
	}

	function deletar_free($id)
	{
		if($this->session->userdata('logado') == true)
		{
			MODEL('system_model');
			error_reporting(0);
			$where = array('token' => $this->session->userdata('token'), 'idprova' => $id);
			if($this->system_model->delete('sep_simulado_montado', $where)){
				redirect('simulado/criar_simulado');
			}
		} else {
			// Redireciona
			redirect('home/');
		}
	}





	/*
	************************************************
	*********** JSON SIMULADO - filter *************
	***********************************************/
	/*
	* json_simulado
	* Generate json to filter
	*/
	function json_simulado()
	{
		$dateIni = date('H:i:s');
		// Load model
		MODEL('simulado_model');
		// load bancas
		$args['bancas'] = $this->simulado_model->get_distinct_banca();

		header('Content-Type: application/json; charset=utf-8');
		// Generate Orgaos
		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/bancas.json', 'w');
		fwrite($fp, json_encode($args['bancas'], JSON_UNESCAPED_UNICODE));
		fclose($fp);
		
		/*
		* Gera Orgaos e Cargos por banca
		*/
		foreach($args['bancas'] as $bancaResult){
			// Load orgaos
			$args['orgaos']['orgao'] = $this->simulado_model->get_orgaos_by_banca( $bancaResult['banca'] );

		
			$count = count($args['orgaos']['orgao']);
			$x = 0;
			while($x < $count)
			{
				$args['cargos'][$bancaResult['banca']]['orgao'][$x][$args['orgaos']['orgao'][$x]['orgao']] =  $this->simulado_model->get_cargos_by_orgao( $bancaResult['banca'], $args['orgaos']['orgao'][$x]['orgao'] );
				$x++;
			}
		}

		





		// Generate json
		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/geral.json', 'w');
		fwrite($fp, json_encode($args['cargos'], JSON_UNESCAPED_UNICODE));
		fclose($fp);
		// Load cargo
		$args['cargo'] 		= $this->simulado_model->get_group_cargos();
		// Load orgaos
		$args['orgao'] 		= $this->simulado_model->get_group_orgaos();
		// Load anos
		$args['ano'] 		= $this->simulado_model->get_distinct_anos();
		// Load provas
		$args['prova']		= $this->simulado_model->get_group_provas();
		// Load disciplinas
		$args['disciplina'] = $this->simulado_model->get_group_disciplinas();
		// Load assuntos
		$args['assunto'] = $this->simulado_model->get_group_assuntos();

		// Generate json's
		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/cargos.json', 'w');
		fwrite($fp, json_encode($args['cargo'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/anos.json', 'w');
		fwrite($fp, json_encode($args['ano'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/provas.json', 'w');
		fwrite($fp, json_encode($args['prova'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/orgaos.json', 'w');
		fwrite($fp, json_encode($args['orgao'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/disciplinas.json', 'w');
		fwrite($fp, json_encode($args['disciplina'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/assuntos.json', 'w');
		fwrite($fp, json_encode($args['assunto'], JSON_UNESCAPED_UNICODE));




		// ONLY WITH VIDEOS
		$args['banca_vimeo'] 		= $this->simulado_model->get_group_bancas_vimeo();

		// Load cargo
		$args['cargo_vimeo'] 		= $this->simulado_model->get_group_cargos_vimeo();
		// Load orgaos
		$args['orgao_vimeo'] 		= $this->simulado_model->get_group_orgaos_vimeo();
		// Load anos
		$args['ano_vimeo'] 			= $this->simulado_model->get_distinct_anos_vimeo();
		// Load disciplinas
		$args['disciplina_vimeo'] 	= $this->simulado_model->get_group_disciplinas_vimeo();
		// Load assuntos
		$args['assunto_vimeo'] 		= $this->simulado_model->get_group_assuntos_vimeo();

		// Generate json's
		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/bancas_vimeo.json', 'w');
		fwrite($fp, json_encode($args['banca_vimeo'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		// Generate json's
		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/cargos_vimeo.json', 'w');
		fwrite($fp, json_encode($args['cargo_vimeo'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/anos_vimeo.json', 'w');
		fwrite($fp, json_encode($args['ano_vimeo'], JSON_UNESCAPED_UNICODE));
		fclose($fp);


		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/orgaos_vimeo.json', 'w');
		fwrite($fp, json_encode($args['orgao_vimeo'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/disciplinas_vimeo.json', 'w');
		fwrite($fp, json_encode($args['disciplina_vimeo'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		$fp = fopen(SERVER . 'application/assets/internaljson/simulados/assuntos_vimeo.json', 'w');
		fwrite($fp, json_encode($args['assunto_vimeo'], JSON_UNESCAPED_UNICODE));


		
		fclose($fp);

		print_r($args['prova']);

		$dateFim = date('H:i:s');
		echo $dateIni . '<br />';
		echo $dateFim;
	}


	
}

/* End of file simulado.php */
/* Location: ./application/controllers/simulado.php */

