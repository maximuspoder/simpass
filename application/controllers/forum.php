<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Forum extends CI_Controller {



	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('authorization');
		MODEL('forum_model');
	}

	function index(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$args['coverPost'] = end(explode('/', $args['cover']));
		$args['profilePost'] = end(explode('/', $args['profile']));

		$args['posts'] 			= $this->forum_model->get_last_posts();
		$args['mais_acessadas'] = $this->forum_model->get_count_posts_by_discipline();

		VIEW('forum/postagens',$args);

	}

	function load_forum()
	{
		MODEL('forum_model');
		// Filter to ajax post
		$args['filter'] = $this->input->post();


		$qcid = ext($args['filter'], 'id') - 375;

		$args['name'] 	= ext($args['filter'], 'name');
		$args['token']	= ext($args['filter'], 'token');
		$args['qcid']	= $qcid;


		$args['forum'] = $this->forum_model->get_forum_by_id($qcid);

		$count = count($args['forum']);

		if($count > 0){

			// LOAD VIEW
			VIEW('forum/default', $args);
		} else {
			VIEW('forum/primeiro', $args);
		}
	}


	function ajax_set_post()
	{
		MODEL('system_model');

		// Filter to ajax post
		$args['filter'] = $this->input->post();

		$qcid = ext($args['filter'], 'id');

		$arrParam['token'] 		= ext($args['filter'], 'token');
		$arrParam['qcid'] 		= $qcid;
		$arrParam['mensagem'] 	= ext($args['filter'], 'duvida');

		error_reporting(0);
		if($this->system_model->insert('sep_simulado_forum', $arrParam)){
			echo 1;
		} else {
			echo 0;
		}
	}


	function ajax_set_questao_erro()
	{
		MODEL('system_model');

		// Filter to ajax post
		$args['filter'] = $this->input->post();

		$qcid = ext($args['filter'], 'id');

		$arrParam['token'] 		= ext($args['filter'], 'token');
		$arrParam['qcid'] 		= decrypt($qcid, QCIDHASH);
		$arrParam['mensagem'] 	= ext($args['filter'], 'erro');

		error_reporting(0);
		if($this->system_model->insert('sep_questoes_erro', $arrParam)){
			echo 1;
		} else {
			echo 0;
		}
	}

	function code($qcid){
		$fernandoEncrypt 	= encrypt($qcid, QCIDHASH);
		$fernandoDecrypt 	= decrypt($fernandoEncrypt, QCIDHASH);

		echo $fernandoEncrypt . "<BR />";
		echo $fernandoDecrypt . "<BR />";


	}

	function questao($id){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();


		if($args['account'] != null){

			$qcid = decrypt($id, QCIDHASH);

			$args['questao'] = $this->forum_model->get_questao_by_id($qcid);
			$args['respostas'] = $this->forum_model->get_forum_comments_by_id($qcid);

			$args['professor'] = $this->forum_model->get_forum_details_teacher_by_id($qcid);
			$args['id'] = $id;


			if(count($args['professor']) < 1){
				$args['professor'] = null;
			}

			VIEW('forum/questao', $args);


		} else {
			redirect('usuario/assinar');
		}
	}



	function responder($id){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$args['post'] = $this->input->post();
		generateLOG($this->session->userdata('token'), 'FORUM/RESPONDER');
		$arr['mensagem']	= ext($args['post'], 'post');
		$arr['qcid']		= decrypt($id, QCIDHASH);
		$arr['token']		= $this->session->userdata('token');

		error_reporting(0);

		if($this->system_model->insert('sep_simulado_forum', $arr)){

			$args['message'] = "Post enviado com sucesso!";
			VIEW('forum/responder', $args);
		} else {
			$args['message'] = "Erro ao tentar postar sua resposta, tente novamente!";
			VIEW('forum/responder', $args);
		}	
	}


	function deletar($id, $forumid){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();
		generateLOG($this->session->userdata('token'), 'FORUM/DELETAR');

		error_reporting(0);


		if($args['account'] != null){

			$qcid = decrypt($id, QCIDHASH);

			$where = ' qcid = '. $qcid . ' AND token LIKE "'. $this->session->userdata('token') . '" AND forumid = '. $forumid;
			$select = $this->system_model->select_where('sep_simulado_forum', $where);

			if(count($select) > 0){
				$where = array('forumid' => $forumid);
				$remove = $this->system_model->delete('sep_simulado_forum', $where);
				redirect('forum/questao/'. $id);
			}
		}
	}
}
/* End of file forum.php */
/* Location: ./application/controllers/forum.php */

