<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assinatura extends CI_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('authorization');
		MODEL('assinatura_model');
	}

	function index()
	{
		// Obriga estar logado
		Authorization::requireSession();
	}


	function mensal_3(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		generateLOG($this->session->userdata('token'), 'ASSINAR/MES_3');

		$url = 'https://ws.pagseguro.uol.com.br/v2/pre-approvals/request';
		// STATIC INFORMATION
		$data['email'] = 'simuleepasse@gmail.com';
		$data['token'] = 'DBC2E196CE12413886672B9AA1C02D57';

		// USER INFORMATION
		$data['senderEmail'] 			= $args['email'];
		$data['senderAddressCountry'] 	= 'BRA';
		$data['preApprovalCharge'] 		= 'auto';

			// STATIC INFORMATION
			$data['preApprovalName'] 		= 'Assinatura Mensal Simule e Passe';
			$data['preApprovalDetails'] 	= 'A assinatura pode ser cancelada a qualquer momento conforme sua solicitacao';
			$data['preApprovalAmountPerPayment'] = '29.90';
			$data['preApprovalPeriod'] = 'Monthly';
			$month 	= date('m');
			if($month > 10){
				$year 	= date('Y') + 1;
				if($month == 10){
					$month = '01';
				}
				else if($month == 11){
					$month = '02';
				}
				else if($month == 12){
					$month = '03';
				}
			} else {
				$year 	= date('Y');
				$month 	= date('m') + 3;
			}
			$day	= date('d');
			$data['preApprovalFinalDate'] = $year.'-'.$month.'-'.$day.'T00:00:000-03:00';
			$data['preApprovalMaxTotalAmount'] = '89.70';
			$data['reference'] = '1';
			$data['redirectURL'] = "http://simuleepasse.com.br/assinatura/retorno";

			// Mount http query
			$data = http_build_query($data);

			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			$xml= curl_exec($curl);

			if($xml == "Unauthorized"){
				// exceptions
				print_r($sql);
			exit;
			}
			curl_close($curl);

			$xml= simplexml_load_string($xml);

			if(count($xml->error) > 0){
				// exception
				print_r($xml->error);
			}

			$transactionid = (string)$xml->code;
			$arrParam['token'] = $this->session->userdata('token');
			$arrParam['transactionid'] = $transactionid;
			$arrParam['tipo_assinatura'] = 'mensal_3';
			$arrParam['status'] = '0';
			$arrParam['data_registro'] = date('Y-m-d H:i:s');

			$insert = $this->system_model->insert('sep_assinaturas', $arrParam);
			if(count($insert) > 0 ){
				header('Location: https://pagseguro.uol.com.br/v2/pre-approvals/request.html?code='. $xml->code);
			} else {
				redirect('assinatura/mensal_3');
			}
	}



	function mensal_6(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();
		generateLOG($this->session->userdata('token'), 'ASSINAR/MES_6');

		$url = 'https://ws.pagseguro.uol.com.br/v2/pre-approvals/request';
		// STATIC INFORMATION
		$data['email'] = 'simuleepasse@gmail.com';
		$data['token'] = 'DBC2E196CE12413886672B9AA1C02D57';

		// USER INFORMATION
		$data['senderEmail'] 			= $args['email'];
		$data['senderAddressCountry'] 	= 'BRA';
		$data['preApprovalCharge'] 		= 'auto';

		// STATIC INFORMATION
		$data['preApprovalName'] 		= 'Assinatura Mensal Simule e Passe';
		$data['preApprovalDetails'] 	= 'A assinatura pode ser cancelada a qualquer momento conforme sua solicitacao';
		$data['preApprovalAmountPerPayment'] = '19.90';
		$data['preApprovalPeriod'] = 'Monthly';
		$month 	= date('m');
		if($month > 6){
			$year 	= date('Y') + 1;
			if($month == 7){
				$month = '01';
			}
			else if($month == 8){
				$month = '02';
			}
			else if($month == 9){
				$month = '03';
			}
			else if($month == 10){
				$month = '04';
			}
			else if($month == 11){
				$month = '05';
			}
			else if($month == 12){
				$month = '06';
			}
		} else {
			$year 	= date('Y');
			$month 	= date('m') + 6;
		}
		$day	= date('d');
		$data['preApprovalFinalDate'] = $year.'-'.$month.'-'.$day.'T00:00:000-03:00';
		$data['preApprovalMaxTotalAmount'] = '119.40';
		$data['reference'] = '1';
		$data['redirectURL'] = "http://simuleepasse.com.br/assinatura/retorno";

		// Mount http query
		$data = http_build_query($data);

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		$xml= curl_exec($curl);

		if($xml == "Unauthorized"){
			// exceptions
			print_r($sql);
		exit;
		}
		curl_close($curl);

		$xml= simplexml_load_string($xml);

		if(count($xml->error) > 0){
			// exceptions
			print_r($xml->error);
		}

		$transactionid = (string)$xml->code;
		$arrParam['token'] = $this->session->userdata('token');
		$arrParam['transactionid'] = $transactionid;
		$arrParam['tipo_assinatura'] = 'mensal_6';
		$arrParam['status'] = '0';
		$arrParam['data_registro'] = date('Y-m-d H:i:s');

		$insert = $this->system_model->insert('sep_assinaturas', $arrParam);
		if(count($insert) > 0 ){
			header('Location: https://pagseguro.uol.com.br/v2/pre-approvals/request.html?code='. $xml->code);
		} else {
			redirect('assinatura/mensal_6');
		}
	}



	function mes(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();
		generateLOG($this->session->userdata('token'), 'ASSINAR/MES');

		$url = 'https://ws.pagseguro.uol.com.br/v2/checkout';

		// STATIC INFORMATION
		$data['email'] = 'simuleepasse@gmail.com';
		$data['token'] = 'DBC2E196CE12413886672B9AA1C02D57';

		// USER INFORMATION
		$data['currency'] 			= "BRL";
		$data['itemId1'] 			= "1";
		$data['itemDescription1'] 	= "Acesso de um mes ao Simule & Passe";
		$data['itemAmount1'] 		= "34.00";
		$data['itemQuantity1']		= "1";
		$data['itemWeight1'] 		= "0";
		$data['reference'] 			= "REF34MES";
		$data['senderName'] 		= $args['name'] . ' ' . $args['surname'];
		$data['senderEmail'] 		= $args['email'];
		$data['shippingType'] 		= "1";
		$data['shippingAddressCountry'] = "BRA";
		$data['redirectURL'] = "http://simuleepasse.com.br/assinatura/retorno/";

		// Mount http query
		$data = http_build_query($data);

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		$xml= curl_exec($curl);

		if($xml == "Unauthorized"){
			// exceptions
		exit;
		}
		curl_close($curl);

		$xml= simplexml_load_string($xml);

		if(count($xml->error) > 0){
			// exceptions
			print_r($xml);
		} else {

			// MOUNT TO INSERT INTO DB
			$transactionid = (string)$xml->code;
			$arrParam['token'] = $this->session->userdata('token');
			$arrParam['transactionid'] = $transactionid;
			$arrParam['tipo_assinatura'] = 'mes';
			$arrParam['status'] = '0';
			$arrParam['data_registro'] = date('Y-m-d H:i:s');	

			$insert = $this->system_model->insert('sep_assinaturas', $arrParam);
			if(count($insert) > 0 ){
				header('Location: https://pagseguro.uol.com.br/v2/checkout/payment.html?code='. $xml->code);
			} else {
				redirect('assinatura/mes');
			}
		}
	}


	function retorno()
	{
		VIEW('pagseguro/retorno');
	}

	function status(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$args['assinaturas'] = $this->assinatura_model->get_status($this->session->userdata('token'));

		VIEW('pagseguro/status', $args);
	}

	function update_status($token, $assinaturaid)
	{

		if($token != null && $assinaturaid != null)
		{

			$args['assinatura'] = $this->assinatura_model->get_assinatura($token, $assinaturaid);

			// Obriga estar logado
			Authorization::requireSession();
			// Passa os dados da sessao para o array, podendo ser usado na view
			$args = Authorization::getSession();

			if(count($args['assinatura']) > 0){
				MODEL('system_model');
				$where = array('token' => $token);
				$arrParam['plano'] = 'S';

				error_reporting(0);
				// IF UPDATE STATUS
				if($this->system_model->update('sis_usuario_dados', $arrParam, $where)){
					//UPDATE JSON STATUS
					restApiUpdateJson($token, 'plano', 'S');
					$args['message'] = 'Seus dados foram atualizados com sucesso. Agora você já poderá usufruir de toda vantagem de ser premium Simule & Passe';
					VIEW('pagseguro/update_status', $args);
				} else{
					$args['message'] = 'Seus dados não puderam ser atualizados neste momento, você pode também realizar logout e login para atualizar!';
					VIEW('pagseguro/update_status', $args);
				}


			} else {

				$args['message'] = 'Seus dados não foram atualizados, provavelmente este pagamento não foi liberado até o momento ou não exista esta compra!';
				VIEW('pagseguro/update_status', $args);
			}

		} else {
			// Redireciona
			redirect('home/');
		}
	}




	function verifica_pagamento()
	{

		redirect('home/');
		$xml =  simplexml_load_file('https://ws.pagseguro.uol.com.br/v2/transactions?initialDate=2014-07-03T00:00&finalDate=2014-07-03T23:00&page=1&maxPageResults=100&email=simuleepasse@gmail.com&token=DBC2E196CE12413886672B9AA1C02D57');

		//$xml->transactions->transaction[3]->reference;
		foreach($xml->transactions->transaction as $resultXML):
			//print_r($values);
			echo "Código da compra: ". $resultXML->reference . "<br/>";
			echo "Status: ". $resultXML->status. "<br/><br/><hr/>";
			//resultXML($result);
			//echo $resultXML[0]->reference;
		endforeach;
	}


}#end class