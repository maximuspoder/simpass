<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('authorization');
		MODEL('usuario_model');
	}

	function index(){

		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$following_info 	= $this->usuario_model->data_view_profile_get_following_info( sessionID() );
		MODEL('timeline_model');
		//$args['timeline'] 	= $this->timeline_model->get_results( $following_info, sessionID() );
		// Carrega quem visualizou perfil do usuário
		$args['profile_view'] = $this->usuario_model->get_profile_view(sessionID());
		// Carrega view de login

		VIEW('usuario/timeline', $args);
	}

	/**
	* @info Visualiza perfil de outro usuário ou
	* próprio
	* @param $userid
	*/
	function perfil($userid = null){

		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		if($userid == null){

			$args['followers_count'] = $this->usuario_model->data_view_profile_get_followers(sessionID());
			$args['following_count'] = $this->usuario_model->data_view_profile_get_following(sessionID());

			$m = new MongoClient();

			$db = $m->timeline;

			$collection = $db->posts;
			$where = array('token' => sessionID());
			$cursor = $collection->find($where);
			$args['cursor'] = $cursor->sort(array('date' => -1));

			// Carrega view de login
			VIEW('usuario/perfil', $args);

		} else {

			if($userid == $args['userid'])
			{
				redirect('usuario/perfil');
			}

			$args['user'] 		= $this->usuario_model->data_view_profile($userid);
			$args['followers'] 	= $this->usuario_model->data_view_profile_get_followers( ext($args['user'][0], 'token') );
			$args['follow'] 	= $this->usuario_model->data_view_profile_follow( ext($args['user'][0], 'token'), sessionID());

			$m = new MongoClient();

			$db = $m->timeline;

			$collection = $db->posts;
			$where = array('token' => $args['user'][0]['token']);
			$cursor = $collection->find($where);
			$args['cursor'] = $cursor->sort(array('date' => -1));
			generateLOG(sessionID(), 'VISITAR/PERFIL', $args['user'][0]['token']);

			// Carrega view de login
			VIEW('usuario/buscar/perfil', $args);

			}
	}


	function sobre($update = null, $userid = null){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		MODEL('usuario_model');
		if($args['userid'] != $userid && !is_null($userid) ){
			//Acessando outro usuário
			$args['usuario_dados'] = $this->usuario_model->data_view_profile($userid);
			$args['followers'] 	= $this->usuario_model->data_view_profile_get_followers( ext($args['usuario_dados'][0], 'token') );
			$args['follow'] 	= $this->usuario_model->data_view_profile_follow( ext($args['usuario_dados'][0], 'token'), $this->session->userdata('token'));
			VIEW('usuario/dados/default_view', $args);

		} else {
			//Acessando meu usuário
			$args['followers_count'] = $this->usuario_model->data_view_profile_get_followers(sessionID());
			$args['following_count'] = $this->usuario_model->data_view_profile_get_following(sessionID());
			// Carrega view de login
			VIEW('usuario/dados/default', $args);
		}
	}

	function editar_meus_dados(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();
		// Carrega o total de seguidores
		$args['followers_count'] = $this->usuario_model->data_view_profile_get_followers(sessionID());
		$args['following_count'] = $this->usuario_model->data_view_profile_get_following(sessionID());
		// Carrega view de login
		VIEW('usuario/dados/editar', $args);
	}

	function editar_meus_dados_process(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$args['filter'] = POST();
		// Log
		generateLOG(sessionID(), 'DADOS/EDITAR');

		$arrParamLogin['nome'] 			= addslashes(ext($args['filter'], 'name'));
		$arrParamLogin['sobrenome'] 	= addslashes(ext($args['filter'], 'surname'));
		$arrParamLogin['sexo']			= htmlentities(addslashes(ext($args['filter'], 'sex')));
		if(ext($args['filter'], 'email') 	!= ''){
			$arrParamLogin['email'] = ext($args['filter'], 'email');
		}
		if(ext($args['filter'], 'senha') != ''){
			$arrParamLogin['senha'] = md5(ext($args['filter'], 'senha'));
		}
		$arrParamDados['sobre'] 	= htmlentities(addslashes(ext($args['filter'], 'about')));
		$arrParamDados['telefone'] 	= htmlentities(addslashes(ext($args['filter'], 'phone')));
		$arrParamDados['email2'] 	= htmlentities(addslashes(ext($args['filter'], 'email2')));
		$arrParamDados['facebook'] 	= htmlentities(addslashes(ext($args['filter'], 'facebook')));
		$arrParamDados['twitter'] 	= htmlentities(addslashes(ext($args['filter'], 'twitter')));
		$arrParamDados['youtube'] 	= htmlentities(addslashes(ext($args['filter'], 'youtube')));
		$arrParamDados['site'] 		= htmlentities(addslashes(ext($args['filter'], 'site')));
		$arrParamDados['cargo'] 	= htmlentities(addslashes(ext($args['filter'], 'cargo')));
		$nascimento 				= explode('/', ext($args['filter'], 'nascimento'));
		$arrParamLogin['dia'] 		= htmlentities(addslashes($nascimento[0]));
		$arrParamLogin['mes'] 		= htmlentities(addslashes($nascimento[1]));
		$arrParamLogin['ano'] 		= htmlentities(addslashes($nascimento[2]));

		$where = array('token' => sessionID());
		if($this->system_model->update('sis_usuario_dados', $arrParamDados, $where))
		{
			$update = $this->system_model->update('sis_usuario', $arrParamLogin, $where);
			redirect('usuario/sobre/updated');
		} else {
			redirect('usuario/editar_meus_dados');
		}
	}


	function update_photo_process(){
		error_reporting(0);
		// Pega tipo de arquivo
		$format = end(explode('.', $_FILES['file_upload']['name']));


		// data e hora
		$data = date('Y-m-d');
		$hour = date('H:i:s');
		// Explode
		$date = explode('-', $data);
		$hora = explode(':', $hour);

		// Catch data coming from ajax
		$args['filter'] = $this->input->post();
		// catch user token
		$token = ext($args['filter'], 'token');

		/*
		* Ordem dos arrays passados
		* string + dia + mes + ano + hora + min + seg 
		*/
		$tokenMD5 = md5($token);
		$file =  string_aleatoria(10) . '_' . $tokenMD5 . '_' .  $data[2] . '_' . $data[1] . '_' . $data[0] . '_' . $hora[0] . '_' . $hora[1] . '_' . $hora[2] . '.' . $format;		


		$cover = ( $args['filter']['update'] == 'cover' ) ? 1 : 0;
		$photo = ( $args['filter']['update'] == 'photo' ) ? 1 : 0;

		if($cover == 1)
		{

			// Move anexo para pasta
			$upload = move_uploaded_file($_FILES['file_upload']['tmp_name'], UPLOAD . 'cover/' . $file);


			if(file_exists(UPLOAD . 'cover/' . $file)){

				$arrParam['foto_capa'] = UPLOADLINK .  'cover/' .  $file;

				generateLOG($this->session->userdata('token'), 'ALTERAR/FOTO_CAPA', $arrParam['foto_capa']);

				// Mount where to update
				$where = array('token' => $token );

				MODEL('system_model');
				if($this->system_model->update('sis_usuario_dados', $arrParam, $where)){

					restApiUpdateJson($token, 'foto_capa', $arrParam['foto_capa']);

					echo "<script>setInterval(function(){ parent.window.location.reload(); }, 1000);</script>";
				}

			} else {
				echo "Erro interno ao realizar upload :(";
			}
		}
		elseif($photo == 1)
		{


			unlink(UPLOAD . 'profile/' . $file);
			// Move anexo para pasta
			$upload = move_uploaded_file($_FILES['file_upload']['tmp_name'], UPLOAD . 'profile/' . $file);

			echo UPLOAD . 'profile/' . $file;
			if(file_exists(UPLOAD . 'profile/' . $file)){

				$arrParam['foto_perfil'] = UPLOADLINK .  'profile/' .  $file;

				generateLOG($this->session->userdata('token'), 'ALTERAR/FOTO_PERFIL', $arrParam['foto_perfil']);
				// Mount where to update
				$where = array('token' => $token );

				MODEL('system_model');
				if($this->system_model->update('sis_usuario_dados', $arrParam, $where)){

					restApiUpdateJson($token, 'foto_perfil', $arrParam['foto_perfil']);

					echo "<script>setInterval(function(){ parent.window.location.reload(); }, 1000);</script>";
				}

			} else {
				echo "Erro interno ao realizar upload :(";
			}
		}
	}

	function buscar(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();
		// Log
		generateLOG(sessionID(), 'BUSCAR/USUARIO');
		// Dados do form
		$args['filter'] = POST();
		// pesquisa
		$search = ext($args['filter'], 'search');

		if($search == ''){

			VIEW('usuario/buscar/default', $args);

		} else {

			$args['search'] = $search = ext($args['filter'], 'search');

			if(count(explode(' ', $search)) > 1) {

  				$explode = explode(' ', $search);
  				$name = $explode[0];
  				$surname = $explode[1] . ' ' . @$explode[2] . ' ' . @$explode[3]. ' ' . @$explode[4];

				$args['result_not_follow'] = $this->usuario_model->get_user_by_searchNotFollow(sessionID(), trim($name), trim($surname));
				$args['result_follow'] = $this->usuario_model->get_user_by_searchFollow(sessionID(), trim($name), trim($surname));
			} else {
				$args['result_not_follow'] = $this->usuario_model->get_user_by_searchNotFollow(sessionID(), $search, null);
				$args['result_follow'] = $this->usuario_model->get_user_by_searchFollow(sessionID(), $search, null);
			}
			// Parceiro
			$args['partner'] = $this->usuario_model->select_partner_rand();
			// Carrega view de login
			VIEW('usuario/buscar/default', $args);

		}
	}


	function set_follower(){
		$args['filter'] = POST();

		$arrParam['token_de']	= substr(ext($args['filter'], 'token_from'), 40);
		$arrParam['token_para']		= substr(ext($args['filter'], 'token_to'), 0, 27);
		generateLOG($arrParam['token_de'], 'SEGUIR/USUARIO');

		MODEL('system_model');
		error_reporting(0);
		$insert = $this->system_model->insert('sep_seguidores', $arrParam);

		while($insert < 1)
		{
			$insert = $this->system_model->insert('sep_seguidores', $arrParam);
		}

		MODEL('api_model');
		$args['following'] = $this->api_model->api_following($arrParam['token_de']);
		header('Content-Type: application/json; charset=utf-8'); 
		$fp = fopen(SERVER . 'application/assets/internaljson/seguindo/' . $arrParam['token_de'] . '.json', 'w');
		fwrite($fp, json_encode($args['following'], JSON_UNESCAPED_UNICODE));
		fclose($fp);

		echo $insert;
	}

	function unset_set_follower(){
		if(POST()){
			$args['filter'] = POST();

			$arrParam['token_de']	= decrypt(ext($args['filter'], 'token_from'), TOKENHASH);
			$arrParam['token_para']	= decrypt(ext($args['filter'], 'token_to'), TOKENHASH);

			MODEL('system_model');

			$where = array('token_de' => $arrParam['token_de'], 'token_para' => $arrParam['token_para']);
			error_reporting(0);
			if($this->system_model->delete('sep_seguidores', $where)){

				MODEL('api_model');
				$args['following'] = $this->api_model->api_following($arrParam['token_de']);

				header('Content-Type: application/json; charset=utf-8'); 
				$fp = fopen(SERVER . 'application/assets/internaljson/seguindo/' . $arrParam['token_de'] . '.json', 'w');
				fwrite($fp, json_encode($args['following'], JSON_UNESCAPED_UNICODE));
				fclose($fp);

				echo 1;
			} else {
				echo 0;
			}
		}
	}

	function seguidores($update = null, $userid = null){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$args['update'] = $update;

		if($args['userid'] != $userid){
			$args['usuario_dados'] = $this->usuario_model->data_view_profile($userid);
			$args['followers'] 	= $this->usuario_model->data_view_profile_get_followers( ext($args['usuario_dados'][0], 'token') );
			$args['followers_info'] 	= $this->usuario_model->data_view_profile_get_followers_info( ext($args['usuario_dados'][0], 'token') );
			$args['follow'] 	= $this->usuario_model->data_view_profile_follow( ext($args['usuario_dados'][0], 'token'), sessionID());
			// Carrega view de login
			VIEW('usuario/seguidores/seguidores', $args);

		} else {
			$args['followers'] 	= $this->usuario_model->data_view_profile_get_followers( $args['token'] );
			$args['followers_info'] = $this->usuario_model->data_view_profile_get_followers_info( $args['token'] );
			$args['followers_count'] = $this->usuario_model->data_view_profile_get_followers(sessionID());
			$args['following_count'] = $this->usuario_model->data_view_profile_get_following(sessionID());
			// Carrega view de login
			VIEW('usuario/seguidores/default', $args);
		}
	}

	function seguindo($p, $userid){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$args['usuario_dados'] = $this->usuario_model->data_view_profile($userid);
		$args['following_info'] = $this->usuario_model->data_view_profile_get_following_info( ext($args['usuario_dados'][0], 'token') );
		$args['follow'] = $this->usuario_model->data_view_profile_follow( ext($args['usuario_dados'][0], 'token'), sessionID());

		$args['followers_count'] = $this->usuario_model->data_view_profile_get_followers(sessionID());
		$args['following_count'] = $this->usuario_model->data_view_profile_get_following(sessionID());
		//Load view
		VIEW('usuario/seguindo/default', $args);

	}

	function cadastrar_usuario(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		if(sessionNivel() == 1 || sessionNivel() == 2 || sessionNivel() == 3){

			$args['usuarios'] = $this->usuario_model->get_users_partner(sessionID());
			VIEW('usuario/geral/cadastrar_usuario', $args);

		}  else {
			redirect('usuario/');
		}
	}

	function cadastrar_usuario_(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();
		if(POST())
		{
			if(sessionNivel() == 1 || sessionNivel() == 2 || sessionNivel() == 3){
				$args['filter'] = POST();

				// Dados do novo usuário
				$arrParam['usuariotipoid'] 	= 4;
				$arrParam['token'] 			=  substr(generate_token_unique( ext($args['filter'], 'email') ), 0, 27);
				$arrParam['usuariovinculo'] = $this->session->userdata('token');
				$arrParam['nome'] 			= ext($args['filter'], 'name');
				$arrParam['sobrenome'] 		= ext($args['filter'], 'surname');
				$arrParam['email'] 			= ext($args['filter'], 'mail');
				$arrParam['sexo'] 			= ext($args['filter'], 'sex');
				$arrParam['ativo'] 			= 'N';
				$arraDados['estado'] 		= ext($args['filter'], 'state');
				$arraDados['cidade'] 		= ext($args['filter'], 'city');
				$arraDados['plano'] 		= ( ext($args['filter'], 'account') == 'S' ) ? 'S' : 'N';
				$arraDados['foto_perfil'] 	= 'http://simuleepasse.com.br/application/assets/uploads/profile/default.png';
				$arraDados['foto_capa'] 	= 'http://simuleepasse.com.br/application/assets/uploads/cover/default.png';
				$arraDados['token'] = $arrParam['token'];

				if($this->system_model->insert('sis_usuario', $arrParam)){
					if($this->system_model->insert('sis_usuario_dados', $arraDados)){
						$insert = 1;
						if($arrParam['plano'] == null){
							$subject = 'Simule & Passe - Estude e passe em concursos públicos';
						} else {
							$subject = '[Acesso Premium] Simule & Passe - Estude e passe em concursos públicos';
						}
						$message = '<strong>Bem-vindo ao Simule & Passe!</strong><br/>';
						$message .= 'Você foi cadastrado em nosso sistema por ' .  $args['name'] . ' ' . $args['surname'] . ',<br/>';
						$message .= 'aproveite agora mesmo para acessar o sistema e começar seus estudos!';
						$message .= '<br /><br />';
						$message .= 'Para acessar seu cadastro utilize o link abaixo:<br/>';
						$message .= 'http://www.simuleepasse.com.br/usuario/validar_cadastro/' . $arrParam['token'] . '/' . $this->session->userdata('token');
						$arrMail = array(
							'to_name' => $arrParam['nome'],
							'to_mail' => $arrParam['email'],
							'subject' => $subject,
							'message' => $message
						);
						send_mailgun($arrMail);
						redirect('usuario/cadastrar_usuario_process');
					}
				}
			} else {
				redirect('usuario/');
			}
		} else {
			redirect();
		}
	}

	function cadastrar_usuario_process(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		if(sessionNivel() == 1 || sessionNivel() == 2 || sessionNivel() == 3){

			generateLOG(sessionID(), 'CADASTRO/USUARIO');
			VIEW('usuario/geral/cadastrar_usuario_process', $args);

		}  else {
			redirect('usuario/');

		}
	}

	function assinar(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		generateLOG(sessionID(), 'PLANOS');

		VIEW('usuario/geral/assinar', $args);
	}

	function validar_cadastro($tokenME = null, $token = null){

		if($token != null &&  $tokenME != null){

			MODEL('usuario_model');
			$args['usuario'] = $this->usuario_model->validar_cadastro($tokenME, $token);
			$args['token'] = $tokenME;
			if(count($args['usuario']) > 0){
				VIEW('usuario/geral/validar_cadastro', $args);	
			} else {
				redirect('home/');
			}
		} else {
			redirect('home/');
		}
	}

	function validar_cadastro_process($token = null, $tokenME = null){
		if($token == null &&  $tokenME == null){

			$args['filter'] = POST();
			$token = ext($args['filter'], 'token');

			$args['usuario'] = $this->usuario_model->validar_cadastro_process($token);


			if(count($args['usuario']) > 0){

				$where = array('token' => $token);
				$arrParam['senha'] = md5( ext($args['filter'], 'senha') );
				$arrParam['ativo'] = 'S';

				error_reporting(0);
				if($this->system_model->update('sis_usuario', $arrParam, $where)){
					$args['message'] = 'OK, seu cadastro foi validado com sucesso!';
					VIEW('usuario/geral/validar_cadastro_process', $args);	
				}

			} else {
				$args['message'] = 'Hey, este cadastro ou não existe ou já foi validado!';
				VIEW('usuario/geral/validar_cadastro_process', $args);	
			}
		}
	}

	function faq(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		generateLOG($this->session->userdata('token'), 'VIEW/FAQ');

		VIEW('usuario/geral/faq', $args);
	}

	function professores(){
		// Obriga estar logado
		Authorization::requireSession();
		// Passa os dados da sessao para o array, podendo ser usado na view
		$args = Authorization::getSession();

		$args['professores'] = $this->usuario_model->get_professores();

		VIEW('usuario/professores/professores', $args);
	}

}/* End of file usuario.php */
/* Location: ./application/controllers/usuario.php */
