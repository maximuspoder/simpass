<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Classe Array_Table
*
* Monta uma tabela de dados através de um array de dados. Suporta adição de colunas
* antes ou depois do processamento do html.
*
*/
class Array_Table {
	// Definição de Atributos
	public $data = array();
	public $html = '';
	public $id = '';
	public $class = '';
	public $columns = array();
	public $columnsBefore = array();
	public $columnsAfter = array();
	public $indexColumnBefore = 0;
	public $indexColumnAfter = 0;
	public $items_per_page = 100;
	public $page_link = '';
	public $actual_page = 1;
	public $columnsID;
	
	/**
	* __construct()
	* Construtor de classe.
    * Define o ID da tabela, classe e o array de dados, caso necessário.
	* @param string id
	* @param string class
	* @param array data
	* @return object
	*/
	public function __construct($id = null, $class= null, $data = array())
	{
        $this->data = (!is_null($data) && count($data) > 0) ? $data : $this->data;
		$this->id  = (!is_null($id)) ? $id : $this->id;
		$this->class = (!is_null($class)) ? $class : $this->class;
	}
	
	/**
	* get_instance()
	* Retorna uma instancia desta classe.
	* @return object array_table
	*/
	public function get_instance()
	{
		return new Array_Table($this->get_id(), $this->get_class(), $this->get_data());
	}
	
	/**
	* set_items_per_page()
	* Seta a quantidade de itens por página, para a tabela.
	* @param integer count
	* @return void
	*/
	public function set_items_per_page($count = null)
	{
		$this->items_per_page = (!is_null($count) && $count != '') ? $count : $this->items_per_page;
	}
	
	/**
	* get_items_per_page()
	* Retorna a quantidade de itens por pagina.
	* @return $this->items_per_page
	*/
	public function get_items_per_page()
	{
		return $this->items_per_page;
	}
	
	/**
	* set_page_link()
	* Seta o link da página da tabela atual, para o submit da paginação.
	* @param string link
	* @return void
	*/
	public function set_page_link($link = null)
	{
		$this->page_link = (!is_null($link)) ? $link : $this->page_link;
	}
	
	/**
	* get_page_link()
	* Retorna o link de submit e paginacao da pagina.
	* @return $this->page_link
	*/
	public function get_page_link()
	{
		return $this->page_link;
	}
	
	/**
	* set_actual_page()
	* Seta a pagina atual da paginacao.
	* @param integer actual_page
	* @return void
	*/
	public function set_actual_page($actual_page = null)
	{
		$this->actual_page = (!is_null($actual_page) && $actual_page != '') ? $actual_page : $this->actual_page;
	}
	
	/**
	* get_actual_page()
	* Retorna a pagina atual da paginação.
	* @return $this->page_link
	*/
	public function get_actual_page()
	{
		return $this->actual_page;
	}
	
	/**
	* set_data()
	* Seta o array de dados para o encaminhado.
	* @param array data
	* @return void
	*/
	public function set_data($data = array())
	{
		$this->data = (!is_null($data) && count($data) > 0 ) ? $data : $this->data;
	}
	
	/**
	* getData()
	* Retorna o array de dados da classe.
	* @return $this->data
	*/
	public function get_data()
	{
		return $this->data;
	}
	
	/**
	* set_columns()
	* Array definindo quais colunas serão visiveis.
	* @param array data
	* @return void
	*/
	public function set_columns($data = array())
	{
		$this->columns = (!is_null($data) && count($data) > 0 ) ? $data : $this->columns;
		
		// Passa valores internos do array para caixa-baixa
		$count_columns = count($this->columns);
		if($count_columns > 0)
		{
			for($i = 0; $i < $count_columns; $i++)
			{
				$aux[] = strtolower($this->columns[$i]);
			}
			$this->columns = $aux;
		}
	}
	
	/**
	* get_columns()
	* Retorna o array de dados da classe.
	* @return $this->columns
	*/
	public function get_columns()
	{
		return $this->columns;
	}
	
	/**
	* empty_columns()
	* Zera todas as colunas da tabela.
	* @param array data
	* @return void
	*/
	public function empty_columns($data = array())
	{
		$this->columns = array();
		$this->columnsBefore = array();
		$this->columnsAfter = array();
	}
	
	/**
	* set_id()
	* Seta o ID encaminhado para o aributo id da classe.
	* @param string id
	* @return void
	*/
	public function set_id($id = null)
	{
		$this->id = (!is_null($id)) ? $id : $this->id;
	}
	
	/**
	* get_id()
	* Retorna o ID do atributo id da classe.
	* @return $this->id
	*/
	public function get_id()
	{
		return $this->id;
	}
	
	/**
	* set_class()
	* Seta o class encaminhado para o aributo class da classe.
	* @param string class
	* @return void
	*/
	public function set_class($class = null)
	{
		$this->class = (!is_null($class)) ? $class : $this->class;
	}
	
	/**
	* get_class()
	* Retorna o Class do atributo class da classe.
	* @return $this->class
	*/
	public function get_class()
	{
		return $this->class;
	}
    
    /**
	* add_column()
	* Este método adiciona uma coluna à tabela. Caso algum número
	* de coluna do SQL esteja entre a tag [NÚMERO_COLUNA], então 
	* a tabela substitui o valor da coluna da linha corrente pela tag.
	* O segundo parâmetro verifica se a coluna deve ser posta no iní-
	* cio da tabela ou após o processamento do SQL.
	* @param string prConteudo
    * @param boolean prBefore
	* @return void
	*/
	public function add_column($prConteudo = null, $prBefore = true)
	{
		if(!is_null($prConteudo))
		{
			if($prBefore)
			{
				$this->columnsBefore[$this->indexColumnBefore] = $prConteudo;
				$this->indexColumnBefore++;
			} else {
				$this->columnsAfter[$this->indexColumnAfter] = $prConteudo;
				$this->indexColumnAfter++;
			}
		}
	}

	function add_columnid($prConteudo){
		//$this->columnsID
		$this->columnsID = $prConteudo;
	}

	/**
	* proccess()
	* Este método processa o array de dados e monta a tabela conforme
	* os dados configurados anteriormente.
	* @return void
	*/
	public function proccess()
	{
		if($this->data != '' && isset($this->data) && count($this->data) > 0)
		{
			// Monta os parâmetros a serem encaminhados para a montagem da Grid
			$id = $this->id;
			$class = $this->class;
			$data = $this->data;
			$count_data = count($this->data);
			$columnsBefore = $this->columnsBefore;
			$columnsID = $this->columnsID;
			$columnsAfter = $this->columnsAfter;
			$total_columns = count($this->columns);
			$objResult = $this->data;
			$objRSHead = $this->data;

			// Cria o layout da tabela
			$auxColNumbers = 0;
			$colNumber     = 0;
			$linhas        = array();
			$counter_total_columns = 0;

			// Inicializa variável que será uitilziada
			// para desabilitar sorter nas colunas de imagens, before e after
			$noSorterColumns = '';

			// Valores class e ID
			$classTable = (!is_null($class) && $class != '') ? 'class="table_sorter ' . $class .'" ' : 'class="table_sorter"';
			$idTable = (!is_null($id) && $id != '') ? 'id="' . $id . '" ' : '';

			// Calcula total de páginas e items por página (50 default), página atual e link da pagina atual
			$diffPages    = 4;
			$actualPage   = $this->actual_page;
			$itemsPerPage = $this->items_per_page;
			$totalPages   = ceil(($count_data / $itemsPerPage));
			$totalPages   = ($totalPages == 0) ? 1 : $totalPages;

			// Link da pagina
			// $page_link = explode('/', $_SERVER['REQUEST_URI'], -1);
			// $page_link = implode('/', $page_link);
			$page_link = $this->page_link;

			$html  = '<div>';
			$html .= '<div class="array_table_count">Total de Registros: <b>' . $count_data . '</b></div>';

			// Adiciona a linha de paginação
			// Adiciona elemento html responsável por paginação

			if($totalPages > 1)
			{
				$html .= '<div id="pagination_area" class="inline top" style="float:right;margin:-35px 0 25px 0;">';
				for($auxCounter = 1; $auxCounter <= $totalPages; $auxCounter++)
				{

					if($auxCounter == 1 || $auxCounter == $totalPages || $auxCounter == $actualPage || (($auxCounter >= ($actualPage - $diffPages) && $auxCounter < $actualPage) || ($auxCounter <= ($actualPage + $diffPages) && $auxCounter > $actualPage) ))
					{
						if($auxCounter == $totalPages && $totalPages > ($actualPage + $diffPages + 1))
						{
							$html .= '<div id="link_no_pagination">. . .</div>';
						}
						$actualPageStyle = ($actualPage == $auxCounter) ? 'class="actual_link"' : '';
						$html .= '<a id="link_paginator" href="javascript:void(0);" onclick="set_form_filter_action(\'' . $page_link . '/' . $auxCounter . '\');"><div id="link" ' . $actualPageStyle . '>' . $auxCounter . '</div></a>';
						if($auxCounter == 1 && $actualPage > ($diffPages + 2))
						{
							$html .= '<div id="link_no_pagination">. . .</div>';
						}
					}
				}
				$html .= '</div>';
			}
			$html .= '</div>';

			// Começa a montar a tabela
			$html .= "\n<table " . $idTable . $classTable . ">";
			$html .= "\n<thead>";
			$html .= "\n\t<tr>";

			// Monta cabeçalho das colunas adicionais
			$iCounterBefore = count($columnsBefore);
			if($iCounterBefore > 0)
			{
				for($auxCounter = 0; $auxCounter < $iCounterBefore; $auxCounter++)
				{
					$html .= "\n\t<th></th>";
					$noSorterColumns .=  $auxCounter . ": { sorter: false },";

					// Para desabilitar o ordenação das colunas anteriores e posteriores
					$counter_total_columns++;
				}
			}

			// Monta cabeçalho das colunas do sql
			$i = 0;
			foreach($objRSHead as $auxHeadIndex => $auxHeadValue)
			{
				// print_r($auxHeadValue);
				foreach($auxHeadValue as $head_index => $head_value)
				{
					if($i == 0)
					{
						if(($colNumber % 2) == 0)
						{
							// print_r($auxHeadValue);
							if($total_columns > 0)
							{
								// echo($head_index . '<br />');
								if(in_array(strtolower($head_index), $this->columns, true) || in_array($auxColNumbers, $this->columns))
								{
									$html .= "\n\t<th>" . $head_index . "</th>";

									// Diz quais colunas que de fato devem ser colocadas nas linhas
									$linhas[] = $auxColNumbers;

									// Conta colunas do meio
									$counter_total_columns++;
								}
							} else {
								$html .= "\n\t<th>" . $head_index . "</th>";

								// Conta colunas do meio
								$counter_total_columns++;
							}
							$auxColNumbers++;
						}
					}
				}
				$i++;
				$colNumber++;
			}

			// Para desabilitar o ordenação das colunas anteriores e posteriores
			$counter_total_columns += $colNumber;

			// Monta cabeçalho das colunas adicionais posterior
			$iCounterAfter = count($columnsAfter);
			if($iCounterAfter > 0)
			{
				// Tem de desabilitar a primeira coluna (ordernação)
				$noSorterColumns .=  ($counter_total_columns - 1) . ": { sorter: false },";
				for($auxCounter = 0; $auxCounter < $iCounterAfter; $auxCounter++)
				{
					$html .= "\n\t<th></th>";
					$noSorterColumns .=  ($counter_total_columns + $auxCounter) . ": { sorter: false },";
				}
			}

			$html .= "\n\t</tr>";
			$html .= "\n</thead>";

			// Começa a montar o corpo da tabela
			$html .= "\n<tbody>";

			// Variavel auxiliar para fazer linhas cor sim, não
			$i = 1;

			// Variável de contagem para LINHAS
			$count_linhas = count($linhas);

			// Define o range dos registros que serao exibidos nessa tela
			$range_data_fim = $this->actual_page * $this->items_per_page;
			$range_data_ini = $range_data_fim - ($this->items_per_page - 1);

			// Varre o SQL para montar linhas
			foreach($objResult as $objRS)
			{
				if($i >= $range_data_ini && $i <= $range_data_fim)
				{
					$setID = $objRS[$columnsID];

					$class = (($i % 2) == 0) ? ' class="odd"' : '';
					$html .= "\n\t<tr" . $class . " id='id_".$setID."'>";

					// Colunas adicionais primárias
					if($iCounterBefore > 0)
					{
						for($auxCounter = 0; $auxCounter < $iCounterBefore; $auxCounter++)
						{
							// O trecho abaixo faz a substituição das tags custom do core
							// pelo valor correspodente da coluna do sql da linha atual
							// Faz o match para ver se uma tag custom foi encontrada

							$html .= "\n\t<td id=\"column_before".'_'.$setID."\">" . replace_tag($columnsBefore[$auxCounter], $objRS) . "</td>";
						}
					}

					// Colunas do SQL
					$counter_column = 0;
					foreach($objRS as $index => $value)
					{
						if($total_columns > 0)
						{
							if(in_array($counter_column, $linhas) || $count_linhas == 0)
							{
								$html .= "\n\t<td id=\"column_middle\">" . $value . "</td>";
							}
						} else {
							$html .= "\n\t<td id=\"column_middle\">" . $value . "</td>";
						}
						$counter_column++;
					}
					
					// Colunas adicionais posteriores
					if($iCounterAfter > 0)
					{
						for($auxCounter = 0; $auxCounter < $iCounterAfter; $auxCounter++)
						{
							// O trecho abaixo faz a substituição das tags custom do core
							// pelo valor correspodente da coluna do sql da linha atual
							$html .= "\n\t<td id=\"column_after\">" . replace_tag($columnsAfter[$auxCounter], $objRS) . "</td>";
						}
					}
					
					// Fecha a linha
					$html .= "\n\t</tr>";
				}
				$i++;
			}
			
			// Fecha table
			$html .= "\n</tbody>";
			$html .= "\n</table>";
			
			// Adiciona elemento html responsável por paginação
			if($totalPages > 1)
			{
				$html .= '<div id="pagination_area">';
				for($auxCounter = 1; $auxCounter <= $totalPages; $auxCounter++)
				{
					
					if($auxCounter == 1 || $auxCounter == $totalPages || $auxCounter == $actualPage || (($auxCounter >= ($actualPage - $diffPages) && $auxCounter < $actualPage) || ($auxCounter <= ($actualPage + $diffPages) && $auxCounter > $actualPage) ))
					{
						if($auxCounter == $totalPages && $totalPages > ($actualPage + $diffPages + 1))
						{
							$html .= '<div id="link_no_pagination">. . .</div>';
						}
						$actualPageStyle = ($actualPage == $auxCounter) ? 'class="actual_link"' : '';
						$html .= '<a id="link_paginator" href="javascript:void(0);" onclick="set_form_filter_action(\'' . $page_link . '/' . $auxCounter . '\');"><div id="link" ' . $actualPageStyle . '>' . $auxCounter . '</div></a>';
						if($auxCounter == 1 && $actualPage > ($diffPages + 2))
						{
							$html .= '<div id="link_no_pagination">. . .</div>';
						}
					}
				}
				$html .= '</div>';
			}
			
			// Processa os noSorters para colunas iniciais e finais
			$noSorterColumns = trim($noSorterColumns, ',');
			$noSorter = ($noSorterColumns != '') ? '{ headers: { ' . $noSorterColumns . ' } }' : '';
			$html .= "\n" . '<script type="text/javascript" language="javascript">$(document).ready(function() { $("#' . $id . '").tablesorter(' . $noSorter . '); } );</script>';
			
			$this->html = $html;
		} else {
			$this->html = get_mensagem('info', 'Consulta vazia', 'Esta consulta não retornou nenhum registro.', false, 'margin-top:15px;');
		}
	}
	
    /**
	* get_html()
	* Este método processa a tabela e retorna o 
	* HTML processado.
	* @return string HtmlTable
	*/
	public function get_html()
	{
		$this->proccess();
		return $this->html;
	}
    
    /**
	* render()
	* Processa a tabela e renderiza o html gerado.
	* @return void
	*/
	public function render()
	{
		$this->process();
		echo $this->html;
	}
}
