<?php
/**
 * author: Fernando Alves
 * Date: 28/08/14
 * Time: 17:04
 */
class Authorization{

    public static $auth;

    static function requireSession(){
        // Instance from Codeigniter
        $CI =& get_instance();
        if($CI->session->userdata('logado') == false){
            self::UNLOGGED();
        }
    }

    static function ajaxCuspe(){
        echo "Você foi cuspido para fora do servidor! Não tente fazer isso novamente!";
    }

    static function getSession(){
        // Instance from Codeigniter
        $CI =& get_instance();
        $sessionData = array(
            'logado'        => $CI->session->userdata('logado'),
            'nivel'         => $CI->session->userdata('nivel'),
            'token'         => $CI->session->userdata('token'),
            'userid'        => $CI->session->userdata('idusuario'),
            'usuariovinculo'=> $CI->session->userdata('usuariovinculo'),
            'date'          => $CI->session->userdata('data_registro'),
            'email'         => $CI->session->userdata('email'),
            'email2'        => $CI->session->userdata('email2'),
            'name'          => $CI->session->userdata('nome'),
            'surname'       => $CI->session->userdata('sobrenome'),
            'type'          => $CI->session->userdata('tipo'),
            'birth'         => $CI->session->userdata('aniversario'),
            'sex'           => $CI->session->userdata('sexo'),
            'day'           => $CI->session->userdata('dia'),
            'month'         => $CI->session->userdata('mes'),
            'year'          => $CI->session->userdata('mes'),
            'cpf'           => $CI->session->userdata('rg'),
            'rg'            => $CI->session->userdata('estado'),
            'state'         => $CI->session->userdata('estado'),
            'city'          => $CI->session->userdata('cidade'),
            'district'      => $CI->session->userdata('bairro'),
            'phone'         => $CI->session->userdata('telefone'),
            'cover'         => $CI->session->userdata('foto_capa'),
            'profile'       => $CI->session->userdata('foto_perfil'),
            'account'       => $CI->session->userdata('plano'),
            'facebook'      => $CI->session->userdata('facebook'),
            'twitter'       => $CI->session->userdata('twitter'),
            'youtube'       => $CI->session->userdata('youtube'),
            'site'          => $CI->session->userdata('site'),
            'about'         => $CI->session->userdata('sobre'),
            'cargo'         => $CI->session->userdata('cargo')
        );
        return $sessionData;
    }

    static function getAccess($module, $userLevel){

        // Mapping to users access
        $map = array(
            0 => 'UNAUTHORIZED',
            1 => 'VIEW',
            3 => 'CRU',
            5 => 'ALL'
        );

        // Instance from Codeigniter
        $CI =& get_instance();
        // Load this library model
        $CI->load->model('authorization_model');

        $permit = $CI->authorization_model->authorization_map($userLevel);

        $meta = explode(' ', $permit['metadata']);

        $metadata = array();
        foreach($meta as $data){
            $temporaryData = explode(':', $data);
            $metadata[$temporaryData[0]]= $temporaryData[1];
        }

        $callback = "";
        foreach($metadata as $moduleDB => $level){
            if($module == $moduleDB){
               $callback = $map[$level];
                break;
            }
        }
        //debug($metadata);
        self::$auth = $callback;
        return $callback;
    }

    static function validatePageAccess(){
        if(self::$auth == 'UNAUTHORIZED'){
            self::UNAUTHORIZED();
        }
    }

    static function permit($action){
        $callback = 0;
        switch ($action) {
            case 'create':
                if(self::$auth == 'ALL' || self::$auth == 'CRU'){
                    $callback = 1;
                }
                break;
            case 'view':
                if(self::$auth != 'UNAUTHORIZED'){
                    $callback = 1;
                }
                break;
            case 'update':
                if(self::$auth == 'ALL' || self::$auth == 'CRU'){
                    $callback = 1;
                }
                break;
            case 'delete':
                if(self::$auth == 'ALL'){
                    $callback = 1;
                }
                break;
            default:
                $callback = 0;
                break;
        }

        return $callback;
    }


    static function validateMenu($module, $userLevel){
        
        $callback = true;

        $callback = self::getAccess($module, $userLevel);

        if($callback == 'UNAUTHORIZED'){
            $callback = false;
        }
        return $callback;
    }

    static function validateListModule($param = array(), $userLevel){
        // Instance from Codeigniter
        $CI =& get_instance();
        // Load this library model
        $CI->load->model('authorization_model');
        // call method from model
        $permit = $CI->authorization_model->authorization_map($userLevel);

        $meta = explode(' ', $permit['metadata']);

        $metadata = array();
        foreach($meta as $data){
            $temporaryData = explode(':', $data);
            $metadata[$temporaryData[0]]= $temporaryData[1];
        }

        $callback = '';
        $count = count($param);
        $x = 0;
        while($x < $count){
            foreach($metadata as $moduleDB => $level){
                if($param[$x] == $moduleDB){
                   if($level > 0){
                        return true;
                   }
                }
            }
            $x++;
        }
        return false;
    }



       static function UNAUTHORIZED(){

        $html = '<!DOCTYPE html>';
        $html .= '<html>';
        $html .= '<head>';
        $html .= '<title>'. TITLE .' - Erro!</title>';
        $html .= '<meta charset="utf-8">';
        $html .= '<link rel="stylesheet" href="'. CSS_PATH . 'theme3.css" type="text/css"/>';
        $html .= '<link rel="stylesheet" href="'. CSS_PATH . 'application.css" type="text/css"/>';
        $html .= '<link rel="stylesheet" href="'. LIBRARY . 'foundation/css/foundation.css" type="text/css"/>';
        $html .= '<script src="'. JS_PATH .'global.js"></script>';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<div id="wide">';
        $html .= '<div class="row">';
        $html .= '<div class="small-12 small-centered columns" style="margin-top:100px">';
        $html .= '<div class="panel">';
        $html .= '<h2>Permissão negada!</h2>';
        $html .= '<p><h4>Você não possui permissão de acesso a este módulo.</h4></p>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</body>';
        $html .= '</html>';
        echo $html;
        die();
    }

    static function UNLOGGED(){
        redirect();
    }
}