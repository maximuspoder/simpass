<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Professores_model extends CI_Model {

	/*
	* package: model.dashboard_model
	* date: 24-08-2013
	* by: Fernando
	*/

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('system_model');
	}

	function get_questoes_listagem($filters){

		$sql = "SELECT 	q.qcid as `ID(#)`,
						q.orgao as `Órgão`,
						q.banca as `Banca`,
						q.disciplina as Disciplina,
						q.questao as `Questão`,
						q.prova as Prova,
						q.questaoid as questaoid
						FROM sep_questoes q
						WHERE q.editado is not null
						AND  NOT EXISTS(
							SELECT  qcid 
									FROM sep_professores_prova p
									WHERE p.qcid = q.qcid
							)
						AND q.resposta_correta NOT LIKE ''";

		$sql .= $this->system_model->get_where_filters($filters);
        // Concatena LIMIT
        //$sql .= ' LIMIT 100';

        $dados = $this->db->query($sql);
        $dados = $dados->result_array();
        return (isset($dados)) ? $dados : array();
	}

	function get_questao_by_id($qcid){
		$sql = "SELECT 	*
						FROM sep_questoes q
						WHERE q.qcid = $qcid";

        $dados = $this->db->query($sql);
        $dados = $dados->result_array();
        return (isset($dados)) ? $dados : array();
	}



	function get_question_teacher_selected($usuarioid, $filters)
	{
		$sql = "SELECT 	q.qcid as `ID(#)`,
						q.orgao as `Órgão`,
						q.banca as `Banca`,
						q.disciplina as Disciplina,
						q.questao as `Questão`,
						q.prova as Prova,
						q.questaoid as questaoid
						FROM sep_professores_prova p
						INNER JOIN sep_questoes q ON (q.qcid = p.qcid)
						WHERE p.usuarioid = $usuarioid
						AND q.resposta_correta <> ' '
						AND p.gravada is null
						";

		$sql .= $this->system_model->get_where_filters($filters);
        // Concatena LIMIT
        $sql .= ' GROUP BY p.qcid';

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();	
	}

}