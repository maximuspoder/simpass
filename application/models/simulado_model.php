<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Simulado_model extends CI_Model {

	/*
	* package: model.dashboard_model
	* date: 24-08-2013
	* by: Fernando
	*/

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/*
    * get_distinct_banca
    * eget distinct banca
    * @param 
    * @return void
    */
	function get_distinct_banca()
	{
		$sql = "SELECT distinct banca
						FROM sep_questoes
						ORDER BY banca ASC";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_distinct_anos
    * eget distinct anos
    * @param 
    * @return void
    */
	function get_distinct_anos()
	{
		$sql = "SELECT distinct ano
						FROM sep_questoes
						ORDER BY ano DESC";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_group_provas
    * eget distinct provas
    * @param 
    * @return void
    */
	function get_group_provas()
	{
		$sql = "SELECT 	banca,
						prova,
						escolaridade,
						ano,
						disciplina, 
						Concat(orgao, ' - ', cargo, ' - ', ano) as prova_nome
					FROM sep_questoes

					GROUP BY prova
					ORDER BY prova_nome";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_group_cargos
    * Get cargos
    * @param 
    * @return void
    */
	function get_group_cargos()
	{
		$sql = "SELECT 	cargo
					FROM sep_questoes
					GROUP BY cargo
					ORDER BY cargo ASC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_group_orgaos
    * Get orgaos
    * @param 
    * @return void
    */
	function get_group_orgaos()
	{
		$sql = "SELECT 	orgao
					FROM sep_questoes
					GROUP BY orgao
					ORDER BY orgao ASC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_group_disciplinas
    * Get disciplinas by group
    * @param 
    * @return void
    */
	function get_group_disciplinas()
	{
		$sql = "SELECT disciplina
					FROM sep_questoes
					GROUP by disciplina
					ORDER BY disciplina ASC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_group_assuntos()
	{
		$sql = "SELECT assunto1
					FROM sep_questoes
					WHERE assunto1 NOT LIKE ''
					GROUP by assunto1
					ORDER BY assunto1 ASC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}
	
	/*
    * get_orgaos_by_banca
    * Get orgaos by banca
    * @param $banca
    * @return void
    */
	function get_orgaos_by_banca($banca)
	{
		$sql = "SELECT 	orgao
					FROM sep_questoes
					WHERE banca LIKE '%$banca%'
					GROUP BY orgao
					ORDER BY orgao ASC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_cargos_by_orgao
    * Get cargos by orgao
    * @param $banca, $orgao
    * @return void
    */
	function get_cargos_by_orgao($banca, $orgao)
	{
		$sql = "SELECT 	cargo
					FROM sep_questoes
					WHERE banca LIKE '%$banca%'
					AND orgao LIKE '%$orgao%'
					GROUP by cargo
					";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_cargos_by_orgao
    * Get cargos by orgao
    * @param $banca, $orgao
    * @return void
    */
	function get_prova_by_name($name)
	{

		$sql = "SELECT 	*
					FROM sep_questoes
					WHERE prova LIKE '$name'";
		
		//print_r($sql);
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();		
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_last_simulado($token)
	{
		$sql = "SELECT 	provaid
					FROM sep_simulado
					WHERE token LIKE '$token'
					ORDER BY provaid DESC
					LIMIT 1";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function create_simulado($sql)
	{
		// Process query
		$this->db->query($sql);

		//$this->db->close();
		$dados = $this->db->affected_rows();	
		// Return result array
		return $dados;
	}

	function get_user_simulado($token, $provaid)
	{
		$sql = "SELECT 	*,
						Q.qcid
					FROM sep_simulado S
					INNER JOIN sep_questoes Q ON (Q.qcid = S.qcid)
					LEFT JOIN sep_professores_prova P ON (P.qcid = Q.qcid)
					WHERE S.token LIKE '$token'
					AND S.provaid = $provaid
					AND S.resposta IS NULL
					ORDER BY Q.disciplina";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_user_simulado_respostas($token, $provaid)
	{
		$sql = "SELECT 	Q.qcid,
						S.resposta_correta
					FROM sep_simulado S
					INNER JOIN sep_questoes Q ON (Q.qcid = S.qcid)
					WHERE S.token LIKE '$token'
					AND S.provaid = $provaid
					AND S.resposta IS NULL
					ORDER BY Q.disciplina";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_user_simulado_limit($token, $provaid, $limit, $questaoID)
	{
		$questaoID = ($questaoID == null) ? '' : ' AND S.qcid > '.$questaoID;
	
		$sql = "SELECT 	DISTINCT Q.qcid,
						Q.questao,
						Q.resposta1,
						Q.resposta2,
						Q.resposta3,
						Q.resposta4,
						Q.resposta5,
						Q.resposta_correta,
						Q.prova,
						Q.orgao,
						Q.cargo,
						Q.escolaridade,
						Q.disciplina,
						Q.ano,
						Q.banca,
						P.professor,
						P.vimeocode,
						S.simuladoid				
					FROM sep_simulado S
					INNER JOIN sep_questoes Q ON (Q.qcid = S.qcid)
					LEFT JOIN sep_professores_prova P ON (P.qcid = Q.qcid)
					WHERE S.token LIKE '$token'
					AND S.provaid = $provaid
					AND S.resposta IS NULL
					 $questaoID 
					GROUP BY Q.qcid
					ORDER BY S.qcid
					LIMIT $limit";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}


	function update_simulado($sql)
	{
		// Process query
		$this->db->query($sql);

		//$this->db->close();
		$dados = $this->db->affected_rows();	
		// Return result array
		return $dados;
	}

	function get_simulado_numeros($token, $provaid)
	{
		$sql = "SELECT 	count(if(resposta LIKE resposta_correta, 1, NULL)) as acertos,
						count(if(resposta NOT LIKE resposta_correta AND resposta NOT LIKE '', 1, NULL)) as erros,
						count(if(resposta LIKE '', 1, NULL)) as branco,
						count(1) as total
						FROM sep_simulado 
						WHERE provaid = $provaid
						AND token LIKE '$token'";

		//print_r($sql); die();
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	
	function get_user_history($token)
	{
		$sql = "SELECT 	count(if(resposta LIKE resposta_correta, 1, NULL)) as acertos,
						count(if(resposta NOT LIKE resposta_correta, 1, NULL)) as erros,
						count(if(resposta LIKE '', 1, NULL)) as branco,
						count(1) as total,
						prova,
						provaid,
						CONCAT(DATE_FORMAT(data_registro, '%d/%m/%y'), ' - ', DATE_FORMAT(data_registro, ' %T'))	as data
						FROM sep_simulado
						WHERE token LIKE '$token'
						GROUP BY provaid";
		//print_r($sql); die();
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function ajax_simulado_filter($whereParam, $AND)
	{		

		$sql = "SELECT 	count(*) as questoes,
						count(DISTINCT banca) as banca,
						count(DISTINCT orgao) as orgao,
						count(DISTINCT cargo) as cargo,
						count(DISTINCT disciplina) as disciplina,
						count(DISTINCT assunto1) as assunto,
						count(DISTINCT escolaridade) as escolaridade,
						count(DISTINCT ano) ano
					FROM sep_questoes
					$whereParam 
					$AND
					AND editado is not null";
		//print_r($sql);
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function ajax_simulado_filter_vimeo($whereParam, $AND)
	{		

		$sql = "SELECT 	count(*) as questoes,
						count(DISTINCT q.banca) as banca,
						count(DISTINCT q.orgao) as orgao,
						count(DISTINCT q.cargo) as cargo,
						count(DISTINCT q.disciplina) as disciplina,
						count(DISTINCT q.assunto1) as assunto,
						count(DISTINCT q.escolaridade) as escolaridade,
						count(DISTINCT q.ano) ano
					FROM sep_questoes q
					INNER JOIN sep_professores_prova p on p.qcid = q.qcid
					$whereParam 
					$AND
					AND p.vimeocode IS NOT NULL
					AND q.editado is not null";
		//print_r($sql); die();
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function ajax_simulado_filter_set_simulado($whereParam, $AND, $limit)
	{		

		$sql = "SELECT 	*
					FROM sep_questoes
					$whereParam 
					$AND
					AND editado is not null
					GROUP BY qcid
					ORDER BY RAND()
					LIMIT $limit";
		//print_r($sql);
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function ajax_simulado_filter_set_simulado_video($whereParam, $AND, $limit)
	{		

		$sql = "SELECT 	*
					FROM sep_questoes q
					INNER JOIN sep_professores_prova p on p.qcid = q.qcid
					$whereParam 
					$AND
					AND p.vimeocode is not null
					AND q.editado is not null
					GROUP BY q.qcid
					LIMIT $limit";
		//print_r($sql);
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_questoes_vimeo_disciplinas()
	{
		$sql = "SELECT DISTINCT q.disciplina
					FROM sep_questoes q
					INNER JOIN sep_professores_prova p on (p.qcid = q.qcid)
					WHERE p.vimeocode is not null";
					
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_questoes_vimeo($disciplina)
	{
		$sql = "SELECT DISTINCT q.qcid,
								q.banca,
								q.orgao,
								q.disciplina,
								q.questao,
								q.resposta1,
								q.resposta2,
								q.resposta3,
								q.resposta4,
								q.resposta5,
								q.resposta_correta,
								p.vimeocode
					FROM sep_questoes q
					INNER JOIN sep_professores_prova p on (p.qcid = q.qcid)
					WHERE p.vimeocode is not null
					AND q.disciplina LIKE '$disciplina'
					AND q.editado is not null
					ORDER BY rand()
					LIMIT 15";
					
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_last_simulado_montado($token)
	{
		$sql = "SELECT 	idprova
					FROM sep_simulado_montado
					WHERE token LIKE '$token'
					AND finalizado LIKE 'S'
					ORDER BY idprova DESC
					LIMIT 1";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function criar_simulado($disciplina, $limit)
	{
		$sql = "SELECT DISTINCT q.qcid 
					FROM sep_questoes q
					INNER JOIN sep_professores_prova p on (p.qcid = q.qcid)
					WHERE p.vimeocode is not null
					AND q.disciplina LIKE '$disciplina'
					limit $limit";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_last_simulado_montado_mount($token, $provaid)
	{
		$sql = "SELECT 	*
					FROM sep_simulado_montado m
					INNER JOIN sep_questoes q on q.qcid = m.qcid
					LEFT JOIN sep_professores_prova p on p.qcid = q.qcid
					WHERE token LIKE '$token'
					AND finalizado LIKE 'S'
					AND token LIKE '$token'
					AND idprova = $provaid
					GROUP BY q.qcid";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}



	/******************************
	 SECTION TO FILTER WITH VIDEOS*
	 ******************************/
	 /*
    * get_group_cargos_vimeo
    * Get cargos
    * @param 
    * @return void
    */
	function get_group_cargos_vimeo()
	{
		$sql = "SELECT 	cargo, vimeocode
						FROM sep_questoes q
						INNER JOIN sep_professores_prova p on p.qcid = q.qcid
						WHERE p.vimeocode is not null
						GROUP BY cargo
						ORDER BY cargo ASC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_group_orgaos
    * Get orgaos
    * @param 
    * @return void
    */
	function get_group_orgaos_vimeo()
	{
		$sql = "SELECT 	orgao
					FROM sep_questoes q
					INNER JOIN sep_professores_prova p on p.qcid = q.qcid
					WHERE p.vimeocode is not null
					GROUP BY orgao
					ORDER BY orgao ASC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}
	/*
    * get_distinct_anos
    * eget distinct anos
    * @param 
    * @return void
    */
	function get_distinct_anos_vimeo()
	{
		$sql = "SELECT distinct ano
						FROM sep_questoes q
						INNER JOIN sep_professores_prova p on p.qcid = q.qcid
						WHERE p.vimeocode is not null
						ORDER BY ano DESC";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}
	/*
    * get_group_disciplinas
    * Get disciplinas by group
    * @param 
    * @return void
    */
	function get_group_disciplinas_vimeo()
	{
		$sql = "SELECT q.disciplina
					FROM sep_questoes q
					INNER JOIN sep_professores_prova p on p.qcid = q.qcid
					WHERE p.vimeocode is not null
					GROUP by disciplina
					ORDER BY disciplina ASC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_group_assuntos_vimeo
    * Get disciplinas by group
    * @param 
    * @return void
    */
	function get_group_assuntos_vimeo()
	{
		$sql = "SELECT assunto1
					FROM sep_questoes q
					INNER JOIN sep_professores_prova p on p.qcid = q.qcid
					WHERE p.vimeocode is not null
					AND assunto1 NOT LIKE ''
					GROUP by assunto1
					ORDER BY assunto1 ASC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_group_bancas_vimeo()
	{
		$sql = "SELECT 	banca
					FROM sep_questoes q
					INNER JOIN sep_professores_prova p on p.qcid = q.qcid
					WHERE p.vimeocode is not null
					GROUP BY banca
					ORDER BY banca ASC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();			
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function mount_simulado_montado($token, $provaid)
	{
		$sql = "SELECT * FROM sep_simulado_montado m
				inner join sep_questoes q on q.qcid = m.qcid
				left join sep_professores_prova p on p.qcid = q.qcid
				where token like '$token' and 
				idprova like '$provaid'";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();			
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function meus_simulados_criados($token)
	{
		$sql = "SELECT * FROM sep_simulado_montado
				where token like '$token'
				GROUP BY idprova";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();			
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_desempenho($token){
		$sql = "SELECT 	disciplina,
						s.resposta,
						q.resposta_correta,
						count(
						CASE 
							WHEN s.resposta = q.resposta_correta then 1
						END) as ponto
						FROM sep_simulado s
						INNER JOIN sep_questoes q on q.qcid = s.qcid
						WHERE token like '$token'
						GROUP BY q.disciplina";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();			
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}



} # end class

