<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model {

	/*
	* package: model.dashboard_model
	* date: 24-08-2013
	* by: Fernando
	*/

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/*
    * get_questions_count
    * Catch all questions/count
    * @param 
    * @return void
    */
	function get_questions_count()
	{
		$sql = "SELECT 	count(DISTINCT qcid) as questions_count
						FROM sep_questoes";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
		
	}

	function get_questions_fix()
	{
		$sql = "";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return result array
		return $dados->result_array();	
	}
	function get_questions_recorded()
	{
		$sql = "SELECT 	count(*) as questions_recorded
						FROM sep_professores_prova 
						WHERE gravada is not null";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return result array
		return $dados->result_array();	
	}

	function get_questions_by_discipline()
	{
		$sql = "SELECT  count(DISTINCT qcid) as questions_count,
						disciplina as discipline
					FROM sep_questoes
					GROUP BY disciplina";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return result array
		return $dados->result_array();
	}
	function get_all_teachers()
	{
		$sql = "";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return result array
		return $dados->result_array();	
	}

	function get_questions_selecteds()
	{
		$sql = "SELECT  count(p1.qcid) as questions_count,
						p1.professor as teacher,
						p1.usuarioid as userid,
						(
							SELECT COUNT(p2.QCID)
								FROM sep_professores_prova p2
								WHERE gravada is not null 
								and p2.usuarioid = p1.usuarioid
								) as status
					FROM sep_professores_prova p1
					GROUP BY usuarioid";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return result array
		return $dados->result_array();	
	}

	function api_following($token)
	{
		$sql = "SELECT 	u.usuarioid,
						u.token,
						u.usuariotipoid as nivel,
						u.nome,
						u.sobrenome,
						d.foto_capa,
						d.foto_perfil,
						s.token_de,
						s.token_para
					FROM sis_usuario u
					INNER JOIN sis_usuario_dados d ON (d.token = u.token)
					LEFT JOIN sep_seguidores s on (s.token_para = u.token)
					WHERE s.token_de LIKE '$token'";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return result array
		return $dados->result_array();
	}


	function generate_users_data()
	{
		$sql = "SELECT 	u.token,
						d.foto_perfil
					FROM sis_usuario u
					INNER JOIN sis_usuario_dados d ON (d.token = u.token)";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return result array
		return $dados->result_array();
	}

	function get_notifications($token)
	{
		$sql = "SELECT 	s.token_de, 
						s.token_para, 
						l.evento, 
						l.data_registro,
						d.foto_perfil,
						Concat(u.nome, ' ', u.sobrenome) as nome
					FROM sep_logs l
					INNER JOIN sis_usuario u on u.token = l.token
					LEFT JOIN sis_usuario_dados d on d.token = u.token
					LEFT JOIN sep_seguidores s on s.token_para = u.token
					WHERE s.token_de like '$token'
					ORDER BY l.logid DESC
					LIMIT 5";
		//print_r($sql); die();
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return result array
		return $dados->result_array();
	}

	function get_question_info($qcid)
	{
		$sql = "SELECT 	qcid as `ID(#)`,
						prova,
						banca,
						orgao,
						disciplina,
						assunto1,
						questao
						FROM sep_questoes
						WHERE qcid = $qcid";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function count_user(){
		$sql = "SELECT 	count(1) as `count_users`
						FROM sis_usuario";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function followers_data($token){
		$sql = "SELECT 	U.nome,
						U.sobrenome,
						U.email,
						D.foto_perfil,
						D.foto_capa,
						U.data_registro,
						D.plano
						FROM sep_seguidores S
						INNER JOIN sis_usuario U on (U.token = S.token_de)
						LEFT JOIN sis_usuario_dados D on (D.token = U.token)
						WHERE token_para LIKE '$token'";
		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	function count_questions_recorded()
	{
		$sql = "SELECT count(1) as count_questions_recorded from sep_professores_prova p 
				inner join sep_questoes q on q.qcid = p.qcid
				where gravada is not null";

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	function user_logs(){
		$sql = "SELECT 	concat(u.nome, ' ',u.sobrenome) as full_name,
						l.evento as event, 
						l.data_registro as log_date, 
						u.data_registro as register_date,
						d.foto_perfil profile_picture
				FROM SEP.sep_logs l
				LEFT JOIN sis_usuario u on u.token = l.token
				LEFT JOIN sis_usuario_dados d on d.token = u.token
				ORDER BY logid DESC
				limit 100";

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	function user_last_log(){
		$sql = "SELECT 	u.token,
						u.nome, 
						u.sobrenome, 
						u.data_registro as cadastro,
						(
							SELECT 	l.data_registro as event
									FROM sep_logs l
									WHERE l.token = u.token
									ORDER BY l.logid DESC
									LIMIT 1
						) as data_evento,
						(
							SELECT 	evento as event
									FROM sep_logs l
									WHERE l.token = u.token
									ORDER BY l.logid DESC
									LIMIT 1
						) as evento						
					FROM sis_usuario u
					WHERE day(u.data_registro) >= 14
					AND   month(u.data_registro) >= 7
				    AND   u.usuariotipoid = 4";

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

} # end class

