<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Professor_model extends CI_Model {

	/*
	* package: model.dashboard_model
	* date: 24-08-2013
	* by: Fernando
	*/

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/*
    * get_question
    * Get corrects question
    * @param 
    * @return void
    */
	function get_question()
	{
		$sql = "SELECT 	*
						FROM sep_questoes q
						WHERE q.editado is not null
						AND  NOT EXISTS(
							SELECT  qcid 
									FROM sep_professores_prova p
									WHERE p.qcid = q.qcid
							)
						AND q.resposta_correta NOT LIKE ''
						LIMIT 100";

		// Processa query
		$dados = $this->db->query( $sql );
		// Retorna array com os valores
		return $dados->result_array();	
	}

	/*
    * get_discipline
    * Get discipline by name
    * @param $discipline
    * @return void
    */
	function get_discipline($discipline)
	{	
		$sql = "SELECT *
						FROM sep_questoes
						WHERE disciplina LIKE '%$discipline%'";
		// Processa query
		$dados = $this->db->query( $sql );
		// Retorna array com os valores
		return $dados->result_array();
	}

	/*
    * get_unique_discipline
    * Get UNIQUE discipline
    * @param 
    * @return void
    */
	function get_unique_discipline()
	{
		$sql = "SELECT disciplina
						FROM sep_questoes
						GROUP BY disciplina";
		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	
	function get_question_by_qcid($qcid)
	{
		$sql = "SELECT *
						FROM sep_questoes
						WHERE qcid = $qcid";
		// Processa query
		$dados = $this->db->query( $sql );
		// Retorna array com os valores
		return $dados->result_array();
	}


	function ajax_get_questionsByDiscipline($discipline)
	{
		$sql = "SELECT 	*
						FROM sep_questoes q
						WHERE q.editado is not null
						AND disciplina LIKE '$discipline'
						AND  NOT EXISTS(
							SELECT  qcid 
									FROM sep_professores_prova p
									WHERE p.qcid = q.qcid
							)
						LIMIT 100";

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	function get_question_teacher($qcid)
	{
		$sql = "SELECT 	*
						FROM sep_professores_prova
						WHERE qcid = $qcid";

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();	
	}

	function get_count_question($usuarioid)
	{
		$sql = "SELECT 	count(*) as total
						FROM sep_professores_prova
						WHERE usuarioid = $usuarioid
						AND gravada is null";
		//print_r($sql);
		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();	
	}


	function get_question_teacher_selected($usuarioid)
	{
		$sql = "SELECT 	*
						FROM sep_professores_prova p
						INNER JOIN sep_questoes q ON (q.qcid = p.qcid)
						WHERE p.usuarioid = $usuarioid
						AND q.resposta_correta <> ' '
						AND p.gravada is null
						GROUP BY p.qcid";	

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();	
	}

	function get_question_teacher_selected_by_id($qcid)
	{
		$sql = "SELECT 	*
						FROM sep_professores_prova p
						INNER JOIN sep_questoes q ON (q.qcid = p.qcid)
						AND q.resposta_correta <> ' '
						AND q.qcid = $qcid
						GROUP BY p.qcid";

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();	
	}

	function get_subject_by_discipline($discipline)
	{
		$sql = "SELECT  	disciplina,
						CASE 
							WHEN assunto1 LIKE '' THEN disciplina
							WHEN assunto1 <> '' THEN assunto1
						END as assunto
						FROM  sep_questoes
						WHERE disciplina like 'DIREITO ELEITORAL'
						GROUP BY assunto1";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return array with result
		return $dados->result_array();	

	}


} # end class

