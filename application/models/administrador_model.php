<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrador_model extends CI_Model {

	/*
	* package: model.dashboard_model
	* date: 24-08-2013
	* by: Fernando
	*/

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/*
    * get_questions_count
    * Catch all questions/count
    * @param 
    * @return void
    */
	function get_questions_count()
	{
		$sql = "SELECT 	count(*) as questions_count
						FROM sep_questoes";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
		
	}

	function get_questions_fix()
	{
		$sql = "";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return result array
		return $dados->result_array();	
	}
	function get_questions_recorded()
	{
		$sql = "SELECT count(*) FROM `sep_professores_prova` WHERE gravada is not null";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return result array
		return $dados->result_array();	
	}
	function get_all_teachers()
	{
		$sql = "";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return result array
		return $dados->result_array();	
	}

} # end class

