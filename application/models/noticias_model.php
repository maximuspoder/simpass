<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias_model extends CI_Model {

	/*
	* package: model.noticias_model
	* date: 13-07-2014
	* by: Fernando
	*/

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_last_news($limit)
	{
		$sql = "SELECT 	*,
						b.data_registro as post_date
						FROM sep_blog b
						INNER JOIN sis_usuario u on u.token = b.token
						LEFT JOIN sis_usuario_dados d on d.token = u.token
						WHERE u.token LIKE 'f57442dfb075acbfc0679fd5256'
						AND b.categoria LIKE 'edital'
						OR b.categoria LIKE 'concursos'
						ORDER BY blogid desc
						LIMIT $limit";
		//print_r($sql); die();
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_last_blog()
	{
		$sql = "SELECT 	*,
						b.data_registro as post_date
						FROM sep_blog b
						INNER JOIN sis_usuario u on u.token = b.token
						LEFT JOIN sis_usuario_dados d on d.token = u.token
						WHERE categoria LIKE 'blog'
						ORDER BY blogid desc";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function get_news_by_id($id)
	{
		$sql = "SELECT 	*,
						b.data_registro as post_date
						FROM sep_blog b
						INNER JOIN sis_usuario u on u.token = b.token
						LEFT JOIN sis_usuario_dados d on d.token = u.token
						WHERE blogid = $id";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function postadas_por_mim($token)
	{
		$sql = "SELECT 	*
						FROM sep_blog b
						WHERE b.token LIKE '$token'";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

} # end class

