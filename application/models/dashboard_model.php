<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	/*
	* package: model.dashboard_model
	* date: 24-08-2013
	* by: Fernando
	*/

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/*
    * get_questoes_imagem_dentro
    * Pega questoes com imagem dentro
    * @param 
    * @return void
    */
	function get_questoes_imagem_dentro()
	{
		$sql = "SELECT *
						FROM sep.sep_questoes 
						WHERE questao LIKE '%src%'
						ORDER BY questaoid asc
						";

		// Processa query
		$dados = $this->db->query( $sql );
		// Retorna array com os valores
		return $dados->result_array();	
	}

	/*
    * controle_usuarios_ativos
    * Lista usuarios ativos
    * @param 
    * @return array string
    */
	function controle_usuarios_ativos()
	{
		$sql = "SELECT 	*
						FROM sis_usuario u
						INNER JOIN sis_usuario_login l ON (l.id = u.idlogin)
						LEFT JOIN sis_usuario_nivel n ON (n.nivel = u.nivelusuario)
						WHERE l.ativo LIKE 'sim'";

		// Processa query
		$dados = $this->db->query( $sql );
		// Retorna array com os valores
		return $dados->result_array();		
	}

	/*
    * controle_usuarios_inativos
    * Lista usuarios inativos
    * @param 
    * @return array string
    */
	function controle_usuarios_inativos()
	{
		$sql = "SELECT 	*
						FROM sis_usuario u
						INNER JOIN sis_usuario_login l ON (l.id = u.idlogin)
						LEFT JOIN sis_usuario_nivel n ON (n.nivel = u.nivelusuario)
						WHERE l.ativo LIKE 'nao'";

		// Processa query
		$dados = $this->db->query( $sql );
		// Retorna array com os valores
		return $dados->result_array();		
	}


	function get_cartoes_user_by_id($idusuario)
	{
		$sql = "SELECT 	* 
						FROM sis_cartoes
						WHERE idlogin = $idusuario";

		// Processa query
		$dados = $this->db->query( $sql );
		// Retorna array com os valores
		return $dados->result_array();	
	}

} # end class

