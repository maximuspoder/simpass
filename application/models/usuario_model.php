<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario_model extends CI_Model {

	/*
	* package: model.dashboard_model
	* date: 24-08-2013
	* by: Fernando
	*/
	
	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/*
    * get_questoes_imagem_dentro
    * Pega questoes com imagem dentro
    * @param 
    * @return void
    */
	function get_user_dados($userid)
	{
		$sql = "SELECT *
						FROM sis_usuario u
						INNER JOIN sis_usuariotipo t ON (t.usuariotipoid = u.usuariotipoid)
					WHERE u.usuarioid = $userid";

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();	
	}

	function get_json_data($token)
	{
		$sql = "SELECT	u.usuarioid,
						u.usuariovinculo,
						u.token,
						d.token,
						u.data_registro,
						u.email,
						d.email2,
						t.nivel,
						t.tipo,
						u.nome,
						u.sobrenome,
						u.sexo,
						u.dia,
						u.mes,
						u.ano,
						Concat(u.dia, '/', u.mes, '/', u.ano) as aniversario,
						d.cpf,
						d.rg,
						d.estado,
						d.cidade,
						d.bairro,
						d.numero,
						d.telefone,
						d.foto_capa,
						d.foto_perfil,
						d.sobre,
						d.plano,
						d.facebook,
						d.twitter,
						d.youtube,
						d.site,
						d.cargo
				FROM sis_usuario u
				INNER JOIN sis_usuariotipo t ON (t.usuariotipoid = u.usuariotipoid)
				LEFT JOIN sis_usuario_dados d ON (d.token = u.token)
				WHERE u.token LIKE '$token'";
		//print_r($sql); die();
		// Processa query
		$dados = $this->db->query( $sql );
		// Retorna array com os valores
		return $dados->result_array();
	}

	function get_user_by_searchNotFollow($token, $name, $surname = null)
	{
		if($surname == null){
			$sql = "SELECT 	U.usuarioid,
							U.usuariovinculo,
							U.token,
							U.data_registro,
							U.email,
							D.email2,
							T.nivel,
							T.tipo,
							U.nome,
							U.sobrenome,
							U.sexo,
							U.dia,
							U.mes,
							U.ano,
							Concat(U.dia, '/', U.mes, '/', U.ano) as aniversario,
							D.estado,
							D.cidade,
							D.bairro,
							D.numero,
							D.telefone,
							D.foto_capa,
							D.foto_perfil,
							D.sobre,
							D.site,
							D.facebook,
							D.youtube,
							D.twitter,
							S.token_de,
							S.token_para,
							D.cargo
						FROM sis_usuario U
						INNER JOIN sis_usuario_dados D ON (D.token = U.token)
						LEFT JOIN sis_usuariotipo T ON (T.usuariotipoid = U.usuariotipoid)
						LEFT JOIN sep_seguidores S ON (S.token_para = U.token)
						WHERE U.nome LIKE '%$name%'
						AND U.ativo LIKE 'S'
						AND U.token NOT LIKE '$token'
						AND NOT EXISTS (
										SELECT * FROM sep_seguidores S
										WHERE S.token_para = U.token
										AND S.token_de LIKE '$token'
										)
						GROUP BY U.token
						ORDER BY U.nome ASC
						LIMIT 50";
		} else {
			$sql = "SELECT 	U.usuarioid,
							U.usuariovinculo,
							U.token,
							U.data_registro,
							U.email,
							D.email2,
							T.nivel,
							T.tipo,
							U.nome,
							U.sobrenome,
							U.sexo,
							U.dia,
							U.mes,
							U.ano,
							Concat(U.dia, '/', U.mes, '/', U.ano) as aniversario,
							D.estado,
							D.cidade,
							D.bairro,
							D.numero,
							D.telefone,
							D.foto_capa,
							D.foto_perfil,
							D.sobre,
							D.site,
							D.facebook,
							D.youtube,
							D.twitter,
							S.token_de,
							S.token_para,
							D.cargo
						FROM sis_usuario U
						INNER JOIN sis_usuario_dados D ON (D.token = U.token)
						LEFT JOIN sis_usuariotipo T ON (T.usuariotipoid = U.usuariotipoid)
						LEFT JOIN sep_seguidores S ON (S.token_para = U.token)
						WHERE U.nome LIKE '%$name%'
						and U.sobrenome LIKE '%$surname%'
						AND U.ativo LIKE 'S'
						AND U.token NOT LIKE '$token'
						AND NOT EXISTS (
										SELECT * FROM sep_seguidores S
										WHERE S.token_para = U.token
										AND S.token_de LIKE '$token'
										)
						GROUP BY U.token	
						ORDER BY U.nome ASC
						LIMIT 50";
		}

		//print_r($sql); die();
		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();		
	}

function get_user_by_searchFollow($token, $name, $surname = null)
	{
		if($surname == null){
			$sql = "SELECT 	U.usuarioid,
							U.usuariovinculo,
							U.token,
							U.data_registro,
							U.email,
							D.email2,
							T.nivel,
							T.tipo,
							U.nome,
							U.sobrenome,
							U.sexo,
							U.dia,
							U.mes,
							U.ano,
							Concat(U.dia, '/', U.mes, '/', U.ano) as aniversario,
							D.estado,
							D.cidade,
							D.bairro,
							D.numero,
							D.telefone,
							D.foto_capa,
							D.foto_perfil,
							D.sobre,
							D.site,
							D.facebook,
							D.youtube,
							D.twitter,
							D.cargo
						FROM sis_usuario U
						INNER JOIN sis_usuario_dados D ON (D.token = U.token)
						LEFT JOIN sis_usuariotipo T ON (T.usuariotipoid = U.usuariotipoid)
						WHERE U.nome LIKE '%$name%'
						AND U.ativo LIKE 'S'
						AND U.token NOT LIKE '$token'
						AND EXISTS (
										SELECT * FROM sep_seguidores S
										WHERE S.token_para = U.token
										AND S.token_de LIKE '$token'
										)
						ORDER BY U.nome ASC
						LIMIT 50";
		} else {
			$sql = "SELECT 	U.usuarioid,
							U.usuariovinculo,
							U.token,
							U.data_registro,
							U.email,
							D.email2,
							T.nivel,
							T.tipo,
							U.nome,
							U.sobrenome,
							U.sexo,
							U.dia,
							U.mes,
							U.ano,
							Concat(U.dia, '/', U.mes, '/', U.ano) as aniversario,
							D.estado,
							D.cidade,
							D.bairro,
							D.numero,
							D.telefone,
							D.foto_capa,
							D.foto_perfil,
							D.sobre,
							D.site,
							D.facebook,
							D.youtube,
							D.twitter,
							D.cargo
						FROM sis_usuario U
						INNER JOIN sis_usuario_dados D ON (D.token = U.token)
						LEFT JOIN sis_usuariotipo T ON (T.usuariotipoid = U.usuariotipoid)
						WHERE U.nome LIKE '%$name%'
						and U.sobrenome LIKE '%$surname%'
						AND U.ativo LIKE 'S'
						AND U.token NOT LIKE '$token'
						AND EXISTS (
										SELECT * FROM sep_seguidores S
										WHERE S.token_para = U.token
										AND S.token_de LIKE '$token'
										)
						ORDER BY U.nome ASC
						LIMIT 50";
		}

		//print_r($sql); die();
		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();		
	}

	function select_partner_rand()
	{
		$sql = "SELECT 	U.usuarioid,
							U.usuariovinculo,
							U.token,
							U.data_registro,
							U.email,
							D.email2,
							T.nivel,
							T.tipo,
							U.nome,
							U.sobrenome,
							U.sexo,
							U.dia,
							U.mes,
							U.ano,
							Concat(U.dia, '/', U.mes, '/', U.ano) as aniversario,
							D.estado,
							D.cidade,
							D.bairro,
							D.numero,
							D.telefone,
							D.foto_capa,
							D.foto_perfil,
							D.sobre,
							D.facebook,
							D.twitter,
							D.youtube,
							D.site,
							D.facebook,
							D.youtube,
							D.twitter,
							D.cargo
						FROM sis_usuario U
						INNER JOIN sis_usuario_dados D ON (D.token = U.token)
						LEFT JOIN sis_usuariotipo T ON (T.usuariotipoid = U.usuariotipoid)
						WHERE T.nivel = 3
						ORDER BY rand()
						LIMIT 1";

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	function data_view_profile($usuarioid)
	{
		$sql = "SELECT 	U.usuarioid,
						U.usuariovinculo,
						U.token,
						U.data_registro,
						U.email,
						D.email2,
						T.nivel,
						T.tipo,
						U.nome,
						U.sobrenome,
						U.sexo,
						U.dia,
						U.mes,
						U.ano,
						Concat(U.dia, '/', U.mes, '/', U.ano) as aniversario,
						D.estado,
						D.cidade,
						D.bairro,
						D.numero,
						D.telefone,
						D.foto_capa,
						D.foto_perfil,
						D.sobre,
						D.site,
						D.facebook,
						D.youtube,
						D.twitter,
						S.token_de,
						S.token_para,
						D.cargo
					FROM sis_usuario U
					INNER JOIN sis_usuario_dados D ON (D.token = U.token)
					LEFT JOIN sis_usuariotipo T ON (T.usuariotipoid = U.usuariotipoid)
					LEFT JOIN sep_seguidores S ON (S.token_para = U.token)
					WHERE U.usuarioid = $usuarioid";
		//print_r($sql);
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return array with result
		return $dados->result_array();
	}

	/*
	* data_view_profile_get_followers()
	* get total of followers
	* @params $token
	*/
	function data_view_profile_get_followers($token)
	{
		$sql = "SELECT count(*) as total
						FROM sep_seguidores 
						WHERE token_para LIKE '$token'";
		//print_r($sql); die();
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return array with result
		return $dados->result_array();
	}

	/*
	* data_view_profile_get_following()
	* get total of people that I follow
	* @params $token
	*/
	function data_view_profile_get_following($token)
	{
		$sql = "SELECT count(*) as total
						FROM sep_seguidores 
						WHERE token_de LIKE '$token'";

		//print_r($sql); die();
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return array with result
		return $dados->result_array();
	}

	/*
	* data_view_profile_get_followers_info()
	* get info of followers
	* @params $token
	*/
	function data_view_profile_get_followers_info($token)
	{
		$sql = "SELECT 	U.usuarioid,
						U.usuariovinculo,
						U.token,
						U.data_registro,
						U.nome,
						U.sobrenome,
						D.cidade,
						D.foto_perfil,
						S.token_de,
						S.token_para,
						D.cargo
						FROM sep_seguidores S
						INNER JOIN sis_usuario U on (U.token = S.token_de)
						LEFT JOIN sis_usuario_dados D on (D.token = U.token)
						WHERE token_para LIKE '$token'";
		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	/*
	* data_view_profile_get_followers_info()
	* get info of followers
	* @params $token
	*/
	function data_view_profile_get_following_info($token)
	{
		$sql = "SELECT 	U.usuarioid,
						U.usuariovinculo,
						U.token,
						U.data_registro,
						U.nome,
						U.sobrenome,
						D.cidade,
						D.foto_perfil,
						S.token_de,
						S.token_para,
						D.cargo
						FROM sep_seguidores S
						INNER JOIN sis_usuario U on (U.token = S.token_para)
						LEFT JOIN sis_usuario_dados D on (D.token = U.token)
						WHERE token_de LIKE '$token'";
		//print_r($sql); die();
		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	function data_view_profile_follow($tokenUser, $tokenMe)
	{
		$sql = "SELECT count(*) as total
						FROM sep_seguidores 
						WHERE token_para LIKE '$tokenUser'
						AND token_de LIKE '$tokenMe'";
		//print_r($sql);
		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	function get_users_partner($token)
	{
		$sql = "SELECT	u.usuarioid,
						u.usuariovinculo,
						u.token,
						d.token,
						u.data_registro,
						u.email,
						u.ativo,
						d.email2,
						t.nivel,
						t.tipo,
						u.nome,
						u.sobrenome,
						u.sexo,
						u.dia,
						u.mes,
						u.ano,
						Concat(u.dia, '/', u.mes, '/', u.ano) as aniversario,
						d.cpf,
						d.rg,
						d.estado,
						d.cidade,
						d.bairro,
						d.numero,
						d.telefone,
						d.foto_capa,
						d.foto_perfil,
						d.sobre,
						d.plano,
						d.cargo
				FROM sis_usuario u
				INNER JOIN sis_usuariotipo t ON (t.usuariotipoid = u.usuariotipoid)
				LEFT JOIN sis_usuario_dados d ON (d.token = u.token)
				WHERE u.usuariovinculo LIKE '$token'";

		//print_r($sql);
		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	function validar_cadastro($tokenME, $token)
	{
		$sql = "SELECT *
						FROM sis_usuario u
						WHERE token LIKE '$tokenME'
						AND usuariovinculo LIKE '$token'
						AND u.ativo LIKE 'N'";

		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	function validar_cadastro_process($tokenME)
	{
		$sql = "SELECT *
						FROM sis_usuario u
						WHERE token LIKE '$tokenME'
						AND usuariovinculo NOT LIKE ''
						AND u.ativo LIKE 'N'";

		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}


	function get_profile_view($token){
		$sql = "SELECT  *
						FROM (
						SELECT
						L.logid, L.descricao,	L.data_registro, U.usuarioid as idusuario,
						L.token,U.nome,U.sobrenome,D.foto_perfil
					FROM sep_logs L
					INNER JOIN sis_usuario U ON U.token = L.token
					LEFT JOIN sis_usuario_dados D ON (D.token = U.token)
					WHERE L.descricao LIKE  '$token'
					AND L.token NOT LIKE '$token'
					ORDER BY L.logid DESC
					LIMIT 5
					) result
					GROUP BY token";
		//print_r($sql);
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Return array with result
		return $dados->result_array();
	}

	function friends_follow($token){
		$sql = "SELECT 	U.usuarioid,
						U.usuariovinculo,
						U.token,
						U.data_registro,
						U.nome,
						U.sobrenome,
						D.cidade,
						D.foto_perfil,
						S.token_de,
						S.token_para,
						D.cargo
						FROM sep_seguidores S
						INNER JOIN sis_usuario U on (U.token = S.token_para)
						LEFT JOIN sis_usuario_dados D on (D.token = U.token)
						WHERE token_de LIKE '$token'
						ORDER BY rand()
						LIMIT 24";
		//print_r($sql); die();
		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	function get_professores(){

		$sql = "SELECT	u.usuarioid,
						u.token,
						u.data_registro,
						u.email,
						u.nome,
						u.sobrenome,
						Concat(u.nome, ' ', u.sobrenome) as nome_completo,
						Concat(u.dia, '/', u.mes, '/', u.ano) as aniversario,
						d.telefone,
						d.foto_capa,
						d.foto_perfil,
						d.sobre,
						d.facebook,
						d.twitter,
						d.youtube,
						d.site,
						d.cargo
				FROM sis_usuario u
				INNER JOIN sis_usuariotipo t ON (t.usuariotipoid = u.usuariotipoid)
				LEFT JOIN sis_usuario_dados d ON (d.token = u.token)
				WHERE u.usuariotipoid = 2";
		//print_r($sql); die();
		// Processa query
		$dados = $this->db->query( $sql );
		// Retorna array com os valores
		return $dados->result_array();
	}

} # end class

