<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forum_model extends CI_Model {

	/*
	* package: model.dashboard_model
	* date: 24-08-2013
	* by: Fernando
	*/

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_forum_by_id($qcid)
	{
		$sql = "SELECT 	*,
						CONCAT(DATE_FORMAT(F.data_registro, '%d/%m/%y'), ' às ', DATE_FORMAT(F.data_registro, ' %T'))	as date_post
					FROM sep_simulado_forum F
					INNER JOIN sis_usuario U ON (U.token = F.token)
					LEFT JOIN sis_usuario_dados D ON (D.token = U.TOKEN)
					WHERE F.qcid = $qcid";

		//print_r($sql); die();
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}


 	function get_last_posts()
 	{
 		$sql = "SELECT 	f.forumid, 
 						f.token, 
 						f.qcid, 
 						f.mensagem, 
 						f.data_registro,
 						(SELECT da.foto_perfil FROM sep_simulado_forum fo
								INNER JOIN sis_usuario su on su.token = fo.token
								LEFT JOIN sis_usuario_dados da on da.token = su.token
							WHERE fo.qcid = f.qcid ORDER BY forumid DESC LIMIT 1
						) as foto_perfil,
						(SELECT CONCAT(su.nome, ' ', su.sobrenome) FROM sep_simulado_forum fo
								INNER JOIN sis_usuario su on su.token = fo.token
							WHERE fo.qcid = f.qcid ORDER BY forumid DESC LIMIT 1
						) as nome,
 						q.disciplina,
 						q.ano,
 						q.cargo,
 						q.questao,
 						q.qcid,
 						q.banca,
 						q.orgao,
 						q.escolaridade,
 						(SELECT fo.data_registro FROM sep_simulado_forum fo
						WHERE fo.qcid = f.qcid ORDER BY fo.data_registro DESC LIMIT 1) as last_coment
						FROM sep_simulado_forum f
						INNER JOIN sis_usuario u on u.token = f.token
						LEFT JOIN sis_usuario_dados d on u.token = d.token
						LEFT JOIN sep_questoes q on q.qcid = f.qcid
						GROUP BY q.qcid
						ORDER BY data_registro DESC
						LIMIT 10";
		//print_r($sql); die();
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
 	}

 	function get_count_posts_by_discipline()
 	{
 		$sql = "SELECT 	( select count(1) from sep_simulado_forum WHERE qcid = f.qcid) as total,
						q.disciplina
					FROM sep_simulado_forum f
					INNER JOIN sep_questoes q on q.qcid = f.qcid
					group by f.qcid
					ORDER BY total DESC
					LIMIT 10";
		//print_r($sql); die();
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
 	}

 	function get_forum_comments_by_id($id)
 	{
		$sql = "SELECT 	f.forumid, 
 						f.token, 
 						f.qcid, 
 						f.mensagem, 
 						f.data_registro,
 						u.nome,
 						u.sobrenome,
 						d.foto_perfil,
 						f.data_registro as last_coment
						FROM sep_simulado_forum f
						INNER JOIN sis_usuario u on u.token = f.token
						LEFT JOIN sis_usuario_dados d on u.token = d.token
						WHERE f.qcid = $id
						ORDER BY f.data_registro ASC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
 	}

 	function get_questao_by_id($id){
 		$sql = "SELECT 	*
						FROM sep_questoes q
						WHERE q.qcid = $id
						GROUP BY q.qcid";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
 	}


 	function get_forum_details_teacher_by_id($id)
 	{
		$sql = "SELECT 	*
						FROM sep_professores_prova p
						INNER JOIN sis_usuario u on u.usuarioid = p.usuarioid
						LEFT JOIN sis_usuario_dados d on d.token = u.token
						WHERE p.qcid = $id";
		//print_r($sql); die();
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
 	}


} # end class

