<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assinatura_model extends CI_Model {


	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_status_active($token)
	{
		$sql = "SELECT 	*
						FROM sep_assinaturas 
						WHERE token LIKE '$token'
						AND status = 2
						ORDER BY assinaturaid DESC";

		//print_r($sql); die();
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function pagseguroCheckStatus($token)
	{
		$sql = "SELECT 	*
						FROM sep_assinaturas 
						WHERE token LIKE '$token'
						ORDER BY assinaturaid DESC";
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}	
	function get_status($token)
	{
		$sql = "SELECT 	*
						FROM sep_assinaturas 
						WHERE token LIKE '$token'";

		//print_r($sql); die();
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}


	function get_assinatura($token, $id)
	{
		$sql = "SELECT 	*
						FROM sep_assinaturas 
						WHERE token LIKE '$token'
						AND assinaturaid = $id
						AND status = 2";

		//print_r($sql); die();
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	function pagseguroRemoveTransactions($sql){
		$dados = $this->db->query( $sql );
		$this->db->close();
	}



} # end class

