<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timeline_model extends CI_Model {

	/*
	* package: model.dashboard_model
	* date: 24-08-2013
	* by: Fernando
	*/

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_results($users, $tokenMe){
		$orStatement = '';
		foreach($users as $tokens):
			$orStatement .= ' OR t.token ="'.$tokens['token'].'" ';
		endforeach;

		$sql = "SELECT  t.postagemid,
						t.conteudo,
						t.data_postagem,
						u.nome,
						u.sobrenome,
						u.token as identificador_usuario,
						d.foto_perfil
					FROM sep_timeline t
					LEFT JOIN sep_timeline_imagens i ON (i.postagemid = t.postagemid)
					LEFT JOIN sis_usuario u ON (u.token = t.token)
					LEFT JOIN sis_usuario_dados d ON (d.token = u.token)
					WHERE t.token = '{$tokenMe}'
					{$orStatement}
					ORDER BY t.postagemid DESC";
		//debug($sql);
		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_timeline_client
    * Catch all posts from client
    * @param $token
    * @return void
    */
	function get_timeline_client($token)
	{
		$sql = "SELECT 	t.postagemid 								as postid,
						t.token,
						CONCAT(u.nome, ' ', u.sobrenome) 			as name,
						d.foto_perfil								as profile_picture,
						CONCAT(DATE_FORMAT(t.data_postagem, '%d/%m/%y'), ', ', DATE_FORMAT(t.data_postagem, ' %T'))	as date_post,
						t.titulo									as title,
						t.conteudo									as content,
						t.imagem 									as image
						FROM sep_timeline t
						LEFT JOIN sis_usuario u on (u.token = t.token)
						LEFT JOIN sis_usuario_dados d on (d.token = t.token)
						WHERE t.token LIKE '$token'
						ORDER BY date_post DESC";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_timeline_normal
    * Catch all posts
    * @param $token
    * @return void
    */
	function get_timeline_normal($token)
	{
		$sql = "SELECT 	t.postagemid 								as postid,
						t.token,
						CONCAT(u.nome, ' ', u.sobrenome) 			as name,
						d.foto_perfil								as profile_picture,
						CONCAT(DATE_FORMAT(t.data_postagem, '%d/%m/%y'), ', ', DATE_FORMAT(t.data_postagem, ' %T'))	as date_post,
						t.titulo									as title,
						t.conteudo									as content,
						t.imagem 									as image		
					FROM sep_seguidores s
					LEFT JOIN sep_timeline t on (t.token = s.token_para)
					LEFT JOIN sis_usuario u on (u.token = t.token)
					LEFT JOIN sis_usuario_dados d on (d.token = t.token)
					WHERE s.token_de LIKE '$token'
					ORDER BY date_post DESC";

		// Process query
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();

	}




	function get_timeline_client_CACHE($token)
	{
		$sql = "SELECT 	t.postagemid 								as postid,
						t.token,
						CONCAT(u.nome, ' ', u.sobrenome) 			as name,
						d.foto_perfil								as profile_picture,
						CONCAT(DATE_FORMAT(t.data_postagem, '%d/%m/%y'), ', ', DATE_FORMAT(t.data_postagem, ' %T'))	as date_post,
						t.titulo									as title,
						t.conteudo									as content,
						t.imagem 									as image
						FROM sep_timeline t
						LEFT JOIN sis_usuario u on (u.token = t.token)
						LEFT JOIN sis_usuario_dados d on (d.token = t.token)
						WHERE t.token LIKE '$token'
						ORDER BY date_post DESC";

		// Process query
		$dados = $this->db->cache_on();
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}

	/*
    * get_timeline_normal
    * Catch all posts
    * @param $token
    * @return void
    */
	function get_timeline_normal_CACHE($token)
	{
		$sql = "SELECT 	t.postagemid 								as postid,
						t.token,
						CONCAT(u.nome, ' ', u.sobrenome) 			as name,
						d.foto_perfil								as profile_picture,
						CONCAT(DATE_FORMAT(t.data_postagem, '%d/%m/%y'), ', ', DATE_FORMAT(t.data_postagem, ' %T'))	as date_post,
						t.titulo									as title,
						t.conteudo									as content,
						t.imagem 									as image		
					FROM sep_seguidores s
					LEFT JOIN sep_timeline t on (t.token = s.token_para)
					LEFT JOIN sis_usuario u on (u.token = t.token)
					LEFT JOIN sis_usuario_dados d on (d.token = t.token)
					WHERE s.token_de LIKE '$token'
					ORDER BY date_post DESC";

		// Process query
		$dados = $this->db->cache_on();
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();

	}

	function cache()
	{
		$sql = "SELECT 	* from sep_questoes ORDER BY questaoid desc limit 1";

		// Process query
		$dados = $this->db->cache_on();
		$dados = $this->db->query( $sql );
		$this->db->close();
		$dados = $dados->result_array();	
		// Return result array
		return (isset($dados) && count($dados) > 0) ? $dados : array();
	}



} # end class

