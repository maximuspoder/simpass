<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questoes_model extends CI_Model {

	/*
	* package: model.dashboard_model
	* date: 24-08-2013
	* by: Fernando
	*/

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/*
    * get_questoes_imagem_dentro
    * Pega questoes com imagem dentro
    * @param 
    * @return void
    */
	function get_questoes_imagem_dentro()
	{
		$sql = "SELECT 	*
						FROM sep_questoes 
						WHERE editado IS NULL
						LIMIT 20";

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();	
	}


	/*
    * get_questoes_imagem_dentro
    * Pega questoes com imagem dentro
    * @param 
    * @return void
    */
	function get_questoes_count()
	{
		$sql = "SELECT 	count(*) as total
						FROM sep_questoes 
						WHERE editado IS NULL";

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();	
	}


	function api_teste()
	{
		$sql = "SELECT *
						FROM sep_questoes";

		// Processa query
		$dados = $this->db->query( $sql );
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();	
	}


	function get_questoes_imagem_dentro_byQCID($qcid)
	{
		$sql = "SELECT 	*
						FROM sep_questoes 
						WHERE qcid = $qcid";

		// Processa query
		$dados = $this->db->query( $sql );
		// Retorna array com os valores
		return $dados->result_array();	
	}

	function get_question_by_qcid($qcid)
	{
		$sql = "SELECT *
						FROM sep_questoes
						WHERE qcid like '%$qcid%'";
		// Processa query
		$dados = $this->db->query( $sql );
		
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}

	function ajax_get_questionsByDiscipline($discipline)
	{
		$sql = "SELECT *
						FROM sep_questoes
						WHERE discipline like '%$discipline";
		// Processa query
		$dados = $this->db->query( $sql );
		
		$this->db->close();
		// Retorna array com os valores
		return $dados->result_array();
	}


} # end class

