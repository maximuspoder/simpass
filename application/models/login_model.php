<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {

	/*
	* package: model.system_model
	* date: 21-08-2013
	* by: Fernando
	*/

	/**
	* __construct()
	* @return object
	*/
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

    /*
    * user_exists
    * Verifica se usuário existe
    * @param @email
    * @return boolean
    */
    function user_exists($email)
	{
		// Monta consulta
		$sql = "SELECT
						email
					FROM sis_usuario
					WHERE email LIKE '$email'
					AND ativo LIKE 'S'";

		// Processa query
		$dados = $this->db->query( $sql );
		// Pega o valor de linhas afetadas
		$dados = $this->db->affected_rows();
		// Retorna o total
		return $dados;
	}


	/*
    * user_pass_exists
    * Verifica se usuário, senha conferem
    * @param $user, $pass
    * @return boolean
    */
	function user_pass_exists($email, $pass)
	{
		// Monta consulta
		$sql = "SELECT
						email,
						senha
					FROM sis_usuario
					WHERE email LIKE '$email'
					AND
					senha LIKE '$pass'";
		//print_r($sql); die();
		// Processa query
		$dados = $this->db->query( $sql );
		// Pega o valor de linhas afetadas
		$dados = $this->db->affected_rows();
		// Retorna o total
		return $dados;	
	}

	function get_user_dados($email, $senha)
	{
		$sql = "SELECT	u.usuarioid,
						u.usuariovinculo,
						u.token,
						d.token,
						u.data_registro,
						u.email,
						d.email2,
						t.nivel,
						t.tipo,
						u.nome,
						u.sobrenome,
						u.sexo,
						u.dia,
						u.mes,
						u.ano,
						Concat(u.dia, '/', u.mes, '/', u.ano) as aniversario,
						d.cpf,
						d.rg,
						d.estado,
						d.cidade,
						d.bairro,
						d.numero,
						d.telefone,
						d.foto_capa,
						d.foto_perfil,
						d.sobre,
						d.plano,
						d.facebook,
						d.twitter,
						d.youtube,
						d.site,
						d.cargo
				FROM sis_usuario u
				INNER JOIN sis_usuariotipo t ON (t.usuariotipoid = u.usuariotipoid)
				LEFT JOIN sis_usuario_dados d ON (d.token = u.token)
				WHERE u.email LIKE '$email'
				AND u.senha LIKE '$senha'";
		// Processa query
		$dados = $this->db->query( $sql );
		// Retorna array com os valores
		return $dados->result_array();
	}



} # end class

