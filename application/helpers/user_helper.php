<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
* userProfileHeader()
* get user navigation header
* param @arrayUser
* @arrayUser 
*/
function userProfileHeader($dataUser){
?>
<nav class="navbar top-nav navbar-fixed-top" role="navigation">
<div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle btn btn-default" data-toggle="collapse" data-target="#navbar-fixed-layout-collapse">
            <i class="fa fa-indent"></i>
        </button>
        <a class="navbar-brand" href="<?php echo BASEURL; ?>usuario">
            <img src="<?php echo IMG; ?>logo-sep_198_47.png" width='100'>
        </a>
    </div>
    
    <div class="collapse navbar-collapse" id="navbar-fixed-layout-collapse">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="<?php echo BASEURL; ?>usuario">Home </a>
            </li>
            <li class="dropdown">
                <a href="<?php echo BASEURL; ?>simulado/novo" class="dropdown-toggle" data-toggle="dropdown">Simulados<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li class="active"><a href="<?php echo BASEURL; ?>simulado/novo">Novo</a></li>
                <li><a href="<?php echo BASEURL; ?>simulado/historico">Histórico</a></li>
                <li><a href="<?php echo BASEURL; ?>simulado/desempenho">Desempenho Geral</a></li>
            </ul>
            </li>
            <li class="dropdown">
                <a href="<?php echo BASEURL; ?>forum/">Fórum</a>
            </li>
            <?php if($dataUser['usertype'] == 'PROFESSOR' || $dataUser['usertype'] == 'ADMINISTRADOR' || $dataUser['usertype'] == 'PARCEIRO'){?>
            <!--
            <li class="dropdown">
                <a href="<?php echo BASEURL; ?>simulado/novo" class="dropdown-toggle" data-toggle="dropdown">Notícias<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li class="active"><a href="<?php echo BASEURL; ?>noticias/nova">Nova</a></li>
                <li class="active"><a href="<?php echo BASEURL; ?>noticias/postadas_por_mim">Minhas Postagens</a></li>
                <li><a href="<?php echo BASEURL; ?>noticias/">Notícias</a></li>
            </ul>
            </li>-->
             <?php } else { ?>
                <li><a href="<?php echo BASEURL; ?>noticias/">Notícias</a></li>
             <?php } ?>
        </ul>
    <div class="form-search-header">
    <form action="<?php echo BASEURL; ?>usuario/buscar" method="post" class="navbar-form navbar-left" role="search">
        <div class="form-group inline-block">
            <input type="text" name="search" class="form-control" placeholder="Pesquisar">
        </div>
        <button type="submit" class="btn btn-primary"><i class="fa fa-search fa-fw"></i></button>
    </form>
    </div>

    <ul class="nav navbar-nav navbar-right">
        <?php if($dataUser['usertype'] != 'PARCEIRO' && $dataUser['usertype'] != null && $dataUser['account'] == null){ ?>
        <li class="innerLR">
            <button onclick="redirect('usuario/assinar')" type="submit" class="btn btn-success navbar-btn">
                <i class="fa fa-credit-card"></i> Assinar
            </button>
        </li>
        <?php } ?>
        <li class="dropdown" id="name-user">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="pull-left innerR">
                    <img src="<?php echo $dataUser['profile']; ?>" alt="user" class="img-circle">
                </span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <?php if($dataUser['usertype'] == 'PROFESSOR' || $dataUser['usertype'] == 'ADMINISTRADOR' || $dataUser['usertype'] == 'PARCEIRO'){?>
                <li><a href="<?php echo BASEURL; ?>usuario/cadastrar_usuario/">Cadastrar Usuários</a></li>
                <?php }?>
                <li><a href="<?php echo BASEURL; ?>assinatura/status/">Acompanhar Pedido de Assinatura</a></li>
                <li><a href="<?php echo BASEURL; ?>usuario/editar_meus_dados/">Editar Meus Dados</a></li>
                <?php if($dataUser['usertype'] == 'PROFESSOR'){ ?>
                 <li><a href="<?php echo BASEURL; ?>professores/">Área do Professor</a></li>
                <?php }?>
                <li><a href="<?php echo BASEURL; ?>usuario/faq">Ajuda</a></li>
                <li><a href="<?php echo BASEURL; ?>login/logout">Sair</a></li>
            </ul>
        </li>
    </ul>
    </div>
</div>
</nav>

<?php
} #end userProfileHeader


/*
* userInfouserInfo()
* get user data
* param @arrayUser
* @arrayUser contains data from user
*/
function userInfo($dataUser = array()){
?>
<div class="widget">
    <div class="widget-body text-center" id="upload_profile">
        <a href="">
            <img src="<?php echo $dataUser['profile']; ?>" width="120" alt="" class="img-circle">
        </a>
        <div id="upload_profile_update"><a href="#modal-profile" data-toggle="modal" >Alterar Foto</a></div>
        <h2 class="strong margin-none"><?php echo $dataUser['name']; ?></h2>
        <div class="innerB"></div>
    </div>
</div>
<?php
} # end userTimeline

/*
* userCover()
* get user cover photo
* param @level
* 2 = teacher
* 4 = user
*
*/
function userCover($arrParam){
?>
<div class="timeline-cover">
    <div class="cover">
        <div class="top">
            <div id="upload">
            <div id="view"><a href="#modal-login" data-toggle="modal" ><span>Alterar Capa</span></a></div>
            <img src="<?php echo $arrParam['cover']; ?>" class="img-responsive"/>
            </div>

        </div>

            <ul class="list-unstyled">
                <li class="active"><a href="<?php echo BASEURL; ?>usuario/"><i class="fa fa-fw fa-clock-o"></i> <span>Timeline</span></a></li>
                <li><a href="<?php echo BASEURL; ?>usuario/sobre/"><i class="fa fa-fw fa-user"></i> <span>Sobre</span></a></li>
                <li><a href="<?php echo BASEURL; ?>usuario/seguindo/p/<?php echo $arrParam['userid']; ?>"><i class="fa fa-fw icon-group"></i> <span>Seguindo (<?php echo $arrParam['following']; ?>)</span></a></li>
                <li><a href="<?php echo BASEURL; ?>usuario/seguidores/p/<?php echo $arrParam['userid']; ?>"><i class="fa fa-fw fa-heart"></i> <span>Seguidores (<?php echo $arrParam['followers']; ?>)</span></a></li>
                <!--<li><a href="<?php echo BASEURL; ?>mensagens/me/<?php echo $arrParam['userid']; ?>/"><i class="fa fa-fw fa-envelope"></i> <span>Mensagens</span></a></li>
                <li><a href="<?php echo BASEURL; ?>fotos/me/<?php echo $arrParam['userid']; ?>/"><i class="fa fa-fw fa-camera"></i> <span>Fotos</span></a></li>-->
            </ul>          
    </div>
    <?php if(@$arrParam['edit_data'] == 1){?>
    <div class="widget">
        <div class="widget-body padding-none margin-none">
            <a href="<?php echo BASEURL; ?>usuario/editar_meus_dados">
                <div class="innerAll">
                    <p class="lead margin-none">Editar meus dados</p>
                </div>
            </a>
        </div>
    </div>
    <?php } ?>
</div>
<?php
} # end userTimeline

function modal_cover_photo($token)
{
?>

<div class="modal fade" id="modal-login">
    <div class="modal-dialog">
    <div class="col-md-10">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Alterar Foto de Capa</h3>
            </div>
            <div class="modal-body">
                <div class="innerAll">
                    <div class="innerLR">
                        <div class="row">
                            <form id="myForm1" action="<?php echo BASEURL; ?>usuario/update_photo_process" method="post" enctype="multipart/form-data">
                                    <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
                                        <div class="form-group">
                                            <div class="col-sm-10">
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-new">Escolher Arquivo</span>
                                                    <span class="fileupload-exists">Mudar</span>
                                                    <input type="file" name="file_upload" id="file_upload" class="margin-none"/>
                                                </span>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-10">
                                                <input type="hidden" name="token"  value="<?php echo $token; ?>">
                                                <input type="hidden" name="update" value="cover">
                                                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none;">&times;</a>         
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <div class="col-sm-10">
                                                 <button class="btn btn-primary">Alterar</button>
                                            </div>
                                         </div>

                                          <div class="form-group">
                                            <div class="col-sm-10">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-primary" id="bar"></div>
                                                </div>
                                                <div id="retorno"></div>
                                            </div>
                                         </div>
                                    </div>
                            </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
}

function modal_profile_photo($token)
{
?>
<div class="modal fade" id="modal-profile">
    <div class="modal-dialog">
    <div class="col-md-10">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Alterar Foto de Perfil</h3>
            </div>
            <div class="modal-body">
                <div class="innerAll">
                    <div class="innerLR">
                        <div class="row">                        
                            <form id="myForm2" action="<?php echo BASEURL; ?>usuario/update_photo_process" method="post" enctype="multipart/form-data">
                                    <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
                                        <div class="form-group">
                                            <div class="col-sm-10">
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-new">Escolher Arquivo</span>
                                                    <span class="fileupload-exists">Mudar</span>
                                                    <input type="file" name="file_upload" id="file_upload" class="margin-none"/>
                                                </span>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-10">
                                                <input type="hidden" name="token" value="<?php echo $token; ?>">
                                                <input type="hidden" name="update" value="photo">
                                                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none;">&times;</a>         
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <div class="col-sm-10">
                                                 <button class="btn btn-primary">Alterar</button>
                                            </div>
                                         </div>

                                          <div class="form-group">
                                            <div class="col-sm-10">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-primary" id="bar1"></div>
                                                </div>
                                                <div id="retorno1"></div>
                                            </div>
                                         </div>
                                    </div>
                            </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
}


function updateNotify($parameter){
?>
<?php if(isset($parameter) && $parameter == 'updated'){ ?>
<ul id="notyfy_container_top"  class="notyfy_container">
    <li class="notyfy_wrapper notyfy_success" style="cursor: pointer;">
        <div id="notyfy_752739675174936200" class="notyfy_bar">
            <div class="notyfy_message">
                <span class="notyfy_text">
                    Dados alterados com <strong>sucesso!</strong>
                </span>
            </div>
        </div>
    </li>
</ul>
<?php } elseif(isset($parameter) && $parameter == 0) { ?>
<ul id="notyfy_container_top" class="notyfy_container">
    <li class="notyfy_wrapper notyfy_error">
        <div id="notyfy_1259075451017146000" class="notyfy_bar">
            <div class="notyfy_message">
                <span class="notyfy_text">
                    <strong>Erro ao alterar seus dados!!</strong> :(
                </span>
            </div>
        </div>
    </li>
</ul>
<?php } ?>
<?php
}



function profileCover($cover, $arraParam){
?>
<div class="timeline-cover">
    <div class="cover">
        <div class="top" style="margin-top:25px;">
            <div id="upload">
            <img src="<?php echo $cover; ?>" class="img-responsive"/>
            </div>

        </div>
            <ul class="list-unstyled">
                <li class="active"><a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $arraParam['userID']; ?>"><i class="fa fa-fw fa-clock-o"></i> <span>Timeline</span></a></li>
                <li><a href="<?php echo BASEURL; ?>usuario/sobre/p/<?php echo $arraParam['userID']; ?>"><i class="fa fa-fw fa-user"></i> <span>Sobre</span></a></li>
                <li><a href="<?php echo BASEURL; ?>usuario/seguidores/p/<?php echo $arraParam['userID']; ?>"><i class="fa fa-fw icon-group"></i> <span>Seguidores(<?php echo $arraParam['followers']; ?>)</span></a></li>
            </ul>
    </div>
    
    <div class="widget">
        <div class="widget-body padding-none margin-none">
            <div class="innerAll">
                <?php if($arraParam['follow'] > 0){ ?>
                <a href="javascript:void(0)"class="btn btn-default" disabled="disabled"><i class="fa fa-check"></i></a>
                Seguindo <b><?php echo $arraParam['name']; ?></b>
                <?php } else {?>
                 <div id="button_follow_0">
                    <a href="javascript:void(0)" onclick="set_follower(1, 0, '<?php echo generate_token_unique($arraParam['tokenME']) . $arraParam['tokenME']; ?>', '<?php echo $arraParam['tokenUSER'] . generate_token_unique('partner'); ?>')" class="btn btn-primary btn-sm">
                        <i class="fa fa-thumbs-up"></i> Seguir
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    
    <br/>
</div>
<?php
}

/*
* userInfouserInfo()
* get user data
* param @arrayUser
* @arrayUser contains data from user
*/
function profileInfo($dataUser = array()){
?>
<div class="widget">
    <div class="widget-body text-center" id="upload_profile">
        <a href="">
            <img src="<?php echo $dataUser['profile']; ?>" width="120" alt="" class="img-circle">
        </a>
        <h2 class="strong margin-none"><?php echo $dataUser['name']; ?></h2>
        <div class="innerB">
            <a href="" class="btn btn-primary text-center btn-block"><?php echo ($dataUser['cargo'] == '') ? 'Usuário' : $dataUser['cargo']; ?></a>
        </div>
        <!--
        <?php if($dataUser['account'] != null){ ?><a href="" class="btn btn-primary text-center btn-block">Usuário master</a><?php } ?>
    -->
        
    </div>
</div>
<?php
} # end userTimeline


function userProfileHeaderUnregistered(){
?>
<nav class="navbar top-nav navbar-fixed-top" role="navigation">
<div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle btn btn-default" data-toggle="collapse" data-target="#navbar-fixed-layout-collapse">
            <i class="fa fa-indent"></i>
        </button>
        <a class="navbar-brand" href="<?php echo BASEURL; ?>">
            <img src="<?php echo IMG; ?>logo-sep_198_47.png">
        </a>

        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="<?php echo BASEURL; ?>">Home </a>
            </li>
            <li class="dropdown">
                <a href="<?php echo BASEURL; ?>noticias">Mais notícias </a>
            </li>
        </ul>
    </div>
</div>
</nav>
<?php
}








#end helper
