<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
* package: helper.dashboard
* date: 23-08-2013
* by: Fernando
*
*/

function dashboard_helper()
{
?>
<div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- start: LOGO -->
                    <a class="navbar-brand" href="index.html">
                        Moura <strong>&</strong> Souza
                    </a>
                    <!-- end: LOGO -->
                </div>
            </div><!-- container -->
</div><!-- navbar -->
<?php
}


function dashboard_menu($nivel){
?>
<div class="main-navigation navbar-collapse collapse">
<div class="navigation-toggler">
    <i class="clip-chevron-left"></i>
    <i class="clip-chevron-right"></i>
</div>
<ul class="main-navigation-menu">

    <li class="active open">
        <a href="<?php echo BASEURL . 'dashboard'; ?>">
        <i class="clip-home-3"></i>
            <span class="title"> Home </span><span class="selected"></span>
        </a>
    </li>
   
   <?php if(get_nivel('menu_cadastros', $nivel) == 1){ ?>
    <li>
        <a href="javascript:void(0)"><i class="clip-file-plus"></i>
            <span class="title"> Cadastros </span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <?php if(get_nivel('menu_cadastro_cliente', $nivel) == 1){ ?>
                <a href="<?php echo BASEURL . 'dashboard/cliente'; ?>">
                        <span class="title"> Cliente </span><span class="selected"></span>
                </a>
                <?php } ?>
                <?php if(get_nivel('menu_cadastro_proprietario', $nivel) == 1){ ?>
                <a href="<?php echo BASEURL . 'dashboard/proprietario'; ?>">
                        <span class="title"> Proprietário </span><span class="selected"></span>
                </a>
                <?php } ?>
                <?php if(get_nivel('menu_cadastro_imovel', $nivel) == 1){ ?>
                <a href="<?php echo BASEURL . 'proprietarios/imovel'; ?>">
                    <span class="title"> Imóvel </span><span class="selected"></span>
                </a>
                <?php } ?>
                <?php if(get_nivel('menu_cadastro_usuario', $nivel) == 1){ ?>
                 <a href="<?php echo BASEURL . 'usuarios/novo_usuario'; ?>">
                    <span class="title"> Usuário </span><span class="selected"></span>
                </a>
                <?php } ?>
            </li>
        </ul>
    </li>
    <?php } ?>

    <?php if(get_nivel('menu_listagens', $nivel) == 1){ ?>
    <li>
        <a href="javascript:void(0)"><i class="clip-file-2"></i>
            <span class="title"> Listagens   </span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <?php if(get_nivel('menu_listagem_cliente', $nivel) == 1){ ?>
                <a href="<?php echo BASEURL . 'clientes/'; ?>">
                        <span class="title"> Clientes </span><span class="selected"></span>
                </a>
                <?php  } ?>
                <?php if(get_nivel('menu_listagem_proprietario', $nivel) == 1){ ?>
                <a href="<?php echo BASEURL . 'proprietarios/'; ?>">
                        <span class="title"> Proprietários </span><span class="selected"></span>
                </a>
                <?php } ?>
                <?php if(get_nivel('menu_listagem_imovel', $nivel) == 1){ ?>
                <a href="<?php echo BASEURL . 'imoveis/'; ?>">
                        <span class="title"> Imóveis </span><span class="selected"></span>
                </a>
                <?php } ?>
                 <?php if(get_nivel('menu_listagem_usuario', $nivel) == 1){ ?>
                <a href="<?php echo BASEURL . 'usuarios/'; ?>">
                        <span class="title"> Usuários </span><span class="selected"></span>
                </a>
                <?php } ?>
            </li>
        </ul>
    </li>
    <?php } ?>
    <?php if(get_nivel('menu_configuracoes', $nivel) == 1){ ?>
    <li>
        <a href="javascript:void(0)"><i class="clip-settings"></i>
            <span class="title"> Configurações   </span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <?php if(get_nivel('menu_configuracao_nivel', $nivel) == 1){ ?>
                <a href="<?php echo BASEURL . 'configuracoes/nivel_acesso'; ?>">
                        <span class="title"> Níveis de acesso </span><span class="selected"></span>
                </a>
                <?php } ?>
            </li>
        </ul>
    </li>
    <?php } ?>
    <li>
        <a href="<?php echo BASEURL; ?>login/logout"><i class="clip-switch"></i>
            <span class="title">Sair</span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
    
    </li>
    
</ul>
<!-- end: MAIN NAVIGATION MENU -->
</div>
<?php
}


