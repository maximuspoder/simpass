<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function percentInfo($arrParam){
?>
<div class="row row-app">
<?php
$count = count($arrParam);
$x = 0;
while($x < $count){
?>
<div class="col-md-3">
    <div class="widget">
        <div class="text-center innerAll inner-2x border-bottom">
            <div class="innerTB">
                <div data-percent="100" data-size="100" class="easy-pie inline-block primary easyPieChart" data-scale-color="false" data-track-color="#efefef" data-line-width="5" style="width: 100px; height: 100px; line-height: 100px;">
                    <div class="value text-center">
                        <span class="strong"><i class="<?php echo $arrParam[$x]['icon']; ?> fa-3x text-primary"></i></span>
                    </div>
                    <canvas width="100" height="100"></canvas>
                <canvas width="100" height="100"></canvas>
            </div>
            </div>
        </div>
        <div class="text-center innerAll inner-2x bg-gray">
            <p class="lead margin-none">
                <span class="text-large text-regular"><?php echo $arrParam[$x]['number']; ?></span>
                <span class="clearfix"></span>
                <span class="text-primary"><?php echo $arrParam[$x]['label']; ?></span>
            </p>
        </div>
    </div>
</div>
<?php
$x++;
}
?>
</div>
<?php
}



function filterBarWithParams($arrParam){
?>
<div class="filter-bar">
    <form class="margin-none form-inline">
<?php
$count = count($arrParam);
$x = 0;
while($x < $count){
?>
 <div class="form-group col-md-2 padding-none">
    <label><?php echo $arrParam[$x]['label']; ?></label>
    <div class="input-group">
        <input type="text" name="<?php echo $arrParam[$x]['input_name']; ?>" id="<?php echo $arrParam[$x]['input_name']; ?>" class="form-control" value="<?php echo $arrParam[$x]['value']; ?>" />
        <span class="input-group-addon"><i class="fa <?php echo $arrParam[$x]['icon']; ?>"></i></span>
    </div>
</div>
<?php
$x++;
}
?>
    </form>
</div>
<?php
}
/*
* filterBar()
* param @arrayUser
* @arrayUser 
*/
function filterBar(){
?>

                            <!-- Filters -->
<div class="filter-bar">
    <form class="margin-none form-inline">
        
       
        <div class="form-group col-md-2 padding-none">
            <label>De:</label>
            <div class="input-group">
                <input type="text" name="from" id="dateRangeFrom" class="form-control" value="08/05/13" />
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
        
        
        
        <div class="form-group col-md-2 padding-none">
            <label>Para:</label>
            <div class="input-group">
                <input type="text" name="to" id="dateRangeTo" class="form-control" value="08/18/13" />
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
        
        
        
        <div class="form-group col-md-2 padding-none">
            <label>Min:</label>
            <div class="input-group">
                <input type="text" name="from" class="form-control" value="100" />
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
            </div>
        </div>
        
        
        
        <div class="form-group col-md-2 padding-none">
            <label>Max:</label>
            <div class="input-group">
                <input type="text" name="from" class="form-control" value="500" />
                <span class="input-group-addon"><i class="fa fa-money"></i></span>
            </div>
        </div>

        

        <div class="form-group col-md-3 padding-none">
            <label class="label-control">Opção:</label>
            <div class="col-md-8 padding-none">
                <select name="from" class="form-control">
                    <option>Alguma opçao</option>
                </select>
            </div>
        </div>

        
        <div class="clearfix"></div>
    </form>
</div>


<?php
}

function filterBarSimulado($provaid, $itemsPerPage, $paginatorID, $lastID){
?>
<div class="filter-bar">
    <form action="" onsubmit="return false" class="margin-none form-inline">
        <div class="form-group col-md-2 padding-none">
            <label class="label-control">Questões por página:</label>
            <div class="col-md-2 padding-none">
                <select name="perpage" id="paginator<?php echo $paginatorID; ?>" class="form-control">
                    <option>5</option>
                    <option>10</option>
                    <option>15</option>
                    <option>20</option>
                </select>
            </div>
        </div>

        <div class="form-group col-md-4 padding-none" style="margin-left:25px;">
            <div class="col-md-3 padding-none">
                <button onclick="redirect_simulado_per_page(<?php echo $provaid; ?>, <?php echo $paginatorID; ?>)" class="btn btn-primary">Ok</button>
            </div>
        </div>



        <div style="float:right; margin-left:10px">
            <div class="col-md-3 padding-none">
                <button onclick="redirect_simulado_next_page(<?php echo $provaid; ?>, <?php echo $itemsPerPage; ?>, '<?php echo $lastID; ?>')" class="btn btn-primary">Próxima Página</button>
            </div>
        </div>

         <div style="float:right; margin-left:10px">
            <div class="col-md-3 padding-none">
                <button onclick="redirect_simulado_end(<?php echo $provaid; ?>)" class="btn btn-info">Finalizar Simulado</button>
            </div>
        </div>
        
        <div class="clearfix"></div>
    </form>
</div>

<?php
}




function filterBarSimuladoFREE($provaid, $itemsPerPage, $paginatorID){
?>
<div class="filter-bar">
    <form action="" onsubmit="return false" class="margin-none form-inline">
        <div class="form-group col-md-2 padding-none">
            <label class="label-control">Questões por página:</label>
            <div class="col-md-2 padding-none">
                <select name="perpage" id="paginator<?php echo $paginatorID; ?>" class="form-control">
                    <option>5</option>
                    <option>10</option>
                    <option>15</option>
                    <option>20</option>
                </select>
            </div>
        </div>

        <div class="form-group col-md-4 padding-none" style="margin-left:25px;">
            <div class="col-md-3 padding-none">
                <button onclick="redirect_simulado_per_page_free(<?php echo $provaid; ?>, <?php echo $paginatorID; ?>)" class="btn btn-primary">Ok</button>
            </div>
        </div>

         <div  style=" float:right">
            <div class="col-md-3 padding-none">
                <button onclick="redirect_simulado_free(<?php echo $provaid; ?>, <?php echo $itemsPerPage; ?>)" class="btn btn-primary">Próxima Página</button>
            </div>
        </div>

        
        <div class="clearfix"></div>
    </form>
</div>

<?php
}



function modal_timeline_image()
{
?>
<div class="modal fade" id="timeline-modal-image">
    <div class="modal-dialog">
    <div class="col-md-10">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Inserir Imagem</h3>
            </div>
<div class="modal-body">
    <div class="innerAll">
        <div class="innerLR">
            <div class="row">                        
                <form id="timelineForm" action="<?php echo BASEURL; ?>timeline/timeline_modal_image" method="post" enctype="multipart/form-data">
                        <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-new">Escolher Arquivo</span>
                                        <span class="fileupload-exists">Mudar</span>
                                        <input type="file" name="file_upload" id="file_upload" class="margin-none"/>
                                    </span>
                                    <span class="fileupload-preview"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-10">
                                    <input type="hidden" name="update" value="photo">
                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none;">&times;</a>         
                                </div>
                            </div>

                             <div class="form-group">
                                <div class="col-sm-10">
                                     <button class="btn btn-primary">Enviar</button>
                                </div>
                             </div>

                              <div class="form-group">
                                <div class="col-sm-10">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-primary" id="bar1"></div>
                                    </div>
                                    <div id="retorno"></div>
                                </div>
                             </div>
                        </div>
                </form>
                </div>
        </div>
    </div>
</div>
        </div>
    </div>
</div>
</div>
<?php
}


function searchUserPagination($count){
    $liHTML = '';
    $x = 1;
    while($x <= $count){
        $liHTML .= '<li><a href="#">'.$x.'</a></li>';
        $x++;
    }
?>
<div class="col-md-12 col-lg-12">
    <div class="widget widget-heading-simple widget-body-gray">
        <div class="widget-body" data-toggle="source-code">
            <div class="margin-none">
            <ul class="pagination margin-none">
                <?php echo $liHTML; ?>
            </ul>
            </div>
        </div>
    </div>
</div>
<?php
}




function friends_follow($arrParam){
if(count($arrParam)>0){
?>
<div class="widget">
    <h5 class="innerAll margin-none border-bottom bg-gray">Seus Amigos</h5>
    <div class="widget-body padding-none" style="display:table;">
    <?php foreach($arrParam as $users): ?>
        <div style="float:left;margin:1px;">
            <a href="<?php echo BASEURL;?>usuario/perfil/<?php echo $users['usuarioid']; ?>">
            <img src="<?php echo $users['foto_perfil']; ?>"class="pull-left media-object" height="40" width="43" title="<?php echo $users['nome']; ?>">
            </a>
        </div>
    <?php endforeach; ?>        
    </div>
</div>

<?php
}
}










#end helper
