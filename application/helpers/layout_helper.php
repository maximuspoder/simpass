<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function footer(){
?>
 <div id="footer" class="hidden-print">
    <div class="copy">&copy; 2014 - <a href="http://www.simuleepasse.com.br">Simule & Passe</a> -
        Todos os direitos reservados. <a href="http://www.simuleepasse.com.br/sobre"
        target="_blank">Saiba mais sobre nós</a> - Versão atual:
        v1.0.1-rc1 / <a target="_blank" href="path-to-version">log de mudanças</a>
    </div>
</div>

<?php
}

function userHEADER($usuario){
?>

<div class="user-action pull-left menu-right-hidden-xs menu-left-hidden-xs border-left">
    <div class="dropdown username pull-left">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <span class="media margin-none">
                <span class="pull-left">
                    <img src="http://icons.iconarchive.com/icons/visualpharm/icons8-metro-style/128/User-Role-Guest-icon.png" alt="user"
                    class="img-circle">
                </span>
                <span class="media-body"><?php echo ext($usuario[0], 'nome') . " " . ext($usuario[0], 'sobrenome'); ?>
                    <span class="caret"></span>
                </span>
            </span>
        </a>
        <ul class="dropdown-menu">
            <li><a href="">Perfil</a></li>
            <li><a href="<?php echo BASEURL; ?>login/logout">Logout</a></li>
        </ul>
    </div>
</div>

<div class="input-group hidden-xs pull-left">
    <span class="input-group-addon"><i class="icon-search"></i>
    </span>
    <input type="text" class="form-control" placeholder="Pesquisar" />
</div>

<?php
}
function menuUSER($nivel = null){
?>
<div id="menu" class="hidden-print hidden-xs sidebar-default sidebar-brand-primary">
            <div id="sidebar-social-wrapper">
                <div id="brandWrapper">
                    <div class="sep-logo"><img src="<?php echo IMG; ?>logo-sep.png"></div>
                </div>
                <ul class="menu list-unstyled" id="navigation_current_page">
                    
                    <li class="hasSubmenu">
                        <a href="<?php echo BASEURL; ?>usuario">
                            <i class="icon-home-2"></i><span>Home do site</span>
                        </a>
                    </li>
                    
                     <li class="hasSubmenu">
                        <a href="#sidebar-professor" data-toggle="collapse">
                        <i class="icon-compose"></i><span>Professor</span></a>
                        <ul id="sidebar-professor" class="collapse">
                            <li class="active">
                                <a href="<?php echo BASEURL; ?>professor"><i class="icon-document-add"></i>
                                    <span>Questões</span>
                                </a>
                            </li>
                             <li class="active">
                                <a href="<?php echo BASEURL; ?>professor/questoes_selecionadas"><i class="icon-document-check"></i>
                                    <span>Selecionadas</span>
                                </a>
                            </li>
                        </ul>
                        
                    </li>
                    

                     <li class="hasSubmenu">
                        <a href="<?php echo BASEURL; ?>login/logout">
                            <i class="icon-user-1"></i><span>Logout</span>
                        </a>                        
                    </li>
              
                </ul>
            </div>
        </div>
 <?php
}

function menuDashboard(){
?>


<div id="menu" class="hidden-print hidden-xs sidebar-default sidebar-brand-primary">
            <div id="sidebar-social-wrapper">
                <div id="brandWrapper">
                    
                        <div class="sep-logo"><img src="<?php echo IMG; ?>logo-sep.png"></div>
                    
                </div>
                <ul class="menu list-unstyled" id="navigation_current_page">
                    
                    <li class="hasSubmenu">
                        <a href="#sidebar-fusion-layout" data-toggle="collapse"><i class="icon-compose"></i><span>Home</span></a>
                        <ul id="sidebar-fusion-layout" class="collapse">
                            <li class="active">
                                <a href="<?php echo BASEURL; ?>questoes/"><i class="icon-documents-check"></i>
                                    <span>Questões</span>
                                </a>
                            </li>
                            
                        </ul>
                    </li>

                     <li class="hasSubmenu">
                        <a href="<?php echo BASEURL; ?>login/logout">
                            <i class="icon-user-1"></i><span>Logout</span>
                        </a>                        
                    </li>
              
                </ul>
            </div>
        </div>

<?php
}
function arrayTable($arrayHead, $arrayDados){
?>
<table class="dynamicTable tableTools table table-striped checkboxs">
    <thead>
        <tr>
            <?php foreach($arrayHead as $header => $dados): ?>
                <td><?php echo $dados; ?></td>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach($arrayDados as $conteudo): ?>
            <tr>
                <td><?php echo $conteudo; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php

}



function teacherMenu($nivel){
?>
<div id="menu" class="hidden-print hidden-xs sidebar-default sidebar-brand-primary">
            <div id="sidebar-social-wrapper">
                <div id="brandWrapper">
                    <div class="sep-logo"><img src="<?php echo IMG; ?>logo-sep.png"></div>
                </div>
                <ul class="menu list-unstyled" id="navigation_current_page">
                    
                    <li class="hasSubmenu">
                        <a href="<?php echo BASEURL; ?>usuario">
                            <i class="icon-home-2"></i><span>Home</span>
                        </a>
                    </li>
                    <?php if($nivel == 1 || $nivel == 2){?>
                     <li class="hasSubmenu">
                        <a href="#sidebar-professor" data-toggle="collapse">
                        <i class="icon-compose"></i><span>Professor</span></a>
                        <ul id="sidebar-professor" class="collapse">
                            <li class="active">
                                <a href="<?php echo BASEURL; ?>professor"><i class="icon-document-add"></i>
                                    <span>Questões</span>
                                </a>
                            </li>
                             <li class="active">
                                <a href="<?php echo BASEURL; ?>professor/questoes_selecionadas"><i class="icon-document-check"></i>
                                    <span>Selecionadas</span>
                                </a>
                            </li>
                        </ul>
                        
                    </li>
                    <?php } ?>

                     <li class="hasSubmenu">
                        <a href="<?php echo BASEURL; ?>login/logout">
                            <i class="icon-user-1"></i><span>Logout</span>
                        </a>                        
                    </li>
              
                </ul>
            </div>
        </div>
 <?php
}

function teacherHEADER($nome, $sobrenome){
?>

<div class="user-action pull-left menu-right-hidden-xs menu-left-hidden-xs border-left">
    <div class="dropdown username pull-left">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <span class="media margin-none">
                <span class="pull-left">
                    <img src="http://icons.iconarchive.com/icons/visualpharm/icons8-metro-style/128/User-Role-Guest-icon.png" alt="user"
                    class="img-circle">
                </span>
                <span class="media-body"><?php echo $nome . " " . $sobrenome; ?>
                    <span class="caret"></span>
                </span>
            </span>
        </a>
        <ul class="dropdown-menu">
            <li><a href="<?php echo BASEURL; ?>login/logout">Logout</a></li>
        </ul>
    </div>
</div>
<?php
}


function adminMenu($nivel){
?>
<div id="menu" class="hidden-print hidden-xs sidebar-default sidebar-brand-primary">
            <div id="sidebar-social-wrapper">
                <div id="brandWrapper">
                    <div class="sep-logo"><img src="<?php echo IMG; ?>logo-sep.png"></div>
                </div>
                <ul class="menu list-unstyled" id="navigation_current_page">
                    
                    <li class="hasSubmenu">
                        <a href="<?php echo BASEURL; ?>administrador">
                            <i class="icon-home-2"></i><span>Home</span>
                        </a>
                    </li>
                    <?php if($nivel == 1){?>
                     <li class="hasSubmenu">
                        <a href="#sidebar-professor" data-toggle="collapse">
                        <i class="icon-compose"></i><span>Professores</span></a>
                        <ul id="sidebar-professor" class="collapse">
                            <li class="active">
                                <a href="<?php echo BASEURL; ?>administrador/gravacoes"><i class="icon-document-add"></i>
                                    <span>Gravações</span>
                                </a>
                            </li>

                            
                        </ul>
                        
                    </li>
                    <?php } ?>

                     <li class="hasSubmenu">
                        <a href="<?php echo BASEURL; ?>login/logout">
                            <i class="icon-user-1"></i><span>Logout</span>
                        </a>                        
                    </li>
              
                </ul>
            </div>
        </div>
 <?php
}