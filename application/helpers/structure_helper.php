<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
*
* package: helper.structure
* date: 21-08-2013
* by: Fernando
*
*/
function sessionID(){
  $CI =& get_instance();
  return $CI->session->userdata('token');
}

function sessionNivel(){
  $CI =& get_instance();
  return $CI->session->userdata('nivel');
}


/**
* POST()
* Recebe um post via form
* @return void
*/
function POST()
{
  $CI =& get_instance();
  return $CI->input->post();
}

/**
* VIEW()
* Chama view, modo simplificado
* @param @view, $param
* @return void
*/
function VIEW($view, $param = null)
{
  $CI =& get_instance();
  if($param != null)
  {
    $CI->load->view($view, $param); 
  } else {
    $CI->load->view($view);
  }
}

/**
* MODEL()
* Chama view, modo simplificado
* @param @view, $param
* @return void
*/
function MODEL($model)
{
  $CI =& get_instance();

  $CI->load->model($model);  
}

/**
* redirect()
* Redireciona para outro lugar
* @param string $to
*/
function redirect($to = null)
{ 
  header('location: ' . BASEURL . $to);
}


/**
* ext()
* Extrai valor do array
* @param array data
* @param string index
* @return mixed value
*/
function ext($data = null, $index = null)
{
	$value = '';
	if(!is_null($data) && !is_null($index) && is_array($data))
	{
		// passa os indices do array encaminhado como param para lower case
		$arr_aux = array_change_key_case($data, CASE_LOWER);
		$value = (isset($arr_aux[strtolower($index)])) ? $arr_aux[strtolower($index)] : '';
	}
	return $value;
}



/* 
 * combo
 *
 * @access  public
 * @param $array => Array com os valores
 * @param $key => value para o select
 * @param $label => label de visualização
 * @param $val_selected => id do item selecionado, null
 * @return  string nome da tabela com dados de acordo com o tipo do usuário
 */
function combo($array, $key, $label, $val_selected = null)
{
  $html = '<option></option>';

  foreach ($array as $value) 
  {
    $selected = ($val_selected == $value["$key"]) ? 'selected="selected"' : '';
    $html .= "<option value='" . $value["$key"] . "' $selected >" . $value["$label"] . "</option>";
  }
  return $html;
}

/* 
 * combo
 *
 * @access  public
 * @param $array => Array com os valores
 * @param $key => value para o select
 * @param $label => label de visualização
 * @param $val_selected => id do item selecionado, null
 * @return  string nome da tabela com dados de acordo com o tipo do usuário
 */
function comboJson($json, $key, $label, $val_selected = null)
{
  $html = '<option></option>';

  foreach ($json as $value) 
  {
    $selected = ($val_selected == $value->{"$key"}) ? 'selected="selected"' : '';
    $html .= "<option value='" . $value->{"$key"} . "' $selected >" . $value->{"$label"} . "</option>";
  }
  return $html;
}


/* 
 * retornames
 *
 * @access  public
 * @param $mes => mes
 * @return  string nome do mês
 */
function retornames($mes)
{
  if($mes == '01')
  {
    return 'Janeiro';
  } 
  else if($mes == '02')
  {
    return 'Fevereiro';
  }
  else if($mes == '03')
  {
    return 'Março';
  }
  else if($mes == '04')
  {
    return 'Abril';
  }
  else if($mes == '05')
  {
    return 'Maio';
  }
  else if($mes == '06')
  {
    return 'Junho';
  }
  else if($mes == '07')
  {
    return 'Julho';
  }
  else if($mes == '08')
  {
    return 'Agosto';
  }
  else if($mes == '09')
  {
    return 'Setembro';
  }
  else if($mes == '10')
  {
    return 'Outubro';
  }
  else if($mes == '11')
  {
    return 'Novembro';
  }
  else if($mes == '12')
  {
    return 'Dezembro';
  }
}

/* 
 * string_aleatoria()
 * Cria uma string aleatoria
 * @access  public
 * @param 
 * @return  string
 */
function string_aleatoria($tamanho) {
  $conteudo = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  $string = '';
  for($i=0;$i<$tamanho;$i++) {
    $string .= $conteudo{rand(0,35)};
  }

  return $string;
}


/* 
 * getAttachmentFile()
 * Verifica se o arquivo existe em um diretório
 * @access 
 * @param 
 * @ret
 */
function getAttachmentFile($folder, $files, $formats){

  $x = 0;
  $arrayResult = array();
  while($x < count($files))
  {
      $y = 0;
      while($y < count($formats))
      {
        if(file_exists(UPLOAD . $folder . $files[$x] . $formats[$y])){
          $arrayResult[] =  UPLOADLINK . $folder . $files[$x] . $formats[$y];
        }
        $y++;
      }
    $x++;
  }
  return $arrayResult;
}

function number_options($count){
  $x = 0;
  $html = '<option></option>';
  while($x < $count){
    $html .= '<option>'. $x . '</option>';
    $x++;
  }
  return $html;
}

function calendar_month()
{
?>
  <option></option>
  <option value="01">Janeiro</option>
  <option value="02">Fevereiro</option>
  <option value="03">Março</option>
  <option value="04">Abril</option>
  <option value="05">Maio</option>
  <option value="06">Junho</option>
  <option value="07">Julho</option>
  <option value="08">Agosto</option>
  <option value="09">Setembro</option>
  <option value="10">Outubro</option>
  <option value="11">Novembro</option>
  <option value="12">Dezembro</option>
<?php
}


function calendar_year()
{
?>
  <option></option>
  <option>2014</option>
  <option>2013</option>
  <option>2012</option>
  <option>2011</option>
  <option>2010</option>
  <option>2009</option>
  <option>2008</option>
  <option>2007</option>
  <option>2006</option>
  <option>2005</option>
  <option>2004</option>
  <option>2003</option>
  <option>2002</option>
  <option>2001</option>
  <option>2000</option>
  <option>1999</option>
  <option>1998</option>
  <option>1997</option>
  <option>1996</option>
  <option>1995</option>
  <option>1994</option>
  <option>1993</option>
  <option>1992</option>
  <option>1991</option>
  <option>1990</option>
  <option>1989</option>
  <option>1988</option>
  <option>1987</option>
  <option>1986</option>
  <option>1985</option>
  <option>1984</option>
  <option>1983</option>
  <option>1982</option>
  <option>1981</option>
  <option>1980</option>
  <option>1979</option>
  <option>1978</option>
  <option>1977</option>
  <option>1976</option>
  <option>1975</option>
  <option>1974</option>
  <option>1973</option>
  <option>1972</option>
  <option>1971</option>
  <option>1970</option>
  <option>1969</option>
  <option>1968</option>
  <option>1967</option>
  <option>1966</option>
  <option>1965</option>
  <option>1964</option>
  <option>1963</option>
  <option>1962</option>
  <option>1961</option>
  <option>1960</option>
  <option>1959</option>
  <option>1958</option>
  <option>1957</option>
  <option>1956</option>
  <option>1955</option>
  <option>1954</option>
  <option>1953</option>
  <option>1952</option>
  <option>1951</option>
  <option>1950</option>
  <option>1949</option>
  <option>1948</option>
  <option>1947</option>
  <option>1946</option>
  <option>1945</option>
  <option>1944</option>
  <option>1943</option>
  <option>1942</option>
  <option>1941</option>
  <option>1940</option>
<?php
}

function calendar_days(){

  $day = cal_days_in_month(CAL_GREGORIAN, 5, 2014);
  $html = '<option></option>';
  
  for($x = 1; $x <= $day; $x++){
    $html .= '<option value="'. $x .'">' . $x . '</option>';
  }#end for
  return $html;
}

function generate_token($add){
  $concatenate = string_aleatoria(100);
  // Token para uso geral
  $subtoken = md5(date('Y-m-d H:i:s') . HASH . '2014-S&P' . $concatenate . $add);
  define('subtoken',  $subtoken);
  $token = subtoken . md5('simuleepasse-2014-building-security');
  define('token', substr($token, 0, 40));
  return token;
}

function generate_token_unique($add){
  $concatenate = string_aleatoria(100);
  // Token para uso geral
  $subtoken = md5(date('Y-m-d H:i:s') . HASH . '2014-S&P' . $concatenate . $add);

  $token = $subtoken . md5($add);
  $token = substr($token, 0, 40);
  return $token;
}

function restApi($Config = array()){

    $cURL = curl_init($Config['url'] . $Config['token']);
    curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($cURL);
    curl_close($cURL);

    $obj = json_decode($result, true);
    return $obj;
}


function restApiReturnJson($token){
  if(file_exists(SERVER . 'application/assets/internaljson/' . $token . '.json')){
                
        $handle = fopen(SERVER . 'application/assets/internaljson/' . $token . '.json', "r+");
        $json = stream_get_contents($handle);
        fclose($handle);      
        // Return json
        $user = json_decode($json);
        return $user;
      }
}

function LoadJSON($folder, $file){

  if(file_exists(SERVER . 'application/assets/internaljson/' . $folder . '/' . $file . '.json')){
                
        $handle = fopen(SERVER . 'application/assets/internaljson/' . $folder . '/' . $file . '.json', 'r+');
        $json = stream_get_contents($handle);
        fclose($handle);      
        // Return json
        $json = json_decode($json);
        return $json;
  } else {
    echo 0;
  }
}

function restApiUpdateJson($token, $field, $value){
  if(file_exists(SERVER . 'application/assets/internaljson/' . $token . '.json')){
                
        $data = file_get_contents(SERVER . 'application/assets/internaljson/' . $token . '.json');
     
        // Return json
        $jsonUpdate = json_decode($data);

        $jsonUpdate[0]->{"$field"} = $value;

        $jsonEncode = json_encode($jsonUpdate, true);

        file_put_contents(SERVER . 'application/assets/internaljson/' . $token . '.json', $jsonEncode);
  }
}


function send_mailgun($arrParam){

  $to_mail  = $arrParam['to_mail'];

  $to_name  = $arrParam['to_name'];
  $subject  = $arrParam['subject'];
  $message  = $arrParam['message'];



  $ch =   curl_init();
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_USERPWD, 'api:key-6wt7avshuw3zyrjbxx9txpbnhumb5n80');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
  curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v2/simuleepasse.com.br/messages');
  curl_setopt($ch, CURLOPT_POSTFIELDS, 
                array('from' => 'Simule & Passe <postmaster@simuleepasse.com.br>',
                      'to' => "$to_name <$to_mail>",
                      'subject' => "$subject",
                      'text' => "$message",
                      'html' => "<html>$message</html>"
                    )
                );

  $result = curl_exec($ch);
  curl_close($ch);
  return $result;
}

function message($type, $message, $style = null){
?>
<div class="alert alert-<?php echo $type; ?>" style="<?php echo $style; ?>">
  <?php echo $message; ?>
</div>
<?php
}

function pagseguroCheckStatus($arrParam){
  error_reporting(0);
  $CI = get_instance();
  $CI->load->model('system_model');
  //die();
  foreach($arrParam as $transactions){
    $xml = simplexml_load_file('https://ws.pagseguro.uol.com.br/v2/transactions/'.  $transactions['transactionid'] .'?email=simuleepasse@gmail.com&token=DBC2E196CE12413886672B9AA1C02D57');
    if($xml){
      if($xml->status == 3){
        $where = array('assinaturaid' => $transactions['assinaturaid']);
        $paramUpdate['status'] = 3;
        $paramUpdate['data_retorno'] = "{$xml->lastEventDate}";

          if($CI->system_model->update('sep_assinaturas', $paramUpdate, $where)){
            $arrParaDados['plano'] = 'S';
            $where = array('token' => $transactions['token']);
              if($CI->system_model->update('sis_usuario_dados', $arrParaDados, $where)){
                  restApiUpdateJson($transactions['token'], 'plano', 'S');
                  return 1;
              }
          }
      }

    } else {
      $where = array('assinaturaid' => $transactions['assinaturaid'] );
      //$CI->system_model->delete('sep_assinaturas', $where);
    }
  }
}

function generateLOG($token, $event, $description = null){
  date_default_timezone_set("America/Sao_Paulo");
  $CI = get_instance();
  $CI->load->model('system_model');
  $arrParam['token']  = $token;
  $arrParam['evento'] = $event;
  $arrParam['descricao'] = ($description == null)? null : $description;
  $arrParam['data_registro'] = date('Y-m-d H:i:s');

  error_reporting(0);
  $CI->system_model->insert('sep_logs', $arrParam);
}



 
function encrypt($sData, $sKey='mysecretkey'){ 
    $sResult = ''; 
    for($i=0;$i<strlen($sData);$i++){ 
        $sChar    = substr($sData, $i, 1); 
        $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1); 
        $sChar    = chr(ord($sChar) + ord($sKeyChar)); 
        $sResult .= $sChar; 
    } 
    return encode_base64($sResult); 
} 

function decrypt($sData, $sKey='mysecretkey'){ 
    $sResult = ''; 
    $sData   = decode_base64($sData); 
    for($i=0;$i<strlen($sData);$i++){ 
        $sChar    = substr($sData, $i, 1); 
        $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1); 
        $sChar    = chr(ord($sChar) - ord($sKeyChar)); 
        $sResult .= $sChar; 
    } 
    return $sResult; 
}

function encode_base64($sData){
  $sBase64 = base64_encode($sData);
    return str_replace('=', '', strtr($sBase64, '+/', '-_'));
}

function decode_base64($sData){
  $sBase64 = strtr($sData, '-_', '+/');
  return base64_decode($sBase64.'==');
}

function timestamp_decode_date($timestamp){
  $date = explode('-', $timestamp);
  $time = explode(' ', $date[2]);
  $callback = $time[0] . '/' . $date[1] . '/' . $date[0];
  return $callback;
}
function timestamp_decode_time($timestamp){
  $date = explode('-', $timestamp);
  $time = explode(' ', $date[2]);
  $callback = $time[1];
  return $callback;
}
function timestamp_decode_day($timestamp){
  $date = explode('-', $timestamp);
  $time = explode(' ', $date[2]);
  $callback = $time[0];
  return $callback;
}

function hour_ago($timestamp){
  date_default_timezone_set('America/Sao_Paulo');
  
  $date     = explode('-', $timestamp);
  $time     = explode(' ', $date[2]);
  $callback =  $time[0] . '/' . $date[1] . '/' . $date[0] . ' '. $time[1];
  echo $callback;
}


function create_url($string){
  
   $map = array(
    'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e', 'ê' => 'e', 'í' => 'i', 'ó' => 'o',
    'ô' => 'o', 'õ' => 'o', 'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
    'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U',
    'Ü' => 'U','Ç' => 'C','!' => '','@' => '','#' => '','$' => '','%' => '','&' => '','*' => '',
    '(' => '',')' => '','+' => '','=' => '','{' => '','}' => '', '[' => '', ']' => '', ':' => '',
    ';' => '','/' => '','|' => '','?' => '',' ' => '-','"' => '', ',' => ''
);
 
echo strtr($string, $map); 
}


function debug($args){
    echo "<pre>";
    print_r($args);
    die();
}


/*
 * to_float()
 * Converte de formato em reais para formato flutuante.
 * @param string valor
 * @return double new_valor
 */
function to_float($valor)
{
    $valorAux = '';
    if($valor != '')
    {
        if(strpos($valor, ','))
        {
            $valorAux = str_replace('.', '',  $valor);
            $valorAux = str_replace(',', '.', $valorAux);
        } else {
            $valorAux = $valor;
        }
        if(is_numeric($valorAux))
        {
            return $valorAux;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}



/**
 * replace_tag()
 * Faz a substituição do valor <NUMERO_COLUNA> em uma string por um valor de
 * um íncide de array.
 * @param string value
 * @param array data
 * @return string new_string
 */
function replace_tag($value = null, $arr_data = array())
{
    // Casa todas as ocorrencias de {TAG_A-Z0-9a-z_-}
    preg_match_all('/{[0-9a-zA-Z_-]+}/', $value, $matches);

    // Varre todas as ocorrencias, substituindo valor do array por tag
    foreach($matches as $match)
    {
        $counter = count($match);
        for($i = 0; $i < $counter; $i++)
        {
            // Coleta o indice do array
            $index = str_replace('}', '', str_replace('{','', $match[$i]));

            // coleta o indice do array por numero, caso tag == numero
            $data = (valida_integer($index)) ? array_values($arr_data) : $arr_data;

            // Faz replace da tag por valor
            $value = str_replace($match[$i], get_value($data, $index), $value);
        }
    }
    return $value;
}

/**
 * valida_integer()
 * Verifica se um valor encaminhado é unicamente inteiro.
 * @param int number
 * @return boolean Valid
 */
function valida_integer($number = null)
{
    if(!is_null($number))
    {
        if(preg_match("/^-?[0-9]+$/", $number))
        {
            return true;
        }
    }
    return false;
}

/**
 * get_value()
 * Retorna um valor de um array, idependentemente do INDEX existir ou não.
 * @param array data
 * @param string index
 * @return mixed value
 */
function get_value($data = null, $index = null)
{
    $value = '';
    if(!is_null($data) && !is_null($index) && is_array($data))
    {
        // passa os indices do array encaminhado como param para lower case
        $arr_aux = array_change_key_case($data, CASE_LOWER);
        $value = (isset($arr_aux[strtolower($index)])) ? $arr_aux[strtolower($index)] : '';
    }
    return $value;
}


/**
 * valida_alfa()
 * Valida uma sequencia de caracteres, especificando se estes são
 * alfa-numéricos ou não. Aceita espaço.
 * @param string word
 * @return boolean valid
 */
function valida_alfa($word = null)
{
    if(!is_null($word))
    {
        if(preg_match("/^[a-zA-Z0-9,á,ã,à,í,ó,ú,é,ê,ç,õ,ó\s]+$/", $word))
        {
            return true;
        }
    }
    return false;
}

/**
 * valida_float()
 * Valida se um número encaminhado é float em seu formato, ou não.
 * @param double number
 * @return boolean valid
 */
function valida_float($number = null)
{
    if(!is_null($number))
    {
        if(preg_match("/^-?[0-9]+(\.[0-9]+)?$/", $number))
        {
            return true;
        }
    }
    return false;
}

/**
 * valida_currency()
 * Valida se um número encaminhado está em formato moeda, com base na localização atual.
 * @param double number
 * @return boolean valid
 */
function valida_currency($number = null)
{
    if(!is_null($number))
    {
        if(preg_match("/^-?[0-9\\.]+([\\,0-9]+)?$/", $number))
        {
            return true;
        }
    }
    return false;
}

/**
 * valida_array_empty_values()
 * Valida se todos os elementos de um array estão com valores nulos ou vazios. Retorna booleano.
 * @param array data
 * @return boolean
 */
function valida_array_empty_values($data = array())
{
    if(count($data) > 0)
    {
        foreach($data as $k => $v)
        {
            if($v != '' && !is_null($v)) { return false; }
        }
    }
    return true;
}

/**
 * get_mensagem()
 * Exibe mensagem customizada, conforme o tipo iformado.
 * @param string tipo
 * @param string titulo
 * @param string descricao
 * @param boolean close
 * @param string style
 * @return void
 */
function get_mensagem($type = 'info', $title = '', $description = '', $close = false, $style = '')
{
    $id = uniqid();
    $type = ($type != 'info' && $type != 'warning' && $type != 'error' && $type != 'success' && $type != 'note') ? 'info' : $type;
    $html  = '<div id="message_general_' . $id . '">';
    $html .= '<div class="msg_general ' . $type . '" ' . (($style != '') ? 'style="' . $style . '"' : '') . '>';
    $html .= '<div>';
    $html .= ($close) ? '<img src="' . IMG . '/icon_close.png" class="close" title="Fechar" onclick="javascript:$(\'#message_general_' . $id . '\').hide();" />' : '';
    $html .= '<img src="' . IMG . '/icon_' . $type . '.png" />';
    $html .= ($title != '') ? '<h5>' . $title . '</h5>' : '';
    $html .= '<div>' . $description . '</div>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}

/**
 * mensagem()
 * Exibe mensagem customizada, conforme o tipo iformado.
 * @param string tipo
 * @param string titulo
 * @param string descricao
 * @param boolean close
 * @param string style
 * @return void
 */
function mensagem($type = 'info', $title = '', $description = '', $close = false, $style = '')
{
    echo(get_mensagem($type, $title, $description, $close, $style));
}
