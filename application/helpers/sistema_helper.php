<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function appMenu(){
?>
 <div id="menu">
        <ul>
            <li class="">
                <a href="<?php echo BASEURL; ?>usuario"><i class="icon-newspaper"></i> Timeline</a>
            </li>
            <li class="">
                <a href="<?php echo BASEURL; ?>usuario/perfil"><i class="icon-user-1"></i> Meu Perfil</a>
            </li>
            <li class="">
                <a href="<?php echo BASEURL; ?>usuario/amigos"><i class="fa fa-group"></i> Amigos</a>
            </li>
            <li class="">
                <a href="<?php echo BASEURL; ?>usuario/professores"><i class="fa fa-group"></i> Professores</a>
            </li>
            <li class="">
                <a href="messages.html"><i class=" icon-comment-heart-fill"></i> Comunidades</a>
            </li>
            <li class="hasSubmenu">
                <a href="#collapse" data-toggle="collapse">
                    <i class="fa fa-chevron-circle-down"></i> Simulados
                </a>
                <ul class="collapse" id="collapse">
                    <li>
                        <a href="#"><i class="fa fa-circle-o"></i> Novo </a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-circle-o"></i> Meu Histórico </a>
                    </li>
                </ul>
            <li class="">
                <a href="messages.html"><i class="icon-task-check"></i> Fórum</a>
            </li>
            </li>
            </li>
            <li class=""><a href="<?php echo BASEURL; ?>login/logout"><i class="icon-lock-fill"></i> Sair</a></li>

            <li class="category border top">Últimas Atualizações</li>
            <li class="reset">
                <ul>
                    <li class="media news-item">
                        <span class="news-item-success pull-right "><i class="fa fa-circle"></i></span>
                <span class="pull-left media-object">
                    <i class="fa fa-fw fa-bell"></i>
                </span>
                        <div class="media-body">
                            <a href="" class="text-white">fernando</a> Acesso ou sistema
                            <span class="time">2 minutos atrás</span>
                        </div>
                    </li>
                    <li class="media news-item">
                        <span class="news-item-success pull-right "><i class="fa fa-circle"></i></span>
                <span class="pull-left media-object">
                    <i class="fa fa-fw fa-bell"></i>
                </span>
                        <div class="media-body">
                            <a href="" class="text-white">fernando</a> Acesso ou sistema
                            <span class="time">2 minutos atrás</span>
                        </div>
                    </li>
                    <li class="media news-item">
                <span class="pull-left media-object">
                    <i class="fa fa-fw fa-bell"></i>
                </span>
                        <div class="media-body">
                            <a href="" class="text-white">fernando</a> Acesso ou sistema
                            <span class="time">2 minutos atrás</span>
                        </div>
                    </li>
                    <li class="media news-item">
                <span class="pull-left media-object">
                    <i class="fa fa-fw fa-bell"></i>
                </span>
                        <div class="media-body">
                            <a href="" class="text-white">fernando</a> Acesso ou sistema
                            <span class="time">2 minutos atrás</span>
                        </div>
                    </li>
                    <li class="media news-item">
                <span class="pull-left media-object">
                    <i class="fa fa-fw fa-bell"></i>
                </span>
                        <div class="media-body">
                            <a href="" class="text-white">Fernando</a> Acesso ou sistema
                            <span class="time">2 minutos atrás</span>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
<?php
}

function widgetUsuario(){
?>
<br/>
<div class="panel panel-default profile-user-box">
    <div class="avatar">
        <center><img src="http://simuleepasse.com.br/application/assets/uploads/profile/2D8G0X8OOS_d49af1f49983272010e4476296163093_1_0_2_13_12_28.jpg" alt="" class="media-object" width="150"></center>
        <h3>Fernando Alves</h3>
    </div>
    <div class="profile-icons">
        <span><i class="fa fa-users"></i> 372 Amigos</span> 
    </div>
    <p>Uma pequena descrição sobre o que eu faço da vida, quem sou eu, e por ai vai.</p>
</div>
<?php
}

function widgetProfessor($arrParam){
?>
<br/>
<div class="panel panel-default profile-user-box">
    <div class="avatar">
        <center><img src="<?php echo $arrParam['profile']; ?>" alt="Foto Professor" class="media-object" width="150"></center>
        <h3><?php echo $arrParam['name']; ?></h3>
    </div>
    <div class="profile-icons">
        <span><i class="fa fa-video-camera"></i> 10 videos</span> 
    </div>
    <p><?php echo $arrParam['sobre']; ?></p>
</div>
<?php
}

function widgetAtividadesAmigos(){
?>
<div class="widget">
    <h5 class="innerAll margin-none border-bottom bg-gray">O que seus amigos estão fazendo</h5>
	<div class="widget-body padding-none" id="updates"></div>
</div>
<?php
}

function appMenuFeed($arrParam){
if(count($arrParam)>0){
?>

<li class="reset">
<div class="innerLR innerB border-bottom"> </div>
<ul>
	<?php foreach($arrParam as $users): ?>
    <li class="media news-item">
        <span class="news-item-success pull-right "></span>
        <span class="pull-left media-object">
            <i class="fa fa-fw fa-bell"></i>
        </span>
        <div class="media-body">
             <a href="<?php echo BASEURL . 'usuario/perfil/'. $users['idusuario']; ?>" class="text-white"><?php echo $users['nome']; ?></a> visualizou seu perfil
             <span class="time"><?php echo timestamp_decode_date($users['data_registro']); ?></span>
        </div>
    </li>
    <?php endforeach; ?>
</ul>

<?php
}
}



function chat(){
?>
   <ul class="chat-filter nav nav-pills ">
        <li class="active"><a href="#" data-target="li">All</a></li>
        <li><a href="#" data-target=".online">Online</a></li>
        <li><a href="#" data-target=".offline">Offline</a></li>
    </ul>
    <ul class="chat-contacts">
        <li class="online" data-user-id="1">
            <a href="#">
                <div class="media">
                    <div class="pull-left">
                        <span class="status"></span>
                        <img src="images/people/110/guy-6.jpg" width="40" class="img-circle"/>
                    </div>
                    <div class="media-body">

                        <div class="contact-name">Fernando Alves.</div>
                        <small>"Free Today"</small>
                    </div>
                </div>
            </a>
        </li>

        <li class="online away" data-user-id="2">
            <a href="#">

                <div class="media">
                    <div class="pull-left">
                        <span class="status"></span>
                        <img src="images/people/110/woman-5.jpg" width="40" class="img-circle"/>
                    </div>
                    <div class="media-body">
                        <div class="contact-name">Mary A.</div>
                        <small>"Feeling Groovy"</small>
                    </div>
                </div>
            </a>
        </li>
        <li class="online" data-user-id="3">
            <a href="#">
                <div class="media">
                    <div class="pull-left">
                        <span class="status"></span>
                        <img src="images/people/110/guy-3.jpg" width="40" class="img-circle"/>
                    </div>
                    <div class="media-body">
                        <div class="contact-name">Adrian D.</div>
                        <small>Busy</small>
                    </div>
                </div>
            </a>
        </li>
        <li class="offline" data-user-id="4">
            <a href="#">

                <div class="media">
                    <div class="pull-left">
                        <img src="images/people/110/woman-6.jpg" width="40" class="img-circle"/>
                    </div>
                    <div class="media-body">
                        <div class="contact-name">Michelle S.</div>
                        <small>Offline</small>
                    </div>
                </div>
            </a>
        </li>
    </ul>
</div>

<script id="chat-window-template" type="text/x-handlebars-template">

    <div class="panel panel-default">
        <div class="panel-heading" data-toggle="chat-collapse" data-target="#chat-bill">
            <a href="#" class="close"><i class="fa fa-times"></i></a>
            <a href="#"><img src="http://simuleepasse.com.br/application/assets/uploads/profile/2D8G0X8OOS_d49af1f49983272010e4476296163093_1_0_2_13_12_28.jpg" width="40" class="pull-left">
                <span class="contact-name">{{user}}</span>
            </a>
        </div>
        <div class="panel-body" id="chat-bill">
            <div class="media">
                <div class="pull-left">
                    <img src="{{user_image}}" width="25" class="img-circle" alt=""/>
                </div>
                <div class="media-body">
                    <span class="message">Td bem?</span>
                </div>
            </div>
            <div class="media right">
                <div class="pull-right">
                    <img src="{{user_image}}" width="25" class="img-circle" alt=""/>
                </div>
                <div class="media-body">
                    <span class="message">sim pq?.</span>
                </div>
            </div>
            <div class="media">
                <div class="pull-left">
                    <img src="{{user_image}}" width="25" class="img-circle" alt=""/>
                </div>
                <div class="media-body">
                    <span class="message">sei la.</span>
                </div>
            </div>
            <div class="media right">
                <div class="pull-right">
                    <img src="{{user_image}}" width="25" class="img-circle" alt=""/>
                </div>
                <div class="media-body">
                    <span class="message">vdd   !</span>
                </div>
            </div>


        </div>
        <input type="text" class="form-control" placeholder=""/>
    </div>
</script>

<div class="chat-window-container"></div>
<?php
}

function widgetPublicar(){
?>
    <br/>
    <div class="timeline-block" style="margin-bottom: 15px; zoom: 1; opacity: 1;">
    <div class="panel panel-default share">
        <div class="panel-heading title"> O que há de novo? </div>
        <div class="panel-body">
            <textarea name="status" class="form-control share-text" rows="3" placeholder="Compartilhe seus pensamentos..."></textarea>
        </div>
        <div class="panel-footer share-buttons">
            <a href="#"> <i class="fa fa-photo"></i> </a>
            <a href="#"> <i class="fa fa-video-camera"></i> </a>
            <button type="submit" class="btn btn-primary btn-xs pull-right display-none" href="#">Publicar</button>
        </div>
    </div>
    </div>
<?php
}

function widgetTimelineItem($params){
?>
    <div class="panel panel-default">
    
    <div class="panel-heading" style="background:#f4f4f4">
        <div class="media">
            <a href="" class="pull-left">
                <img src="<?php echo $params['profile_picture']; ?>" width="50" class="media-object">
            </a>
            <div class="media-body">
                <a href="" style="color:#666"><?php echo $params['name']; ?></a>
                <br/>
                <span style="color:#666"><?php echo $params['date']; ?></span>
            </div>
        </div>
    </div>
    
    <div class="panel-body">
        <p><?php echo $params['post']; ?></p>
    </div>
    <!--
    <div class="view-all-comments"><a href="#"><i class="fa fa-comments-o"></i> Ver todos</a>  x comentarios</div>
    -->
    <ul class="comments">
        <li>
            <div class="media">
                <a href="" class="pull-left">
                    <img src="http://simuleepasse.com.br/usuario" width="34" class="media-object">
                </a>
                <div class="media-body">
                    <a href="" class="comment-author">Julia</a>
                    <span>Concordo!</span>
                    <div class="comment-date">12:45</div>
                </div>
            </div>
        </li>
        <li>
            <div class="media">
                <a href="" class="pull-left"><img src="http://simuleepasse.com.br/application/assets/uploads/profile/2D8G0X8OOS_d49af1f49983272010e4476296163093_1_0_2_13_12_28.jpg" width="34" class="media-object"></a>
                <div class="media-body">
                    <a href="" class="comment-author">Fernando Alves.</a>
                    <span>vlw julia</span>
                    <div class="comment-date">13:15</div>
                </div>
            </div>
        </li>
        <li class="comment-form">
            <div class="media">
                <a href="" class="pull-left"><img src="http://simuleepasse.com.br/application/assets/uploads/profile/2D8G0X8OOS_d49af1f49983272010e4476296163093_1_0_2_13_12_28.jpg" width="34" class="media-object"></a>
                <div class="media-body">
                    <div class="input-group">
                        <input type="text" class="form-control">
                            <span class="input-group-addon">
                                   <a href=""><i class="fa fa-photo"></i></a>
                            </span>
                    </div>
                </div>
            </div>
        </li>
    </ul>

</div>
<?php
}

function widgetCoverProfile($arrParam){
?>
<div class="cover">
    <ul class="list-unstyled">
        <li <?php echo ($arrParam['active'] == 'timeline') ? 'class="active"': ''; ?>>
            <a href="<?php echo BASEURL; ?>usuario/perfil"><i class="fa fa-clock-o"></i> <span>Timeline</span></a>
        </li>
        <li <?php echo ($arrParam['active'] == 'sobre') ? 'class="active"': ''; ?>>
            <a href="<?php echo BASEURL; ?>usuario/sobre"><i class="fa fa-user"></i> <span>Sobre</span></a>
        </li>
        <li <?php echo ($arrParam['friends'] == 'amigos') ? 'class="active"': ''; ?>>
            <a href="<?php echo BASEURL; ?>usuario/amigos"><i class="fa fa-group"></i><span> Amigos </span><small>(19)</small></a>
        </li>
        <li <?php echo ($arrParam['active'] == 'mensagens') ? 'class="active"': ''; ?>>
            <a href="<?php echo BASEURL; ?>usuario/mensagens"><i class="fa fa-envelope"></i> <span>Mensagens</span></a>
        </li>
        <li <?php echo ($arrParam['active'] == 'historico') ? 'class="active"': ''; ?>>
            <a href="<?php echo BASEURL; ?>usuario/historico"><i class="fa fa-cog"></i> <span>Histórico</span></a>
        </li>
    </ul>
    <img src="<?php echo $arrParam['coverIMG']; ?>" alt="" class="img-responsive">
</div>
<?php
}