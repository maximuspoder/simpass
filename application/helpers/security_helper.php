<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
* date: 2014-01-14
* by: Fernando Alves
* description: helper responsable to get control
* to users level
*/

function get_nivel($metodo, $nivel)
{
    $CI =& get_instance();
    // Carrega model responsável
    $CI->load->model('security_model'); 
    // Realiza pesquisa no banco de dados
    $args['retorno'] = $CI->security_model->get_metodo($metodo, $nivel);

   	if(ext($args['retorno'][0], "nivel{$nivel}") == "sim"){
   		return 1;
   	} else {
   		return 0;
   	}
}

function validate_security_controller($metodo, $nivel)
{
    $CI =& get_instance();
    // Carrega model responsável
    $CI->load->model('security_model'); 
    // Realiza pesquisa no banco de dados
    $args['retorno'] = $CI->security_model->get_metodo($metodo, $nivel);

    if(ext($args['retorno'][0], "nivel{$nivel}") == "nao"){

      redirect('dashboard/');

    } 
}