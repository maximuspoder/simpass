/**
*   altera_agenciabb()
*	Função que altera a agência do Banco do Brasil de um determinada responsável financeiro.
*/
function altera_agenciabb(values)
{
	// alert(values);
	// Testa se existe algum radio checado
	if(values != undefined && values != null)
	{
		// Só troca o idagencia caso o input idagencia exista
		if(parent.$('#idagencia').length > 0)
		{
			// Separa valores do input radio
			var agencia = values.split("|");
			parent.$('#idagencia').val(agencia[0]);
			
			// Caso exista label_agencia, altera o conteudo dele (exibição do cod agencia e nome dela)
			if(parent.$('#label_agencia').length > 0)
			{
				parent.$('#label_agencia').html('NOME: ' + agencia[2] + ' - COD. AGENCIA: ' + agencia[1]);
				
				// JAVASCRIPT
				// parent.document.getElementById('aviso_alteracao_agencia').innerHTML = "<br />Você precisa salvar o cadastro para que estas alterações tenham efeito.<br /><br />";
				
				// JQUERY
				parent.$('#aviso_alteracao_agencia').html('<br />Você precisa salvar o cadastro para que estas alterações tenham efeito.<br /><br />');
			}
			
			// Fecha modal
			parent.close_modal();
		}
	} else {
		alert('Selecione uma agência.')
	}
}

/**
* ajax_get_cidade_by_uf_bb()
* Ajax que popula um combo target com options de municipio, através de uma uf encaminhada.
* Caso retorno vazio, entao limpa combo de cidades target.
* @param string municipio
* @param string id_target
* @return void
*/
function ajax_get_cidade_by_uf_bb(municipio, id_target)
{
	if(municipio == '')
	{
		$('#' + id_target).html('');
	} else
	{
		// Habilita loading
		enable_loading();
		
		// Dispara o ajax da url
		$.ajax(
		{
			url: $('#URL_EXEC').val() + 'pesquisa_agencia_bb/ajax_get_options_cidade_by_uf_bb/' + municipio,
			context: document.body,
			cache: false,
			async: false,
			type: 'POST',
			success: function(data)
			{
				// Popula combo
				$('#' + id_target).html(data);
				
				// Desabilita o loading
				disable_loading();
			}
		});
	}
}

/**
* ajax_pesquisar_livros()
* Ajax que gera a pesquisa externa de livros.
* Caso retorno vazio não mostra nada.
* @return void
*/
function ajax_pesquisar_livros()
{
	// Habilita loading
	enable_loading();
		
	// Dispara o ajax da url
	$.ajax({
		url: $('#URL_EXEC').val() + 'acesso_externo/ajax_pesquisar_livros/',
		data: "campo_pesquisa=" + $("#campo_pesquisa").val(),
		context: document.body,
		cache: false,
		async: false,
		type: 'POST',
		success: function(data){
			// Popula combo
			$("#box_resultado_pesquisa_livro").html('<div><h4 style="color:#333;margin:30px 0 10px 0;">Resultados para "<span class="italic">' + $("#campo_pesquisa").val() + '</span>":</h4></div>' + data);
			
			// Desabilita o loading
			disable_loading();
		}
	});
}

/**
* ajax_save_config_pedido()
* Ajax que salva configurações do pedido, alteradas na página de gerência administrativa do pedido.
* @param integer idpedido
* @return void
*/
function ajax_save_config_pedido(idpedido)
{
	// Habilita loading
	enable_loading();
	$("#ajax_loading").show();
	
	// Dispara o ajax da url
	$.ajax({
		url: $('#URL_EXEC').val() + 'pedido/ajax_save_config_pedido/' + idpedido,
		data: 'liberar_ins_documento=' + (($('#liberar_ins_documento').attr('checked') == 'checked') ? 'S' : 'N'), 
		context: document.body,
		cache: false,
		async: false,
		type: 'POST',
		success: function(data){
			// Informa save
			alert("ATENÇÃO!\n\nSalvo com sucesso.");
			
			// Desabilita o loading
			disable_loading();
			$("#ajax_loading").hide();
			
			// Mostra msg salvo com sucesso.
			id_success = "#ajax_complete";
			$(id_success).fadeIn();
			setTimeout('$(id_success).fadeOut();', 3000);
		}
	});
}

/**
* ajax_pesquisar_estabelecimento()
* Ajax que gera o retorno da pesquisa de estabelecimentos.
* @return void
*/
function ajax_pesquisar_estabelecimento()
{
	// Habilita loading
	enable_loading();
		
	// Dispara o ajax da url
	$.ajax({
		url: $('#URL_EXEC').val() + 'pesquisa/ajax_pesquisar_estabelecimento/',
		data: "campo_pesquisa=" + $("#campo_pesquisa").val(),
		context: document.body,
		cache: false,
		async: false,
		type: 'POST',
		success: function(data){
			// Popula combo
			$("#box_resultado_pesquisa").html('<div><h4 style="color:#333;margin:30px 0 10px 0;">Resultados para "<span class="italic">' + $("#campo_pesquisa").val() + '</span>":</h4></div>' + data);
			
			// Desabilita o loading
			disable_loading();
		}
	});
}

/**
* ajax_get_cidades_by_uf()
* Ajax que popula um combo target com options de cidades, através de uma uf encaminhada.
* Caso retorno vazio, entao limpa combo de cidades target.
* @param string iduf
* @param string id_target
* @return void
*/
function ajax_get_cidades_by_uf(iduf, id_target)
{
	if(iduf == '')
	{
		$('#' + id_target).html('');
	} else {
		// Habilita loading
		enable_loading();
		
		// Dispara o ajax da url
		$.ajax({
			url: $('#URL_EXEC').val() + 'cidade/ajax_get_options_cidades_by_uf/' + iduf,
			context: document.body,
			cache: false,
			async: false,
			type: 'POST',
			success: function(data){
				// Popula combo
				$('#' + id_target).html(data);
				
				// Desabilita o loading
				disable_loading();
			}
		});
	}
}

/**
* ajax_get_bairro_by_cidade()
* Ajax que popula um combo target com options de bairro, através de uma idcidade de cidade encaminhada.
* Caso retorno vazio, entao limpa combo de cidades target.
* @param string idcidade
* @param string id_target
* @return void
*/
function ajax_get_bairro_by_cidade(idcidade, id_target)
{
	if(idcidade == '')
	{
		$('#' + id_target).html('');
	} else {
		// Habilita loading
		enable_loading();
		
		// Dispara o ajax da url
		$.ajax({
			url: $('#URL_EXEC').val() + 'endereco/ajax_get_options_bairro_by_cidade/' + idcidade,
			context: document.body,
			cache: false,
			async: false,
			type: 'POST',
			success: function(data){
				// Popula combo
				$('#' + id_target).html(data);		
				// Desabilita o loading
				disable_loading();
			}
		});
	}
}


/**
* ajax_save_conferencia_administrativa()
* Ajax que salva crédito na tabela pedido_valor_entrega.
* @param integer idpedido
* @return void
*/
function ajax_save_conferencia_administrativa(idpedido)
{
	var MY_data = "";
	
	// Monta querystring para envio no ajax
	$("#form_pedido_" + idpedido + " input").each(function(){
		// Só coleta o valor de checkbox caso este esteja marcado
		if($(this).attr('type') == 'checkbox') {
			MY_data += ($(this).is(":checked")) ? $(this).attr("id") + "=" + $(this).val() + "&" : $(this).attr("id") + "=&";
		} else {
			MY_data += $(this).attr("id") + "=" + $(this).val() + "&";
		}
	});
	
	// DEBUG: 
	// alert(data);
	
	// Habilita loading
	enable_loading();
	$("#ajax_loading_" + idpedido).show();

	// Dispara o ajax da url
	$.ajax({
		url: $('#URL_EXEC').val() + 'pedido/conferencia_administrativa_pedido_proccess/',
		context: document.body,
		cache: false,
		async: false,
		data: MY_data,
		type: 'POST',
		success: function(data){
			// Informa save
			alert("ATENÇÃO!\n\nSalvo com sucesso.");
			
			// Desabilita o loading
			disable_loading();
			$("#ajax_loading_" + idpedido).hide();
			
			// Mostra msg salvo com sucesso.
			id_success = "#ajax_complete_" + idpedido;
			$(id_success).fadeIn();
			setTimeout('$(id_success).fadeOut();', 3000);
		}
	});
}

/**
* ajax_finaliza_conferencia_aceite()
* Janelinha de aceite da finalização de conferência.
* @param integer idpedido
* @return void
*/
function ajax_finaliza_conferencia_aceite(idpedido, tipo_usuario, numero_entrega)
{
	var msg = '';
	msg  = 'Você está finalizando a ' + numero_entrega + 'ª entrega. Caso não exista nenhuma incoerência nas informações prestadas, a entrega será marcada como finalizada (após pdv e biblioteca inserirem documentos relativos) e o valor desta entrega será encaminhado para a liberação, realizada através da conferência administrativa feita por um de nossos administradores.<br /><br />O valor, uma vez liberado para pagamento, será creditado no cartão do responsável financeiro. Aguarde a confirmação do crédito no cartão via comunicado na sua área de trabalho e email do responsável financeiro.<br /><br /><input type="checkbox" id="aceite" />&nbsp;&nbsp;Declaro que as informações e documentos aqui cadastrados correspondem à realidade concordada com o ponto de venda.<br /><br /><div class="fnt_error italic">*Uma vez finalizada esta etapa da conferência, não será possível alterar os dados informados (documentos, etc) até aqui. Você terá apenas acesso visual ao que já foi conferido.</div><div style="margin-top:10px"><hr /><div class="inline top"><button onclick="if($(\'#aceite\').is(\':checked\')){ ajax_finaliza_conferencia(' + idpedido + ',' + tipo_usuario + ') } else { alert(\'Você deve concordar com o aceite acima, antes de finalizar a conferência.\'); }">OK</button></div><div class="inline middle" style="padding:7px 0 0 5px">ou <a href="javascript:void(0)" onclick="parent.close_modal();">cancelar</a></div></div>';
	open_modal('Termo de aceitação', msg, $('#URL_IMG').val() + 'icon_question.png', 450, 500);
}

/**
* ajax_finaliza_conferencia()
* Finaliza a conferencia de pedidos via ajax. Os resultados são postos em uma dialog modal.
* @param integer idpedido
* @return void
*/
function ajax_finaliza_conferencia(idpedido, tipo_usuario)
{
	// Fecha modal anterior
	parent.close_modal();
	
	// Habilita loading
	enable_loading();

	// Dispara o ajax da url
	$.ajax({
		url: $('#URL_EXEC').val() + 'pedido/finaliza_conferencia/' + idpedido + '/' + tipo_usuario,
		context: document.body,
		cache: false,
		async: false,
		// data: 'email=' + prUser,
		type: 'POST',
		success: function(data){
			// Coloca o resultado da finalizacao em dialog
			if(tipo_usuario == 4)
			{
				open_modal('Finalização de Entrega', data, $('#URL_IMG').val() + 'icon_conferencia.png', 300, 500);
			} else {
				open_modal('Finalização de Entrega', data, $('#URL_IMG').val() + 'icon_conferencia.png', 700, 900);
			}
			
			// Desabilita o loading
			disable_loading();
		}
	});
}

/**
* ajax_finaliza_conferencia()
* Finaliza a conferencia de pedidos via ajax. Os resultados sao postos em uma dialog modal.
* @param integer idpedido
* @return void
*/
function ajax_save_qtde_entregas(idpedido, qtde_entregas)
{
	if(qtde_entregas == 0 || qtde_entregas == undefined)
	{
		alert("ATENÇÃO!\n\nVocê deve selecionar uma opção para salvar.");
	} else {
		if(qtde_entregas == 1)
		{
			if(!window.confirm("ATENÇÃO!\n\nAo alterar a quantidade de entregas para uma somente, todos os documentos da segunda entrega serão passados para a primeira entrega e caso você queira removê-los, terá de fazer manualmente."))
			{
				return false;
			}
		}
		// Habilita loading
		enable_loading();

		// Dispara o ajax da url
		$.ajax({
			url: $('#URL_EXEC').val() + 'pedido/save_qtde_entregas/' + idpedido + '/' + qtde_entregas,
			context: document.body,
			cache: false,
			async: false,
			// data: 'email=' + prUser,
			type: 'POST',
			success: function(data){
				// Coloca o resultado da finalizao em dialog
				alert('Quantidade de entregas salvo com sucesso.');
				
				// Seta o hidden para o valor salvo
				$('#qtd_entregas_salvo').val(qtde_entregas);
				
				// Desabilita o loading
				disable_loading();
				
				// Carrega a tabela de comprovantes/documentos novamente
				ajax_load_documentos(idpedido);
			}
		});
	}
}

/**
* ajax_load_documentos()
* Carrega a tabela de documentos do pedido, joga o resultado na div de id 'box_documentos'.
* @param integer idpedido
* @return void
*/
function ajax_load_documentos(idpedido)
{
	// Habilita loading
	enable_loading();

	// Carrega comprovantes de entrega
	$.ajax({
		url: $('#URL_EXEC').val() + 'pedido/load_documentos_pedido/' + idpedido + '/comprovantes',
		context: document.body,
		cache: false,
		async: false,
		// data: 'email=' + prUser,
		type: 'POST',
		success: function(data){
			// alert(data);
			// Seta altura e largura
			
			// Seta o resultado para a DIV 'box_documentos'
			$('#box_documentos', parent).html('');
			$('#box_documentos').html(data);
		}
	});
	
	// Carrega notas fiscais agora
	$.ajax({
		url: $('#URL_EXEC').val() + 'pedido/load_documentos_pedido/' + idpedido + '/notas_fiscais',
		context: document.body,
		cache: false,
		async: false,
		// data: 'email=' + prUser,
		type: 'POST',
		success: function(data){
			// alert(data);
			// Seta altura e largura
			
			// Seta o resultado para a DIV 'box_documentos'
			$('#box_notas', parent).html('');
			$('#box_notas').html(data);
		}
	});
	
	// Desabilita o loading
	disable_loading();
}

/**
* ajax_load_itens_pedido()
* Carrega a tabela de itens do pedido, joga o resultado na div de id 'box_table_itens'.
* @param integer idpedido
* @param string titulo
* @param string is_pdv
* @return void
*/
function ajax_load_itens_pedido(idpedido, titulo, is_pdv)
{
	// Habilita loading
	enable_loading();
	
	titulo = (titulo == undefined) ? '' : titulo;
	is_pdv = (is_pdv == undefined) ? '' : 1;

	// Dispara o ajax da url
	$.ajax({
		url: $('#URL_EXEC').val() + 'pedido/load_table_itens_pedido/' + idpedido + '/' + encodeURIComponent(titulo) + '/' + is_pdv,
		context: document.body,
		cache: false,
		async: false,
		// data: 'email=' + prUser,
		type: 'POST',
		success: function(data){
			// alert(data);
			// Seta altura e largura
			
			// Seta o resultado para a DIV 'box_documentos'
			$('#box_table_itens', parent).html('');
			$('#box_table_itens').html(data);
			
			// Desabilita o loading
			disable_loading();
		}
	});
}

/**
* add_option_documento()
* Adiciona um option para todos os combos de cada item, com o numero do 
* documento recem inserido.
* @param string label
* @param string value
* @return void
*/
function add_option_documento(label, value)
{
	// Habilita loading
	enable_loading();
	
	// alert(label);
	// alert(value);
	
	// Varre todos os selects e adiciona o option do doc gerado
	$('#form_itens_conferencia select').each(function(index) {
		// alert(index + ': ' + $(this).value());
		$(this).append(new Option(label, value, true, false));
	});
	
	// Desabilita o loading
	disable_loading();
}

/**
* del_option_documento()
* Remove um option para todos os combos de cada item, do numero do documento recem retirado.
* @param string value
* @return void
*/
function del_option_documento(value)
{
	// Habilita loading
	enable_loading();
	
	// alert(label);
	
	// Varre todos os selects e adiciona o option do doc gerado
	$('#form_itens_conferencia select').each(function(index) {
		// alert(index + ': ' + $(this).val());
		$("#" + $(this).attr('id') + ' option[value="' + value + '"]').remove();
		
	});
	
	// Desabilita o loading
	disable_loading();
}
/**
*   AlteraEndereco()
*	Método que altera label de endereço.
*/
function AlteraEndereco(values){
			
if(parent.$('#label_endereco').length > 0)
	{
	if (values != null)
	{
		Logradouro = values.split('|');
		parent.$('#label_endereco').text('RUA ' + Logradouro[5] + " - " + Logradouro[3] + ", " + Logradouro[4] + ", " + Logradouro[1] + " - " + Logradouro[2])
		parent.document.getElementById('idlogradouro').value = Logradouro[0];
		parent.document.getElementById('aviso_alteracao_endereco').innerHTML = "<br />Você precisa salvar o cadastro para que estas alterações tenham efeito.<br /><br />";
		parent.close_modal();
	} else	{
		alert('Selecione um logradouro.')
	}
			
	} else {
		alert('Alteração não habilitada no momento.');
	}
}

/**
*   AlteraEndereco()
*	Método que altera label de endereço.
*/
function InformaEmailEncaminhamento(email){
	if(email == ''){
		 $('#aviso_email_encaminhamento').text("Selecione um cadastro que possua email.");
		 $("input[type=submit]").attr("disabled", "disabled");
	}	else {
		$('#aviso_email_encaminhamento').text("Será encaminhado para " + email + " um email de reativação de cadastro");
		$("input[type=submit]").removeAttr("disabled");
	}
}

/**
*   verificaMarcado()
*	Verifica se algum dos cadastros está 
*   marcado, se sim libera o submit.
*/
function verificaMarcado(campoMarcado){
	if(campoMarcado == null){
		alert('Selecione um dos cadastros que está sendo visualizado.')
		return false;
	} else {
		return true;
	}
}

/**
* ajax_get_cidades_by_uf_externo()
* Ajax que popula um combo target com options de cidades, através de uma uf encaminhada.
* Caso retorno vazio, entao limpa combo de cidades target. UTILIZADO PARA CHAMADAS EXTERNAS.
* @param string iduf
* @param string id_target
* @return void
*/
function ajax_get_cidades_by_uf_externo(iduf, id_target)
{
	if(iduf == '')
	{
		$('#' + id_target).html('');
	} else {
		// Habilita loading
		enable_loading();
		
		// Dispara o ajax da url
		$.ajax({
			url: $('#URL_EXEC').val() + 'acesso_externo/ajax_get_options_cidades_by_uf/' + iduf,
			context: document.body,
			cache: false,
			async: false,
			type: 'POST',
			success: function(data){
				// Popula combo
				$('#' + id_target).html(data);
				
				// Desabilita o loading
				disable_loading();
			}
		});
	}
}