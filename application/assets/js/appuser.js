var BASEURL = "http://localhost/";
//var BASEURL = "http://simuleepasse.com.br/";



function redirect(url)
{

	window.location.href = BASEURL +  url;
}

function set_post(token, post){
	
	$.post(BASEURL + "timeline/ajax_set_post", {token: token, post: post}).done(function(result){
		alert(result)
	})
}

function ajax_get_timeline(token){
	$.post(BASEURL + "timeline/ajax_get_timeline", {token: token, title: title, post: post}).done(function(result){
		//$('#resultAjax').html(result);
		alert(result)
	})
}

function tPush(token, concat){
	$.post(BASEURL + "timeline/ajax_tpush", {token: token, concat: concat}).done(function(result){
		/*$('.galcolumn').append(result);*/
		/*$('#timeline').append(result);*/

	})
}

function ajax_restBanca(banca){
	$.post(BASEURL + "simulado/ajax_restSimuladoEspelho", {banca: banca}).done(function(result){
		
		htmlComboOrgaos = '<option></option>';
		htmlComboCargos = '<option></option>';

		totalOrgaos = result.orgao.length;
		x = 0;
		while(x < totalOrgaos){
			
			/*Monta combo de Orgaos*/
			htmlComboOrgaos += '<option>' + Object.keys( result.orgao[x] ) + '</option>';
			/* Pega as chaves com nome dos orgaos */
			keyValue = Object.keys( result.orgao[x] );
			/* Conta o total de cargos por orgaos */
			totalCargos = result.orgao[x][keyValue].length;
			y = 0;
			while(y < totalCargos)
			{
				/* Monta combo com os cargos */
				htmlComboCargos += '<option>' +  result.orgao[x][keyValue][y].cargo  + '</option>';
				y++;
			}
			x++;
		}
		$('#orgao').html(htmlComboOrgaos);
		$('#cargo').html(htmlComboCargos);
	})	
}

function ajax_restOrgao(banca, orgao){
	$.post(BASEURL + "simulado/ajax_restSimuladoEspelho", {banca: banca}).done(function(result){
		totalOrgaos = result.orgao.length;

		htmlComboCargos = '<option></option>';
		x = 0;
		while(x < totalOrgaos){
			
			if(orgao == Object.keys( result.orgao[x] ) ){
				keyValue = Object.keys( result.orgao[x] );
				totalCargos = result.orgao[x][keyValue].length;
				y = 0;
				while(y < totalCargos)
				{
					/* Monta combo com os cargos */
					htmlComboCargos += '<option>' +  result.orgao[x][keyValue][y].cargo  + '</option>';
					y++;
				}
			}
			x++;
		}
		$('#cargo').html(htmlComboCargos);
	})	
}


function ajax_get_complementar(html, div) {

	$.ajax(  html ).done(function(result){
		$('#' + div).html(result);
	})
}

function getAttach(id){

     $('#viewAttachment' + id).toggle();
}


function load_forum(id, name, token){

	$.post(BASEURL + "forum/load_forum", {id: id, name: name, token: token}).done(function(result){
		$('#forum_' + id).html(result);
	})
}


function set_forum_post(id, token){

	var duvida = $('#duvida_' + id).val();
	$.post(BASEURL + "forum/ajax_set_post", {id: id, token: token, duvida: duvida}).done(function(result){
		if(result == 1){
			$('#forumfirst_' + id).html("<center><h4>Dúvida enviada com sucesso!</h4></center>");
		}
	})
}

function ajax_set_questao_erro(id, token){

	var erro = $('#erro_' + id).val();

	$.post(BASEURL + "forum/ajax_set_questao_erro", {id: id, token: token, erro: erro}).done(function(result){
		if(result == 1){
			$('#err_' + id).val('');
			$('#infoerro_' + id).html("<center><h4>Mensagem enviada com sucesso!</h4></center>");
		}
	})
}

function prova_set_result(provaid, id, token, tabIterator){
	
	var resposta = get_checked_value('resposta_' + tabIterator)

	$.post(BASEURL + "simulado/prova_set_result", {provaid: provaid, token: token, id: id, resposta: resposta}).done(function(result){
		//alert(result);
		if(result == resposta){
			//$('#correcao_' + tabIterator + '_' + result).css({border: "1px solid green", borderRadius: "7px"});
			$('#info_' + tabIterator).show();
			$('#info_' + tabIterator).css({Color: "green"});
			$('#info_' + tabIterator).html('<div class="alert alert-success"><strong>Parabéns!</strong> Você acertou!!</div>');
		} else {
			//$('#correcao_' + tabIterator + '_' + result).css({border: "1px solid green", borderRadius: "7px"});
			//$('#correcao_' + tabIterator + '_' + resposta).css({border: "1px solid red", borderRadius: "7px"});
			$('#info_' + tabIterator).show();
			$('#info_' + tabIterator).css({Color: "red"});
			$('#info_' + tabIterator).html('<div class="alert alert-danger"><strong>Que pena,</strong> você errou!! A resposta correta é a <strong>'+ result +'</strong></div>');
		}
		$('#btnResult_' + tabIterator).attr('disabled', 'disabled');
	})
}

function get_checked_value(name)
{
	var retorno;
	$('input[name="' + name + '"]').each(function(){
		if($(this).is(':checked'))
		{ 
			retorno = $(this).val(); 
		}
	});
	return retorno;
}



function set_follower(update, div_id, token_from, token_to){
	$.post(BASEURL + "usuario/set_follower", {token_from: token_from, token_to: token_to})
	.done(function(result){
		if(result == 1){
			$('#button_follow_' + div_id).html('<a href="javascript:void(0)" class="btn btn-default mini" disabled="disabled"><i class="fa fa-check"></i></a>');
			if(update == 1){
				location.reload();
				 $.post("http://localhost:8080/"+ token_from +".json").done(function(){
					console.log('executado realod... token: ' + token_from);
				})
			} else {
				token = token_from.substr(40)
				$.ajax({
						type: "GET",
						url: "http://localhost:8080/"+ token +".json",
						dataType: "jsonp",
					}).done(function(r){
					})
				
			}
		}
		
	})
}


function unset_follower(token_from, token_to){
	$.post(BASEURL + "usuario/unset_set_follower", {token_from: token_from, token_to: token_to})
	.done(function(result){
		if(result == 1){
			$('#' + token_to).remove();
		} else {
			alert('Não foi possível remover o usuário, tente novamente!');
		}
	})
}

function redirect_simulado(provaid, itemPerPage){
	window.location.href = BASEURL +  'simulado/prova/' + provaid + '/' + itemPerPage;
}
function redirect_simulado_end(provaid){
	window.location.href = BASEURL +  'simulado/finalizar_prova/' + provaid;
}
function redirect_simulado_next_page(provaid, itemPerPage, id){
	window.location.href = BASEURL +  'simulado/prova/' + provaid + '/' + itemPerPage +'/'+id;
}

function redirect_simulado_per_page(provaid, paginatorID){
	var itemPerPage = $('#paginator' + paginatorID).val();
	window.location.href = BASEURL +  'simulado/prova/' + provaid + '/' + itemPerPage;
}
function redirect_simulado_free(provaid, itemPerPage){
	window.location.href = BASEURL +  'simulado/prova_free/' + provaid + '/' + itemPerPage;
}
function redirect_simulado_per_page_free(provaid, paginatorID){
	var itemPerPage = $('#paginator' + paginatorID).val();
	window.location.href = BASEURL +  'simulado/prova_free/' + provaid + '/' + itemPerPage;
}

function getUpdates(id){
	$.ajax({
          url: BASEURL + "api/get_notifications/" + id,
          async: false,
          success: function(data){
 	        $('#updates').html(data)          	
          }
  });

}

function get_teacher_disciplinas_vimeo(token, provaid){
	disciplina = $('#disciplina').val()
	$.post(BASEURL + "simulado/get_teacher_disciplinas_vimeo", {disciplina: disciplina, token: token, provaid: provaid})
	.done(function(result){
		$('#list').html(result)
	})
}

function set_teacher_disciplinas_vimeo(token, provaid, questaoid){
	$.post(BASEURL + "simulado/set_teacher_disciplinas_vimeo", {questaoid: questaoid, token: token, provaid: provaid})
	.done(function(result){
		if(result == 1){
			$('#result_'+questaoid).remove();
		} 
		else if(result == 3){
			alert('Você atingiu o limite máximo de questões para um simulado grátis!');
		}
		else if(result == 0){
			alert('Esta questão pode já ter sido escolhida!');
		}
	})
}

function set_simulado_criado(token, id){
	$.post(BASEURL + "simulado/set_simulado_criado", { token: token, provaid: id })
	.done(function(result){
		$('#list').remove();
		$('#simulado_link').html(result);
	})
}

function ajax_call_message(to, from, unique){
	$.post(BASEURL + "mensagens/ajax_call_message", {to: to, from: from, unique: unique })
	.done(function(result){
		$('#view_message').html(result)
	})
}
