/**
* redirect()
* Redireciona página para url encaminhada.
* @param string url
* @return void
*/
function redirect(url)
{
	window.location.href = url;
}

/**
* set_form_filter_action()
* Seta o action do formulário de filtros e dispara o submit (necessário para utilização de paginação)
* @param string url
* @return void
*/
function set_form_filter_action(url)
{
	$('#form_filter').attr('action', url);
	$('#form_filter').submit();
}

/**
* change_doc_mask()
* Troca a máscara e validador do tipo de documento conforme o valor aqui encaminhado. Espera-se que
* exista um input com o id 'cpf_cnpj'.
* @param string tipo_pessoa
* @return void
*/
function change_doc_mask(tipo_pessoa)
{
	if(tipo_pessoa.toLowerCase() == 'pf')
	{
		$('#cpf_cnpj').attr('class', 'validate[required,custom[cpf]]');
		$('#cpf_cnpj').attr('alt', 'cpf');
	} else {
		$('#cpf_cnpj').attr('class', 'validate[required,custom[cnpj]]');
		$('#cpf_cnpj').attr('alt', 'cnpj');
	}
	$('input:text').setMask();
}

/**
* show_filter()
* Habilita, desabilita formulário de filtro.
* @return void
*/
function show_filter()
{
	if($('#form_filter').is(':visible') ) {
		$('#form_filter').hide();
	} else {
		$('#form_filter').show();
	}
}

/**
* set_form_redirect()
* Seta as urls de redirect (ok e aplicar) para o formulário encaminhado, depois submita o formulário. Quarto
* parâmetro diz se ação é ok ou aplicar.
* @param string link_ok
* @param string link_aplicar
* @param string id_form
* @param string action
* @return void
*/
function set_form_redirect(link_ok, link_aplicar, id_form, action)
{
	// Tratamento para action nulo
	action = (action == null || action == undefined) ? 'ok' : action;
	
	// Append dos inputs no form encaminhado
	$('#' + id_form).append('<input type="hidden" name="redirect_action" id="redirect_action" value="' + action + '" />');
	$('#' + id_form).append('<input type="hidden" name="redirect_ok" id="redirect_ok" value="' + link_ok + '" />');
	$('#' + id_form).append('<input type="hidden" name="redirect_aplicar" id="redirect_aplicar" value="' + link_aplicar + '" />');
	$('#' + id_form).submit();
}

/**
* show_area()
* Collapsa uma area de id indicado. Espera-se que exista uma imagem
* de id igual ao id encaminhado + _img para que se possa fazer a troca
* da imagem do bullet.
* @param string id_area
* @return void
*/
function show_area(id)
{
	if($('#' + id).is(":visible"))
	{
		var src = $('#' + id + '_img').attr('src').replace("minus", "plus");
		$('#' + id + '_img').attr("src", src);
		$('#' + id).hide();
		// $('#' + id).slideUp();
	} else {
		var src = $('#' + id + '_img').attr('src').replace("plus", "minus");
		$('#' + id + '_img').attr("src", src);
		$('#' + id).show();
		// $('#' + id).slideDown();
	}
}

/**
* get_checked_value()
* Coleta o valor de um checkbox que está marcado, através do nome do elemento check.
* @param string name
* @return mixed value
*/
function get_checked_value(name)
{
	var retorno;
	$('input[name="' + name + '"]').each(function(){
		if($(this).is(':checked'))
		{ 
			retorno = $(this).val(); 
		}
	});
	return retorno;
}

/**
* open_modal()
* Cria uma modal on the fly colocando conteudo dentro dela.
* @param string titulo
* @param string conteudo
* @param string imagem
* @param string style
* @return void
*/
function open_modal(titulo, conteudo, imagem, height, width, close)
{
	var html  = '';
	var style_modal = '';
	var style_content = '';
	
	// Seta altura e largura
	height = (height != '' && height != null) ? height : 650;
	width = (width  != '' && width  != null) ? width : 800;
	style_modal = ' style="width:' + width + 'px;height:' + height + 'px"';
	style_content = ' style="height:' + (height - 80) + 'px !important;"';
	
	// Seta o boolean para close
	var bool_close = (close == false) ? false : true;
	
	// Img to close
	var close_img  = (close == false) ? '' : '<img src="' + $("#URL_IMG").val() + 'icon_close.png" id="img_close" class="img_close" title="Fechar" />';
	
	// Inicializa html da janela modal
	html += '<div class="modal" id="modal"' + style_modal + '>';
	html += '<div id="header">' + close_img + '<img src="' + imagem + '" /><h6>' + titulo  + '</h6></div>';
	html += '<div id="content" ' + style_content + '>' + conteudo + '</div><div id="footer"></div>';
	html += '</div>';
	
	// Inicializa a própria janela modal
	$.modal(html, {opacity: 60, zIndex: 10000, closeClass: 'img_close', escClose: bool_close, position: [0,0], close: bool_close});
}

/**
* iframe_modal()
* Cria uma modal on the fly colocando conteudo uma pagina iframe.
* @param string titulo
* @param string url_param
* @param string imagem
* @param string style
* @return void
*/
function iframe_modal(titulo, url_param, imagem, height, width, close)
{
	var html  = '';
	var style_modal = '';
	var style_content = '';
	
	// Seta altura e largura
	height = (height != '' && height != null) ? height : 650;
	width = (width  != '' && width  != null) ? width : 800;
	style_modal = ' style="width:' + width + 'px;height:' + height + 'px"';
	style_content = ' style="height:' + (height - 80) + 'px !important;"';
	
	// Seta o boolean para close
	var bool_close = (close == false) ? false : true;
	
	// Img to close
	var close_img  = (close == false) ? '' : '<img src="' + $("#URL_IMG").val() + 'icon_close.png" id="img_close" class="img_close" title="Fechar" />';
	
	// Inicializa html da janela modal
	html += '<div class="modal" id="modal"' + style_modal + '>';
	html += '<div id="header">' + close_img + '<img src="' + imagem + '" /><h6>' + titulo  + '</h6></div>';
	html += '<div id="content" ' + style_content + '><iframe src="' + url_param + '" style="width:100%;height:' + (height - 80) + 'px !important;max-height:670px;border:0px solid green;z-index:10006;position:relative" align="left" frameborder="no"></iframe></div><div id="footer"></div>';
	html += '</div>';
	
	// Inicializa a própria janela modal
	$.modal(html, {opacity: 60, zIndex: 10000, closeClass: 'img_close', escClose: bool_close, position: [0,0], close: bool_close});
}

/**
* ajax_modal()
* Cria uma modal on the fly colocando conteudo de ajax dentro dela.
* @param string titulo
* @param string url
* @param string imagem
* @param string style
* @return void
*/
function ajax_modal(titulo, url_param, imagem, height, width)
{
	var html  = '';
	var style_modal = '';
	var style_content = '';
	
	// Habilita loading
	enable_loading();

	// Dispara o ajax da url
	$.ajax({
		url: url_param,
		context: document.body,
		cache: false,
		async: false,
		// data: 'email=' + prUser,
		type: 'POST',
		success: function(data){
			// alert(txt);
			// Seta altura e largura
			height = (height != '' && height != null) ? height : 650;
			width = (width  != '' && width  != null) ? width : 800;
			style_modal = ' style="width:' + width + 'px;height:' + height + 'px"';
			style_content = ' style="height:' + (height - 80) + 'px !important;"';
			
			// Inicializa html da janela modal
			html += '<div class="modal" id="modal"' + style_modal + '>';
			html += '<div id="header"><img src="' + 'http://localhost/innbrasil/moura/application/' + 'icon_close.png" class="img_close" id="img_close" title="Fechar" /><img src="' + imagem + '" /><h6>' + titulo  + '</h6></div>';
			html += '<div id="content" ' + style_content + '>' + data + '</div><div id="footer"></div>';
			html += '</div>';
			
			// Inicializa a própria janela modal
			$.modal(html, {opacity: 60, zIndex: 10000, closeClass: 'img_close', position: [0,0]});
			
			// Desabilita o loading
			disable_loading();
		}
	});
}

/**
* close_modal()
* Destrói a janela modal corrente da tela.
* @return void
*/
function close_modal()
{
	// Linha utilizada para fechamento de dialog quando chamada por iframe
	$.modal.close();
	
	// Linha utilizada para fechamento de dialog quando chamada por ajax ou conteudo
	$('#simplemodal-overlay').remove();
	$('#simplemodal-container').remove();
}

/**
* enable_loading()
* Habilita loading.
* @return void
*/
function enable_loading()
{
	// Append do conteudo no body
	if($("#loading_layer").length <= 0)
	{
		/* $('body').append('<div id="loading_layer" style=\'position:absolute\'></div><div style="position:absolute; width:250px; height:80px; top:50%; left:50%;margin-left:-125px; background-color:red; z-index:1000;opacity:0%;"> <img src="' + $('#URL_IMG').val() + 'loading.gif"> Carregando... </div>'); */
		 $('body').append('<div id="loading_layer"></div>');
	} else {
		$("#loading_layer").show();
	}
}

/**
* enable_loading()
* Desabilita loading.
* @return void
*/
function disable_loading()
{
	// Verifica se o elemento existe, para fazer display:none;
	if($("#loading_layer").length > 0)
	{
		$("#loading_layer").hide();
	}
}

/**
* habilita_edital()
* Redireciona para pagina que Habilita o usuario para edital de 
* idprograma encaminhado.
* @param integer idprograma
* @return void
*/
function habilita_edital(idprograma)
{
	// Verifica se o elemento check do edital equivalente existe
	if($('#habilitar_cad_' + idprograma).is(':checked'))
	{
		window.location.href = $('#URL_EXEC').val() + 'home/habilita/' + idprograma;
	} else {
		alert("ATENÇÃO!\n\nVocê deve confirmar o termo de declaração.");
	}
}

/**
* check_browser_compatibilty()
* Verifica a compatibilidade de browser, caso seja uma das versões não compativel,
* indica ao usuario que ele deve fazer atualizacao do browser atual ou download de
* um outro browser.
* @return boolean is_compatible
*/
function check_browser_compatibilty()
{
	// Varre toda a estrutura de browser, verificando se o 
	// browser atual está dentro das versões comportadas
	var browser  = $.browser;
	var titulo   = 'Você deve atualizar o seu navegador'; 
	var conteudo  = '';
	var navegador = '';
	var version   = '';
	var is_compatible = true;
	
	if(browser.msie == true && browser.version <= 7)
	{
		navegador = '<strong>Internet Explorer</strong>';
		version   = browser.version;
		conteudo  = '<strong>Atenção!</strong> Detectamos que você está utilizando um navegador que não é totalmente compatível com as funcionalidades deste portal.<br /><br />';
		conteudo += 'Você está utilizando o navegador ' + navegador + ' na versão ' + version + ', contudo, somente existe compatibilidade para este navegador a partir da versão 8.0.<br /><br />Recomendamos a você as seguintes sugestões:<br />';
		conteudo += '<strong>1) Atualizar seu navegador para uma versão mais recente (Recomendável):</strong><br />';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_internet_explorer.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://windows.microsoft.com/pt-BR/internet-explorer/downloads/ie" target="_blank">Atualização para Internet Explorer</a>';
		conteudo += '</div>';
		conteudo += '<br />';
		conteudo += '<strong>2) Fazer download e instalação de um dos seguintes navegadores:</strong><br />';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_firefox.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://br.mozdev.org/firefox/download/" target="_blank">Mozilla Firefox</a>';
		conteudo += '</div>';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_opera.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://www.opera.com/download/" target="_blank">Opera Browser</a>';
		conteudo += '</div>';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_safari.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://www.apple.com/br/safari/download/" target="_blank">Apple Safari</a>';
		conteudo += '</div>';
		open_modal(titulo, conteudo, $('#URL_IMG').val() + 'icon_warning.png', 340, 500, false);
		
		// Seta o body para hide, para nao exibir scroll com conteudo quebrado
		$('#page_content').hide();
		is_compatible = false;
	}
	
	if(browser.mozilla == true && browser.version < 4)
	{
		navegador = '<strong>Mozilla Firefox</strong>';
		version   = browser.version;
		conteudo  = '<strong>Atenção!</strong> Detectamos que você está utilizando um navegador que não é totalmente compatível com as funcionalidades deste portal.<br /><br />';
		conteudo += 'Você está utilizando o navegador ' + navegador + ' na versão ' + version + ', contudo, somente existe compatibilidade para este navegador a partir da versão 4.0.<br /><br />Recomendamos a você as seguintes sugestões:<br />';
		conteudo += '<strong>1) Atualizar seu navegador para uma versão mais recente (Recomendável):</strong><br />';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_firefox.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://br.mozdev.org/firefox/download/" target="_blank">Atualização para Mozilla Firefox</a>';
		conteudo += '</div>';
		conteudo += '<br />';
		conteudo += '<strong>2) Fazer download e instalação de um dos seguintes navegadores:</strong><br />';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_internet_explorer.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://windows.microsoft.com/pt-BR/internet-explorer/downloads/ie" target="_blank">Internet Explorer</a>';
		conteudo += '</div>';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_opera.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://www.opera.com/download/" target="_blank">Opera Browser</a>';
		conteudo += '</div>';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_safari.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://www.apple.com/br/safari/download/" target="_blank">Apple Safari</a>';
		conteudo += '</div>';
		open_modal(titulo, conteudo, $('#URL_IMG').val() + 'icon_warning.png', 340, 500, false);
		
		// Seta o body para hide, para nao exibir scroll com conteudo quebrado
		$('#page_content').hide();
		is_compatible = false;
	}
	
	if(browser.opera == true && browser.version < 11)
	{
		navegador = '<strong>Opera Browser</strong>';
		version   = browser.version;
		conteudo  = '<strong>Atenção!</strong> Detectamos que você está utilizando um navegador que não é totalmente compatível com as funcionalidades deste portal.<br /><br />';
		conteudo += 'Você está utilizando o navegador ' + navegador + ' na versão ' + version + ', contudo, somente existe compatibilidade para este navegador a partir da versão 11.0.<br /><br />Recomendamos a você as seguintes sugestões:<br />';
		conteudo += '<strong>1) Atualizar seu navegador para uma versão mais recente (Recomendável):</strong><br />';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_opera.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://www.opera.com/download/" target="_blank">Atualização para Opera Browser</a>';
		conteudo += '</div>';
		conteudo += '<br />';
		conteudo += '<strong>2) Fazer download e instalação de um dos seguintes navegadores:</strong><br />';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_internet_explorer.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://windows.microsoft.com/pt-BR/internet-explorer/downloads/ie" target="_blank">Internet Explorer</a>';
		conteudo += '</div>';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_firefox.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://br.mozdev.org/firefox/download/" target="_blank">Mozilla Firefox</a>';
		conteudo += '</div>';
		conteudo += '<div style="height:20px;">';
		conteudo += '<img src="' + $('#URL_IMG').val() + 'icon_safari.png" style="float:left;margin:2px 5px 0 0;" />';
		conteudo += '<a href="http://www.apple.com/br/safari/download/" target="_blank">Apple Safari</a>';
		conteudo += '</div>';
		open_modal(titulo, conteudo, $('#URL_IMG').val() + 'icon_warning.png', 340, 500, false);
		
		// Seta o body para hide, para nao exibir scroll com conteudo quebrado
		$('#page_content').hide();
		is_compatible = false;
	}
	
	return is_compatible;
}

/**
* round_number()
* Arredonda um numero em duas casas decimais.
* @param double rnum
* @return double rnum
*/
function round_number(rnum)
{
	return Math.round(rnum*Math.pow(10,2))/Math.pow(10,2);
}

/**
* to_moeda()
* Passa um numero para formato moeda.
* @param double num
* @return string rnum
*/
function to_moeda(num) 
{
    x = 0;
    if(num<0) {
		num = Math.abs(num);
		x = 1;
	}
	if(isNaN(num)) num = "0";
		cents = Math.floor((num*100+0.5)%100);

	num = Math.floor((num*100+0.5)/100).toString();

	if(cents < 10) cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
			num = num.substring(0,num.length-(4*i+3))+'.'+num.substring(num.length-(4*i+3));
	
	ret = num + ',' + cents;
	if (x == 1) ret = ' - ' + ret; return ret;
}

/**
* to_float()
* Passa um numero para formato float, recebido como moeda.
* @param string moeda
* @return double rnum
*/
function to_float(moeda)
{
	moeda = moeda.replace(".","");
	moeda = moeda.replace(",",".");
	return parseFloat(moeda);
}

/**
* get_enter()
* Dispara uma funcao quando a tecla pressionada é 13 (enter).
* @param string event
* @param string function_eval
* @return void
*/
function get_enter(event, function_eval)
{
	if(event.which == 13) {	eval(function_eval); /*event.preventDefault();*/ }
}

/**
* download_excel()
* Cria um iframe em tempo real onde o target será uma página que exporta um arquivo excel.
* @param string url_export
* @return void
*/
function download_excel(url_export)
{
	$("body").append('<iframe src="' + url_export + '" style="display:none"></iframe>');
}

