var BASEURL = "http://localhost/";
$(document).ready(function(){
	get_image_size();
	upload_image()
})
function uploadImage(){
	$("#uploadForm").on('submit',(function(e){
		e.preventDefault();
		$.ajax({
			url: "http://simuleepasse.com.br/timeline/timeline_modal_image",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				$('#conteudo').html('<img src="'+data+'" alt="Imagem">');
				$('#wbbmodal').remove();
			},
			error: function(){
				alert('Erro ao realizar o upload da imagem, tente novamente!');
			}
		});
	}));
}

function get_image_size(){
	$('.img_timeline').each(function(){
		if($(this).width() > $('.grid').width()){
			$(this).attr('style', 'width:100%');
		}
	})
};

function upload_image(){
	$('.fileupload').click(function(e) {
    	$(this).find('input[type="file"]').click();
	});

	$('.fileupload input').click(function(e) {
	    e.stopPropagation();
	});

	if($('#timeline_image').val()){
	}

}

var appTimeline = function(){
	self = this;

	self.init = function(){
		self.postar($.cookie('id'));
		self.video()
		self.videoIframe()
	}

	self.postar = function(id){
		$('#btnPublicar').click(function(){
			var content = $('#editor').val();
			$.ajax({
				type: 'POST',
				url: BASEURL + 'timeline/ajax_set_post',
				data: {conteudo: content, id: id},
				success: function(data){
					if(data == 1){
						$('#editor').val('')
					}
				}
			})
		})
	}

	self.simulePost = function(){
	}

	self.video = function(){
		$('#videoItem').click(function(){
			$('#video_container').toggle('slow')
		})
	}

	self.videoIframe = function(){
		//<iframe width="560" height="315" src="//www.youtube.com/embed/pRFtbZGlsng" frameborder="0" allowfullscreen></iframe>
		//http://img.youtube.com/vi/pRFtbZGlsng/1.jpg
		$('#GetItemVideo').keyup(function(){
			item = $('#GetItemVideo').val().split('=');
			setTimeout(function(){
				$('#containerItems').append('<img src="http://img.youtube.com/vi/pRFtbZGlsng/1.jpg">');
				$('#video_container').toggle()
			},3000)
		})
	}
}