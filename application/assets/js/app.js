/*
* Arquivo de funcões jquery e js para
* o sistema
*
* author   	Fernando Alves
* date 		27-08-2013
*/

var BASEURL = "http://localhost/";
//var BASEURL = "http://simuleepasse.com.br/";


/**
 * set_form_filter_action()
 * Seta o action do formulário de filtros e dispara o submit (necessário para utilização de paginação)
 * @param string url
 * @return void
 */
function set_form_filter_action(url)
{
    $('#form_filter').attr('action', url);
    $('#form_filter').submit();
}

function redirect_to_unset(url){
	$('#q__qcid').val();
	$('#q__orgao').val('');
	$('#q__banca').val('');
	$('#q__disciplina').val('');
	$('#q__assunto1').val('');
	$('#q__assunto2').val('');
	window.location.href = url;
}

function redirect_to(url)
{
	window.location.href = url;
}

function simule_redirect(url)
{
	//Producao
	window.location.href = 'http://www.simuleepasse.com.br/' + url;
	
	//Desenvolvimento
	//window.location.href =  'http://localhost/' + url;
}

function ajax_get_questionsByDiscipline(discipline){
	$.post(BASEURL + "professor/ajax_get_questionsByDiscipline", {discipline: discipline}).done(function(result){
		$('#resultAjax').html(result);
	})
}

function ajax_get_complementar(html, div) {

	$.ajax(  html ).done(function(result){
		$('#' + div).html(result);
	})
}

function ajax_professor_set_question(usuarioid, professor, questaoid, qcid, prova, disciplina){
	$.post(BASEURL + "professor/ajax_professor_set_question", {usuarioid: usuarioid, professor: professor, questaoid: questaoid, qcid: qcid, prova: prova, disciplina: disciplina }).done(function(result){
	
		if(result == 1){
			alert(result);
		} else {
			alert(result);
		}
	})
}

function ajax_professor_set_question_eliminate_div(usuarioid, professor, questaoid, qcid, prova, disciplina){
	
	
	//$.post(BASEURL + "professor/ajax_professor_set_question", {usuarioid: usuarioid, professor: professor, questaoid: questaoid, qcid: qcid, prova: prova, disciplina: disciplina }).done(function(result){
		$.post("http://simuleepasse.com.br/professores/ajax_professor_set_question", {usuarioid: usuarioid, professor: professor, questaoid: questaoid, qcid: qcid, prova: prova, disciplina: disciplina }).done(function(result){

		if(result == 1){
			// erro
			alert(result);
		} else {
			$('#id_' + qcid).remove();
			alert(result);
		}
	})
	

}

function getAttach(qcid){

     $('#viewAttachment' + qcid).toggle();
}

function ajax_set_gravadaOkay(qcid){
	$.post(BASEURL + "professor/ajax_set_gravadaOkay", {qcid: qcid}).done(function(result){
		if(result == 1){
			alert(' Gravada com sucesso ;) ')
		} else {	
			alert('Erro ao gravar questão =[');
		}
	})
}

function question_delete(qcid){

	$.post(BASEURL + "professor/question_delete", {qcid: qcid}).done(function(result){
		
		if(result == 1){
			$('#result'+qcid).remove();
		} else {	
			alert('Erro ao remover questão =[');
		}
	})
}
