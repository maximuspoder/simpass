//var BASEURL = "http://localhost/";
var BASEURL = "http://simuleepasse.com.br/";



function simulado_filter(){
	
	$('#simulado_filter').submit(function(event){
		banca = $('#banca').val();
        orgao = $('#orgao').val();
        cargo = $('#cargo').val();
        disciplina = $('#disciplina').val();
        assunto = $('#assunto').val();
        nivel = $('#nivel').val();
        ano = $('#ano').val();

       $.post(BASEURL + "simulado/ajax_simulado_filter", 
       	{	
       		banca: banca,
	        orgao: orgao,
	        cargo: cargo,
	        disciplina: disciplina,
	        assunto: assunto,
	        nivel: nivel,
	        ano: ano
       	}
       	).done(function(result){
			$('#filter_result').html(result)		
		})
        event.preventDefault();
     })
}

function ajax_simulado_filter_set_simulado(){
	banca = $('#banca').val();
    orgao = $('#orgao').val();
    cargo = $('#cargo').val();
    disciplina = $('#disciplina').val();
    assunto = $('#assunto').val();
    nivel = $('#nivel').val();
    ano = $('#ano').val();
    total_questoes = $('#total_questoes').val();
    token = $('#token').val();
    if(total_questoes == ''){
    	alert('Informe quantas questões deseja responder!');
    	$('#total_questoes').focus();
    	//event.preventDefault();
    } else {

	   $.post(BASEURL + "simulado/ajax_simulado_filter_set_simulado", 
	   	{	
	   		banca: banca,
	        orgao: orgao,
	        cargo: cargo,
	        disciplina: disciplina,
	        assunto: assunto,
	        nivel: nivel,
	        ano: ano,
	        total_questoes: total_questoes,
	        token: token
	   	}
	   	).done(function(result){
			if(result > 0){
				window.location.href =  BASEURL + 'simulado/prova/' + result;
				
			}
		})
    }
}



function simulado_filter_video(){
	
	$('#simulado_filter_video').submit(function(event){
		banca = $('#banca_vimeo').val();
        orgao = $('#orgao_vimeo').val();
        cargo 	= $('#cargo_vimeo').val();
        disciplina = $('#disciplina_vimeo').val();
        assunto = $('#assunto_vimeo').val();
        nivel = $('#nivel_vimeo').val();
        ano = $('#ano_vimeo').val();

       $.post(BASEURL + "simulado/ajax_simulado_filter/1", 
       	{	
       		banca: banca,
	        orgao: orgao,
	        cargo: cargo,
	        disciplina: disciplina,
	        assunto: assunto,
	        nivel: nivel,
	        ano: ano
       	}
       	).done(function(result){
			$('#filter_result').html(result)		
		})
        event.preventDefault();
     })
}

function ajax_simulado_filter_set_simulado_video(){
	banca = $('#banca_vimeo').val();
    orgao = $('#orgao_vimeo').val();
    cargo = $('#cargo_vimeo').val();
    disciplina = $('#disciplina_vimeo').val();
    assunto = $('#assunto_vimeo').val();
    nivel = $('#nivel_vimeo').val();
    ano = $('#ano_vimeo').val();
    total_questoes = $('#total_questoes').val();
    token = $('#token').val();
    if(total_questoes == ''){
    	alert('Informe quantas questões deseja responder!');
    	$('#total_questoes').focus();
    	//event.preventDefault();
    } else {

	   $.post(BASEURL + "simulado/ajax_simulado_filter_set_simulado/1", 
	   	{	
	   		banca: banca,
	        orgao: orgao,
	        cargo: cargo,
	        disciplina: disciplina,
	        assunto: assunto,
	        nivel: nivel,
	        ano: ano,
	        total_questoes: total_questoes,
	        token: token
	   	}
	   	).done(function(result){
			if(result > 0){
				window.location.href =  BASEURL + 'simulado/prova/' + result;
				
			}
		})
    }
}


