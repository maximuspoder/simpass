<?php
/*
* 	package: assets
*	subpackage: assets.config
*	date: 21-08-2013
*/
#pekuhn10 pedrokuhn@terra.com.br
define('ENVIRONMENT', 'testing'); // development | testing | production
define('PRODUCTION', 1);
define('QCIDHASH', 'zplask19102');
define('TOKENHASH', '2V1Mul');
define('tokenTIMELINE', 'BRASILEPENTA');

define('URL_FACEBOOK','http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);

// Url do sistema
define('BASEURL', 'http://localhost:3000/');
// Link de conexão
define('DB_HOST',     'localhost');
// Nome do banco
define('DB_NAME', 'sep_development');
// Usuário do banco
define('DB_USER',     'root');
// Senha do usuário
define('DB_PASSWORD', '');


// Arquivos de javascript
define('JS', 		BASEURL . 'application/assets/js/');
// Arquivos de css
define('CSS', 		BASEURL . 'application/assets/css/');
// Arquivos de imagens
define('IMG', 		BASEURL . 'application/assets/img/');
// Pasta de libraries
define('LIBRARY', 	BASEURL . 'application/assets/library/');

define('LIBRARY_DEFAULT', 	BASEURL . 'application/assets/library/lib/');
// SERVER
define('SERVER',   $_SERVER['DOCUMENT_ROOT'] . '/');
// Pasta de thirdparty
define('THIRDPARTY', 	SERVER . 'application/third_party/');
// Pasta de uploads
define('UPLOAD', 	SERVER . 'application/assets/uploads/');
// Pasta de uploads LINK
define('UPLOADLINK', 	BASEURL . 'application/assets/uploads/');
// Hash para uso em senhas
define('HASH', 'AKSLDJALS7239ALSK-123KLASD-123JASKDJKJSAH8-234LASD-2UNJAW-ALKSDJ-S&P');


// Title do sistema
define('TITLE', 'Simule & Passe');
// Palavras chaves para busca
define('KEYWORDS', '');
// Descrição do site
define('CONTENT', '');

// Estilo parte de dentro
define('frontTheme', LIBRARY . 'inside/');
define('OLDfrontTheme', LIBRARY . 'oldlibraryinside/');
define('front_loginTheme', LIBRARY . 'outside/');
define('hometheme', LIBRARY . 'layout-frente/');
define('ASSETS', LIBRARY . 'assets_inside/');
