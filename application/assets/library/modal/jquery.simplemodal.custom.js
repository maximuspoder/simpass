/**
 * Created by fernando on 22/08/14.
 */
BASEURL = 'http://localhost/';


/**
 * open_modal()
 * Cria uma modal on the fly colocando conteudo dentro dela.
 * @param string titulo
 * @param string conteudo
 * @param string imagem
 * @param string style
 * @return void
 * chamada: <a href="javascript:void(0);" onclick="open_modal('Dados do usuário', '<strong>Vejaos dados do usuario</strong><br />', '', 300, 400);">LABEL</a>
 */
function open_modal(titulo, conteudo, imagem, height, width, close)
{
    var html  = '';
    var style_modal = '';
    var style_content = '';

    // Seta altura e largura
    height = (height != '' && height != null) ? height : 650;
    width = (width  != '' && width  != null) ? width : 800;
    style_modal = ' style="width:' + width + 'px;height:' + height + 'px"';
    style_content = ' style="height:' + (height - 80) + 'px !important;"';

    // Seta o boolean para close
    var bool_close = (close == false) ? false : true;

    // Img to close
    var close_img  = (close == false) ? '' : '<img src="' + $("#URL_IMG").val() + 'icon_close.png" id="img_close" class="img_close" title="Fechar" />';

    // Inicializa html da janela modal
    html += '<div class="modal" id="modal"' + style_modal + '>';
    html += '<div id="header">' + close_img + '<img src="' + imagem + '" /><h6>' + titulo  + '</h6></div>';
    html += '<div id="content" ' + style_content + '>' + conteudo + '</div><div id="footer"></div>';
    html += '</div>';

    // Inicializa a própria janela modal
    $.modal(html, {opacity: 60, zIndex: 10000, closeClass: 'img_close', escClose: bool_close, position: [0,0], close: bool_close});
}


/**
 * ajax_modal()
 * Cria uma modal on the fly colocando conteudo de ajax dentro dela.
 * @param string titulo
 * @param string url
 * @param string imagem
 * @param string style
 * @return void
 * chamada: <a href="javascript:void(0);" onclick="ajax_modal(\'Visualizar Dados\', \'' . URL_EXEC . 'controller/method/{0}\', \'' . URL_IMG . 'icon_view.png\', 600, 800);"><img src="' . URL_IMG . 'icon_view.png" title="Visualizar" /></a>
 */
function ajax_modal(titulo, url_param, imagem, height, width)
{
    var html  = '';
    var style_modal = '';
    var style_content = '';

    // Habilita loading
    enable_loading();

    // Dispara o ajax da url
    $.ajax({
        url: url_param,
        context: document.body,
        cache: false,
        async: false,
        // data: 'email=' + prUser,
        type: 'POST',
        success: function(data){
            // alert(txt);
            // Seta altura e largura
            height = (height != '' && height != null) ? height : 650;
            width = (width  != '' && width  != null) ? width : 800;
            style_modal = ' style="width:' + width + 'px;height:' + height + 'px"';
            style_content = ' style="height:' + (height - 80) + 'px !important;"';

            // Inicializa html da janela modal
            html += '<div class="modal" id="modal"' + style_modal + '>';
            html += '<div id="header"><img src="' + BASEURL + 'application/assets/img/close_22.png" class="img_close" id="img_close" title="Fechar" /><img src="' + imagem + '" /><h6>' + titulo  + '</h6></div>';
            html += '<div id="content" ' + style_content + '>' + data + '</div><div id="footer"></div>';
            html += '</div>';

            // Inicializa a própria janela modal
            $.modal(html, {opacity: 60, zIndex: 10000, closeClass: 'img_close', position: [0,0]});

            // Desabilita o loading
            disable_loading();
        }
    });
}




/**
 * iframe_modal()
 * Cria uma modal on the fly colocando conteudo uma pagina iframe.
 * @param string titulo
 * @param string url_param
 * @param string imagem
 * @param string style
 * @return void
 */
function iframe_modal(titulo, url_param, imagem, height, width, close)
{
    var html  = '';
    var style_modal = '';
    var style_content = '';

    // Seta altura e largura
    height = (height != '' && height != null) ? height : 650;
    width = (width  != '' && width  != null) ? width : 800;
    style_modal = ' style="width:' + width + 'px;height:' + height + 'px"';
    style_content = ' style="height:' + (height - 80) + 'px !important;"';

    // Seta o boolean para close
    var bool_close = (close == false) ? false : true;

    // Img to close
    var close_img  = '<img src="' + BASEURL + 'application/assets/images/close_22.png" class="img_close" id="img_close" title="Fechar" />'

    // Inicializa html da janela modal
    html += '<div class="modal" id="modal"' + style_modal + '>';
    html += '<div id="header">' + close_img + '<img src="' + imagem + '" /><h6>' + titulo  + '</h6></div>';
    html += '<div id="content" ' + style_content + '><iframe src="' + url_param + '" style="width:100%;height:' + (height - 80) + 'px !important;max-height:670px;border:0px solid green;z-index:10006;position:relative" align="left" frameborder="no"></iframe></div><div id="footer"></div>';
    html += '</div>';

    // Inicializa a própria janela modal
    $.modal(html, {opacity: 60, zIndex: 10000, closeClass: 'img_close', escClose: bool_close, position: [0,0], close: bool_close});
}


/**
 * enable_loading()
 * Habilita loading.
 * @return void
 */
function enable_loading()
{
    // Append do conteudo no body
    if($("#loading_layer").length <= 0)
    {
        /* $('body').append('<div id="loading_layer" style=\'position:absolute\'></div><div style="position:absolute; width:250px; height:80px; top:50%; left:50%;margin-left:-125px; background-color:red; z-index:1000;opacity:0%;"> <img src="' + $('#URL_IMG').val() + 'loading.gif"> Carregando... </div>'); */
        $('body').append('<div id="loading_layer"></div>');
    } else {
        $("#loading_layer").show();
    }
}


/**
 * enable_loading()
 * Desabilita loading.
 * @return void
 */
function disable_loading()
{
    // Verifica se o elemento existe, para fazer display:none;
    if($("#loading_layer").length > 0)
    {
        $("#loading_layer").hide();
    }
}
/**
 * close_modal()
 * Destrói a janela modal corrente da tela.
 * @return void
 */
function close_modal()
{
    // Linha utilizada para fechamento de dialog quando chamada por iframe
    $.modal.close();

    // Linha utilizada para fechamento de dialog quando chamada por ajax ou conteudo
    $('#simplemodal-overlay').remove();
    $('#simplemodal-container').remove();
}