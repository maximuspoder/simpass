(function($) { "use strict";
var revapi;
	jQuery(document).ready(function() {

		   revapi = jQuery('.tp-banner').revolution(
			{
				delay:5000,
				startwidth:1170,
				startheight:600,
				hideThumbs:10,
				fullWidth:"off",
				fullScreen:"on",
				fullScreenOffsetContainer: "",
				navigationType:"bullet",
				navigationArrows:"solo",						// use none, bullet or thumb
				navigationStyle:"round",     // round, square, navbar, round-old, square-old, navbar-old 
				touchenabled:"on",
				onHoverStop:"off"
			});

	});
 })(jQuery); 
