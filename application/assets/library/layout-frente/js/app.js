var BASEURL = "http://simuleepasse.com.br/";


function redirect(url)
{
	window.location.href = BASEURL +  url;
}

function new_register(){

	required = ["name", "surname", "day", "month","year", "sex", "password", "mail"];
	for (i=0;i<required.length;i++) {
		var input = $('#'+required[i]);
		if (input.val() == "") {
			input.addClass("required-field");
		} else if ($("input").length > 0) {
			input.addClass("success-field");
			input.removeClass("required-field");
		}
		else {
			input.removeClass("required-field");
		}
	}

	if ($(":input").hasClass("required-field")){
		return false;
	} else {
		var name = $("#name").val();
		var surname = $("#surname").val();
		var mail = $("#mail").val();
		var password = $("#password").val();
		var sex = $("#sex").val();
		var day = $("#day").val();
		var month = $("#month").val();
		var year = $("#year").val();
		$.post(BASEURL + "login/validate_mail", {mail: mail}).done(function(result){
			if(result == 0){
				$.post(BASEURL + "login/new_register", 
				{name: name, surname: surname, mail: mail, password: password, sex: sex, day: day, month: month, year: year}).done(function(resultRegister){
					if(resultRegister == 2){
						redirect('login/register_success');
					}
				})
			} else {
				$('#mail').val('');
				$('#mail').attr('placeholder', 'Este email já consta em nosso sistema!');
			}
		})
	}
}/*end function*/