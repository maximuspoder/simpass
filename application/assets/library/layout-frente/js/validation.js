$(document).ready(function(){

	required = ["name", "surname", "day", "month","year", "sex", "password", "mail", "message"];
	email = $("#mail");
	errornotice = $("#error");
	
	$("form").submit(function(){	

		for (i=0;i<required.length;i++) {
			var input = $('#'+required[i]);
			if (input.val() == "") {
				input.addClass("required-field");
				errornotice.fadeIn(750);
			} else if ($("input").length > 0) {
				input.addClass("success-field");
				input.removeClass("required-field");
			}
			else {
				input.removeClass("required-field");
			}
		}
	
		if (!/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email.val())) {
			email.addClass("required-field");
		} else {
			email.addClass("success-field");
			email.removeClass("required-field");
		}

		if ($(":input").hasClass("required-field")) {
			console.log('nopfoi')
			return false;
		} else {
			console.log('foi')
			return true;
		}
	});
	
	
});	