(function($)
{

	$(window).on('load', function(){
		setTimeout(function(){
			$("#news-featured-1").owlCarousel({
		      	slideSpeed : 300,
		      	paginationSpeed : 400,
		      	singleItem: true,
		      	navigation: true,
		      	navigationText: ['Mais atuais', 'Anteriores']
			});
		},100);
	});

})(jQuery);