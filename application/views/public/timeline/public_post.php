<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="author" content="Simule & Passe"/>
	<meta name="robots" content="Index,Follow"/>
	<meta name="description" content="A primeira Rede Social voltada para Concursos, com questões comentadas em vídeo."/>
	<meta name="keywords" content="concurso público, concursos, concurso, concursos públicos, questões, simulados, questões para concursos, questões resolvidas, questões com video, questões gabarito, prova de concurso, prova, CEF, BB, INSS, TCE, TRE, TRT, PRF, TJ, BANCO DO BRASIL, RECEITA FEDERAL, conhecimentos bancários, matemática, língua portuguesa, português, informática, noções de informática,técnico judiciário, direito, direito administrativo, direito constitucional, analista administrativo, técnico administrativo, analista judiciário, auditor fiscal, banca,banca organizadora, organizadora, pesquisar, aprendizado, estudo,  compartilhar, curtir, rede social, rede social para concursos, resolver, comentar, fórum, notícias"/>
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo JS; ?>appuser.js"></script>
</head>
<body class=" menu-right-hidden">
	<div class="container-fluid ">
		<div id="content">
		<?php
		if($this->session->userdata('logado')){
        userProfileHeader(
            $usuario = array(
                            'name' => $nameLOGGED . ' ' . $surnameLOGGED, 
                            'profile' => $profileLOGGED, 
                            'usertype' => $typeLOGGED, 
                            'account' => $accountLOGGED,
                            'token' => $this->session->userdata('token')
                        )
        );
    	} else {
        	userProfileHeaderUnregistered();
        }
        ?>


<div class="container">
<div class="innerAll">
	<div class="row">
	<div class="col-lg-9 col-md-8">
	<br/>
	<div class="timeline-cover">
	<div class="widget border-bottom">

		<div class="widget-body border-bottom">
			<div class="media">
				<div class="pull-left innerAll">
					<img src="<?php echo $profile; ?>" alt="" class="img-circle">
				</div>
				<div class="media-body">
					<h4>
						<a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $userid; ?>" data-toggle="tooltip" data-original-title="Tooltip on Left" data-placement="left">
							<?php echo $name . ' ' . $surname; ?>
						</a>
					</h4>
					<div class="clearfix"></div>
					<p>
						<?php echo $cursor['post']; ?></p>					
				</div>
			</div>
		</div>

	</div>

	<div class="timeline-bottom innerT half">
	  	<span class="innerL"><i class="fa fa-calendar fa-fw"></i>
	    <?php 
	    date_default_timezone_set('America/Sao_Paulo');
	    echo date('d/m/Y H:i:s', $cursor['date']->sec); 
	    ?>
	    </span>
    </div>
    <div id="fb-like-box">
		<div class="fb-like" data-href="http://simuleepasse.com.br/timeline/public_post/<?php echo encrypt($cursor['_id'], tokenTIMELINE); ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
	</div>
	</div>

	</div>
	</div>
</div>
</div>

	<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=1447963622096096&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
</body>
</html>