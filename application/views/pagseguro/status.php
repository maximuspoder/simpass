<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
             <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>
            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            
                            


                            <div class="widget widget-body-white" style="margin-top:20px;">
                        <div class="widget-head">
                            <h4 class="heading glyphicons calendar"><i></i> Seus histórico de assinaturas até <?php echo date('d/m/Y')?></h4>
                        </div>
                        
                        
                        <div class="widget-body">
                        
                        
                            <div class="separator bottom">
                                Total de requisições para compra: <b><?php echo count($assinaturas); ?></b>
                               
                                <div class="clearfix"></div>
                            </div>
                            
                            <table class="table table-responsive table-condensed table-striped table-primary table-vertical-center checkboxs">
                                <thead>
                                    <tr>
                                        <th class="center">ID Transação(#)</th>
                                        <th class="center">Data</th>
                                        <th class="center">Tipo da Compra</th>
                                        <th class="center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if(count($assinaturas) > 0){
                                foreach($assinaturas as $data): 
                                ?>
                                    <tr class="selectable">
                                        <td class="center"><?php echo $data['transactionid']; ?></td>
                                        <td class="center"><?php echo $data['data_registro']; ?></td>
                                        <td class="center"><?php echo ($data['tipo_assinatura'] == 'mes') ? 'Apenas um mês' : 'Assinatura mensal'; ?></td>
                                        <td class="center"><?php echo ($data['status'] == 0) ? 'Aguardando pagamento ou liberação pelo PagSeguro' : 'Liberado'; ?></td>
                                    </tr>
                                <?php 
                                endforeach; 
                                } else {
                                ?>
                                <tr class="selectable">
                                    <td class="center">Nenhuma compra realizada até o momento</td>
                                    <td class="center">-</td>
                                    <td class="center">-</td>
                                </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                                        
                        </div>
                    </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#5dc94b',
        dangerColor = '#ff553e',
        successColor = '#80cf47',
        infoColor = '#6fa3cb',
        warningColor = '#f48f3e',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>



<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_gridalicious/jquery.gridalicious.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/media_gridalicious/gridalicious.init.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script> 
<script src="<?php echo frontTheme; ?>assets/plugins/forms_elements_jasny-fileupload/js/bootstrap-fileupload.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
<script src="<?php echo JS; ?>appuser.js"></script>
</body>
</html>