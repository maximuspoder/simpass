<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="author" content="Simule & Passe"/>
	<meta name="robots" content="Index,Follow"/>
	<meta name="description" content="A primeira Rede Social voltada para Concursos, com questões comentadas em vídeo."/>
	<meta name="keywords" content="concurso público, concursos, concurso, concursos públicos, questões, simulados, questões para concursos, questões resolvidas, questões com video, questões gabarito, prova de concurso, prova, CEF, BB, INSS, TCE, TRE, TRT, PRF, TJ, BANCO DO BRASIL, RECEITA FEDERAL, conhecimentos bancários, matemática, língua portuguesa, português, informática, noções de informática,técnico judiciário, direito, direito administrativo, direito constitucional, analista administrativo, técnico administrativo, analista judiciário, auditor fiscal, banca,banca organizadora, organizadora, pesquisar, aprendizado, estudo,  compartilhar, curtir, rede social, rede social para concursos, resolver, comentar, fórum, notícias"/>
	<title><?php echo TITLE; ?></title>
	<meta name="google-site-verification" content="qXEQf03xEeXOpI-kwqgPfEIQS8FhvjAiAnb8TPkYU1U" />
</head>
<body data-offset="62" data-spy="scroll" data-target=".navbar">

<div id="pageloader">
	<div class="loader-img">
		<img alt="loader" src="<?php echo hometheme; ?>img/loader.gif?data=2014" />
	</div>
</div>


<section id="contact">
<div class="fullwidth-section">
<div class="parallax img-overlay4" style="background-image: url('<?php echo hometheme; ?>img/pictures/4.jpg?data=2014')" data-stellar-background-ratio="0.3"></div>
<div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.7);"></div>
	<div class="container">
		<div class="row" style="margin-bottom: 20px;">
		<div class="col-md-6 col-md-offset-3 text-center">

		<h1 class="weight-800 kill-top-margin uppercase color-white">Simule & Passe</h1>
		<h4 class="color-white weight-400">Informe os seus dados de acesso abaixo para entrar no sistema.</h4>
		</div>
		</div>

			<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<form action="<?php echo BASEURL; ?>login/login_process" method="post" role="form">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Email</label>
										<input class="form-control" name="email" id="email" placeholder="" type="email" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Senha</label>
										<input class="form-control" name="senha" id="senha" placeholder=""  type="password" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									
									<div class="text-center"><br/>
										<button class="btn btn-primary" type="submit">Entrar</button><br>
										<div id="password_recovery"><a href="<?php echo BASEURL; ?>login/recuperar_senha">Recuperar minha senha</a></div>
								</div>
							</div>
						</form>
			</div>
			</div>
	</div>
</div>
</section>

<div class="fullwidth-section half-padding footer">
	<div class="container">
		<div class="row social-icons text-center">
			<div class="col-md-12">

			</div>
		</div>
		<div class="row text-center" style="margin-top: 20px">
			<div class="col-md-12">
				<br />© 2014 Todos direitos reservados a Simule & Passe<br />
				Contato: administrador@simuleepasse.com.br<br/>
				Porto Alegre - RS
			</div>
		</div>
	</div>
</div>

	<link href="<?php echo CSS; ?>home.css" media="screen" rel="stylesheet" type="text/css">
	<link href="<?php echo hometheme; ?>css/color-schemes/marine.css?data=<?php echo date('Y-m-d'); ?>" media="screen" rel="stylesheet" type="text/css">
	<link href="<?php echo hometheme; ?>css/bootstrap.min.css?data=<?php echo date('Y-m-d'); ?>" media="screen" rel="stylesheet" type="text/css">
	<link href="<?php echo hometheme; ?>css/buttons.css?data=<?php echo date('Y-m-d'); ?>" media="screen" rel="stylesheet" type="text/css">	
	<link id="header-switch" href="<?php echo hometheme; ?>css/headers/dark.css?data=<?php echo date('Y-m-d'); ?>" media="screen" rel="stylesheet" type="text/css">	
	<link href="<?php echo hometheme; ?>css/astonish.min.css?data=<?php echo date('Y-m-d'); ?>" media="screen" rel="stylesheet" type="text/css">

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js?data=<?php echo date('Y-m-d'); ?>"></script>
	<script type="text/javascript" src="<?php echo hometheme; ?>js/closure.jquery.easing.1.3.js?data=<?php echo date('Y-m-d'); ?>"></script>
	<script type="text/javascript" src="<?php echo hometheme; ?>js/counter.js?data=<?php echo date('Y-m-d'); ?>"></script>
	<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.isotope.min.js?data=<?php echo date('Y-m-d'); ?>"></script>
	<script type="text/javascript" src="<?php echo hometheme; ?>js/animation-engine.js?data=<?php echo date('Y-m-d'); ?>"></script>
	<script type="text/javascript" src="<?php echo hometheme; ?>js/validation.js?data=<?php echo date('Y-m-d'); ?>"></script>
	<script type="text/javascript" src="<?php echo hometheme; ?>js/astonish.js?data=<?php echo date('Y-m-d'); ?>"></script>
	<script type="text/javascript" src="<?php echo JS; ?>home.js"></script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-50527649-1', 'simuleepasse.com.br');
	  ga('send', 'pageview');
	</script>	
</body>
</html>