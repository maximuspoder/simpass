<!DOCTYPE html>
<html lang="pt-br">
<head>
	
	<meta name="google-site-verification" content="qXEQf03xEeXOpI-kwqgPfEIQS8FhvjAiAnb8TPkYU1U" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta property="og:locale" content="pt_BR">
	<meta property="og:url" content="http://simuleepasse.com.br">
	<meta property="og:title" content="Simule & Passe">
	<meta property="og:site_name" content="Simule & Passe">
	<meta property="og:description" content="O S&P é a maior rede social para concurseiros, tendo os melhores professores do mercado e também uma técnica inovadora para estudos baseado em questões com videos.">
	<meta property="og:image" content="http://simuleepasse.com.br/application/assets/uploads/cover/default.png">
	<meta property="og:image:width" content="963">
	<meta property="og:image:height" content="399">
 
	<meta name="author" content="Simule & Passe"/>
	<meta name="robots" content="Index,Follow"/>
	<meta name="description" content="A primeira Rede Social voltada para Concursos, com questões comentadas em vídeo."/>
	<meta name="keywords" content="questões comentadas, provas de concursos, provas de concursos públicos, provas concurso, curso para concurso, concurso público, concursos, concurso, concursos públicos, questões, simulados, questões para concursos, questões resolvidas, questões com video, questões gabarito, prova de concurso, prova, CEF, BB, INSS, TCE, TRE, TRT, PRF, TJ, BANCO DO BRASIL, RECEITA FEDERAL, conhecimentos bancários, matemática, língua portuguesa, português, informática, noções de informática,técnico judiciário, direito, direito administrativo, direito constitucional, analista administrativo, técnico administrativo, analista judiciário, auditor fiscal, banca,banca organizadora, organizadora, pesquisar, aprendizado, estudo,  compartilhar, curtir, rede social, rede social para concursos, resolver, comentar, fórum, notícias"/>
	<title><?php echo TITLE; ?></title>
</head>
<body data-offset="62" data-spy="scroll" data-target=".navbar">

<div id="pageloader">
	<div class="loader-img"><img alt="loader" src="<?php echo hometheme; ?>img/loader.gif?data=<?php echo date('Y'); ?>" /></div>
</div>


<nav class="navbar navbar-default navbar-fixed-top nav-fadein" role="navigation">
  <div class="container">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-menu">
      <i class="fa fa-bars fa-fw"></i>
      </button>
      <a class="navbar-brand weight-900" href="#home"><img alt="" id="Simule & Passe" src="<?php echo IMG; ?>logo-sep_198_47.png?data=<?php echo date('Y'); ?>"></a>
    </div>


    <div class="collapse navbar-collapse" id="main-menu">
      <ul class="nav navbar-nav navbar-right">
        <li><a class="scrollto" href="#home">Home</a></li>
		<li><a class="scrollto" href="#sobre">Sobre</a></li>
		<li><a class="scrollto" href="<?php echo BASEURL; ?>noticias">Notícias</a></li>
		<li><a class="scrollto" href="#recursos">Recursos</a></li>
		<li><a class="scrollto" href="#professores">Professores</a></li>
		<li><a class="scrollto" href="#cadastro">Cadastrar</a></li>
		<li><a class="scrollto" href="http://simuleepasse.com.br/simulado/free/n2a5k7Cqz2ePY7CrpZePkn6lz2u5Yn6lpWOK/1﻿">Simulado</a></li>
		<li><a class="scrollto" href="#login">Login</a></li>
		<li>
			<div class="social-icons">
				<a href="https://www.facebook.com/simuleepasse" target="_blank"><i class="im-facebook"></i></a>
			</div>
		</li>
      </ul>
    </div>
  </div>
</nav>
<section id="home">
<div class="tp-banner-container">
		<div class="tp-banner" >
			<ul>
				<li data-transition="fade" data-slotamount="5" data-masterspeed="100" class="parallax" data-stellar-background-ratio="0.2">
					<img src="<?php echo hometheme; ?>img/pictures/4.jpg?data=<?php echo date('Y'); ?>"  alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
					<div class="img-overlay3"></div>
 					<div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.3);"></div>

					
					<div class="tp-caption medium_light_white sft text-center"
						data-x="center"
						data-hoffset="0"
						data-y="150"
						data-voffset="0"
						data-speed="1000"
						data-start="500"
						data-easing="Back.easeInOut"
						data-endspeed="300">
						Você está pronto para algo
					</div>
					<div class="tp-caption super_large_bold_white sfb"
						data-x="center"
						data-hoffset="0"
						data-y="center"
						data-voffset="0"
						data-speed="1000"
						data-start="1200"
						data-easing="Back.easeInOut"
						data-endspeed="300">Jamais Visto Antes?
					</div>
					<div class="tp-caption bottom_uppercase fadein text-center"
						data-x="center"
						data-hoffset="0"
						data-y="450"
						data-voffset="0"
						data-speed="2000"
						data-start="2000"
						data-easing="Back.easeInOut"
						data-endspeed="300">
					</div>
				</li>
				<li data-transition="fade" data-slotamount="5" data-masterspeed="700">
					<img src="<?php echo hometheme; ?>img/pictures/4.jpg?data=<?php echo date('Y'); ?>"   alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
					<div class="img-overlay3"></div>
 					<div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.3);"></div>

					<div class="tp-caption large_bold_white sft text-center"
						data-x="center"
						data-hoffset="0"
						data-y="150"
						data-voffset="0"
						data-speed="1000"
						data-start="500"
						data-easing="Back.easeInOut"
						data-endspeed="300">
						<img src="<?php echo IMG; ?>logo-sep.png" width="400">
					</div>
					<div class="tp-caption small_light_white sfb"
						data-x="center"
						data-hoffset="0"
						data-y="center"
						data-voffset="0"
						data-speed="1000"
						data-start="1200"
						data-easing="Back.easeInOut"
						data-endspeed="300">
						O website mais completo da internet<br />
						para você que deseja passar em um concurso economizando!<br />
						Prepare-se para a revolução.
					</div>

					<div class="tp-caption fadein text-center"
						data-x="center"
						data-hoffset="0"
						data-y="400"
						data-voffset="0"
						data-speed="2000"
						data-start="2000"
						data-easing="Back.easeInOut"
						data-endspeed="300">
						<a class="scrollto btn btn-lg btn-default btn-outline" href="#cadastro">Cadastrar \o/</a>
					</div>
				</li>
			</ul>
		
		</div>
	</div>
</section>









<section id="sobre">
<div class="fullwidth-section" style="background-color: #fff">
<div class="container">
	<div class="row" style="margin-bottom: 60px;">
		<div class="col-md-8 col-md-offset-2 text-center">
			<h1 class="weight-800 kill-top-margin uppercase">Sobre nós</h1>
			<h4 class="weight-400">As funcionalidades do sistema:</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="icon-feature-horizontal">
				<div class="icon">
					<i class="fa-globe color-primary ae"></i></div>
				<div class="content">
					<h4 class="uppercase weight-700">Notícias</h4>
					<p>Fique por dentro de tudo o que acontece no mundo dos concursos.</p>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="icon-feature-horizontal">
				<div class="icon">
					<i class="im-support color-primary ae" data-delay="250"></i>
				</div>
				<div class="content">
					<h4 class="uppercase weight-700">Atendimento ao cliente</h4>
					<p>Ainda está com dúvidas? Entre em contato! Retornaremos logo para você.</p>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="icon-feature-horizontal">
				<div class="icon">
					<i class="fa-comment-o color-primary ae" data-delay="500"></i></div>
				<div class="content">
					<h4 class="uppercase weight-700">Área Social</h4>
					<p>Faça novas amizades, troque informações sobre concursos, siga professores.</p>
				</div>
			</div>
		</div>
	</div>
		<div class="row">
			<div class="col-md-4">
				<div class="icon-feature-horizontal">
					<div class="icon">
						<i class="fa-video-camera color-primary ae" ></i></div>
					<div class="content">
						<h4 class="uppercase weight-700">Videos</h4>
						<p>Estude por questões, videos e tire sua dúvida no fórum.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="icon-feature-horizontal">
					<div class="icon">
						<i class="fa-quote-right color-primary ae" data-delay="250"></i></div>
					<div class="content">
						<h4 class="uppercase weight-700">Dicas</h4>
						<p> Os melhores professores, as melhores dicas.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="icon-feature-horizontal">
					<div class="icon">
						<i class="im-menu color-primary ae"  data-delay="500"></i></div>
					<div class="content">
						<h4 class="uppercase weight-700">Muito mais</h4>
						<p>Não fique fora dessa! Cadastre-se agora mesmo. Você não paga nada e descobre muito mais.</p>
					</div>
				</div>
			</div>
		</div>
</div>
</div>
<div class="fullwidth-section">
<div class="parallax img-overlay3" style="background-image: url('<?php echo hometheme; ?>img/pictures/4.jpg')" data-stellar-background-ratio="0.2"></div>
<div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.5);"></div>
	<div class="container">
		<div class="row color-white counter-section">
			<div class="col-md-3 text-center">
			<i class="im-users color-white"></i>
				<div class="counter" data-end-count="19" data-speed="2500">0</div>
				<h4 class="color-white uppercase weight-600">Professores</h4>
			</div>
			<div class="col-md-3 text-center">
				<a href="https://www.facebook.com/simuleepasse" target="_blank">
				<i class="im-facebook color-white"></i>
				</a>
				<div id="facebook_count" class="counter" data-end-count="0" data-speed="2500">0</div>
				<h4 class="color-white uppercase weight-600">Facebook</h4>
			
			</div>
			<div class="col-md-3 text-center">
				<i class="fa-video-camera color-white"></i>
				<div id="questions_count_recorded" class="counter" data-end-count="" data-speed="2500">0</div>
					<h4 class="color-white uppercase weight-600">Vídeos </h4>
			</div>
			<div class="col-md-3 text-center">
				<i class="fa-file-text-o color-white"></i>
				<div id="questions_count" class="counter" data-end-count="0" data-speed="2500">0</div>
				<h4 class="color-white uppercase weight-600">Questões + </h4>
			</div>
		</div>
	</div>
</div>
</section>



<section id="recursos">
<div class="fullwidth-section" style="background-color: #F5F5F5">
<div class="container">
<div class="row" style="margin-bottom: 40px;">
<div class="col-md-6 col-md-offset-3 text-center">
<h1 class="weight-800 kill-top-margin uppercase">Recursos</h1>
<h4 class="weight-400">Apenas alguns de muitos:</h4>
</div>
</div>
<div class="row">
<div class="col-md-4">
	<div class="icon-feature-horizontal ae" data-animation="bounceInLeft" data-offset="70%">
		<div class="icon"><i class="im-laptop color-primary"></i></div>
			<div class="content">
				<h4 class="uppercase weight-700">100% Mobile</h4>
				<p>Acesse de qualquer lugar, em qualquer hora.</p>
			</div>
	</div>
	<div class="icon-feature-horizontal ae" data-animation="bounceInLeft" data-offset="70%">
		<div class="icon"><i class="im-file color-primary"></i></div>
		<div class="content">
			<h4 class="uppercase weight-700">Questões</h4>
			<p>Cada vez mais questões em nosso banco de dados.</p>
		</div>
	</div>
	<div class="icon-feature-horizontal ae" data-animation="bounceInLeft" data-offset="70%">
		<div class="icon"><i class="im-film color-primary"></i></div>
		<div class="content">
			<h4 class="uppercase weight-700">Videos</h4>
			<p>Professores empenhados em oferecer a melhor qualidade para você.</p>
		</div>
	</div>
	<div class="icon-feature-horizontal ae" data-animation="bounceInLeft" data-offset="70%">
		<div class="icon"><i class="fa-group color-primary"></i></div>
		<div class="content">
			<h4 class="uppercase weight-700">Amigos</h4>
			<p>Siga professores e seus amigos.</p>
		</div>
	</div>
</div>
	<div class="col-md-8 ae" style="margin-top: 40px" data-animation="bounceInRight">
	<img alt="" class="img-responsive img-center" src="<?php echo hometheme; ?>img/macbook.png">
	</div>
</div>
</div>
</div>
</section>



<section id="professores">
<div class="fullwidth-section">
	<div class="container">
		<div class="row" >
			<div class="col-md-6 col-md-offset-3 text-center">
				<h1 class="weight-800 kill-top-margin uppercase">Uma parte da equipe</h1>
				<h5>Um time de professores, com vasta experiência no ramo dos concursos, que decidiu participar de um projeto
					inovador: A primeira rede social voltada para concursos, com questões comentadas em video.</h5>
			</div>
		</div>
	</div>
</div>



<div class="fullwidth-section half-padding">
<div class="container">
	<div class="row">
	    <div id="team-slider" class="owl-carousel">
			
			<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons">
					<a href="https://www.facebook.com/alinedoval" target="_blank"><i class="im-facebook"></i></a>
				</div>
				</div>
				<img alt="Aline Doval" src="<?php echo IMG; ?>professores/aline_doval.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Aline Doval</h4>
			<h5 class="color-primary uppercase">Direito Administrativo e Previdenciário</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Vanessa Loureiro" src="<?php echo IMG; ?>professores/vanessa_loureiro.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Vanessa Loureiro</h4>
			<h5 class="color-primary uppercase">Português</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Fábio D'Ávila" src="<?php echo IMG; ?>professores/fabio_davila.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Fábio D'Ávila</h4>
			<h5 class="color-primary uppercase">Português</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons">
					<a href="https://www.facebook.com/professorataisflores" target="_blank">
						<i class="im-facebook"></i>
					</a>
				</div>
				</div>
				<img alt="Táis Flores" src="<?php echo IMG; ?>professores/tais_flores.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Taís Flores</h4>
			<h5 class="color-primary uppercase">Direito Administrativo e Eleitoral</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Carmen Alessandra" src="<?php echo IMG; ?>professores/carmen.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Carmen Alessandra</h4>
			<h5 class="color-primary uppercase">Português</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Newton Jancowski" src="<?php echo IMG; ?>professores/newton_jancowski.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Newton Jancowski</h4>
			<h5 class="color-primary uppercase">Direito Processual Civil</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons">
					<a href="https://www.facebook.com/marcosvice" target="_blank">
					<i class="im-facebook"></i>
					</a>
				</div>
				</div>
				<img alt="Marcos de Araujo" src="<?php echo IMG; ?>professores/marcos.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Marcos de Araujo</h4>
			<h5 class="color-primary uppercase">Português</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Francine Villar" src="<?php echo IMG; ?>professores/francine_villar.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Francine Villar</h4>
			<h5 class="color-primary uppercase">Direito Constitucional</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Daniel Topanoti" src="<?php echo IMG; ?>professores/daniel_topanoti.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Daniel Topanoti</h4>
			<h5 class="color-primary uppercase">Matemática e Raciocínio Lógico</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Jean Gamboa" src="<?php echo IMG; ?>professores/jean.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Jean Gamboa</h4>
			<h5 class="color-primary uppercase">Conhecimentos Bancários</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Strt teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Leonidas Cavalcante" src="<?php echo IMG; ?>professores/leonidas_cavalcante.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Leonidas Cavalcante</h4>
			<h5 class="color-primary uppercase">Direito Processual Penal</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Leandro Laste" src="<?php echo IMG; ?>professores/leandro_laste.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Leandro Laste</h4>
			<h5 class="color-primary uppercase">Matemática Financeira e Raciocínio Lógico</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Renato Pinheiro" src="<?php echo IMG; ?>professores/renato_pinheiro.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Renato Pinheiro</h4>
			<h5 class="color-primary uppercase">Noções de Informática, Redes de Computadores e Sistemas Operacionais</h5>
			<p></p>
		   	<hr/>
		   	</div>
		   	
		   

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Hendel" src="<?php echo IMG; ?>professores/hendel.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Hendel</h4>
			<h5 class="color-primary uppercase">Direito Empresarial e Comercial</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons">
					<a href="https://www.facebook.com/profile.php?id=100007859385456&fref=ts" target="_blank">
					<i class="im-facebook"></i>
					</a>
				</div>
				</div>
				<img alt="Carlos Pozzer" src="<?php echo IMG; ?>professores/carlos_pozzer.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Carlos Pozzer</h4>
			<h5 class="color-primary uppercase">Direito do Trabalho</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   	<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Gabriel Nuñez" src="<?php echo IMG; ?>professores/gabriel_nunez.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Gabriel Nuñez</h4>
			<h5 class="color-primary uppercase">Direito Processual do Trabalho</h5>
			<p></p>
		   	<hr/>
		   	</div>

		   

		   		<!-- Start teacher -->
			<div class="team-wrapper">
				<div class="team-img-wrapper">
				<div class="team-img-wrapper-hover">
				<div class="social-icons"><i class="im-facebook"></i></div>
				</div>
				<img alt="Fábio Castilhos" src="<?php echo IMG; ?>professores/fabio_castilhos.jpg?data=<?php echo date('Y'); ?>" class="img-responsive img-center">
				</div>
				<h4 class="weight-700">Fábio Castilhos</h4>
			<h5 class="color-primary uppercase">Português</h5>
			<p></p>
		   	<hr/>
		   	</div>

	

	    </div>
	</div>
</div>
</div>
</section>




<section id="login">
<div class="fullwidth-section">
<div class="parallax img-overlay4" style="background-image: url('<?php echo hometheme; ?>img/pictures/4.jpg?data=2014')" data-stellar-background-ratio="0.3"></div>
<div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.7);"></div>
	<div class="container">
		<div class="row" style="margin-bottom: 20px;">
		<div class="col-md-6 col-md-offset-3 text-center">

		<h1 class="weight-800 kill-top-margin uppercase color-white">Simule & Passe</h1>
		<h4 class="color-white weight-400">Informe os seus dados de acesso abaixo para entrar no sistema.</h4>
		</div>
		</div>

			<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<form action="<?php echo BASEURL; ?>login/login_process" method="post" role="form">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Email</label>
										<input class="form-control" name="email" id="email" placeholder="" type="email" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Senha</label>
										<input class="form-control" name="senha" id="senha" placeholder=""  type="password" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									
									<div class="text-center"><br/>
										<button class="btn btn-primary" type="submit">Entrar</button><br>
										<div id="password_recovery"><a href="<?php echo BASEURL; ?>login/recuperar_senha">Recuperar minha senha</a></div>
								</div>
							</div>
						</form>
			</div>
			</div>
	</div>
</div>
</section>





<section id="cadastro">
<div class="fullwidth-section" style="background-color: #fff">
<div class="container">
<div class="row" style="margin-bottom: 60px;">

<div class="col-md-8 col-md-offset-2 text-center">
<h1 class="weight-800 kill-top-margin uppercase">Cadastre-se!</h1>
<h4 class="weight-400">
 O <em>Simule & Passe</em> é uma ferramenta completa para você concurseiro. É de graça, cadastre-se!
</h4>

</div>
</div>


<div class="row">
<div class="col-md-8 col-md-offset-2">

	<form action="javascript:void(0)" name="form_default" id="form_default" method="post">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Nome</label>
					<input type="text" class="form-control" name="name" id="name"/>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Sobrenome</label>
					<input type="text" class="form-control" name="surname" id="surname"/>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" name="email" id="email" style="display:none" />
					<input type="email" class="form-control" name="mail" id="mail" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Senha</label>
					<input type="password" class="form-control" name="password" id="password" />
				</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-6">
				<div class="form-group">
				<label>Sexo</label>
					<select name="sex" id="sex" class="form-control">
						<option></option>
						<option value="F">Feminino</option>
						<option value="M">Masculino</option>
					</select>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label>Dia</label>
					<select name="day" id="day" class="form-control">
						<?php echo calendar_days(); ?>
					</select>
				</div>
			</div>
			
			<div class="col-md-2">
				<div class="form-group">
					<label>Mês</label>
					<select name="month" id="month" class="form-control">
						<?php calendar_month() ?>
					</select>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label>Ano</label>
					<select name="year" id="year" class="form-control">
						<?php calendar_year() ?>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="text-center"><br/>
					<button class="btn btn-primary btn-round" onclick="new_register()">Cadastrar</button>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</section>




<section id="contato">
<div class="fullwidth-section">
<div class="parallax img-overlay4" style="background-image: url('<?php echo hometheme; ?>img/pictures/4.jpg')" data-stellar-background-ratio="0.3"></div>
<div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.7);"></div>
<div class="container">
<div class="row" style="margin-bottom: 20px;">
<div class="col-md-6 col-md-offset-3 text-center">

<h1 class="weight-800 kill-top-margin uppercase color-white">Contato</h1>
<h4 class="color-white weight-400">Para entrar em contato conosco, preencha os campos abaixo com seus dados e aguarde. Logo retornaremos.</h4>
</div>
</div>

<div class="row">
<div class="col-md-8 col-md-offset-2">
	<form action="<?php echo BASEURL; ?>contato/enviar_mensagem" method="post" role="form">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Diga-nos seu nome</label>
					<input class="form-control" name="name_contact" id="name_contact" placeholder="" type="text" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Qual seu email?</label>
					<input class="form-control" name="email_contact" id="email_contact" placeholder=""  type="email" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label>Qual sua dúvida?</label>
					<textarea class="form-control" id="message_contact" name="message_contact" placeholder="" rows="8"></textarea>
				</div>
				<div class="text-center"><br/>
					<button class="btn btn-primary btn-round" type="submit">Enviar a mensagem</button>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>
</section>

<div class="fullwidth-section half-padding footer">
	<div class="container">
		<div class="row social-icons text-center">
			<div class="col-md-12">

			</div>
		</div>
		<div class="row" style="margin-top: 20px;">
			<div class="col-md-2"></div>
			<div class="col-md-5">
				<br />© 2014 Todos direitos reservados a Simule & Passe<br />
				Contato: <a href="mailto:administrador@simuleepasse.com.br">administrador@simuleepasse.com.br</a><br/>
				Porto Alegre - RS<br />
				<br />


				




			</div>
			<div class="col-md-6">
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=569282369859117&version=v2.0";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
			</div>
			<div class="fb-like-box" data-href="https://www.facebook.com/simuleepasse" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>

		</div>
	</div>
</div>


<link href="<?php echo CSS; ?>home.css?data=<?php echo date('Y'); ?>" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/bootstrap.min.css?data=<?php echo date('Y'); ?>" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/buttons.min.css?data=<?php echo date('Y'); ?>" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/font-awesome.min.css?data=<?php echo date('Y'); ?>" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/icomoon.min.css?data=<?php echo date('Y'); ?>" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/slider-settings.min.css?data=<?php echo date('Y'); ?>" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/prettyPhoto.min.css?data=<?php echo date('Y'); ?>" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/animate.min.css?data=<?php echo date('Y'); ?>" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/astonish.min.css?data=<?php echo date('Y'); ?>" media="screen" rel="stylesheet" type="text/css">
<link id="header-switch" href="<?php echo hometheme; ?>css/headers/light.min.css?data=<?php echo date('Y'); ?>" media="screen" rel="stylesheet" type="text/css">

<script src="<?php echo JS; ?>jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/stellar.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/counter.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.nicescroll.plus.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.themepunch.revolution.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.themepunch.rev-defaults.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/animation-engine.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/astonish.js"></script>
<script type="text/javascript" src="<?php echo JS; ?>validation.min.js"></script>
<script type="text/javascript" src="<?php echo JS; ?>home.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50527649-1', 'simuleepasse.com.br');
  ga('send', 'pageview');
</script>
</body>
</html>