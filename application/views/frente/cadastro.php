<!DOCTYPE html>
<html lang="pt-br">
<head>

<meta name="google-site-verification" content="qXEQf03xEeXOpI-kwqgPfEIQS8FhvjAiAnb8TPkYU1U" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Simule e Passe</title>

<link href="<?php echo hometheme; ?>css/bootstrap.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/buttons.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/font-awesome.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/icomoon.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/slider-settings.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/prettyPhoto.css" media="screen" rel="stylesheet" type="text/css">
<link id="header-switch" href="<?php echo hometheme; ?>css/headers/dark.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/animate.css" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo hometheme; ?>css/astonish.css" media="screen" rel="stylesheet" type="text/css">
</head>
<body data-offset="62" data-spy="scroll" data-target=".navbar" onload="initialize()">

<div id="pageloader">
	<div class="loader-img">
		<img alt="loader" src="<?php echo hometheme; ?>img/loader.gif" /> </div>
</div>


<section id="home">

<div class="tp-banner-container">
		<div class="tp-banner" >
			<ul>
				
				<li data-transition="fade" data-slotamount="5" data-masterspeed="700" class="parallax" data-stellar-background-ratio="0.2">
					
				
					<img src="<?php echo hometheme; ?>img/pictures/4.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
					<div class="img-overlay3"></div>
 <div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.3);"></div>

					
					<div class="tp-caption medium_light_white sft text-center"
						data-x="center"
						data-hoffset="0"
						data-y="150"
						data-voffset="0"
						data-speed="1000"
						data-start="500"
						data-easing="Back.easeInOut"
						data-endspeed="300">
						Você está pronto para algo
					</div>

					<div class="tp-caption super_large_bold_white sfb"
						data-x="center"
						data-hoffset="0"
						data-y="center"
						data-voffset="0"
						data-speed="1000"
						data-start="1200"
						data-easing="Back.easeInOut"
						data-endspeed="300">Jamais Visto Antes?
					</div>
<div class="tp-caption bottom_uppercase fadein text-center"
						data-x="center"
						data-hoffset="0"
						data-y="450"
						data-voffset="0"
						data-speed="2000"
						data-start="2000"
						data-easing="Back.easeInOut"
						data-endspeed="300">
						Comece agora mesmo!
					</div>
					<div class="tp-caption fadein text-center"
						data-x="center"
						data-hoffset="0"
						data-y="500"
						data-voffset="0"
						data-speed="2000"
						data-start="2000"
						data-easing="Back.easeInOut"
						data-endspeed="300">
						<a class="scrollto" href="#about"><i class="slider-button-round fa-angle-double-down"></i></a>
					
					</div>
				</li>
				<!-- SLIDE  -->
				<li data-transition="fade" data-slotamount="5" data-masterspeed="700" >
					<!-- MAIN IMAGE -->
					
				
					<img src="<?php echo hometheme; ?>img/pictures/4.jpg"   alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
				
					<div class="img-overlay3"></div>
 <div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.3);"></div>

					<!-- LAYERS -->
					<div class="tp-caption large_bold_white sft text-center"
						data-x="center"
						data-hoffset="0"
						data-y="150"
						data-voffset="0"
						data-speed="1000"
						data-start="500"
						data-easing="Back.easeInOut"
						data-endspeed="300">
						Simule & Passe
					</div>
					<div class="tp-caption small_light_white sfb"
						data-x="center"
						data-hoffset="0"
						data-y="center"
						data-voffset="0"
						data-speed="1000"
						data-start="1200"
						data-easing="Back.easeInOut"
						data-endspeed="300">
						O website mais completo da internet<br />
						para você que deseja passar em um concurso economizando!<br />
						Prepare-se para a revolução!!!
					</div>

					<div class="tp-caption fadein text-center"
						data-x="center"
						data-hoffset="0"
						data-y="400"
						data-voffset="0"
						data-speed="2000"
						data-start="2000"
						data-easing="Back.easeInOut"
						data-endspeed="300">
						<a class="scrollto btn btn-lg btn-white btn-outline" href="#contact">SIM! Começar</a>
					
					</div>
				</li>
			</ul>
		
		</div>
	</div>

<!-- ======================================= -->
<!-- ========== END REVOLUTION SLIDER ========== -->
<!-- ======================================== -->
</section>



<section id="contact">
<div class="fullwidth-section">
<div class="parallax img-overlay4" style="background-image: url('img/pictures/photo.png')" data-stellar-background-ratio="0.3"></div>
<div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.7);"></div>
<div class="container">
<div class="row" style="margin-bottom: 20px;">
<div class="col-md-6 col-md-offset-3 text-center">

<h1 class="weight-800 kill-top-margin uppercase">Quem somos</h1>
<h4 class="color-white weight-400">Our support is top notch so please don't hesitate to contact us if you need some help.</h4>
</div>
</div>
<!-- START FORM WRAPPER -->

<div class="row">
<div class="col-md-8 col-md-offset-2">

	<form action="contact-form.php" method="post" role="form">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Tell us your name</label>
							<input class="form-control" name="name" id="name" placeholder="" type="text" />
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>What&#39;s your email address?</label>
							<input class="form-control" name="email" id="email" placeholder=""  type="email" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Give us the details</label>
							<textarea class="form-control" id="message" placeholder="" rows="8"></textarea>
						</div>
						<div class="text-center"><br/>
							<button class="btn btn-primary btn-round" type="submit">Send Message
							</button><br>
							<div id="error">Please fill out all required fields</div></div>
					</div>
				</div>
			</form>
</div>
</div>

<!-- END FORM WRAPPER -->
</div>
</div>


<!-- ======================================== -->
<!-- ================ END MAP DISPLAY =========== -->
<!-- ======================================== -->
</section>


<div class="scrollup">
	<a class="scrolltotop" href="#"><i class="fa fa-angle-double-up"></i></a></div>

<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/stellar.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/counter.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.nicescroll.plus.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.themepunch.rev-defaults.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/animation-engine.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/validation.js"></script>
<script type="text/javascript" src="<?php echo hometheme; ?>js/astonish.js"></script>
</body>
</html>