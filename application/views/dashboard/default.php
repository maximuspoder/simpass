<!DOCTYPE html>
<html class="sidebar sidebar-social footer-sticky">
<head>
    <title><?php echo TITLE; ?></title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo1.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/less-js/less.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script>
    if ( /*@cc_on!@*/ false && document.documentMode === 10)
    {
        document.documentElement.className += ' ie ie10';
    }
    </script>
</head>
<body class=" menu-right-hidden">
    <!-- Main Container Fluid -->
    <div class="container-fluid menu-hidden ">
        <!-- Main Sidebar Menu -->
        <?php menuDashboard(); ?>
       
        <!-- Content START -->
        <div id="content">
            <div class="navbar hidden-print navbar-default box main" role="navigation">
                
                <div class="user-action user-action-btn-navbar pull-left">
                    <a href="#menu" class="btn btn-sm btn-navbar btn-open-left"><i class="fa fa-bars fa-2x"></i></a>
                </div>
                
             
                <div class="input-group hidden-xs pull-left">
                    <span class="input-group-addon"><i class="icon-search"></i>
                    </span>
                    <input type="text" class="form-control" placeholder="Pesquisar" />
                </div>
            </div>
            <!-- <div class="layout-app">  -->
            <div class="innerAll">
                <div class="row">
                    <div class="col-lg-9 col-md-8">
                        <div class="timeline-cover">
                            <div class="cover">
                                <div class="top">
                                    <img src="../assets//images/photodune-2755655-party-time-s.jpg" class="img-responsive"
                                    />
                                </div>
                                <ul class="list-unstyled">
                                    <li class="active"><a href="index.html?lang=en"><i class="fa fa-fw fa-clock-o"></i> <span>Timeline</span></a>
                                    </li>
                                    <li><a href="about_1.html?lang=en"><i class="fa fa-fw fa-user"></i> <span>Sobre</span></a>
                                    </li>
                                    <li><a href="media_1.html?lang=en"><i class="fa fa-fw icon-photo-camera"></i> <span>Meu histórico</span> </a>
                                    </li>
                                    <li><a href="contacts_1.html?lang=en"><i class="fa fa-fw icon-group"></i><span> O que sigo </span><small>(19)</small></a>
                                    </li>
                                    <li><a href="messages.html?lang=en"><i class="fa fa-fw icon-envelope-fill-1"></i> <span>Notificações</span> <small>(2 novas)</small></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="widget">
                                <div class="widget-body padding-none margin-none">
                                    <div class="innerAll">
                                        <i class="fa fa-quote-left text-muted pull-left fa-fw"></i> 
                                        <p class="lead margin-none">Texto do dia</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="widget">
                            <div class="widget-body text-center">
                                <a href="">
                                    <img src="../assets/images/people/250/22.jpg" width="120"
                                    alt="" class="img-circle">
                                </a>
                                <h2 class="strong margin-none">Nome usuario</h2>
                                <div class="innerB">O que faz</div>
                                <a href="" class="btn btn-primary text-center btn-block">Conta</a>
                                <div class="btn-group-vertical btn-block">
                                    <a href="" class="btn btn-default"><i class="fa fa-cog pull-right"></i>Editar conta</a>
                                    <a href="" class="btn btn-default"><i class="fa fa-cog pull-right"></i>Sair</a>
                                </div>
                            </div>
                        </div>
                        <div class="widget">
                            <h5 class="innerAll margin-none border-bottom bg-gray">Atividades recentes</h5>
                            <div class="widget-body padding-none">
                                <div class="media border-bottom innerAll margin-none">
                                    <img src="../assets/images/people/35/22.jpg" class="pull-left media-object" />
                                    <div class="media-body">
                                        <a href="" class="pull-right text-muted innerT half">
                                            <i class="fa fa-comments"></i> 4
                                        </a>
                                        <h5 class="margin-none"><a href="" class="text-inverse">Social Admin Released</a>
                                        </h5>
                                        <small>on February 2nd, 2014</small>
                                    </div>
                                </div>
                                <div class="media border-bottom innerAll margin-none">
                                    <img src="../assets/images/people/35/22.jpg" class="pull-left media-object" />
                                    <div class="media-body">
                                        <a href="" class="pull-right text-muted innerT half">
                                            <i class="fa fa-comments"></i> 4
                                        </a>
                                        <h5 class="margin-none"><a href="" class="text-inverse">Timeline Cover Page</a>
                                        </h5>
                                        <small>on February 2nd, 2014</small>
                                    </div>
                                </div>
                                <div class="media border-bottom innerAll margin-none">
                                    <img src="../assets/images/people/35/22.jpg" class="pull-left media-object" />
                                    <div class="media-body">
                                        <a href="" class="pull-right text-muted innerT half">
                                            <i class="fa fa-comments"></i> 4
                                        </a>
                                        <h5 class="margin-none"><a href="" class="text-inverse">1000+ Sales</a>
                                        </h5>
                                        <small>on February 2nd, 2014</small>
                                    </div>
                                </div>
                                <div class="media border-bottom innerAll margin-none">
                                    <img src="../assets/images/people/35/22.jpg" class="pull-left media-object" />
                                    <div class="media-body">
                                        <a href="" class="pull-right text-muted innerT half">
                                            <i class="fa fa-comments"></i> 4
                                        </a>
                                        <h5 class="margin-none"><a href="" class="text-inverse">On-Page Optimization</a>
                                        </h5>
                                        <small>on February 2nd, 2014</small>
                                    </div>
                                </div>
                                <div class="media border-bottom innerAll margin-none">
                                    <img src="../assets/images/people/35/22.jpg" class="pull-left media-object" />
                                    <div class="media-body">
                                        <a href="" class="pull-right text-muted innerT half">
                                            <i class="fa fa-comments"></i> 4
                                        </a>
                                        <h5 class="margin-none"><a href="" class="text-inverse">14th Admin Template</a>
                                        </h5>
                                        <small>on February 2nd, 2014</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                     
                    </div>
                </div>
            </div>
        </div>
        <!-- // Content END -->
        <div class="clearfix"></div>
      
      
    </div>
    <!-- // Main Container Fluid END -->
    <!-- Global -->
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <script src="<?php echo frontTheme; ?>assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/breakpoints/breakpoints.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/preload/pace/pace.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/menus/sidr/jquery.sidr.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/twitter/assets/js/twitter.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/holder/holder.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/common/gallery/gridalicious/assets/lib/jquery.gridalicious.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/common/gallery/gridalicious/assets/custom/gridalicious.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/maps/google/assets/custom/maps-google.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false&callback=initGoogleMaps"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/modals/assets/js/bootbox.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/sidebar.main.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/sidebar.collapse.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/menus/menus.sidebar.chat.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/mixitup/jquery.mixitup.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/mixitup/mixitup.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/core.init.js?v=v1.0.1-rc2"></script>
</body>
</html>