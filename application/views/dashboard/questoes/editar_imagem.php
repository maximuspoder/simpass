<!DOCTYPE html>
<html class="sidebar sidebar-social footer-sticky">
<head>
    <title><?php echo TITLE; ?></title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    
    <link rel="stylesheet" href="<?php echo OLDfrontTheme; ?>assets/css/admin/estilo1.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/less-js/less.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script>
    if ( /*@cc_on!@*/ false && document.documentMode === 10)
    {
        document.documentElement.className += ' ie ie10';
    }
    </script>
    <style type="text/css">
    #bar{ width: 0%; }
    </style>
</head>
<body class=" menu-right-hidden">
    <!-- Main Container Fluid -->
    <div class="container-fluid menu-hidden ">
        <!-- Main Sidebar Menu -->
        <?php menuDashboard(); ?>
       
        <!-- Content START -->
        <div id="content">
            <div class="navbar hidden-print navbar-default box main" role="navigation">
                
                <div class="user-action user-action-btn-navbar pull-left">
                    <a href="#menu" class="btn btn-sm btn-navbar btn-open-left"><i class="fa fa-bars fa-2x"></i></a>
                </div>
                
             
                
                    <div class="input-group hidden-xs pull-left">
                    <form action="<?php echo BASEURL; ?>questoes/buscar" method="post">
                        <span class="input-group-addon"><i class="icon-search"></i>
                        </span>
                        <input type="text" name="questao" class="form-control" placeholder="Pesquisar" />
                    </form>
                    </div>
               
            </div>
            <!-- <div class="layout-app">  -->
            <div class="innerAll">
                <div class="row">
                    

                    <div class="widget widget-body-white widget-heading-simple">
                    <div class="widget-body">
                    <h3>Editar questões com ID <b>Q<?php echo $qcid; ?></b></h3>

                       

                        <div class="row">
                        <div class="col-md-12">

                            <div class="widget-body">
                            <div class="row">
                            <form id="myForm" action="<?php echo BASEURL; ?>questoes/editar_imagem_upload" method="post" enctype="multipart/form-data">
                                <div class="col-md-6">
                                    <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-new">Escolher Arquivo</span>
                                            <span class="fileupload-exists">Mudar</span>
                                            <input type="file" name="file_upload" id="file_upload" class="margin-none"/>
                                        </span>
                                         <span class="fileupload-preview"></span>
                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a>
                                        <button class="btn btn-primary">Upload</button>
                                        <br />
                                        <br />
                                         <div class="progress">
                                            <div class="progress-bar progress-bar-primary" id="bar"></div>
                                        </div>

                                       
                                        <div id="retorno"></div>
                                    </div>
                                </div>
                            </form>
                            </div>
                            </div>
                            

                            <form id="myForm" action="<?php echo BASEURL; ?>questoes/editar_imagem_process" method="post" >
                            <input type="hidden" name="qcid" value="<?php echo $qcid; ?>">
                            <label for="textDescription">Questão</label>

                            <textarea name="questao" class="form-control" rows="8"><?php echo trim(ext($questao[0], 'questao')); ?></textarea>

                            <label for="textDescription">Respostas</label>
                            <textarea name="resposta1" class="form-control" rows="2"><?php echo trim(ext($questao[0], 'resposta1')); ?></textarea>
                            <textarea name="resposta2" class="form-control" rows="2"><?php echo trim(ext($questao[0], 'resposta2')); ?></textarea>
                            <textarea name="resposta3" class="form-control" rows="2"><?php echo trim(ext($questao[0], 'resposta3')); ?></textarea>
                            <textarea name="resposta4" class="form-control" rows="2"><?php echo trim(ext($questao[0], 'resposta4')); ?></textarea>
                            <textarea name="resposta5" class="form-control" rows="2"><?php echo trim(ext($questao[0], 'resposta5')); ?></textarea>
                          

                            <br />
                            <button class="btn btn-primary">Salvar</button>
                        </div>



                        <div class="col-md-12" style="margin-top:20px">
                            <h3>Questão</h3>
                            <hr>
                        
                            <?php echo ext($questao[0], 'questao'); ?><br /><br />
                            <?php echo ext($questao[0], 'resposta1'); ?><br /><br />
                            <?php echo ext($questao[0], 'resposta2'); ?><br /><br />
                            <?php echo ext($questao[0], 'resposta3'); ?><br /><br />
                            <?php echo ext($questao[0], 'resposta4'); ?><br /><br />
                            <?php echo ext($questao[0], 'resposta5'); ?><br /><br />
                        </div>








                        </div>


                    </div>
                </div>


                </div>
            </div>
        </div>
        <!-- // Content END -->
        <div class="clearfix"></div>
      
      
    </div>
    <!-- // Main Container Fluid END -->
    <!-- Global -->
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/breakpoints/breakpoints.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/preload/pace/pace.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/menus/sidr/jquery.sidr.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/twitter/assets/js/twitter.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/holder/holder.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/gallery/gridalicious/assets/lib/jquery.gridalicious.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/gallery/gridalicious/assets/custom/gridalicious.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/maps/google/assets/custom/maps-google.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false&callback=initGoogleMaps"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/modals/assets/js/bootbox.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.main.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.collapse.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/menus/menus.sidebar.chat.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/mixitup/jquery.mixitup.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/mixitup/mixitup.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/core.init.js?v=v1.0.1-rc2"></script>

  
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/forms/elements/jasny-fileupload/assets/js/bootstrap-fileupload.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <!-- Aplicação -->
    <script src="<?php echo JS; ?>app.js"></script>
    <script src="<?php echo JS; ?>jquery.form.js"></script>
    

    <script>
        $(document).ready(function(){

        var options = { 
        beforeSend: function() {
            //clear everything
            $("#bar").width('0%');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            $("#bar").width(percentComplete+'%');
        },
        success: function() {
            $("#bar").width('100%');
        },
        complete: function(response) {
            $("#retorno").html(response.responseText);
        },
        error: function(){
            $("#retorno").html("Erro ao realizar o upload :(");
        } 
        }; 
        $("#myForm").ajaxForm(options);

        });
    </script>

</body>
</html>