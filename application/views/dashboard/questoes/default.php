<?php if(isset($notify) && $notify == 1){ ?>
<ul id="notyfy_container_top"  class="notyfy_container">
    <li class="notyfy_wrapper notyfy_success" style="cursor: pointer;">
        <div id="notyfy_752739675174936200" class="notyfy_bar">
            <div class="notyfy_message">
                <span class="notyfy_text">
                    Questão alterada com <strong>sucesso!</strong>
                </span>
            </div>
        </div>
    </li>
</ul>
<?php } elseif(isset($notify) && $notify == 0) { ?>
<ul id="notyfy_container_top" class="notyfy_container">
    <li class="notyfy_wrapper notyfy_error">
        <div id="notyfy_1259075451017146000" class="notyfy_bar">
            <div class="notyfy_message">
                <span class="notyfy_text">
                    <strong>Erro ao realizar alteração na questão!!</strong> :(
                </span>
            </div>
        </div>
    </li>
</ul>
<?php } ?>
<!DOCTYPE html>
<html class="sidebar sidebar-social footer-sticky">
<head>
    <title><?php echo TITLE; ?></title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    
    <link rel="stylesheet" href="<?php echo OLDfrontTheme; ?>assets/css/admin/estilo1.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/less-js/less.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script>
    if ( /*@cc_on!@*/ false && document.documentMode === 10)
    {
        document.documentElement.className += ' ie ie10';
    }
    </script>
</head>
<body class=" menu-right-hidden">
    <!-- Main Container Fluid -->
    <div class="container-fluid menu-hidden ">
        <!-- Main Sidebar Menu -->
        <?php menuDashboard(); ?>
       
        <!-- Content START -->
        <div id="content">
            <div class="navbar hidden-print navbar-default box main" role="navigation">
                
                <div class="user-action user-action-btn-navbar pull-left">
                    <a href="#menu" class="btn btn-sm btn-navbar btn-open-left"><i class="fa fa-bars fa-2x"></i></a>
                </div>
                
             
                <div class="input-group hidden-xs pull-left">
                    <form action="<?php echo BASEURL; ?>questoes/buscar" method="post">
                        <span class="input-group-addon"><i class="icon-search"></i>
                        </span>
                        <input type="text" name="questao" class="form-control" placeholder="Pesquisar" />
                    </form>
                </div>
            </div>
            <!-- <div class="layout-app">  -->
            <div class="innerAll">
                <div class="row">
                    

                    <div class="widget widget-body-white widget-heading-simple">
                    <div class="widget-body">
                    <h3><?php echo count($questoes); ?> Questões exibidas, restando <?php echo ext($total[0], 'total'); ?></h3>
                       <table class="dynamicTable tableTools table table-striped checkboxs">
                            <thead>
                                <tr>
                                    <td>Questões ID(#)</td>
                                    <td>Prova</td>
                                    <td>Ação</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($questoes as $conteudo): ?>

                                    <tr>
                                        <td><b><?php echo "Q".strip_tags($conteudo["qcid"]); ?></b></td>
                                        <td><?php echo strip_tags($conteudo["prova"]); ?></td>
                                        <td><button class="btn btn-primary" onclick="simule_redirect('questoes/editar_imagem/<?php echo strip_tags($conteudo["qcid"]); ?>')">Editar</button></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>


                </div>
            </div>
        </div>
        <!-- // Content END -->
        <div class="clearfix"></div>
      
      
    </div>
    <!-- // Main Container Fluid END -->
    <!-- Global -->
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/breakpoints/breakpoints.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/preload/pace/pace.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/menus/sidr/jquery.sidr.js?v=v1.0.1-rc2"></script>
    
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/holder/holder.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/gallery/gridalicious/assets/lib/jquery.gridalicious.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/gallery/gridalicious/assets/custom/gridalicious.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/maps/google/assets/custom/maps-google.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>

    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/modals/assets/js/bootbox.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.main.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.collapse.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/menus/menus.sidebar.chat.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/mixitup/jquery.mixitup.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/mixitup/mixitup.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/core.init.js?v=v1.0.1-rc2"></script>



    <!-- Aplicação -->
    <script src="<?php echo JS; ?>jquery.min.js"></script>
    <script src="<?php echo JS; ?>app.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#notyfy_752739675174936200').click(function(){
            $('#notyfy_752739675174936200').hide('slow');
        });
        $('#notyfy_1259075451017146000').click(function(){
            $('#notyfy_1259075451017146000').hide('slow');
        })
    })
    </script>
</body>
</html>