<!DOCTYPE html>
<html class="sidebar sidebar-social footer-sticky">
<head>
    <title><?php echo TITLE; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid menu-hidden ">
         <?php adminMenu($this->session->userdata('nivel')); ?>
        <div id="content">
            <div class="navbar hidden-print navbar-default box main" role="navigation">
               <div class="user-action user-action-btn-navbar pull-left">
                    <a href="#menu" class="btn btn-sm btn-navbar btn-open-left"><i class="fa fa-bars fa-2x"></i></a>
                </div>
                <?php teacherHEADER($this->session->userdata('nome'), $this->session->userdata('sobrenome')); ?>
            </div>

            
            <div class="innerAll">
                <div class="row">
                    
                



                <div class="widget widget-heading-simple widget-body-white">
                <div class="widget-head height-auto">
                    <h4 class=" heading margin-none pull-left"><i class="icon-compose innerR"></i>Total de questões selecionadas por professor</h4>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-body padding-none">
                    <table class="table table-vertical-center table-striped  margin-none">
                    <thead>
                        <tr>
                            <th class="center">ID(#).</th>
                            <th>Nome</th>
                            <th>Total Selecionadas</th>
                            <th>Total Gravadas</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($questions_selecteds as $result): ?>
                        <tr>
                            <td class="center"><?php echo $result['userid']; ?></td>
                            <td><span><?php echo $result['teacher']; ?></span></td>
                            <td><?php echo $result['questions_count']; ?></td>
                            <td><?php echo $result['status']; ?></td>
                        </tr>
                        
                    <?php endforeach; ?>
                    </tbody>
                    </table>
                </div>
                </div>


                   <div class="widget widget-heading-simple widget-body-white">
                <div class="widget-head height-auto">
                    <h4 class=" heading margin-none pull-left"><i class="icon-compose innerR"></i>Total de questões por disciplina</h4>
                    <div class="clearfix"></div>
                </div>
                <div class="widget-body padding-none">
                    <table class="table table-vertical-center table-striped  margin-none">
                    <thead>
                        <tr>
                            <th>Disciplina</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($questions_by_discipline as $result): ?>
                        <tr>
                            <td><span><?php echo $result['discipline']; ?></span></td>
                            <td><?php echo $result['questions_count']; ?></td>
                        </tr>
                        
                    <?php endforeach; ?>
                    </tbody>
                    </table>
                </div>
                </div>







                </div>
            </div>
        </div>
        <div class="clearfix"></div>
      
      
    </div>
    <script data-id="App.Config">
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo1.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo JS; ?>jquery.min.js"></script>
    <script src="<?php echo JS; ?>app.js"></script>

    <script src="<?php echo frontTheme; ?>assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/breakpoints/breakpoints.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/menus/sidr/jquery.sidr.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/holder/holder.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/sidebar.main.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/sidebar.collapse.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/menus/menus.sidebar.chat.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/core.init.js?v=v1.0.1-rc2"></script>

</body>
</html>