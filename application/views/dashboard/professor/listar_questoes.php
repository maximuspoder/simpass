<!DOCTYPE html>
<html class="sidebar sidebar-social footer-sticky">
<head>
    <title><?php echo TITLE; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <script src="<?php echo JS; ?>jquery.min.js"></script>
    <script src="<?php echo JS; ?>app.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid menu-hidden ">
    <?php teacherMenu($this->session->userdata('nivel')); ?>
    <div id="content">
            <div class="navbar hidden-print navbar-default box main" role="navigation">
               <div class="user-action user-action-btn-navbar pull-left">
                    <a href="#menu" class="btn btn-sm btn-navbar btn-open-left"><i class="fa fa-bars fa-2x"></i></a>
                </div>
                <?php teacherHEADER($this->session->userdata('nome'), $this->session->userdata('sobrenome')); ?>
            </div>
        <div class="innerAll">
        <div class="row">
            
            <div class="widget widget-body-white widget-heading-simple">
            <div class="widget-body">
                <h3>Escolher Questões</h3>
                <form method="post" action="<?php echo BASEURL; ?>professor/listar_questoes">
                    <div class="col-lg-4 col-md-8">
                        <label> Filtrar Disciplina</label>
                        <select name="disciplina" class="form-control" required>
                            <?php echo combo($discipline, 'disciplina', 'disciplina', $disciplina); ?>
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-8" style="margin-top:21px;">
                        <button class="btn btn-primary">Buscar</button>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            </div>

            <div class="col-lg-12 col-md-8" style="background:#fff; margin-top:10px">
            <?php if(count($data) > 0) { ?>
            <?php foreach ($data as $content){ ?>
            <div id="result_<?php echo $content['qcid']; ?>">
                <div class="wizard-head">
                    <ul class="bwizard-steps">
                        <li class="active"><a href="#tab4" data-toggle="tab"><?php echo "ID(#) " . $content['qcid']; ?></a></li>
                        <li class="active"><a href="#tab4" data-toggle="tab"><?php echo $content['prova']; ?></a></li>
                        <li class="active"><a href="#tab5" data-toggle="tab"> <?php echo $content['disciplina']; ?></a></li>
                    </ul>
                </div>
                <!-- question -->
                <div class="questao col-lg-12 col-md-8" style="margin-top:20px;">
                    <?php echo $content['questao']; ?>
                </div>
                <!-- attach file -->
                <?php 
                    $arrayAttachment = array(
                        'Q'. $content['qcid'], 
                        'Q'. $content['qcid'] . '-A',
                        'Q'. $content['qcid'] . '-B',
                        'Q'. $content['qcid'] . '-C',
                        'Q'. $content['qcid'] . '-D',
                        'Q'. $content['qcid'] . '-E'
                    );
                    $arrayFormats = array(
                        '.html', 
                        '.htm', 
                        '.jpg', 
                        '.png'
                    );
                    
                    $result = getAttachmentFile('complementar/', $arrayAttachment, $arrayFormats);
                    if(count($result) > 0){
                ?>
                <div class="col-lg-12 col-md-8">
                    <button class="btn btn-info btn-stroke" onclick="getAttach(<?php echo $content['qcid']; ?>)">Ver texto anexo</button>
                </div>
                <div class="col-lg-12 col-md-8" style="margin-top:20px; display:none" id="viewAttachment<?php echo $content['qcid']; ?>">
                <?php foreach ($result as $attach) { ?>
                    <?php if(!strstr($attach, '.htm')){ ?>
                        <img src="<?php echo $attach; ?>" width="400">
                        <br />
                    <?php } else { ?>
                        <script> ajax_get_complementar('<?php echo $attach; ?>', 'ajax' + <?php echo $content['qcid']; ?>) </script>
                        <div id="ajax<?php echo $content['qcid']; ?>"></div>
                    <?php } ?>

                <?php } ?>
                    
                </div>
                <?php } ?>
               
                <!-- answers -->
                <div class="col-lg-12 col-md-8" style="margin-top:20px;">
                    <?php if($content['resposta_correta'] == "A"){  echo "<font color='green'>".  $content['resposta1'] ."</font>";  } else { echo  $content['resposta1']; } ?><br />
                    <?php if($content['resposta_correta'] == "B"){  echo "<font color='green'>".  $content['resposta2'] ."</font>";  } else { echo  $content['resposta2']; } ?><br />
                    <?php if($content['resposta_correta'] == "C"){  echo "<font color='green'>".  $content['resposta3'] ."</font>";  } else { echo  $content['resposta3']; } ?><br />
                    <?php if($content['resposta_correta'] == "D"){  echo "<font color='green'>".  $content['resposta4'] ."</font>";  } else { echo  $content['resposta4']; } ?><br />
                    <?php if($content['resposta_correta'] == "E"){  echo "<font color='green'>".  $content['resposta5'] ."</font>";  } else { echo  $content['resposta5']; } ?><br />
                </div>

                <div class="col-lg-12 col-md-8" style="margin-bottom:50px;">
                    <button class="btn btn-default" onclick="ajax_professor_set_question_eliminate_div(<?php echo $this->session->userdata('idusuario'); ?>, '<?php echo $this->session->userdata("nome") ." ".$this->session->userdata("sobrenome") ; ?>', <?php echo strip_tags($content["questaoid"]); ?>,<?php echo strip_tags($content["qcid"]); ?>, '<?php echo trim(strip_tags($content["prova"])); ?>', '<?php echo trim(strip_tags($content["disciplina"])); ?>')">Adicionar a lista</button>
                </div>
            </div>
            <?php } ?>

            </div>
            <?php } else { ?>
            <div class="col-lg-10 col-md-8" style="margin: 30px 0; width:95%">
                <ul id="notyfy_container_top"  class="">
                    <li class="notyfy_wrapper notyfy_success">
                        <div id="notyfy_752739675174936200" class="notyfy_bar">
                            <div class="notyfy_message">
                                <span class="notyfy_text">
                                    As questões para esta disciplina estão sendo tratadas no banco de dados!<strong> Logo estarão disponíveis ;)</strong>
                                </span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <?php } ?>


        </div>
        </div>
    </div>
    <div class="clearfix"></div>
      
      
    </div>
    <script data-id="App.Config">
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <link rel="stylesheet" href="<?php echo OLDfrontTheme; ?>assets/css/admin/estilo1.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    

    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/breakpoints/breakpoints.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/menus/sidr/jquery.sidr.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/holder/holder.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.main.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.collapse.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/menus/menus.sidebar.chat.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/core.init.js?v=v1.0.1-rc2"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        
    })
    </script>
</body>
</html>