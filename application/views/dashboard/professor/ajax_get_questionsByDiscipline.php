<table class="dynamicTable tableTools table table-striped checkboxs">
    <thead>
        <tr>
            <td>Questões ID(#)</td>
            <td>Disciplina</td>
            <td>Prova</td>
            <td>Ver</td>
            <td>Selecionar</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($data as $content): ?>
            <tr>
                <td><b><?php echo strip_tags($content["qcid"]); ?></b></td>
                <td><?php echo strip_tags($content["disciplina"]); ?></td>
                <td><?php echo strip_tags($content["prova"]); ?></td>
                <td><button class="btn btn-primary" onclick="simule_redirect('professor/questao/<?php echo strip_tags($content["qcid"]); ?>')">Ver Questão</button></td>
                <td><button id="result<?php echo strip_tags($content["qcid"]); ?>" class="btn btn-primary" onclick="ajax_professor_set_question_eliminate_div(<?php echo $this->session->userdata('idusuario'); ?>, '<?php echo $this->session->userdata("nome") ." ".$this->session->userdata("sobrenome") ; ?>', <?php echo strip_tags($content["questaoid"]); ?>,<?php echo strip_tags($content["qcid"]); ?>, '<?php echo trim(strip_tags($content["prova"])); ?>', '<?php echo trim(strip_tags($content["disciplina"])); ?>')">Adicionar a lista</button></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>