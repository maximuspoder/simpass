<html class="sidebar sidebar-social footer-sticky">
<head>
    <title><?php echo TITLE; ?></title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    
    <link rel="stylesheet" href="<?php echo OLDfrontTheme; ?>assets/css/admin/estilo1.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/less-js/less.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script>
    if ( /*@cc_on!@*/ false && document.documentMode === 10)
    {
        document.documentElement.className += ' ie ie10';
    }
    </script>
</head>
<body class=" menu-right-hidden">
    <!-- Main Container Fluid -->
    <div class="container-fluid menu-hidden ">
        <?php teacherMenu($this->session->userdata('nivel')); ?>

        <div id="content">
            <div class="navbar hidden-print navbar-default box main" role="navigation">
               <div class="navbar hidden-print navbar-default box main" role="navigation">
               <div class="user-action user-action-btn-navbar pull-left">
                    <a href="#menu" class="btn btn-sm btn-navbar btn-open-left"><i class="fa fa-bars fa-2x"></i></a>
                </div>
                <?php teacherHEADER($this->session->userdata('nome'), $this->session->userdata('sobrenome')); ?>
            </div>
            <!-- <div class="layout-app">  -->
            <div class="innerAll">
                <div class="row">
                    

                    <div class="widget widget-body-white widget-heading-simple">
                    <div class="widget-body">
                    <h3><?php echo ext($data_count[0], 'total'); ?> Questões selecionadas</h3>

                        
                        <div class="clearfix"></div>
                        <div id="resultAjax">
                            <table class="dynamicTable tableTools table table-striped checkboxs">
                                <thead>
                                    <tr>
                                        <td>ID(#)</td>
                                        <td>Disciplina</td>
                                        <td>Prova</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($data as $content): ?>
                                        <tr id="result<?php echo $content['qcid']; ?>">
                                            <td><b><?php echo strip_tags($content["qcid"]); ?></b></td>
                                            <td><?php echo strip_tags($content["disciplina"]); ?></td>
                                            <td><?php echo strip_tags($content["prova"]); ?></td>
                                            <td><button class="btn btn-primary" onclick="simule_redirect('professor/gravar/<?php echo strip_tags($content["qcid"]); ?>')">Ver</td>
                                            <td><button class="btn btn-primary" onclick="simule_redirect('professor/gravar/<?php echo strip_tags($content["qcid"]); ?>')">Gravar</td>
                                            <td><button class="btn btn-primary" onclick="question_delete('<?php echo strip_tags($content["qcid"]); ?>')">Deletar</td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                </div>
            </div>
        </div>
        <!-- // Content END -->
        <div class="clearfix"></div>
      
      
    </div>
    <!-- // Main Container Fluid END -->
    <!-- Global -->
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/breakpoints/breakpoints.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/preload/pace/pace.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/menus/sidr/jquery.sidr.js?v=v1.0.1-rc2"></script>
    
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/holder/holder.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/gallery/gridalicious/assets/lib/jquery.gridalicious.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/gallery/gridalicious/assets/custom/gridalicious.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/maps/google/assets/custom/maps-google.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>

    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/modals/assets/js/bootbox.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.main.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.collapse.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/menus/menus.sidebar.chat.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/mixitup/jquery.mixitup.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/mixitup/mixitup.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/core.init.js?v=v1.0.1-rc2"></script>

    
    <!-- Aplicação -->
    <script src="<?php echo JS; ?>jquery.min.js"></script>
    <script src="<?php echo JS; ?>app.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#get_discipline').change(function(){
            discipline = $(this).val();
            ajax_get_questionsByDiscipline(discipline);
        });
    })
    </script>
</body>
</html>