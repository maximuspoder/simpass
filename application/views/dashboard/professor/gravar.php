<html class="sidebar sidebar-social footer-sticky">
<head>
    <title><?php echo TITLE; ?></title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    
    <link rel="stylesheet" href="<?php echo OLDfrontTheme; ?>assets/css/admin/estilo1.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/less-js/less.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <!-- Aplicação -->	
    <script src="<?php echo JS; ?>jquery.min.js"></script>
    <script src="<?php echo JS; ?>app.js"></script>
    <style type="text/css">
    .text-attachment-display{}
    /*.text-attachment-display img{ width: 80%; height: 170% }*/
    /*.text-attachment-display img{ width: 90%; height: 170% }
    .text-attachment-display img{  height: 300% }*/
    .text-attachment-display img{ width: 90%; height: 150% }
    .text-attachment-display{ font-size: 40px;color: black}
    #question{ }
    /*#question img{width: 10%; height: 10%;}*/
    </style>
</head>
<body class=" menu-right-hidden">
    <!-- Main Container Fluid -->
    <div class="container-fluid menu-hidden ">
        <!-- Main Sidebar Menu -->
       
        <!-- Content START -->
        
            <div class="navbar" role="navigation">
               
                
               <div class="user-action user-action-btn-navbar pull-left">
                    <a href="<?php echo BASEURL; ?>professores/questoes" class="btn btn-sm btn-navbar btn-open-left">Voltar</a>
                </div>

               <div class="user-action user-action-btn-navbar pull-left">
                    <a href="javascript:void(0)" class="btn btn-sm btn-navbar btn-open-left">
                        ID(#) <?php echo ext($data[0], 'qcid'); ?>
                    </a>
                </div> 
                
                <div class="user-action user-action-btn-navbar pull-left">
                    <a href="javascript:void(0)" onclick="ajax_set_gravadaOkay(<?php echo ext($data[0], 'qcid')?>)" class="btn btn-sm btn-navbar btn-open-left">Gravada OKAY</a>
                </div>

<div class="user-action user-action-btn-navbar pull-left">
                    <a href="javascript:void(0)" id="btnDim" class="btn btn-sm btn-navbar btn-open-left">- Imagem</a>
                </div>
<div class="user-action user-action-btn-navbar pull-left">
                    <a href="javascript:void(0)" id="btnAum" class="btn btn-sm btn-navbar btn-open-left">+ Imagem</a>
                </div>
<div class="user-action user-action-btn-navbar pull-left">
                    <a href="javascript:void(0)" id="btnDimLar" class="btn btn-sm btn-navbar btn-open-left">- largura Imagem</a>
                </div>
<div class="user-action user-action-btn-navbar pull-left">
                    <a href="javascript:void(0)" id="btnAumLar" class="btn btn-sm btn-navbar btn-open-left">+ Largura Imagem</a>
                </div>

<div class="user-action user-action-btn-navbar pull-left">
                    <a href="javascript:void(0)" id="btnDimAlt" class="btn btn-sm btn-navbar btn-open-left">- altura Imagem</a>
                </div>
<div class="user-action user-action-btn-navbar pull-left">
                    <a href="javascript:void(0)" id="btnAumAlt" class="btn btn-sm btn-navbar btn-open-left">+ altura Imagem</a>
                </div>
                <!--<?php userHEADER($usuario); ?> -->
            </div>
            <!-- <div class="layout-app">  -->
            <div class="innerAll">
                <div class="row">
                    

                  

                    <div class="col-lg-12 col-md-8" style="">
                        <div class="wizard-head">
                        <ul class="bwizard-steps">
                            <li class="active"><a href="#tab2" data-toggle="tab"><?php echo ext($data[0], 'escolaridade'); ?></a></li>
                            <li class="active"><a href="#tab3" data-toggle="tab"><?php echo ext($data[0], 'ano'); ?></a></li>
                            <li class="active"><a href="#tab4" data-toggle="tab"><?php echo ext($data[0], 'prova'); ?></a></li>
                            <li class="active"><a href="#tab5" data-toggle="tab"> <?php echo ext($data[0], 'disciplina'); ?></a></li>
                        </ul>
                        </div>
                            





                        <div id="print_question">





                            <div class="col-lg-12 col-md-8" style="margin-top:10px">
                            <?php if(isset($imagem)){ ?>
                                <a href="javascript:void(0)" class="text-attachment-link" id="text-attachment-display-show">Ver texto/imagem anexo</a>
                                <div id="text-attachment-display" class="text-attachment-display">

                                    <?php if(isset($imagem[0])){?>
                                        <?php if(strstr(($imagem[0]), "htm")){ ?>
                                            <!-- conteudo em html -->
                                            <script type="text/javascript">ajax_get_complementar('<?php echo $imagem[0] ?>', 'result1');</script>
                                            <div id="result1"></div>
                                        <?php } else { ?>
                                            <img src="<?php echo $imagem[0]; ?>" alt="imagem1">
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if(isset($imagem[1])){?>
                                         <?php if(strstr(($imagem[1]), "htm")){?>
                                            <!-- conteudo em html -->
                                            <script type="text/javascript">ajax_get_complementar('<?php echo $imagem[1] ?>', 'result2');</script>
                                            <div id="result2"></div>
                                        <?php } else { ?>
                                            <img src="<?php echo $imagem[1]; ?>" alt="imagem2">
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if(isset($imagem[2])){?>
                                         <?php if(strstr(($imagem[2]), "htm")){?>
                                            <!-- conteudo em html -->
                                            <script type="text/javascript">ajax_get_complementar('<?php echo $imagem[2] ?>', 'result3');</script>
                                            <div id="result3"></div>
                                        <?php } else { ?>
                                            <img src="<?php echo $imagem[2]; ?>" alt="imagem3">
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if(isset($imagem[3])){?>
                                         <?php if(strstr(($imagem[3]), "htm")){?>
                                            <!-- conteudo em html -->
                                            <script type="text/javascript">ajax_get_complementar('<?php echo $imagem[3] ?>', 'result4');</script>
                                            <div id="result4"></div>
                                        <?php } else { ?>
                                            <img src="<?php echo $imagem[3]; ?>" alt="imagem4">
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            </div>




                        
                        <div class="col-lg-12 col-md-8" id="question" style="margin-top:20px; font-size:40px; color:#000">
                            <?php echo ext($data[0], 'questao'); ?>
                        </div>


                            <input type="hidden" id="valWIDTH" value="90">
                            <input type="hidden" id="valHEIGHT" value="150">
                            
                            
                            <div class="col-lg-12 col-md-8" style="margin-top:20px; font-size:40px; color:#000">
                                <?php if(ext($data[0], 'resposta_correta') == "A"){  echo "<font color='green'>".  ext($data[0], 'resposta1') ."</font>";  } else { echo  ext($data[0], 'resposta1'); } ?><br />
                                <?php if(ext($data[0], 'resposta_correta') == "B"){  echo "<font color='green'>".  ext($data[0], 'resposta2') ."</font>";  } else { echo  ext($data[0], 'resposta2'); } ?><br />
                                <?php if(ext($data[000], 'resposta_correta') == "C"){  echo "<font color='green'>".  ext($data[0], 'resposta3') ."</font>";  } else { echo  ext($data[0], 'resposta3'); } ?><br />
                                <?php if(ext($data[0], 'resposta_correta') == "D"){  echo "<font color='green'>".  ext($data[0], 'resposta4') ."</font>";  } else { echo  ext($data[0], 'resposta4'); } ?><br />
                                <?php if(ext($data[0], 'resposta_correta') == "E"){  echo "<font color='green'>".  ext($data[0], 'resposta5') ."</font>";  } else { echo  ext($data[0], 'resposta5'); } ?><br />
                            </div>

                            

                    </div>
                    


                </div>
            </div>
        </div>
        <!-- // Content END -->
        <div class="clearfix"></div>
      
      
    </div>
    <!-- // Main Container Fluid END -->
    <!-- Global -->
    <script data-id="App.Config">
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/breakpoints/breakpoints.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/preload/pace/pace.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/menus/sidr/jquery.sidr.js?v=v1.0.1-rc2"></script>
    
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/holder/holder.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/gallery/gridalicious/assets/lib/jquery.gridalicious.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/gallery/gridalicious/assets/custom/gridalicious.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/maps/google/assets/custom/maps-google.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>

    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/modals/assets/js/bootbox.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.main.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.collapse.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/menus/menus.sidebar.chat.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/mixitup/jquery.mixitup.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/mixitup/mixitup.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/core.init.js?v=v1.0.1-rc2"></script>



    <!-- Aplicação -->
    <script src="<?php echo JS; ?>jquery.min.js"></script>
    <script src="<?php echo JS; ?>app.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#text-attachment-display-show').click(function(){
            $('#text-attachment-display').show('slow');
        });
        $('#btnPrint').click(function(){
            var conteudo =$('#print_question').html(),
            tela_impressao = window.open('about:blank');
            tela_impressao.document.write(conteudo);
            tela_impressao.window.print();
            tela_impressao.window.close();
        });

	
	;
		$('#btnAum').click(function(){
	
			var Width = $('#valWIDTH').val();
			var Height = $('#valHEIGHT').val();

			$('#valHEIGHT').val(parseInt(Height) + parseInt(10));
			$('#valWIDTH').val(parseInt(Width) + parseInt(10));
			  
			$('.text-attachment-display img').css({width: (parseInt(Width) + parseInt(10)) + '%',  height: (parseInt(Height) + parseInt(10)) + '%'});
		})

		$('#btnDim').click(function(){
	
			var Width = $('#valWIDTH').val();
			var Height = $('#valHEIGHT').val();

			$('#valHEIGHT').val(parseInt(Height) - parseInt(10));
			$('#valWIDTH').val(parseInt(Width) - parseInt(10));
			  
			$('.text-attachment-display img').css({width: (parseInt(Width) - parseInt(10)) + '%',  height: (parseInt(Height) - parseInt(10)) + '%'});
		})

$('#btnDimLar').click(function(){
	
	var Width = $('#valWIDTH').val();

	$('#valWIDTH').val(parseInt(Width) - parseInt(10));
	  
	$('.text-attachment-display img').css({width: (parseInt(Width) - parseInt(10)) + '%'});
})

$('#btnAumLar').click(function(){
	
	var Width = $('#valWIDTH').val();

	$('#valWIDTH').val(parseInt(Width) + parseInt(10));
	  
	$('.text-attachment-display img').css({width: (parseInt(Width) + parseInt(10)) + '%'});
})

$('#btnDimAlt').click(function(){
	
	var Height = $('#valHEIGHT').val();

	$('#valHEIGHT').val(parseInt(Height) - parseInt(10));
	  
	$('.text-attachment-display img').css({ height: (parseInt(Height) - parseInt(10)) + '%'});
})

$('#btnAumAlt').click(function(){
	
	var Height = $('#valHEIGHT').val();

	$('#valHEIGHT').val(parseInt(Height) + parseInt(10));
	  
	$('.text-attachment-display img').css({ height: (parseInt(Height) + parseInt(10)) + '%'});
})

    })
    </script>
</body>
</html>
