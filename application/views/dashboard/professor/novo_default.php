<!DOCTYPE html>
<html class="sidebar sidebar-social footer-sticky">
<head>
    <title><?php echo TITLE; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid menu-hidden ">
         <?php teacherMenu($this->session->userdata('nivel')); ?>
        <div id="content">
            <div class="navbar hidden-print navbar-default box main" role="navigation">
               <div class="user-action user-action-btn-navbar pull-left">
                    <a href="#menu" class="btn btn-sm btn-navbar btn-open-left"><i class="fa fa-bars fa-2x"></i></a>
                </div>
                <?php teacherHEADER($this->session->userdata('nome'), $this->session->userdata('sobrenome')); ?>
            </div>

            
            <div class="innerAll">
                <div class="row">
                    

                <div class="widget widget-body-white widget-heading-simple">
                <div class="widget-body">
                    <h3>Escolher Questões</h3>
                    <form method="post" action="<?php echo BASEURL; ?>professor/listar_questoes">
                        <div class="col-lg-4 col-md-8">
                            <label> Filtrar Disciplina</label>
                            <select name="disciplina" class="form-control" required>
                                <?php echo combo($discipline, 'disciplina', 'disciplina'); ?>
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-8" style="margin-top:21px;">
                            <button class="btn btn-primary">Buscar</button>
                        </div>
                    </form>

                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <center><h3>Selecione uma disciplina para iniciar</h3></center><br />
                    
                    <center><h1>;)</h1></center>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <div class="clearfix"></div>
                </div>
                </div>


                </div>
            </div>
        </div>
        <div class="clearfix"></div>
      
      
    </div>
    <script data-id="App.Config">
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <link rel="stylesheet" href="<?php echo OLDfrontTheme; ?>assets/css/admin/estilo1.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo JS; ?>jquery.min.js"></script>
    <script src="<?php echo JS; ?>app.js"></script>

    <script src="<?php echo OLDfrontTheme; ?>assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/breakpoints/breakpoints.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/menus/sidr/jquery.sidr.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/plugins/holder/holder.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.main.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/sidebar.collapse.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/modules/admin/menus/menus.sidebar.chat.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo OLDfrontTheme; ?>assets/components/core/js/core.init.js?v=v1.0.1-rc2"></script>
</body>
</html>