<!DOCTYPE html>
<html class="sidebar sidebar-social footer-sticky">
<head>
    <title><?php echo TITLE; ?></title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo1.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/less-js/less.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <style type="text/css">
    #menu{
        display: none;
    }
    </style>
    <!-- Aplicação -->
    <script src="<?php echo JS; ?>jquery.min.js"></script>
    <script src="<?php echo JS; ?>app.js"></script>
</head>
<body>
    <!-- Main Container Fluid -->
    <div class="container-fluid menu-hidden ">
        <!-- Main Sidebar Menu -->
         
       
        <!-- Content START -->
        <div id="content">
            <div class="navbar hidden-print navbar-default box main" role="navigation">
                
               <div class="user-action user-action-btn-navbar pull-left">
                    <a href="#menu" class="btn btn-sm btn-navbar btn-open-left"><i class="fa fa-bars fa-2x"></i></a>
                </div>
                <?php userHEADER($usuario); ?>
            </div>
            <!-- <div class="layout-app">  -->
            <div class="innerAll">
                <div class="row">
                    

                    <div class="col-lg-12 col-md-8" style="display:none">
                       <div class="wizard-head">
                        <ul class="bwizard-steps">
                            <li class="active"><a href="#tab1" data-toggle="tab"><?php echo ext($data[0], 'banca'); ?></a></li>
                            <li class="active"><a href="#tab2" data-toggle="tab"><?php echo ext($data[0], 'escolaridade'); ?></a></li>
                            <li class="active"><a href="#tab3" data-toggle="tab"><?php echo ext($data[0], 'ano'); ?></a></li>
                            <li class="active"><a href="#tab4" data-toggle="tab"><?php echo ext($data[0], 'prova'); ?></a></li>
                            <li class="active"><a href="#tab5" data-toggle="tab"> <?php echo ext($data[0], 'disciplina'); ?></a></li>
                            <?php if(count(ext($data[0], 'assunto1')) > 0 and ext($data[0], 'assunto1') != ''){ ?>
                                <li class="active"><a href="#tab6" data-toggle="tab"><?php echo ext($data[0], 'assunto1'); ?></a></li>
                            <?php } ?>
                        </ul>
                        </div>
                            
                        <div class="col-lg-12 col-md-8" style="margin-top:20px;font-size:40px">
                            <?php $questao = ext($data[0], 'questao'); ?>
                            <?php echo str_replace("<br />", "", $questao); ?>
                        </div>
                            
                        <div class="col-lg-12 col-md-8" style="margin-top:20px;font-size:40px">
                            <?php if(ext($data[0], 'resposta_correta') == "A"){  echo "<font color='green'>".  ext($data[0], 'resposta1') ."</font>";  } else { echo  ext($data[0], 'resposta1'); } ?><br />
                            <?php if(ext($data[0], 'resposta_correta') == "B"){  echo "<font color='green'>".  ext($data[0], 'resposta2') ."</font>";  } else { echo  ext($data[0], 'resposta2'); } ?><br />
                            <?php if(ext($data[0], 'resposta_correta') == "C"){  echo "<font color='green'>".  ext($data[0], 'resposta3') ."</font>";  } else { echo  ext($data[0], 'resposta3'); } ?><br />
                            <?php if(ext($data[0], 'resposta_correta') == "D"){  echo "<font color='green'>".  ext($data[0], 'resposta4') ."</font>";  } else { echo  ext($data[0], 'resposta4'); } ?><br />
                            <?php if(ext($data[0], 'resposta_correta') == "E"){  echo "<font color='green'>".  ext($data[0], 'resposta5') ."</font>";  } else { echo  ext($data[0], 'resposta5'); } ?><br />
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- // Content END -->
        <div class="clearfix"></div>
      
      
    </div>
    <!-- // Main Container Fluid END -->
    <!-- Global -->
    <script data-id="App.Config">
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <script src="<?php echo frontTheme; ?>assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/breakpoints/breakpoints.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/preload/pace/pace.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/menus/sidr/jquery.sidr.js?v=v1.0.1-rc2"></script>
    
    <script src="<?php echo frontTheme; ?>assets/components/plugins/holder/holder.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/common/gallery/gridalicious/assets/lib/jquery.gridalicious.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/common/gallery/gridalicious/assets/custom/gridalicious.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/maps/google/assets/custom/maps-google.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>

    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/modals/assets/js/bootbox.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/sidebar.main.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/sidebar.collapse.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/common/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/menus/menus.sidebar.chat.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/mixitup/jquery.mixitup.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/mixitup/mixitup.init.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/core.init.js?v=v1.0.1-rc2"></script>



    <!-- Aplicação -->
    <script src="<?php echo JS; ?>jquery.min.js"></script>
    <script src="<?php echo JS; ?>app.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#text-attachment-display-show').click(function(){
            $('#text-attachment-display').show('slow');
        });
    
    })
    </script>
</body>
</html>