<!DOCTYPE html>
<html class="sidebar sidebar-social footer-sticky">
<head>
    <title><?php echo TITLE; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo1.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/less-js/less.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
     <!-- application -->
    <script src="<?php echo JS; ?>jquery.min.js"></script>
    <script src="<?php echo JS; ?>app.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid menu-hidden ">
         <?php menuUSER(ext($usuario[0], 'nivel')); ?>
        <div id="content">
            <div class="navbar hidden-print navbar-default box main" role="navigation">
                
               <div class="user-action user-action-btn-navbar pull-left">
                    <a href="#menu" class="btn btn-sm btn-navbar btn-open-left"><i class="fa fa-bars fa-2x"></i></a>
                </div>
                <?php userHEADER($usuario); ?>
            </div>
            <div class="innerAll">
                <div class="row">
                <div class="col-lg-12 col-md-8">
                    <?php foreach($data as $content): ?>

                        <div class="wizard-head">
                        <ul class="bwizard-steps">
                            <li class="active"><a href="#tab1" data-toggle="tab"><?php echo $content['banca']; ?></a></li>
                            <li class="active"><a href="#tab2" data-toggle="tab"><?php echo $content['escolaridade']; ?></a></li>
                            <li class="active"><a href="#tab3" data-toggle="tab"><?php echo $content['ano']; ?></a></li>
                            <li class="active"><a href="#tab4" data-toggle="tab"><?php echo $content['prova']; ?></a></li>
                            <li class="active"><a href="#tab5" data-toggle="tab"> <?php echo $content['disciplina']; ?></a></li>
                            <?php if(count($content['assunto1']) > 0 and $content['assunto1'] != ''){ ?>
                                <li class="active"><a href="#tab6" data-toggle="tab"><?php echo $content['assunto1']; ?></a></li>
                            <?php } ?>
                        </ul>
                        </div>
                        <br />
                        <div class="clearfix"></div>
                        <div class="font_18 col-lg-6 col-md-8">
                             <?php echo $content['qcid']; ?> - 
                            <?php echo $content['questao']; ?>
                        </div>

                        <div class="col-lg-6 col-md-8">
                            <?php 
                            if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '.html')) { $format1 = '.html';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '.htm')){ $format1 = '.htm';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '.jpg')){ $format1 = '.jpg';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '.png')){ $format1 = '.png';}

                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-A.html')){ $format1 = '-A.html';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-A.htm')){ $format1 = '-A.htm';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-A.jpg')){ $format1 = '-A.jpg';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-A.png')){ $format1 = '-A.png';}

                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-B.html')){ $format2 = '-B.html';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-B.htm')){ $format2 = '-B.htm';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-B.jpg')){ $format2 = '-B.jpg';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-B.png')){ $format2 = '-B.png';}

                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-C.html')){ $format3 = '-C.html';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-C.htm')){ $format3 = '-C.htm';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-C.jpg')){ $format3 = '-C.jpg';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-C.png')){ $format3 = '-C.png';}

                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-D.html')){ $format4 = '-D.html';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-D.htm')){ $format4 = '-D.htm';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-D.jpg')){ $format4 = '-D.jpg';}
                            else if(file_exists(UPLOAD . 'complementar/Q'. $content['qcid'] . '-D.png')){ $format4 = '-D.png';}
                            ?>
                            <script type="text/javascript">ajax_get_complementar("<?php echo UPLOADLINK.'complementar/Q'. $content['qcid'] . $format1; ?>", "result<?php echo $content['qcid']; ?>A");</script>
                            <!--
                            <div id="result<?php echo $content['qcid']; ?>A"></div>
                            <div id="result<?php echo $content['qcid']; ?>B"></div>
                            <div id="result<?php echo $content['qcid']; ?>C"></div>
                            <div id="result<?php echo $content['qcid']; ?>D"></div>
                            -->
                        </div>

                        <div class="font_14 col-lg-12 col-md-8" style="margin-top:20px">
                            <?php if($content['resposta_correta'] == "A"){  echo "<font color='green'>".  $content['resposta1'] ."</font>";  } else { echo  $content['resposta1']; } ?><br />
                            <?php if($content['resposta_correta'] == "B"){  echo "<font color='green'>".  $content['resposta2'] ."</font>";  } else { echo  $content['resposta2']; } ?><br />
                            <?php if($content['resposta_correta'] == "C"){  echo "<font color='green'>".  $content['resposta3'] ."</font>";  } else { echo  $content['resposta3']; } ?><br />
                            <?php if($content['resposta_correta'] == "D"){  echo "<font color='green'>".  $content['resposta4'] ."</font>";  } else { echo  $content['resposta4']; } ?><br />
                            <?php if($content['resposta_correta'] == "E"){  echo "<font color='green'>".  $content['resposta5'] ."</font>";  } else { echo  $content['resposta5']; } ?><br />
                        </div>
                        <div class="clearfix"></div>
                    <?php endforeach; ?>
                </div>
                </div>
            </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <script src="<?php echo frontTheme; ?>assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/breakpoints/breakpoints.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/menus/sidr/jquery.sidr.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/sidebar.main.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/sidebar.collapse.init.js?v=v1.0.1-rc2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/core.init.js?v=v1.0.1-rc2"></script>
   
</body>
</html>