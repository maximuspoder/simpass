<!DOCTYPE html>
<html lang="pt_br">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo TITLE; ?></title>
    <link href="<?php echo LIBRARY_DEFAULT; ?>bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo LIBRARY_DEFAULT; ?>css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo LIBRARY_DEFAULT; ?>font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo LIBRARY_DEFAULT; ?>css/style.css" rel="stylesheet">
    <link href="<?php echo LIBRARY_DEFAULT; ?>css/style-responsive.css" rel="stylesheet" />
</head>

<body>

<section id="container">
    <header class="header fixed-top clearfix">
        <div class="brand">
            <a href="<?php echo BASEURL; ?>" class="logo"><img src="<?php echo IMG; ?>logo-sep_198_47.png" alt=""></a>
            <div class="sidebar-toggle-box">
                <div class="fa fa-bars"></div>
            </div>
        </div>
    </header>

<aside>
    <div id="sidebar" class="nav-collapse">
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a href="<?php echo BASEURL; ?>professores/">
                        <i class="fa fa-home"></i><span>Onde tudo começa - <b><i>Filtrar</i></b></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo BASEURL; ?>professores/questoes">
                        <i class="fa fa-video-camera"></i><span>Minhas Questões - <b><i>Lista</i></b></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</aside>


<section id="main-content">
    <section class=" wrapper site-min-height">
        
        <div class="col-lg-12">

        <div class="alert alert-info fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
                <i class="fa fa-times"></i>
            </button>
            <strong>Hey Prof!</strong> Ajude a melhorar editando você mesmo a questão.
        </div>
        <section class="panel">
            <header class="panel-heading"><i class="fa fa-edit"></i> Editar Questão</header>
            <div class="panel-body">
                <div class="position-left">
                <form class="form-inline" action="<?php echo BASEURL; ?>professores/editar_process/<?php echo ext($questao[0], 'qcid')?>" method="post">
                    <div class="form-group">
                        <label>ID(#)</label><br/>
                        <input type="text" disabled="disabled" value="<?php echo ext($questao[0], 'qcid')?>" id="" class="form-control" placeholder="">
                    </div>

                    <div class="form-group">
                        <label>Prova</label><br/>
                        <input type="text" disabled="disabled" value="<?php echo ext($questao[0], 'prova')?>" id="" class="form-control" placeholder="">
                    </div>
                    <div class="form-group">
                        <label>Banca</label><br/>
                        <input type="text" disabled="disabled" value="<?php echo ext($questao[0], 'banca')?>" id="" class="form-control" placeholder="">
                    </div>

                    <div class="form-group">
                        <label>Órgão</label><br/>
                        <input type="text" disabled="disabled" value="<?php echo ext($questao[0], 'banca')?>" id="" class="form-control" placeholder="">
                    </div>

                    <div class="form-group">
                        <label>Disciplina</label><br/>
                        <input type="text" name="disciplina" value="<?php echo ext($questao[0], 'disciplina')?>" id="" class="form-control" placeholder="">
                    </div>

                    <div class="form-group">
                        <label>Assunto 1</label><br/>
                        <input type="text" name="assunto1" value="<?php echo ext($questao[0], 'assunto1')?>" id="" class="form-control" placeholder="">
                    </div>
                    <div class="form-group">
                        <label>Assunto 2</label><br/>
                        <input type="text" name="assunto2" value="<?php echo ext($questao[0], 'assunto2')?>" id="" class="form-control" placeholder="">
                    </div>
                    <div class="form-group">
                        <label>Assunto 3</label><br/>
                        <input type="text" name="assunto3" value="<?php echo ext($questao[0], 'assunto3')?>" id="" class="form-control" placeholder="">
                    </div>

                    <div class="form-group" style="min-height:200px; width: 100%">
                        <label>Questão</label><br/>
                        <textarea name="questao" class="form-control" style="min-height:200px; width:100%"><?php echo ext($questao[0], 'questao')?></textarea>
                    </div><br/>

                     <div class="form-group">
                        <label>Resposta 1</label><br/>
                        <input type="text" name="resposta1" value="<?php echo ext($questao[0], 'resposta1')?>" id="" class="form-control" placeholder="">
                    </div>

                     <div class="form-group">
                        <label>Resposta 2</label><br/>
                        <input type="text" name="resposta2" value="<?php echo ext($questao[0], 'resposta2')?>" id="" class="form-control" placeholder="">
                    </div>

                     <div class="form-group">
                        <label>Resposta 3</label><br/>
                        <input type="text" name="resposta3" value="<?php echo ext($questao[0], 'resposta3')?>" id="" class="form-control" placeholder="">
                    </div>
                     <div class="form-group">
                        <label>Resposta 4</label><br/>
                        <input type="text" name="resposta4" value="<?php echo ext($questao[0], 'resposta4')?>" id="" class="form-control" placeholder="">
                    </div>
                     <div class="form-group">
                        <label>Resposta 5</label><br/>
                        <input type="text" name="resposta5" value="<?php echo ext($questao[0], 'resposta5')?>" id="" class="form-control" placeholder="">
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <button class="btn">Alterar</button>
                    </div>
                </form>
                </div>
            </div>
        </section>
        </div>

    </section>
</section>



</section><!-- container-->

<!--Core js-->
<script src="<?php echo LIBRARY_DEFAULT; ?>js/jquery.js"></script>
<script src="<?php echo LIBRARY_DEFAULT; ?>bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo LIBRARY_DEFAULT; ?>js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo LIBRARY_DEFAULT; ?>js/jquery.scrollTo.min.js"></script>
<script src="<?php echo LIBRARY_DEFAULT; ?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php echo LIBRARY_DEFAULT; ?>js/jquery.nicescroll.js"></script>
<!--common script init for all pages-->
<script src="<?php echo LIBRARY_DEFAULT; ?>js/scripts.js"></script>

</body>
</html>
