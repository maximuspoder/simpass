<!DOCTYPE html>
<html lang="pt_br">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo TITLE; ?></title>
    <link href="<?php echo LIBRARY_DEFAULT; ?>bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo LIBRARY_DEFAULT; ?>css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo LIBRARY_DEFAULT; ?>font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo LIBRARY_DEFAULT; ?>css/style.css" rel="stylesheet">
    <link href="<?php echo LIBRARY_DEFAULT; ?>css/style-responsive.css" rel="stylesheet" />
</head>

<body>

<section id="container">
    <header class="header fixed-top clearfix">
        <div class="brand">
            <a href="<?php echo BASEURL; ?>" class="logo"><img src="<?php echo IMG; ?>logo-sep_198_47.png" alt=""></a>
            <div class="sidebar-toggle-box">
                <div class="fa fa-bars"></div>
            </div>
        </div>
    </header>

<aside>
    <div id="sidebar" class="nav-collapse">
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a href="<?php echo BASEURL; ?>professores/">
                        <i class="fa fa-home"></i><span>Onde tudo começa</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</aside>


<section id="main-content">
    <section class=" wrapper site-min-height">
        
        <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">Filtrar Questões</header>
            <div class="panel-body">
                <div class="position-center">

                </div>
            </div>
        </section>
        </div>

    </section>
</section>



</section><!-- container-->

<!--Core js-->
<script src="<?php echo LIBRARY_DEFAULT; ?>js/jquery.js"></script>
<script src="<?php echo LIBRARY_DEFAULT; ?>bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo LIBRARY_DEFAULT; ?>js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo LIBRARY_DEFAULT; ?>js/jquery.scrollTo.min.js"></script>
<script src="<?php echo LIBRARY_DEFAULT; ?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php echo LIBRARY_DEFAULT; ?>js/jquery.nicescroll.js"></script>
<!--common script init for all pages-->
<script src="<?php echo LIBRARY_DEFAULT; ?>js/scripts.js"></script>

</body>
</html>
