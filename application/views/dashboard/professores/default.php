<!DOCTYPE html>
<html lang="pt_br">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo TITLE; ?></title>
    <link href="<?php echo LIBRARY_DEFAULT; ?>bs3/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo LIBRARY_DEFAULT; ?>css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo LIBRARY_DEFAULT; ?>font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo LIBRARY_DEFAULT; ?>css/style.css" rel="stylesheet">
    <link href="<?php echo LIBRARY_DEFAULT; ?>css/style-responsive.css" rel="stylesheet" />
    <!-- app -->
    <script src="<?php echo JS; ?>jquery.min.js"></script>
    <script src="<?php echo JS; ?>app.js"></script>
    <!-- Array_table -->
    <link href="<?php echo LIBRARY; ?>array_table/application.css" rel="stylesheet" />
    <script src="<?php echo LIBRARY; ?>array_table/jquery.tablesorter.js"></script>
    <!-- modal -->
    <script src="<?php echo LIBRARY; ?>modal/jquery.simplemodal.js" type="text/javascript"></script>
    <script src="<?php echo LIBRARY; ?>modal/jquery.simplemodal.custom.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<?php echo LIBRARY; ?>modal/theme.css" type="text/css"/>
</head>

<body>

<section id="container">
    <header class="header fixed-top clearfix">
        <div class="brand">
            <a href="<?php echo BASEURL; ?>" class="logo"><img src="<?php echo IMG; ?>logo-sep_198_47.png" alt=""></a>
            <div class="sidebar-toggle-box">
                <div class="fa fa-bars"></div>
            </div>
        </div>
    </header>

<aside>
    <div id="sidebar" class="nav-collapse">
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a href="<?php echo BASEURL; ?>professores/">
                        <i class="fa fa-home"></i><span>Onde tudo começa - <b><i>Filtrar</i></b></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo BASEURL; ?>professores/questoes">
                        <i class="fa fa-video-camera"></i><span>Minhas Questões - <b><i>Ver mais</i></b></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</aside>


    <section id="main-content">
        <section class=" wrapper site-min-height">
            <div class="col-lg-12">

            <section class="panel">
            <header class="panel-heading"><i class="fa fa-search"></i> Filtrar Questões</header>
            <div class="panel-body">
            <div class="position-left">


        <form action="<?php echo BASEURL;?>professores/show" method="post" class="form-inline" id="form_filter">
            <div class="form-group">
                <input type="text" name="q__qcid" id="q__qcid" value="<?php echo (!is_null(ext($filtro, 'q__qcid')))? ext($filtro, 'q__qcid') : ''; ?>" class="form-control" placeholder="ID(#)" style="width:79px">
            </div>
            <div class="form-group">
                <input type="text" name="q__orgao" id="q__orgao"  value="<?php echo (!is_null(ext($filtro, 'q__orgao')))? ext($filtro, 'q__orgao') : ''; ?>" class="form-control" style="width:120px" placeholder="Órgão">
            </div>
             <div class="form-group">
                <input type="text" name="q__banca" id="q__banca" value="<?php echo (!is_null(ext($filtro, 'q__banca')))? ext($filtro, 'q__banca') : ''; ?>" class="form-control" style="width:120px" placeholder="Banca">
            </div>
            <div class="form-group">
                <input type="text" name="q__disciplina" id="q__disciplina" value="<?php echo (!is_null(ext($filtro, 'q__disciplina')))? ext($filtro, 'q__disciplina') : ''; ?>" class="form-control" style="width:120px" placeholder="Disciplina">
            </div>
            <div class="form-group">
                <input type="text" name="q__assunto1" id="q__assunto1" value="<?php echo (!is_null(ext($filtro, 'q__assunto1')))? ext($filtro, 'q__assunto1') : ''; ?>" class="form-control" style="width:120px" placeholder="Assunto 1">
            </div>
            <div class="form-group">
                <input type="text" name="q__assunto2" id="q__assunto2" value="<?php echo (!is_null(ext($filtro, 'q__assunto2')))? ext($filtro, 'q__assunto2') : ''; ?>" class="form-control" style="width:120px" placeholder="Assunto 2">
            </div>
            <div class="form-group" style="margin-top:5px">
                <button class="btn">Buscar</button>
                <button class="btn" onclick="redirect_to_unset('professores/index')">Limpar Filtros</button>
            </div>
        </form>

            </div>
            </div>
            </section>

            <section class="panel">
                <header class="panel-heading"><i class="fa fa-search"></i> Listagem de Questões</header>
                <div class="panel-body">
                    <div class="position-left">
                        <?php echo $module_table; ?>
                    </div>
                </div>
            </section>

            </div>

        </section>
    </section>



</section><!-- container-->

<!--Core js-->

<script src="<?php echo LIBRARY_DEFAULT; ?>bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo LIBRARY_DEFAULT; ?>js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo LIBRARY_DEFAULT; ?>js/jquery.scrollTo.min.js"></script>
<script src="<?php echo LIBRARY_DEFAULT; ?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php echo LIBRARY_DEFAULT; ?>js/jquery.nicescroll.js"></script>
<!--common script init for all pages-->
<script src="<?php echo LIBRARY_DEFAULT; ?>js/scripts.js"></script>

</body>
</html>

