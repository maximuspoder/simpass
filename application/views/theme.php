<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo TITLE; ?></title>
    <link href="<?php echo LIBRARY; ?>layout/css/estilo.css" rel="stylesheet">
    <script src="<?php echo LIBRARY; ?>layout/plugins/jquery/jquery.min.js"></script>
</head>
<body>

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <a href="#sidebar-menu" id="toggle-sidebar-menu" class="visible-xs"><i class="fa fa-bars fa-2x"></i></a>
        <ul class="nav navbar-nav">
            <div class="input-group hidden-xs pull-left">
                <span class="input-group-addon"><i class="icon-search"></i></span>
                <input type="text" class="form-control" placeholder="Encontrar alguém">
            </div>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="pull-right">
                <a href="#" id="toggle-chat">
                    <i class="fa fa-comments"></i>
                </a>
            </li>
        </ul>
    </div>
</div>
    <div class="sidebar left hidden-xs">
    <a id="brand" href="#">Simule & Passe</a>
    <?php appMenu() ?>
</div>
    <div class="sidebar right hidden-xs">
    <div class="chat-search">
        <input type="text" class="form-control" placeholder="Buscar"/>
    </div>

    <!-- chat helper -->
    <?php chat(); ?>

    <div id="content">
        <div class="container-fluid">

            <!-- center container -->
            <div class="col-xs-12 col-md-8">
                <!-- conteudo aqui -->
            </div>

            <!-- right container -->
            <div class="col-xs-6 col-md-4">
                <?php widgetUsuario(); ?>
            </div>

        </div>
    </div>

<script src="<?php echo LIBRARY; ?>layout/js/vendor.min.js"></script>
<script src="<?php echo LIBRARY; ?>layout/js/scripts.js"></script>
</body>
</html>
