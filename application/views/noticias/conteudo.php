<!DOCTYPE html>
<html class="footer-sticky">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="author" content="Simule & Passe"/>
	<meta name="robots" content="Index,Follow"/>
	<meta name="description" content="A primeira Rede Social voltada para Concursos, com questões comentadas em vídeo."/>
	<meta name="keywords" content="concurso público, concursos, concurso, concursos públicos, questões, simulados, questões para concursos, questões resolvidas, questões com video, questões gabarito, prova de concurso, prova, CEF, BB, INSS, TCE, TRE, TRT, PRF, TJ, BANCO DO BRASIL, RECEITA FEDERAL, conhecimentos bancários, matemática, língua portuguesa, português, informática, noções de informática,técnico judiciário, direito, direito administrativo, direito constitucional, analista administrativo, técnico administrativo, analista judiciário, auditor fiscal, banca,banca organizadora, organizadora, pesquisar, aprendizado, estudo,  compartilhar, curtir, rede social, rede social para concursos, resolver, comentar, fórum, notícias"/>
	
	<meta property="og:locale" content="pt_BR">
	<meta property="og:url" content="<?php echo URL_FACEBOOK; ?>">
	<meta property="og:title" content="<?php echo ext($news[0], 'titulo'); ?>">
	<meta property="og:site_name" content="Simule & Passe">
	<meta property="og:description" content="O S&P é a maior rede social para concurseiros, tendo os melhores professores do mercado e também uma técnica inovadora para estudos baseado em questões com videos.">
	<meta property="og:image" content="<?php echo BASEURL; ?>application/assets/uploads/noticias/thumb/<?php echo ext($news[0], 'imagem'); ?>">
	<meta property="og:image:width" content="503">
	<meta property="og:image:height" content="377">
 
	<meta property="og:type" content="article">
	<meta property="article:author" content="<?php echo ext($news[0], 'nome') . ' '. ext($news[0], 'sobrenome'); ?>">
	<meta property="article:section" content="<?php echo ext($news[0], 'categoria'); ?>">
	<meta property="article:tag" content="Noticías para concurso público, concurso público, edital, concurso da cef, concurso bb, concurso inss, concurso trt, concurso trf">
	<meta property="article:published_time" content="<?php echo hour_ago(ext($news[0], 'post_date')); ?>">




<link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
<link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>	<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
	
</head>
<body class=" menu-right-hidden">

	<?php
		if($this->session->userdata('logado')){
        userProfileHeader(
            $usuario = array(
                            'name' => $name . ' ' . $surname, 
                            'profile' => $profile, 
                            'usertype' => $type, 
                            'account' => $account,
                            'token' => $this->session->userdata('token')
                        )
        );
    	} else {
        	userProfileHeaderUnregistered();
        }
        ?>

    <br /><br /><br /><br />
	<div class="container"><div id="pdfTarget">
	<div class="innerAll shop-client-products cart invoice">
		
		
		<div class="col-md-12">
			<h1><?php echo ext($news[0], 'titulo'); ?></h1>
			<div class="news-user">
				<img class="media-object pull-left thumb" src="<?php echo ext($news[0], 'foto_perfil'); ?>" alt="<?php echo ext($news[0], 'nome'); ?>" height="45">
			</div>
			Escrito em: <?php echo hour_ago(ext($news[0], 'post_date')); ?><br />
			Por: <?php echo ext($news[0], 'nome') . ' ' . ext($news[0], 'sobrenome'); ?>
		</div>
		<div class="clearfix"></div>

		<div class="col-md-12">

			<div class="col-md-9">
				<div class="box-generic view-news" style="display:table">
					<div class="content">
						<img src="<?php echo BASEURL; ?>application/assets/uploads/noticias/thumb/<?php echo ext($news[0], 'imagem'); ?>" alt="<?php echo ext($news[0], 'titulo'); ?>">
					</div>
					<br />
					<?php echo ext($news[0], 'conteudo'); ?>		
				</div>
			</div>

			<div class="col-md-3">
				<div class="widget">
	            <h5 class="innerAll margin-none border-bottom bg-gray">Mais notícias</h5>
		            <div class="widget-body padding-none" id="updates">
		            <?php if(count($sepNews) > 0){?>
		            <?php foreach($sepNews as $news):?>
		                <div class="media border-bottom innerAll margin-none">
		                    <div class="media-body">
		                        <h5 class="margin-none">
		                        	<i class="icon-newspaper"></i>
		                        	<a href="<?php echo BASEURL; ?>noticias/conteudo/<?php echo $news['categoria']; ?>/<?php echo $news['blogid']; ?>/<?php echo create_url($news['titulo']); ?>" class="text-inverse">
		                        		<?php echo $news['titulo']; ?>
		                        	</a>
		                        </h5>
		                        <small> <?php echo hour_ago($news['post_date']); ?> </small> 
		                    </div>
		                </div>
		            <?php endforeach; ?>
		            <?php } else {?>
		            	<div class="media border-bottom innerAll margin-none">
		                    <div class="media-body">
		                        <h5 class="margin-none">
		                        	Sem notícias novas até o momento
		                        </h5>
		                    </div>
		                </div>
		            <?php } ?>
		            </div>
	            </div>
			</div>
		</div>

		<div class="col-md-10" style="margin-left:10px; margin-bottom:100px">
		<div class="fb-comments" data-width="600" data-href="<?php URL_FACEBOOK; ?>" data-numposts="5" data-colorscheme="light"></div>
		</div>
	</div>
</div>
</div>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core_preload/preload.pace.init.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=569282369859117&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>