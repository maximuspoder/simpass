<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="author" content="Simule & Passe"/>
	<meta name="robots" content="Index,Follow"/>
	<meta name="description" content="A primeira Rede Social voltada para Concursos, com questões comentadas em vídeo."/>
	<meta name="keywords" content="concurso público, concursos, concurso, concursos públicos, questões, simulados, questões para concursos, questões resolvidas, questões com video, questões gabarito, prova de concurso, prova, CEF, BB, INSS, TCE, TRE, TRT, PRF, TJ, BANCO DO BRASIL, RECEITA FEDERAL, conhecimentos bancários, matemática, língua portuguesa, português, informática, noções de informática,técnico judiciário, direito, direito administrativo, direito constitucional, analista administrativo, técnico administrativo, analista judiciário, auditor fiscal, banca,banca organizadora, organizadora, pesquisar, aprendizado, estudo,  compartilhar, curtir, rede social, rede social para concursos, resolver, comentar, fórum, notícias"/>
<link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
<link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>	<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
	
</head>
<body class=" menu-right-hidden">
	<div class="container-fluid ">
		<div id="content">
		<?php
		if($this->session->userdata('logado')){
        userProfileHeader(
            $usuario = array(
                            'name' => $name . ' ' . $surname, 
                            'profile' => $profile, 
                            'usertype' => $type, 
                            'account' => $account,
                            'token' => $this->session->userdata('token')
                        )
        );
    	} else {
        	userProfileHeaderUnregistered();
        }
        ?>
	


	<div class="container">
	<div class="innerLR">
	<div class="innerB">
		<br />
		<div class="clearfix"></div>
	</div>
	<!-- row- -->
	<div class="row">
		<!-- col -->
		<div class="col-md-12">
			<div class="widget border-top-none ">
				<div id="news-featured-2" class="owl-carousel owl-theme">
				<?php foreach($sepNews as $news):?>
					<div class="item">
						<div class="box-generic padding-none margin-none overflow-hidden">
							<div class="relativeWrap overflow-hidden" data-height="175px">
								<a href="<?php echo BASEURL; ?>noticias/conteudo/<?php echo $news['categoria']; ?>/<?php echo $news['blogid']; ?>/<?php echo create_url($news['titulo']); ?>">
								<img src="<?php echo BASEURL; ?>application/assets/uploads/noticias/thumb/<?php echo $news['imagem']; ?>" alt="<?php echo $news['titulo']; ?>" class="img-responsive padding-none border-none" />
								</a>
								<div class="fixed-bottom bg-inverse-faded">
									<div class="media margin-none innerAll">
										<a href="javascript:void(0)" class="pull-left"><img src="<?php echo $news['foto_perfil']; ?>" alt="Simule & Passe" /></a>
										<div class="media-body text-white" style="color:white">
											por <strong><a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $news['usuarioid']; ?>"> <?php echo $news['nome'] . ' '. $news['sobrenome']; ?> </a></strong>
											<p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> <?php echo hour_ago($news['post_date']); ?></p>
										</div>
									</div>
								</div>
							</div>
							<div class="innerAll inner-2x border-bottom news-title" >
								<a href="<?php echo BASEURL; ?>noticias/conteudo/<?php echo $news['categoria']; ?>/<?php echo $news['blogid']; ?>/<?php echo create_url($news['titulo']); ?>">
								<p class="margin-none"><?php echo $news['titulo']; ?></p>
								</a>
							</div>
						</div>
					</div>
				<?php endforeach;?>
				</div>
			</div>

		<div class="widget">
			<div id="news-featured-1" class="owl-carousel owl-theme">
				<?php foreach($sepBlog as $news):?>
				<div class="item">
				<div class="row row-merge bg-gray">
					<div class="col-md-12">
						<div class="innerAll inner-2x">
							<h4>
								<a href="<?php echo BASEURL; ?>noticias/conteudo/<?php echo $news['categoria']; ?>/<?php echo $news['blogid']; ?>/<?php echo create_url($news['titulo']); ?>">
								<?php echo $news['titulo']; ?>
								</a>
							</h4>
							<p class="text-muted"><?php echo hour_ago($news['post_date']); ?> - por: <a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $news['usuarioid']; ?>"><?php echo $news['nome'] . ' '. $news['sobrenome']; ?></a> </p>
							<p class="margin-none"><?php echo substr( $news['conteudo'], 0, 300); ?></p>
						</div>
					</div>				
				</div>
				</div>
			<?php endforeach; ?>
			</div>
		</div>


		
		</div>
		</div>
	</div> 
</div>
		</div> 
	</div>			
</div>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core_preload/preload.pace.init.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_owl-carousel/owl.carousel.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/admin_news/news-featured-2.init.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/components/admin_news/news-featured-1.init.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/components/admin_news/news-featured-3.init.js?v=v2.0.0-rc8"></script>
</body>
</html>