<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>
     $(document).ready(function(){
        var wbbOpt = {buttons: "bold,italic,underline,|,link,|,quote,code,img"}
        $("#editor").wysibb(wbbOpt);           
        $('#btnPost').click(function(){
            if( $('textarea#content').html($('#conteudo').html())){
                $('#validateSubmitForm').submit();
            }
            
        })
        var conteudo=$('textarea#hidden').val();
        $('#conteudo').html(conteudo)
    })
    </script>

</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
             <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>


            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            
                        <form method="post" action="<?php echo BASEURL;?>noticias/editar_process" enctype="multipart/form-data" class="form-horizontal margin-none" id="validateSubmitForm">
                        

                        <div class="widget-body">

                        <div class="col-lg-12 col-md-8">
                            <label> Título </label>
                            <input type="text" name="titulo" id="titulo" value="<?php echo ext($noticia[0], 'titulo'); ?>" placeholder="Título para a notícia" class="form-control">
                        </div>

                        <div class="col-lg-6 col-md-8">
                            <label> Categoria </label>
                            <select name="categoria" id="categoria" class="form-control">
                                <option value="concursos" <?php echo (ext($noticia[0], 'categoria') == 'concursos') ? 'selected' : ''; ?>>Concursos</option>
                                <option value="blog" <?php echo (ext($noticia[0], 'categoria') == 'blog') ? 'selected' : ''; ?>>Blog</option>
                                <option value="edital" <?php echo (ext($noticia[0], 'categoria') == 'edital') ? 'selected' : ''; ?>>Edital</option>
                            </select>
                            <input name="idnoticia" value="<?php echo $idnoticia; ?>" type="hidden">
                        </div>

                        <div class="col-lg-6 col-md-8">
                            <label> Imagem </label>
                            <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
                                <div class="input-group">
                                    <div class="form-control col-md-3"><i class="fa fa-file fileupload-exists"></i> 
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-new">Selecionar Imagem</span>
                                            <span class="fileupload-exists">Mudar</span>
                                        <input type="file" name="imagem" id="imagem" class="margin-none">
                                        </span>
                                        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
                                    </span>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-8">

                            <label> Conteúdo </label>

                            <div class="widget-body">
                                <div class="wysibb-editor">
                                    <textarea id="hidden" name="hidden" style="display:none"><?php echo ext($noticia[0], 'conteudo') ?></textarea>
                                    <textarea id="editor" name="editor"></textarea>
                                </div>
                            </div>
                            <textarea id="content" name="content" style="display:none;">
                            </textarea>
                        </div>

                        </div>

                        </form>
                        <button id="btnPost" class="btn btn-primary" style="margin-top:5px;">Editar</button>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>




<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/forms_elements_jasny-fileupload/js/bootstrap-fileupload.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
<link rel="stylesheet" href="<?php echo LIBRARY; ?>wysibb/theme/default/wbbtheme.css" />
<script src="<?php echo LIBRARY; ?>wysibb/jquery.wysibb.js"></script>
<script src="<?php echo JS; ?>appuser.js"></script>

</body>
</html>