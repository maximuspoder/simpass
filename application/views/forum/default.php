<div class="widget">
<h4 class="innerAll border-bottom margin-bottom-none">Postagens <div class="pull-right display-block"></div></h4>
                            
    <?php foreach($forum as $data):?>        
    <div class="innerAll border-bottom tickets">
        <div class="row">
        <div class="col-sm-10">
            <ul class="media-list">
                <li class="media">
                   
                    <a class="pull-left" href="#">
                    <img class="media-object" src="<?php echo $data['foto_perfil']; ?>" alt="...">
                    </a>
                    <div class="media-body">
                        <a href="" class="media-heading"><?php echo $name; ?></a>
                        <!--<label class="label label-default">Data</label>-->
                        <div class="clearfix"></div>
                        <strong>[Mensagem:]</strong> 
                        <?php echo $data['mensagem']; ?>
                        <div class="clearfix"></div>
                        <small>
                        <i class="icon-time-clock fw"></i> em: <?php echo $data['date_post']; ?></small>
                    </div>
                </li>
            </ul>
        </div>
        </div>
    </div>
     <?php endforeach; ?>       


<div class="widget-body" id="forumfirst_<?php echo $qcid; ?>">
<div class="innerLR">
    <form class="form-horizontal" onsubmit="return false;" role="form">
    <div class="form-group">
        <label class="col-sm-2 control-label">Usuário</label>
        <div class="col-sm-10">
            <p class="form-control-static"><?php echo $name; ?></p>
        </div>
    </div>
    <div class="form-group">
        <label for="inputDuvida" class="col-sm-2 control-label">Qual sua dúvida?</label>
        <div class="col-sm-10">
            <textarea id="duvida_<?php echo $qcid; ?>" class="form-control"></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button onclick="set_forum_post(<?php echo $qcid; ?>, '<?php echo $token; ?>')" id="btnForum" class="btn btn-primary">Postar</button>
        </div>
    </div>
    </form>
</div>
</div>
</div>

                                   