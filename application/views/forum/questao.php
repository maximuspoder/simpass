<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
             <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>


            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">



                <div class="widget">

                <div class="innerAll border-bottom media">
                    <?php if($professor != null){?>
                    <a class="pull-left" href="<?php echo BASEURL; ?>usuario/perfil/<?php echo ext($professor[0], 'usuarioid')  ?>">
                        <img src="<?php echo ext($professor[0], 'foto_perfil')  ?>" alt="<?php echo ext($professor[0], 'nome') . ' ' . ext($professor[0], 'sobrenome') ?>"></a>
                    <?php } else { ?>
                    <a class="pull-left" href="javascript:void(0)" target="_blank"><img src="<?php echo BASEURL; ?>application/assets/uploads/cover/default.png" alt="Simule & Passe" height="40" width="0"></a>
                    <?php }?>
                    <div class="media-body">
                        <h3 class="margin-bottom-none padding-none"> <?php echo ext($questao[0], 'questao')?></h3>
                        <br /><br />
                        <h4 class="margin-bottom-none padding-none">
                            Resposta Correta:<br />
                            <?php 
                                if(ext($questao[0], 'resposta_correta') == 'A'){
                                    echo ext($questao[0], 'resposta1');
                                }
                                elseif(ext($questao[0], 'resposta_correta') == 'B'){
                                    echo ext($questao[0], 'resposta2');
                                }
                                elseif(ext($questao[0], 'resposta_correta') == 'C'){
                                    echo ext($questao[0], 'resposta3');
                                }
                                elseif(ext($questao[0], 'resposta_correta') == 'D'){
                                    echo ext($questao[0], 'resposta4');
                                }
                                elseif(ext($questao[0], 'resposta_correta') == 'E'){
                                    echo ext($questao[0], 'resposta5');
                                }
                            ?>
                        </h4><br />
                        <?php if($professor != null){?>
                        <span class="text-muted-dark">Professor <a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo ext($professor[0], 'usuarioid')  ?>"><?php echo ext($professor[0], 'nome') . ' ' . ext($professor[0], 'sobrenome') ?></a> </span>
                        <?php } else { ?>
                        <span class="text-muted-dark">Simule & Passe</span>
                        <?php }?>
                    </div>
                </div>

                <div class="innerAll inner-2x">
                   
                </div>

                <div class="border-top border-bottom text-center innerAll bg-gray">
                    <a href="#write" data-toggle="collapse" class="btn btn-primary strong btn-sm">Escrever Resposta <i class="icon-compose fa fa-fw"></i></a>
                </div>

                <div class="collapse" id="write">
                    <form action="<?php echo BASEURL; ?>forum/responder/<?php echo $id;?>" method="post">
                    <div class="innerAll bg-gray inner-2x border-bottom">
                        <textarea name="post" class=" form-control" rows="3" placeholder="Sua resposta..."></textarea>
                        <div class="text-right innerT">
                            <button class="btn btn-success">Enviar</button>
                        </div>
                    </div>
                    </form>
                </div>


                <?php foreach($respostas as $data): ?>
                <div class="innerAll border-bottom tickets">
                <div class="media innerAll inner-2x border-bottom margin-none">

                    <div class="pull-left media-object" style="width:100px">
                        <div class="text-center">                            
                            <a href="#" class="clearfix">
                                <img src="<?php echo $data['foto_perfil']; ?>" class="rounded-none">
                            </a> 
                        </div>
                    </div>

                    <div class="media-body">
                        <?php if($data['token'] == $this->session->userdata('token') ){?>
                        <div class="btn-group pull-right btn-group-xs">
                            <a href="<?php echo BASEURL; ?>forum/deletar/<?php echo $id; ?>/<?php echo $data['forumid'];?>" class="btn btn-info">
                                <i class="fa fa-eraser"></i> Deletar
                            </a>
                        </div>
                        <?php } ?>
                        <small class="pull-right label label-default"><?php echo timestamp_decode_date($data['last_coment']); ?> -  <?php echo timestamp_decode_time($data['last_coment']); ?></small>
                        <h5><a href="" class="text-primary"><?php echo $data['nome'] . ' ' . $data['sobrenome']; ?></a> <span>escreveu:</span></h5>
                        <p><?php echo $data['mensagem']; ?></p>
                        
                    </div>
                </div>
                </div>
                <?php endforeach; ?>




                </div>











    


                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#5dc94b',
        dangerColor = '#ff553e',
        successColor = '#80cf47',
        infoColor = '#6fa3cb',
        warningColor = '#f48f3e',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>



<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script> 
<script src="<?php echo JS; ?>appuser.js"></script>
</body>
</html>
