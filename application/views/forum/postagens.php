<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
             <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>


            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
    

                        <div class="col-lg-3 col-md-8">
                        <h3>Mais Acessadas</h3>
                        <div class="separator-h"></div>
                        <div class="widget">
                            <h4 class="innerAll bg-gray margin-none">Disciplina</h4>
                            <ul class="list-group list-group-1 margin-none borders-none">
                            <?php foreach($mais_acessadas as $disciplina): ?>
                                <li class="list-group-item border-bottom-none animated fadeInUp">
                                    <a href="#"><span class="badge pull-right badge-default hidden-md"><?php echo $disciplina['total']; ?></span><?php echo $disciplina['disciplina']; ?></a>
                                </li>
                            <?php endforeach; ?>
                            </ul>
                        </div>
                        </div>





                        <div class="col-md-9" style="margin-top:57px">
                        <div class="widget">
                
            
                        <div class="innerAll bg-gray border-bottom">
                            <!--<a href="#" class="btn btn-xs btn-inverse pull-right"><i class="fa fa-plus fa-fw"></i> New Topic</a>-->
                            <h4 class="margin-none">Últimas Questões Comentadas</h4>
                            <div class="clearfix"></div>
                        </div>
                        <?php if(count($posts) > 0){ ?>
                        <?php foreach($posts as $result): ?>
            
                                <div class="bg-gray-hover overflow-hidden">
                                <div class="row innerAll half border-bottom">
                                    <div class="col-sm-6 col-xs-8">
                                        <ul class="media-list margin-none">
                                            <li class="media">
                                                <a class="pull-left innerAll half " href="#">
                                                    <span class="empty-photo"><i class="fa fa-edit fa-2x text-muted"></i></span>
                                                </a>
                                                <div class="media-body">
                                                    <div class="innerAll half">
                                                        <h4 class="margin-none">
                                                            <a href="<?php echo BASEURL; ?>forum/questao/<?php echo encrypt($result['qcid'], QCIDHASH); ?>" class="media-heading strong text-primary">
                                                                <?php echo substr($result['questao'], 0, 100); ?>
                                                            </a>
                                                        </h4>
                                                        <div class="clearfix"></div>
                                                        <a href="<?php echo BASEURL; ?>forum/questao/<?php echo encrypt($result['qcid'], QCIDHASH); ?>"><small class="margin-none">Ver mais</small></a>
                                                    </div>
                                                    
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3 col-xs-4">
                                        <div class="text-center">
                                            <p class="lead margin-bottom-none">
                                                <?php echo timestamp_decode_date($result['last_coment']); ?>
                                            </p>
                                            <span class="text-muted">
                                                <?php echo timestamp_decode_time($result['last_coment']); ?>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-hidden">
                                        <div class="innerAll half">
                                            <div class="media">
                                                <a href="" class="pull-left">
                                                    <img src="<?php echo $result['foto_perfil']; ?>" class="media-object"/>
                                                </a>
                                                <div class="media-body">
                                                    <a href="" class="text-small"></a>
                                                    <div class="clearfix"></div>
                                                    <small>Último comentário por: <strong><?php echo $result['nome']; ?></strong></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                        <?php endforeach; ?>
                        <?php } else { ?>

                        <?php 
                            echo message('info', 'Não existe tópicos até o momento.', 'margin:200px;');
                        } 
                        ?>
                        </div>
                        </div>


                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#5dc94b',
        dangerColor = '#ff553e',
        successColor = '#80cf47',
        infoColor = '#6fa3cb',
        warningColor = '#f48f3e',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>



<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_gridalicious/jquery.gridalicious.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/media_gridalicious/gridalicious.init.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script> 
<script src="<?php echo JS; ?>appuser.js"></script>
</body>
</html>
