<center><h3> Nada postado até o momento! (<small>Seja o primeiro!</small>)</h3></center>                  
<div class="widget-body" id="forumfirst_<?php echo $qcid; ?>">
<div class="innerLR">
    <form class="form-horizontal" onsubmit="return false;" role="form">
    <div class="form-group">
        <label class="col-sm-2 control-label">Usuário</label>
        <div class="col-sm-10">
            <p class="form-control-static"><?php echo $name; ?></p>
        </div>
    </div>
    <div class="form-group">
        <label for="inputDuvida" class="col-sm-2 control-label">Qual sua dúvida?</label>
        <div class="col-sm-10">
            <textarea id="duvida_<?php echo $qcid; ?>" class="form-control"></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button onclick="set_forum_post(<?php echo $qcid; ?>, '<?php echo $token; ?>')" id="btnForum" class="btn btn-primary">Postar</button>
        </div>
    </div>
    </form>
</div>
</div>