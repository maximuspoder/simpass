<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />


<script src="<?php echo JS; ?>appuser.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script> <script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
        <?php 
        userProfileHeader(
            $usuario = array(
                'name' => $name . ' ' . $surname, 
                'profile' => $profile, 
                'usertype' => $type, 
                'account' => $account,
                'token' => $this->session->userdata('token')

            )); 
        ?>




<div class="container" style="margin-top:30px">
    <div class="innerLR">
    
    
   
    
    <?php if(ext($simulado_final[0], 'total') > 0){ ?>
     <h2 class="margin-none pull-left">
        Seus números neste simulado&nbsp;
        <i class="fa fa-fw icon-cool text-muted"></i>
    </h2>
    
    <br />
    <br />
    <br />

    <!-- content -->
    <div class="row row-app">
        <div class="col-md-3">
            <div class="widget">
                <div class="text-center innerAll inner-2x border-bottom">
                    <div class="innerTB">
                        <div data-percent="100" data-size="100" class="easy-pie inline-block primary easyPieChart" data-scale-color="false" data-track-color="#efefef" data-line-width="5" style="width: 100px; height: 100px; line-height: 100px;">
                            <div class="value text-center">
                                <span class="strong"><i class="icon-document-bar fa-3x text-primary"></i></span>
                            </div>
                            <canvas width="100" height="100"></canvas>
                        </div>
                    </div>
                </div>
                <div class="text-center innerAll inner-2x bg-gray">
                    <p class="lead margin-none">
                        <span class="text-large text-regular"><?php echo ext($simulado_final[0], 'total'); ?></span>
                        <span class="clearfix"></span>
                        <span class="text-primary">Total de questões</span>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="widget">
                <div class="text-center innerAll inner-2x border-bottom">
                    <div class="innerTB">
                        <div data-percent="<?php echo (ext($simulado_final[0], 'acertos') / ext($simulado_final[0], 'total')) * 100; ?>" data-size="100" class="easy-pie inline-block primary easyPieChart" data-scale-color="false" data-track-color="#efefef" data-line-width="5" style="width: 100px; height: 100px; line-height: 100px;">
                            <div class="value text-center">
                                <span class="strong"><i class="icon-document-check fa-3x text-primary"></i></span>
                            </div>
                        <canvas width="100" height="100"></canvas></div>
                    </div>
                </div>
                <div class="text-center innerAll inner-2x bg-gray">
                    <p class="lead margin-none">
                        <span class="text-large text-regular"><?php echo ext($simulado_final[0], 'acertos'); ?></span>
                        <span class="clearfix"></span>
                        <span class="text-primary">Total de acertos</span>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="widget">
                <div class="text-center innerAll inner-2x border-bottom">
                    <div class="innerTB">
                        <div data-percent="<?php echo (ext($simulado_final[0], 'erros') / ext($simulado_final[0], 'total')) * 100; ?>" data-size="100" class="easy-pie inline-block primary easyPieChart" data-scale-color="false" data-track-color="#efefef" data-line-width="5" style="width: 100px; height: 100px; line-height: 100px;">
                            <div class="value text-center">
                                <span class="strong"><i class="icon-document-delete fa-3x text-primary"></i></span>
                            </div>
                        <canvas width="100" height="100"></canvas></div>
                    </div>
                </div>
                <div class="text-center innerAll inner-2x bg-gray">
                    <p class="lead margin-none">
                        <span class="text-large text-regular"><?php echo ext($simulado_final[0], 'erros'); ?></span>
                        <span class="clearfix"></span>
                        <span class="text-primary">Total de erros</span>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="widget">
                <div class="text-center innerAll inner-2x border-bottom">
                    <div class="innerTB">
                        <div data-percent="<?php echo (ext($simulado_final[0], 'branco') / ext($simulado_final[0], 'total')) * 100; ?>" data-size="100" class="easy-pie inline-block primary easyPieChart" data-scale-color="false" data-track-color="#efefef" data-line-width="5" style="width: 100px; height: 100px; line-height: 100px;">
                            <div class="value text-center">
                                <span class="strong"><i class="icon-document-blank fa-3x text-primary"></i></span>
                            </div>
                        <canvas width="100" height="100"></canvas></div>
                    </div>
                </div>
                <div class="text-center innerAll inner-2x bg-gray">
                    <p class="lead margin-none">
                        <span class="text-large text-regular"><?php echo ext($simulado_final[0], 'branco'); ?></span>
                        <span class="clearfix"></span>
                        <span class="text-primary">Total em branco</span>
                    </p>
                </div>
            </div>
        </div>
       
    </div>
   

     
    <div class="col-sm-12">
        <button id="btnInfo" onclick="redirect('simulado/novo')" class="btn btn-primary"  style="width:100%">Novo Simulado</button>
        <center>OU</center>
        <button id="btnInfo" onclick="redirect('usuario/')" class="btn btn-primary"  style="width:100%">Home</button>
    </div>

    <?php } else { ?>
        <div class="alert alert-danger">
            <button type="button" class="close" onclick="redirect('usuario/')" >×</button>
            <strong>Hey!</strong> O simulado requisitado <b>não existe</b> ou <b>não pertence a você</b>!
        </div>
         <div class="col-sm-12" style="margin-left:-4px">
        <button id="btnInfo" onclick="redirect('usuario/')" class="btn btn-primary" >Voltar a sua página</button>
    </div>
    <?php }  ?>
     











        </div>
    </div>
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#5dc94b',
        dangerColor = '#ff553e',
        successColor = '#80cf47',
        infoColor = '#6fa3cb',
        warningColor = '#f48f3e',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>




<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/charts_easy_pie/js/jquery.easy-pie-chart.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/charts_easy_pie/easy-pie.init.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/jquery.flot.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/jquery.flot.resize.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/plugins/jquery.flot.tooltip.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/charts_flot/flotcharts.common.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/charts_flot/flotchart-simple.init.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/components/menus/sidebar.main.init.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/components/menus/sidebar.collapse.init.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/components/menus/menus.sidebar.chat.init.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/other_mixitup/jquery.mixitup.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/other_mixitup/mixitup.init.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script>
    
</body>
</html>