<!DOCTYPE html>
<html class="footer-sticky"lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?&v=v0.0.1.2"></script>
    <script src="<?php echo JS; ?>appuser.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
        <?php 
        userProfileHeader(
            $usuario = array(
                'name' => $name . ' ' . $surname, 
                'profile' => $profile, 
                'usertype' => $type, 
                'account' => $account,
                'token' => $this->session->userdata('token')
            )); 
        ?>


            
        
        <div class="container">
            <div class="innerAll">
                <div class="row">
                    <div class="col-lg-12 col-md-8">
           




                    <div class="widget widget-body-white" style="margin-top:20px;">
                        <div class="widget-head">
                            <h4 class="heading glyphicons calendar"><i></i> Seus histórico de simulados até <?php echo date('d/m/Y')?></h4>
                            <div style="float:right">
                                <span onclick="redirect('simulado/limpar/historico')" class="label label-success label-stroke" style="cursor:pointer">Limpar Histórico<span>
                               </div>
                        </div>
                        
                        
                        <div class="widget-body">
                        
                        
                            <div class="separator bottom">
                                Total de simulados: <b><?php echo count($history); ?></b>
                                <div class="clearfix"></div>
                            </div>
                            
                            <table class="table table-condensed table-striped table-primary table-vertical-center checkboxs">
                                <thead>
                                    <tr>
                                        <th class="center">ID(#)</th>
                                        <th class="center">Data</th>
                                        <th class="center">Prova</th>
                                        <th class="center">Total de Questões</th>
                                        <th class="center">Acertos</th>
                                        <th class="center">Erros</th>
                                        <th class="center">Em Branco</th>
                                        <th class="center">Desempenho</th>
                                        <th class="center">Deletar</th>


                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if(count($history) > 0){
                                foreach($history as $data): 
                                ?>
                                    <tr class="selectable">
                                        <td class="center"><?php echo $data['provaid']; ?></td>
                                        <td class="center"><?php echo $data['data']; ?></td>
                                        <td class="center"><?php echo ($data['prova'] == 0) ? 'Múltiplas Provas' : $data['prova']; ?></td>
                                        <td class="center"><?php echo $data['total']; ?></td>
                                        <td class="center"><?php echo $data['acertos']; ?></td>
                                        <td class="center"><?php echo $data['erros']; ?></td>
                                        <td class="center"><?php echo $data['branco']; ?></td>
                                        <td class="center"><a href="<?php echo BASEURL; ?>simulado/prova/<?php echo $data['provaid']; ?>" class="btn btn-default">Ver</a></td>
                                        <td class="center"><a href="<?php echo BASEURL; ?>simulado/deletar/<?php echo $data['provaid']; ?>" class="btn btn-default">Deletar</a></td>
                                    </tr>
                                <?php 
                                endforeach; 
                                } else {
                                ?>
                                <tr class="selectable">
                                    <td class="center">Nenhum simulado realizado até o momento</td>
                                    <td class="center">-</td>
                                    <td class="center">-</td>
                                    <td class="center">-</td>
                                    <td class="center">-</td>
                                    <td class="center">-</td>
                                    <td class="center">-</td>
                                </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                                        
                        </div>
                    </div>

                    </div>
                </div>
            </div>                   
        </div>
        </div>
    </div>
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#5dc94b',
        dangerColor = '#ff553e',
        successColor = '#80cf47',
        infoColor = '#6fa3cb',
        warningColor = '#f48f3e',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_gridalicious/jquery.gridalicious.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/media_gridalicious/gridalicious.init.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script> 
</body>
</html>