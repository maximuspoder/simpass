<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
     <script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?&v=v0.0.1.2"></script>
    <script src="<?php echo JS; ?>appuser.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
        <?php 
        userProfileHeader(
            $usuario = array(
                'name' => $name . ' ' . $surname, 
                'profile' => $profile, 
                'usertype' => $type, 
                'account' => $account,
                'token' => $this->session->userdata('token')
            )); 
        ?>


            
        
        <div class="container">
            <div class="innerAll">
                <div class="row">



                    <div class="col-lg-12 col-md-8">
                    <br />
                    <?php filterBarSimuladoFREE($provaid, $itemsPerPage, 1); ?>
                    <br />

            <?php
                $tabIterator = 1;
                foreach($simulado as $data): 
            ?>


            <div id="simulado">
               <div class="wizard-head">
                <ul class="bwizard-steps">
                    <li class="active"><a href="#tab2" data-toggle="tab"><?php echo $data['banca']; ?></a></li>
                    <li class="active-1"><a href="#tab3" data-toggle="tab"><?php echo $data['orgao']; ?></a></li>
                    <li class="active-1"><a href="#tab3" data-toggle="tab"><?php echo $data['cargo']; ?></a></li>
                    <li class="active-1"><a href="#tab4" data-toggle="tab"><?php echo $data['disciplina']; ?></a></li>
                    <li class="active-1"><a href="#tab5" data-toggle="tab"><?php echo $data['escolaridade']; ?></a></li>
                    <li class="active-1"><a href="#tab5" data-toggle="tab"><?php echo $data['ano']; ?></a></li>
                </ul>
                </div>
            <div class="innerLR"  style="margin-left:-10px">

                <div class="wizard">
                    <div class="widget widget-tabs widget-tabs-responsive">
                        
                        <div class="widget-head">
                            <ul>
                                <li class="active"><a href="#tab<?php echo $tabIterator; ?>-1" class="glyphicons pencil" data-toggle="tab"><i></i>Questão</a></li>
                                <li><a href="#tab<?php echo $tabIterator; ?>-2" onclick="load_forum(<?php echo $data['qcid'] + 375; ?>, '<?php echo $name . ' ' . $surname; ?>', '<?php echo $token ?>')" class="glyphicons eye_open" data-toggle="tab"><i></i>Fórum</a></li>
                                <li><a href="#tab<?php echo $tabIterator; ?>-3" class="glyphicons circle_info" data-toggle="tab"><i></i>Encontrou algum erro? Informe!</a></li>
                            </ul>
                        </div>
                        
                        <div class="widget-body">
                            <div class="tab-content">
                            
                                <div class="tab-pane active" id="tab<?php echo $tabIterator; ?>-1">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-8">

                                            <?php 
                                                $arrayAttachment = array(
                                                    'Q'. $data['qcid'], 
                                                    'Q'. $data['qcid'] . '-A',
                                                    'Q'. $data['qcid'] . '-B',
                                                    'Q'. $data['qcid'] . '-C',
                                                    'Q'. $data['qcid'] . '-D',
                                                    'Q'. $data['qcid'] . '-E'
                                                );
                                                $arrayFormats = array(
                                                    '.html', 
                                                    '.htm', 
                                                    '.jpg', 
                                                    '.png'
                                                );
                                                
                                                $result = getAttachmentFile('complementar/', $arrayAttachment, $arrayFormats);

                                                if(count($result) > 0){
                                            ?>
                                             <div class="col-lg-12 col-md-8">
                                                <button class="btn btn-default" onclick="getAttach(<?php echo $data['qcid']; ?>)">Conteúdo em anexo</button>
                                            </div>

                                            <div class="col-lg-12 col-md-8" style="margin-top:20px; margin-bottom:20px; display:none; border:1px solid #ccc; border-radius:7px;" id="viewAttachment<?php echo $data['qcid']; ?>">
                                            <?php foreach ($result as $attach) { ?>
                                                <?php if(!strstr($attach, '.htm')){ ?>
                                                    <img src="<?php echo $attach; ?>" alt="imagem complementar">
                                                    <br />
                                                <?php } else { ?>
                                                    <script> ajax_get_complementar('<?php echo $attach; ?>', 'ajax' + <?php echo $data['qcid']; ?>) </script>
                                                    <div id="ajax<?php echo $data['qcid']; ?>"></div>
                                                <?php } ?>

                                            <?php } ?>
                                                
                                            </div>
                                            <?php } ?>


                                            <br />
                                            <br />
                                            <?php echo $data['questao']; ?>
                                            <br />
                                            <br />


                                            <?php 
                                            if(!strstr($data['resposta1'], "CERTO") && !strstr($data['resposta1'], "ERRADO")){
                                            ?>
                                            <!-- A -->
                                            <div id="correcao_<?php echo $tabIterator; ?>_A">
                                                <div class="input_simulado" style="float:left; padding-top:0px">
                                                    <input type="radio" name="resposta_<?php echo $tabIterator; ?>" class="resposta_<?php echo $tabIterator; ?>" value="A">&nbsp;&nbsp;
                                                </div>
                                                    <?php echo $data['resposta1']; ?>
                                            </div>
                                            <div style="clear:both"></div>
                                            <!-- B -->
                                            <div id="correcao_<?php echo $tabIterator; ?>_B">
                                                <div class="input_simulado" style="float:left; padding-top:0px">
                                                    <input type="radio" name="resposta_<?php echo $tabIterator; ?>" class="resposta_<?php echo $tabIterator; ?>" value="B">&nbsp;&nbsp;
                                                </div>
                                                    <?php echo trim($data['resposta2']); ?>
                                            </div>
                                            <div style="clear:both"></div>
                                             <!-- C -->
                                             <div id="correcao_<?php echo $tabIterator; ?>_C">
                                                    <div class="input_simulado" style="float:left; padding-top:0px">
                                                        <input type="radio" name="resposta_<?php echo $tabIterator; ?>" class="resposta_<?php echo $tabIterator; ?>" value="C">&nbsp;&nbsp;
                                                    </div>
                                                     <?php echo $data['resposta3']; ?>
                                            </div>
                                             <div style="clear:both"></div>
                                            <!-- D -->
                                            <div id="correcao_<?php echo $tabIterator; ?>_D">
                                                <div class="input_simulado" style="float:left; padding-top:0px">
                                                    <input type="radio" name="resposta_<?php echo $tabIterator; ?>" class="resposta_<?php echo $tabIterator; ?>" value="D">&nbsp;&nbsp;
                                                </div>
                                                    <?php echo $data['resposta4']; ?>
                                            </div>
                                            <div style="clear:both"></div>
                                             <!-- E -->
                                             <div id="correcao_<?php echo $tabIterator; ?>_E">
                                                <div class="input_simulado" style="float:left; padding-top:0px">
                                                    <input type="radio" name="resposta_<?php echo $tabIterator; ?>" class="resposta_<?php echo $tabIterator; ?>" value="E">&nbsp;&nbsp;
                                                </div>
                                                    <?php echo $data['resposta5']; ?>
                                            </div>
                                            <?php } else { ?>
                                            <!-- A -->
                                            <div id="correcao_<?php echo $tabIterator; ?>_A">
                                                <div class="input_simulado" style="float:left; padding-top:0px">
                                                    <input type="radio" name="resposta_<?php echo $tabIterator; ?>" class="resposta_<?php echo $tabIterator; ?>" value="A">&nbsp;&nbsp;
                                                </div>
                                                    a) <?php echo $data['resposta1']; ?>
                                            </div>
                                            <div style="clear:both"></div>
                                            <!-- B -->
                                            <div id="correcao_<?php echo $tabIterator; ?>_B">
                                                <div class="input_simulado" style="float:left; padding-top:0px">
                                                    <input type="radio" name="resposta_<?php echo $tabIterator; ?>" class="resposta_<?php echo $tabIterator; ?>" value="B">&nbsp;&nbsp;
                                                </div>
                                                    b) <?php echo $data['resposta2']; ?>
                                            </div>
                                            <div style="clear:both"></div>
                                            <?php } ?>

                                            <div id="info_<?php echo $tabIterator; ?>" style="margin-top:14px; display:none"></div>
                                             
                                                <button id="btnResult_<?php echo $tabIterator; ?>" onclick="prova_set_result(<?php echo $provaid; ?>,<?php echo $data['qcid']; ?>, '<?php echo $this->session->userdata('token'); ?>', <?php echo $tabIterator; ?>);" class="btn btn-default">Resolver</button>
                                            
                                        </div>

                                        <div class="col-lg-6 col-md-8">
                                            <iframe src="<?php echo $data['vimeocode']; ?>" width="550" height="281" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                        </div>


                                        
                                    </div>
                                </div>
                            
                                <div class="tab-pane" id="tab<?php echo $tabIterator; ?>-2">                                 
                                    <div id="forum_<?php echo $data['qcid'] + 375; ?>"></div>                                    
                                </div>
                                <div class="tab-pane" id="tab<?php echo $tabIterator; ?>-3">
                                    <div class="widget-body">
                                    <div class="innerLR">
                                        <form class="form-horizontal" onsubmit="return false;" role="form">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Usuário</label>
                                            <div class="col-sm-10">
                                                <p class="form-control-static"><?php echo $email; ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-2 control-label">Qual o erro nesta questão?</label>
                                            <div class="col-sm-10">
                                                <textarea name="erro" id="erro_<?php echo encrypt($data['qcid'], QCIDHASH); ?>" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button id="btnInfo" onclick="ajax_set_questao_erro('<?php echo encrypt($data['qcid'], QCIDHASH); ?>', '<?php echo $token; ?>')" class="btn btn-primary">Enviar</button>
                                            </div>
                                        </div>
                                        </form>

                                        <div id="infoerro_<?php echo encrypt($data['qcid'], QCIDHASH); ?>"></div>
                                    </div>
                                    </div>
                                </div>
                               
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <?php 
                if($tabIterator == $itemsPerPage){
                  break;
                }
                $tabIterator++;
                endforeach; 
            ?>



<?php filterBarSimuladoFREE($provaid, $itemsPerPage, 2); ?>
                
                        
                    </div>                    
                    
                </div>
            </div>
        </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#5dc94b',
        dangerColor = '#ff553e',
        successColor = '#80cf47',
        infoColor = '#6fa3cb',
        warningColor = '#f48f3e',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
   <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/media_gridalicious/jquery.gridalicious.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/components/media_gridalicious/gridalicious.init.js?v=v2.0.0-rc8"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script> 

    
    <script type="text/javascript">
    $(document).ready(function(){
        $('#ano_maior').change(function(){        
            if($('#ano_maior').val() < $('#ano_menor').val()){
                alert('O ano não pode ser menor que o ano menor!');
                $("#ano_menor option").prop("selected", false);
                $("#ano_maior option").prop("selected", false);
            }
        });
        $('#banca').change(function(){
            banca = $(this).val();
            ajax_restBanca( banca );
        });

        $('#orgao').change(function(){
            banca = $('#banca').val();
            orgao = $(this).val();
            if(banca != ''){
                ajax_restOrgao(banca, orgao);
            }
        });
    })
    </script>
</body>
</html>