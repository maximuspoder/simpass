<!DOCTYPE html>
<html class="footer-sticky"lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />

<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
        <?php 
        userProfileHeader(
            $usuario = array(
                'name' => $name . ' ' . $surname, 
                'profile' => $profile, 
                'usertype' => $type, 
                'account' => $account,
                'token' => $this->session->userdata('token')
            )); 
        ?>       
        <div class="container" style="margin-top:20px">
            <div class="innerAll">
                <div class="row">
                    <div class="col-lg-12 col-md-8">
            
                    <div class="widget widget-body-white overflow-hidden">
                    <div class="widget-head innerAll half">
                        <h4 class="margin-none">
                        <i class="fa fa-fw icon-document-bar"></i> Acertos Por Disciplina</h4>
                    </div>
                        <?php if( count($history) > 0){ ?>
                        <canvas id="myChart" width="1000" height="400"></canvas>
                         <?php }else{ 
                            echo message('info', 'Você precisa responder alguns simulados para analisar seu desempenho.', 'margin:100px');
                         } ?>
                        <br/>
                        <div class="fb-like" data-href="http://simuleepasse.com.br/simulado/public_performance/<?php echo encrypt($this->session->userdata('token'), tokenTIMELINE); ?>" data-action="like" data-share="true"></div>
                    </div>


                  
                    </div>
                </div>
            </div>                   
        </div>
        </div>
    </div>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core_preload/preload.pace.init.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo LIBRARY; ?>charts/Chart.js"></script>
<script>
$(document).ready(function(){
    var data = {
    labels: [<?php foreach($history as $result):  $disciplina = $result['disciplina']; if($result['ponto'] > 0){echo "'$disciplina',";} endforeach; ?>],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(151,187,205,0.5)",
            strokeColor: "rgba(151,187,205,0.8)",
            highlightFill: "rgba(151,187,205,0.75)",
            highlightStroke: "rgba(151,187,205,1)",
            data: [<?php foreach($history as $result):  $ponto = $result['ponto']; if($ponto > 0){echo "'$ponto',";} endforeach; ?>]
        }
    ]
    };
    var ctx = document.getElementById("myChart").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(data, {
    });
})
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=569282369859117&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
 
</body>
</html>