<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
   

<script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>


</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
        <?php 
        userProfileHeader(
            $usuario = array(
                'name' => $name . ' ' . $surname, 
                'profile' => $profile, 
                'usertype' => $type, 
                'account' => $account,
                'token' => $this->session->userdata('token')
            )); 
        ?>


            
        
        <div class="container">
            <div class="innerAll">
                <div class="row">
                    <div class="col-lg-9 col-md-8">
                        <?php userCover(
                                $arrParam = array(
                                    'cover' => $cover,
                                    'following' => ext($following_count[0], 'total'),
                                    'followers' => ext($followers_count[0], 'total'),
                                    'userid' => $userid
                                    )
                                 );
                            ?>

                        
            <div id="simulado">
                <div id="iniciar-label"><h3>Iniciar Novo Simulado</h3></div>
            <div class="innerLR"  style="margin-left:0px">

                <div class="wizard">
                    <div class="widget widget-tabs widget-tabs-responsive">
                        <!-- Widget heading -->
                        <div class="widget-head">
                            <ul>
                                <li class="active"><a href="#tab1-1" class="glyphicons home" data-toggle="tab"><i></i>Por Prova</a></li>
                                <li><a href="#tab2-1" class="glyphicons search" data-toggle="tab"><i></i>Filtro</a></li>
                                <li><a href="#tab3-1" class="glyphicons film" data-toggle="tab"><i></i>Filtro - Apenas Questões com Videos</a></li>

                                <!--<li><a href="#tab2-2" class="glyphicons gift" data-toggle="tab"><i></i>Filtro Inteligênte</a></li>-->
                            </ul>
                        </div>
                        <!-- // Widget heading END -->
                        <div class="widget-body">
                            <div class="tab-content">
                                <!-- Step 1 -->
                                <div class="tab-pane active" id="tab1-1">
                                <form method="post" action="<?php echo BASEURL; ?>simulado/create">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <strong>Por Prova</strong>
                                            <p class="muted">Escolha a prova desejada e comece agora mesmo a responder.</p>
                                        </div>
                                        <div class="col-md-9">
                                            <label for="inputTitle">Provas</label>
                                            <select name="prova" id="prova" class="col-md-6 form-control">
                                                <?php echo comboJson($provas, 'prova', 'prova_nome'); ?>
                                            </select>
                                        </div>

                                        <ul class="pagination margin-bottom-none">
                                            <li class="primary previous first"><button class="btn btn-primary">Iniciar Simulado</button></li>
                                        </ul>
                                    </div>
                                </form>
                                </div>
                                <!-- // Step 1 END -->
                                <!-- Step 2 -->
                                <div class="tab-pane" id="tab2-1">
                                <?php if($account == null){ ?>
                                    <center>
                                        <h3> Assine agora mesmo e usufrua desta funcionalidade</h3>
                                        <button class="btn btn-success" onclick="redirect('usuario/assinar')"><i class="fa fa-credit-card"></i> Assinar </button>
                                    </center>
                                <?php } else { ?>
                                <form id="simulado_filter">
                                    <div class="row">
                                    <div class="col-md-4">
                                        <label for="inputTitle">Banca:</label>
                                        <select name="banca" id="banca" class="col-md-6 form-control">
                                            <?php echo comboJson($bancas, 'banca', 'banca'); ?>
                                        </select>
                                        <div class="separator"></div>
                                    </div>
                                
                                    <div class="col-md-4">
                                        <label for="textDescription">Orgão:</label>
                                        <select name="orgao" id="orgao" class="col-md-6 form-control">
                                           <?php echo comboJson($orgaos, 'orgao', 'orgao'); ?>
                                        </select>
                                    </div>

                                     <div class="col-md-4">
                                        <label for="textDescription">Cargo:</label>
                                        <select name="cargo" id="cargo" class="col-md-6 form-control">
                                           <?php echo comboJson($cargos, 'cargo', 'cargo'); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                   
                                     <div class="col-md-6">
                                        <label for="textDescription">Disciplina:</label>
                                        <select name="disciplina" id="disciplina" class="col-md-6 form-control">
                                           <?php echo comboJson($disciplinas, 'disciplina', 'disciplina'); ?>
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="textDescription">Assunto:</label>
                                        <select name="assunto" id="assunto" class="col-md-6 form-control">
                                           <?php echo comboJson($assuntos, 'assunto1', 'assunto1'); ?>
                                        </select>
                                    </div>  
                                    
                                </div>

                                 

                                 <div class="row">

                                    <div class="col-md-6">
                                        <label for="textDescription">Nível:</label>
                                        <select name="nivel" id="nivel" class="col-md-6 form-control">
                                            <option></option>
                                            <option>Médio</option>
                                            <option>Superior</option>
                                        </select>
                                    </div>  

                                    <div class="col-md-6">
                                       <label for="textDescription">Ano:</label>
                                        <select name="ano" id="ano" class="col-md-6 form-control">
                                            <?php echo comboJson($anos, 'ano', 'ano'); ?>
                                        </select>
                                    </div>
                                </div>

                                 <ul class="pagination margin-bottom-none">
                                    <input type="hidden" id="token" value="<?php echo $this->session->userdata('token'); ?>">
                                    <li class="primary previous first"><button class="btn btn-primary">Buscar Questões</button></li>
                                </ul>
                                </form>
                                <?php } ?>
                                </div>
                                <!-- // Step 2 END -->
                                


                                <!-- Step 3 -->
                                <div class="tab-pane" id="tab3-1">
                                    <?php if($account == null){ ?>
                                    <center>
                                        <h3> Assine agora mesmo e usufrua desta funcionalidade</h3>
                                        <button class="btn btn-success"  onclick="redirect('usuario/assinar')"><i class="fa fa-credit-card"></i> Assinar </button>
                                    </center>
                                <?php } else { ?>
                                <form id="simulado_filter_video">
                                    <div class="row">
                                    <div class="col-md-4">
                                        <label for="inputTitle">Banca:</label>
                                        <select name="banca" id="banca_vimeo" class="col-md-6 form-control">
                                            <?php echo comboJson($bancas_vimeo, 'banca', 'banca'); ?>
                                        </select>
                                        <div class="separator"></div>
                                    </div>
                                
                                    <div class="col-md-4">
                                        <label for="textDescription">Orgão:</label>
                                        <select name="orgao" id="orgao_vimeo" class="col-md-6 form-control">
                                           <?php echo comboJson($orgaos_vimeo, 'orgao', 'orgao'); ?>
                                        </select>
                                    </div>

                                     <div class="col-md-4">
                                        <label for="textDescription">Cargo:</label>
                                        <select name="cargo" id="cargo_vimeo" class="col-md-6 form-control">
                                           <?php echo comboJson($cargos_vimeo, 'cargo', 'cargo'); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                   
                                     <div class="col-md-6">
                                        <label for="textDescription">Disciplina:</label>
                                        <select name="disciplina" id="disciplina_vimeo" class="col-md-6 form-control">
                                           <?php echo comboJson($disciplinas_vimeo, 'disciplina', 'disciplina'); ?>
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="textDescription">Assunto:</label>
                                        <select name="assunto" id="assunto_vimeo" class="col-md-6 form-control">
                                           <?php echo comboJson($assuntos_vimeo, 'assunto1', 'assunto1'); ?>
                                        </select>
                                    </div>  
                                    
                                </div>

                                 

                                 <div class="row">

                                    <div class="col-md-6">
                                        <label for="textDescription">Nível:</label>
                                        <select name="nivel" id="nivel_vimeo" class="col-md-6 form-control">
                                            <option></option>
                                            <option>Médio</option>
                                            <option>Superior</option>
                                        </select>
                                    </div>  

                                    <div class="col-md-6">
                                       <label for="textDescription">Ano:</label>
                                        <select name="ano" id="ano_vimeo" class="col-md-6 form-control">
                                            <?php echo comboJson($anos_vimeo, 'ano', 'ano'); ?>
                                        </select>
                                    </div>
                                </div>

                                 <ul class="pagination margin-bottom-none">
                                    <input type="hidden" id="token" value="<?php echo $this->session->userdata('token'); ?>">
                                    <li class="primary previous first"><button class="btn btn-primary">Buscar Questões</button></li>
                                </ul>
                                </form>
                                <?php } ?>
                                </div>


                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            </div>

                    
                       <div id="filter_result">


                       </div>

                        
                    </div>                    
                    <div class="col-md-4 col-lg-3" id="app-userinfo">
                        <?php userInfo($array = array('name' => $name . ' ' . $surname, 'account' => $account, 'profile' => $profile)); ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script> 
<script src="<?php echo JS; ?>simulado.js"></script> 
<script src="<?php echo JS; ?>appuser.js"></script> 


    <script type="text/javascript">
    $(document).ready(function(){
        simulado_filter();
        simulado_filter_video();
    })
    </script>
</body>
</html>