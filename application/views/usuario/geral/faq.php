<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />

<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo JS; ?>appuser.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
             <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>
            <div class="container">
                <div class="innerAll">
                    <div class="row">

             <div class="container" style="margin-top:-50px">
                <div class="innerLR">

<h3>AJUDA</h3>
<div class="separator-h"></div>
<div class="widget widget-tabs widget-tabs-double-2">
    <div class="widget-head">
        <ul>
            <li class="active"><a class="glyphicons list" href="#tabAll" data-toggle="tab"><i></i><span>Principais Informações</span></a></li>
            <li><a class="glyphicons credit_card" href="#tabAccount" data-toggle="tab"><i></i><span>Assinaturas</span></a></li>
            <li><a class="glyphicons cogwheel" href="#tabSupport" data-toggle="tab"><i></i><span>Suporte</span></a></li>        
        </ul>
    </div>
    <!-- // Widget heading END -->
    
    <div class="widget-body">
        <div class="tab-content">
        
            <!-- Tab content -->
            <div id="tabAll" class="tab-pane active">
                <div class="panel-group accordion accordion-2" id="accordion">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a class="accordion-toggle glyphicons font" data-toggle="collapse" data-parent="#accordion" href="#collapse-1"><i></i>O que é o Simule & Passe?</a></h4>
                        </div>
                        <div id="collapse-1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <i class="fa fa-5x pull-left text-primary fa-question-circle"></i>
                                O Simule & Passe é uma rede social voltada ao estudo direcionado a concursos públicos, baseados em provas, simulados já realizados por bancas organizadoras,
                                com questões comentadas por nossos professores.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a class="accordion-toggle glyphicons circle_question_mark" data-toggle="collapse" data-parent="#accordion" href="#collapse-2"><i></i>Por que usar?</a></h4>
                        </div>
                        <div id="collapse-2" class="panel-collapse collapse">
                            <div class="panel-body">
                               Por ser um método muito utilizado até mesmo dentro de empresas de concursos acreditamos que a resolução de questões, com questões comentadas ajudam o concurseiro a estar mais perto
                               da realidade de um concurso público, 100% baseado em questões de concursos anteriores esse é um método eficaz que vai lhe garantir a aprovação!
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a class="accordion-toggle glyphicons roundabout" data-toggle="collapse" data-parent="#accordion" href="#collapse-3"><i></i>Atualizações</a></h4>
                        </div>
                        <div id="collapse-3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Estamos trabalhando para toda semana entregarmos novas funcionalidades no sistema, mais questões, mais vídeos.</p>
                                <p>Fiquem atentos, muitas funcionalidades para usuários pró estão por vir.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // Tab content END -->
            
            <!-- Tab content -->
            <div id="tabAccount" class="tab-pane">        
                <div class="panel-group accordion accordion-2" id="tabAccountAccordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a class="accordion-toggle glyphicons right_arrow" data-toggle="collapse" data-parent="#tabAccountAccordion" href="#collapse-1-1"><i></i>Quais os tipos de assinatura?</a></h4>
                        </div>
                        <div id="collapse-1-1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                Atualmente o Simule & Passe conta com 4 tipos de contas, sendo elas <strong>FREE, 1 Mês, 3 Meses e 6 Meses</strong> para saber mais sobre
                            valores clique <a href="<?php echo BASEURL; ?>usuario/assinar">aqui</a> ou sinta-se a vontade para nos enviar um email, administrador@simuleepasse.com.br.
                            </div>
                        </div>                        
                    </div>
                   
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                            <a class="accordion-toggle glyphicons right_arrow" data-toggle="collapse" data-parent="#tabAccountAccordion" href="#collapse-2-1"><i></i>
                                Diferença entre os planos
                            </a>
                            </h4>
                        </div>
                        <div id="collapse-2-1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Atualmente existe apenas a diferenciação entre usuários pró e free, onde o free não possui direito a participar do fórum, assistir videos com a explicação do professor, utilizar os filtros avançados para a escolha das questões e muito mais que ainda está por vir.</p>
                                <p>Utilizamos o sistema de 1, 3 e 6 meses para que você sinta-se a vontade para escolher ao que melhor se adapta ao seu orçamento.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // Tab content END -->
            
            <!-- Tab content -->
            <div id="tabSupport" class="tab-pane widget-body-regular">
                <h5>Suporte</h5>
                <p>Nosso suporte está sempre disponível para lhe auxiliar no que for necessário, não fique em dúvida, entre em contato e lhe orientamos da melhor forma possível.</p>
            </div>
            
        </div>
    </div>
</div>
<!-- // Widget END -->



    <h3>Contato</h3>
    <div class="separator-h"></div>

    <div class="row">       
        </div>
        <div class="col-md-5">
            <div class="well">
                <address class="margin-none">
                    <h2>Simule & Passe</h2>
                    <strong><a href="#">Informações</a></strong><br> 
                    <abbr title="Work email">e-mail:</abbr> <a href="mailto:#">administrador@simuleepasse.com.br</a><br /> 
                    <abbr title="Work Phone">Telefone:</abbr> (051) 3072-0028<br/>
                    <div class="separator bottom"></div>
                    <p class="margin-none"><strong>Entre em contato</strong><br/>Informe-se sobre sua dúvida e turbine ainda hoje os seus estudos.</p>
                </address>
            </div>

        </div>
    </div>

</div>  
        </div>   
        </div>
        
        <div class="clearfix"></div>
        <!-- // Sidebar menu & content wrapper END -->
                
                        

                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
</body>
</html>
