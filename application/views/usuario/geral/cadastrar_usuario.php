<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/less-js/less.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
            <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>

            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        <div class="col-lg-12 col-md-8" id="cadastro-usuarios">
                            

                        <h2>Cadastrar Novo Usuário</h2>


                        <div class="wizard">
                        <div class="widget widget-tabs widget-tabs-responsive">
    
                        <div class="widget-head">
                            <ul>
                                <li class="active"><a href="#tab1-1" class="glyphicons circle_plus" data-toggle="tab"><i></i>Novo Cadastro</a></li>
                                <li><a href="#tab2-1" class="glyphicons circle_ok" data-toggle="tab"><i></i>Usuários cadastrador por mim</a></li>
                            </ul>
                        </div>

                        </div>
                        </div>

                          <div class="widget-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1-1">
                                    
                            <form autocomplete="off" id="validateSubmitForm" method="post" action="<?php echo BASEURL; ?>usuario/cadastrar_usuario_" >
                            <div class="col-sm-12">
                                <div class="widget">
                                    <div class="widget-head border-bottom bg-gray">
                                        <h5 class="innerAll pull-left margin-none">Dados do Usuário</h5>
                                        <div class="pull-right">
                                            <a href="" class="text-muted">
                                                <i class="fa fa-pencil innerL"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="row">
                                            <div class="col-sm-3">Nome:</div>
                                            <div class="col-sm-9 text-LEFT">
                                                <input type="text" name="name" id="name" value="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">Sobrenome:</div>
                                            <div class="col-sm-9 text-LEFT">
                                                 <input type="text" name="surname" id="surname" value="" class="form-control">
                                            </div>
                                        </div>
                                      

                                         <div class="row">
                                            <div class="col-sm-3">Sexo: </div>
                                            <div class="col-sm-9 text-LEFT">
                                                <select name="sex" id="sex" class="form-control">
                                                    <option value="M">Masculino</option>
                                                    <option value="F">Feminino</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">Email:</div>
                                            <div class="col-sm-9 text-LEFT">
                                                 <input type="text" name="mail" id="mail" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">Tipo de conta:</div>
                                            <div class="col-sm-9 text-LEFT">
                                                 <select name="account" id="account" class="form-control">
                                                    <option value="N">Conta FREE</option>
                                                    <option value="S">Conta PRÓ</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">Estado:</div>
                                            <div class="col-sm-9 text-LEFT">
                                                 <select name="state" id="state" class="form-control">
                                                    <option value="estado"></option> 
                                                    <option value="ac">Acre</option> 
                                                    <option value="al">Alagoas</option> 
                                                    <option value="am">Amazonas</option> 
                                                    <option value="ap">Amapá</option> 
                                                    <option value="ba">Bahia</option> 
                                                    <option value="ce">Ceará</option> 
                                                    <option value="df">Distrito Federal</option> 
                                                    <option value="es">Espírito Santo</option> 
                                                    <option value="go">Goiás</option> 
                                                    <option value="ma">Maranhão</option> 
                                                    <option value="mt">Mato Grosso</option> 
                                                    <option value="ms">Mato Grosso do Sul</option> 
                                                    <option value="mg">Minas Gerais</option> 
                                                    <option value="pa">Pará</option> 
                                                    <option value="pb">Paraíba</option> 
                                                    <option value="pr">Paraná</option> 
                                                    <option value="pe">Pernambuco</option> 
                                                    <option value="pi">Piauí</option> 
                                                    <option value="rj">Rio de Janeiro</option> 
                                                    <option value="rn">Rio Grande do Norte</option> 
                                                    <option value="ro">Rondônia</option> 
                                                    <option value="rs">Rio Grande do Sul</option> 
                                                    <option value="rr">Roraima</option> 
                                                    <option value="sc">Santa Catarina</option> 
                                                    <option value="se">Sergipe</option> 
                                                    <option value="sp">São Paulo</option> 
                                                    <option value="to">Tocantins</option> 
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-3">Cidade:</div>
                                            <div class="col-sm-9 text-LEFT">
                                                 <input type="text" name="city" id="city" class="form-control">
                                            </div>
                                        </div>

                                    </div>
                                </div>


                            </div>

                            <div class="col-sm-3">
                                <input type="submit" value="Cadastrar" class="btn btn-primary" style="width:100%">
                            </div>
                        </form>
                        
                                </div>

                                <div class="tab-pane" id="tab2-1">
                                    <?php if(count($usuarios) > 0){ ?>



                                     <div class="widget-body">
                        
                        
                                        <div class="separator bottom">
                                            Total de usuários: <b><?php echo count($usuarios); ?></b>
                                           
                                            <div class="clearfix"></div>
                                        </div>
                                        
                                        <table class="table table-condensed table-striped table-primary table-vertical-center checkboxs">
                                            <thead>
                                                <tr>
                                                    <th class="center">ID(#)</th>
                                                    <th class="center">Nome</th>
                                                    <th class="center">Sobrenome</th>
                                                    <th class="center">Data de cadastro</th>
                                                    <th class="center">Ativo</th>
                                                    <th class="center">Plano</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                            foreach($usuarios as $data): 
                                            ?>
                                                <tr class="selectable">
                                                    <td class="center"><?php echo $data['usuarioid']; ?></td>
                                                    <td class="center"><?php echo $data['nome']; ?></td>
                                                    <td class="center"><?php echo $data['sobrenome']; ?></td>
                                                    <td class="center"><?php echo $data['data_registro']; ?></td>
                                                    <td class="center"><?php echo ($data['ativo'] == 'S') ? 'Sim' : 'Não'; ?></td>
                                                    <td class="center"><?php echo ($data['plano'] == null) ? 'Free' : 'Pró'; ?></td>
                                                </tr>
                                            <?php 
                                            endforeach; 
                                            ?>
                                            </tbody>
                                        </table>
                                                    
                                    </div>
                                    <?php 
                                    } else { 

                                        message('info', "Você ainda não cadastrou usuários no sistema!!! <strong>:(</strong>");
                                    } ?>
                                </div>
                            </div>
                        </div>

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/forms_validator/jquery-validation/dist/jquery.validate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/forms_validator/form-validator.init.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script> 
<script src="<?php echo JS; ?>appuser.js"></script>
  
</body>
</html>