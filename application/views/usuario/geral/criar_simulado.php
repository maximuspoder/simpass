<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
<style>
.ms-container{
    margin:10px 0 10px 10px;
}
</style>

</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
            <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>

            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        <div class="col-lg-12 col-md-8" id="cadastro-usuarios">
                            

                        <h2>Criar Simulado</h2>


                        <div class="wizard">
                        <div class="widget widget-tabs widget-tabs-responsive">
    
                        <div class="widget-head">
                            <ul>
                                <li class="active"><a href="#tab1-1" class="glyphicons circle_plus" data-toggle="tab"><i></i>Novo Simulado</a></li>
                                <li><a href="#tab2-1" class="glyphicons circle_ok" data-toggle="tab"><i></i>Meus Simulados</a></li>
                            </ul>
                        </div>

                        </div>
                        </div>

                          <div class="widget-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1-1">






                            <div class="wizard">
                            <div class="widget widget-tabs widget-tabs-double widget-tabs-vertical row row-merge">
                            <!-- Navigation -->
                            <div class="widget-head col-md-3">
                            <ul>
                                <li class="primary">
                                    <a href="#tab1-4" class="glyphicons file" data-toggle="tab">
                                        <i></i>
                                        <span>Selecione as Disciplinas</span>
                                    </a>
                                </li>
                            </ul>
                            </div>
        
        
                            <div class="widget-body col-md-9">
                            <div class="tab-content">
            
                            <!-- STEP 1 -->
                            <div class="tab-pane active" id="tab1-4">
                                <div class="row">
                                        <div class="col-lg-4 col-md-8">
                                            <label> Filtrar Disciplina</label>
                                            <select name="disciplina" id="disciplina" class="form-control" required>
                                                <?php echo combo($disciplina, 'disciplina', 'disciplina'); ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 col-md-8" style="margin-top:21px;">
                                            <button class="btn btn-primary" onclick="get_teacher_disciplinas_vimeo('<?php echo $this->session->userdata("token"); ?>', '<?php echo $id; ?>')">Buscar</button>
                                            <button type="submit" class="btn btn-primary" onclick="set_simulado_criado('<?php echo $this->session->userdata('token'); ?>', <?php echo $id; ?>)">
                                                <i class="fa fa-check-circle"></i> Criar
                                            </button>
                                        </div> 


                                        <div class="col-lg-12 col-md-8" id="list"></div>
                                </div>
                            </div>
               
                            <!-- STEP 2 -->
                            <div class="tab-pane" id="tab2-4">
                                <div class="row">
                                    
                                </div>
                            </div>
                
                <!-- Step 3 -->
                            <div class="tab-pane" id="tab3-4">
                                <div class="filter-bar">
                                <input type="hidden" name="disciplinas" id="items">
                                    <div class="form-group col-md-3 padding-none">
                                        <label>De:</label>
                                        <div class="input-group">
                                            <input type="text" name="inputmask-date-1" id="inputmask-date-1" class="form-control span8" value="<?php echo date("d/m/YY");   ?>"/>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
        
                                    <div class="form-group col-md-3 padding-none">
                                        <label>Até:</label>
                                        <div class="input-group">
                                            <input type="text" name="inputmask-date-2" id="inputmask-date-2" class="form-control span8" placeholder="dd/mm/yyyy"/>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-2 padding-none">
                                        <div class="input-group">
                                            <button type="submit" class="btn btn-primary" onclick="set_simulado_criado('<?php echo $this->session->userdata('token'); ?>', <?php echo $id; ?>)">
                                                <i class="fa fa-check-circle"></i> Criar
                                            </button>
                                        </div>
                                    </div>
        
                                <div class="clearfix"></div>
                                </div>
                            </div>
            
                            <div id="simulado_link"></div>
        
                            </div>
                            </div> 
                        </div>
                        </div>




                        </div>

                        <div class="tab-pane" id="tab2-1">
                            <div class="widget-body">
                        
                        
                            <div class="separator bottom">
                                Total de simulados: <b><?php echo count($meus_simulados); ?></b>
                                <div class="clearfix"></div>
                            </div>
                            
                            <table class="table table-condensed table-striped table-primary table-vertical-center checkboxs">
                                <thead>
                                    <tr>
                                        <th class="center">ID(#)</th>
                                        <th class="center">Data</th>
                                        <th class="center">Link</th>
                                        <th class="center">Ver</th>
                                        <th class="center">Deletar</th>


                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if(count($meus_simulados) > 0){
                                foreach($meus_simulados as $data): 
                                ?>
                                    <tr class="selectable">
                                        <td class="center"><?php echo $data['idprova']; ?></td>
                                        <td class="center"><?php echo $data['data_inicio']; ?></td>
                                        <td class="center"><?php echo BASEURL; ?>simulado/free/<?php echo encrypt($this->session->userdata('token'), TOKENHASH); ?>/<?php echo $data['idprova']; ?></td>
                                        <td class="center"><a href="<?php echo BASEURL; ?>simulado/free/<?php echo encrypt($this->session->userdata('token'), TOKENHASH); ?>/<?php echo $data['idprova']; ?>" class="btn btn-default"> Ver</a></td>
                                        <td class="center"><a href="<?php echo BASEURL; ?>simulado/deletar_free/<?php echo $data['idprova']; ?>" class="btn btn-default">Deletar</a></td>
                                    </tr>
                                <?php 
                                endforeach; 
                                } else {
                                ?>
                                <tr class="selectable">
                                    <td class="center">Nenhum simulado criado até o momento</td>
                                    <td class="center">-</td>
                                    <td class="center">-</td>
                                </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                                        
                        </div>

                        </div>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script> 
<script src="<?php echo JS; ?>appuser.js"></script>

<!-- Mask-->
<script src="<?php echo frontTheme; ?>assets/plugins/forms_elements_inputmask/jquery.inputmask.bundle.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/forms_elements_inputmask/inputmask.init.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/forms_elements_bootstrap-datepicker/js/bootstrap-datepicker.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<!-- multiselect -->
<script src="<?php echo frontTheme; ?>assets/plugins/forms_elements_multiselect/js/jquery.multi-select.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo JS; ?>appuser.js"></script>
<script>
$(function()
{
    $('#multiselect-optgroup').multiSelect({ selectableOptgroup: true });
    $('#pre-selected-options').multiSelect();
    $('#multiselect-custom').multiSelect({
        selectableHeader: "<div class='custom-header'>Itens a Selecionar</div>",
        selectionHeader: "<div class='custom-header'>Itens Selecionados</div>",
        selectableFooter: "<div class='custom-header custom-footer'>Simule & Passe</div>",
        selectionFooter: "<div class='custom-header custom-footer'>Simule & Passe</div>"
    }); 
});
</script>
</body>
</html>