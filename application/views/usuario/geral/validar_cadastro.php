<!DOCTYPE html>
<html class="paceCounter paceSocial footer-sticky" lang="pt-br">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
<link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo JS; ?>appuser.js"></script>
	
</head>
<body class="menu-right-hidden">




	<div class="container">
                <div class="innerAll">
                    <br/>
                    <br/>
                   <h4 class="innerAll bg-white margin-none"><?php echo ext($usuario[0], 'nome'); ?><span> seja bem-vindo!</span></h4>
                   <div class="separator"></div>

                   <div class="widget">
                        <div class="widget-body padding-none">
                            <div class="jumbotron margin-none center bg-white">
                                <h1 class="separator bottom">Ativar Seu Cadastro</h1>
                                <p>Adicione uma senha a sua conta <strong>Simule & Passe</strong> e comece agora mesmo a usar o sistema.</p>
                                <p><?php echo message('info', 'Esta senha ficará associada ao email em que você recebeu este link.'); ?></p>

                                <form method="post" action="<?php echo BASEURL; ?>usuario/validar_cadastro_process">
										<input type="password" name="senha" class="form-control" placeholder="Informe sua senha aqui..."><br />
										<input type="hidden" name="token" value="<?php echo $token; ?>">
										<input type="submit" value="OK, Cadastrar Senha" class="btn btn-primary btn-lg">
								</form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>





	<!--

    <div id="pdfTarget" style="margin-top:90px;">
	<div class="innerAll shop-client-products cart invoice">
		<table class="table table-invoice">
			<tbody>
				<tr>
					<td style="width:58%;">
						<div class="media">
							<div class="media-body hidden-print">
								<div class="alert alert-primary margin-none">
									<strong>Bem-vindo <?php echo ext($usuario[0], 'nome'); ?>!</strong><br>
									Informe uma senha para acessar agora mesmo o sistema<br />
									<form method='post' action="<?php echo BASEURL; ?>usuario/validar_cadastro_process">
										<input type="text" name="senha" placeholder="senha"><br />
										<input type="hidden" name="token" value="<?php echo $token; ?>">
										<input type="submit" value="Cadastrar Senha" class="btn btn-primary">
									</form>
								</div>
								<div class="separator bottom"></div>
							</div>
							<button onclick="redirect('usuario/cadastrar_usuario')" class="btn btn-primary">Voltar</button>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	-->




</body>

	<script data-id="App.Config">
	var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#5dc94b',
        dangerColor = '#ff553e',
        successColor = '#80cf47',
        infoColor = '#6fa3cb',
        warningColor = '#f48f3e',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
		</script>
	
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script>	
</body>
</html>
