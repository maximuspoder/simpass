<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />

<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo JS; ?>appuser.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
             <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>

            


            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        
                        

<div class="table-pricing-3">
    <ul class="list-unstyled row">
        <li class="col-md-4">
            <div class="innerAll">
                <h3>MÊS</h3>
                <div class="body">
                    <div class="price">
                        <span class="figure">R$ 34,90</span>
                        <span class="term">por 1 mês</span>
                    </div>
                </div>
                <div class="features">
                    <ul>
                        <li>Suporte</li>
                        <li>Fórum</li>
                        <li>Filtro de pesquisa avançada</li>
                        <li><strong>Videos</strong> nas questões</li>
                        <li>Acompanhar seu Desempenho Geral</li>
                    </ul>
                </div>
                <div class="footer">
                    <a onclick="redirect('assinatura/mes')" class="btn btn-success">Comprar</a>
                </div>
            </div>
        </li>
        <li class="col-md-4 active">
            <div class="innerAll">
                <h3>PRÓ I</h3>
                <div class="body">
                    <div class="price">
                        <span class="figure">R$ 29,90</span>
                        <span class="term">3/meses</span>
                    </div>
                </div>
                <div class="features">
                    <ul>
                        <li>Suporte</li>
                        <li>Fórum</li>
                        <li>Filtro de pesquisa avançada</li>
                        <li><strong>Videos</strong> nas questões</li>
                        <li>Acompanhar seu Desempenho Geral</li>
                    </ul>
                </div>
                <div class="footer">
                    <a onclick="redirect('assinatura/mensal_3')" class="btn btn-success">Assinar</a>
                </div>
            </div>
        </li>
        <li class="col-md-4">
            <div class="innerAll">
                <h3>PRÓ II</h3>
                <div class="body">
                    <div class="price">
                        <span class="figure">R$ 19,90</span>
                        <span class="term">6/meses</span>
                    </div>
                </div>
                <div class="features">
                    <ul>
                        <li>Suporte</li>
                        <li>Fórum</li>
                        <li>Filtro de pesquisa avançada</li>
                        <li><strong>Videos</strong> nas questões</li>
                        <li>Acompanhar seu Desempenho Geral</li>
                    </ul>
                </div>
                <div class="footer">
                    <a onclick="redirect('assinatura/mensal_6')" class="btn btn-success">Assinar</a>
                </div>
            </div>
        </li>
        <div class="clearfix"></div>
    </ul>
</div>




                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#5dc94b',
        dangerColor = '#ff553e',
        successColor = '#80cf47',
        infoColor = '#6fa3cb',
        warningColor = '#f48f3e',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script> 
</body>
</html>
