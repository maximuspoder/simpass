<!DOCTYPE html>
<html class="paceCounter paceSocial footer-sticky" lang="pt-br">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
<link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo JS; ?>appuser.js"></script>
	
</head>
<body class="menu-right-hidden">

	<div class="container">

		 <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>


    <div id="pdfTarget" style="margin-top:90px;">
	<div class="innerAll shop-client-products cart invoice">
		<table class="table table-invoice">
			<tbody>
				<tr>
					<td style="width:58%;">
						<div class="media">
							<div class="media-body hidden-print">
								<div class="alert alert-primary margin-none">
									<strong>Hey!!!</strong><br>
									Um novo usuário foi cadastrado no sistema, o mesmo receberá dentro de instantes em seu email um link
									para ativar a sua conta.
								</div>
								<div class="separator bottom"></div>
							</div>
							<button onclick="redirect('usuario/cadastrar_usuario')" class="btn btn-primary">Voltar</button>
						</div>
					</td>
				</tr>
			</tbody>
		</table>

		
	
			
		</div>
		<!-- // Row END -->
		
	</div>
</div>

</body>

	<script data-id="App.Config">
	var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#5dc94b',
        dangerColor = '#ff553e',
        successColor = '#80cf47',
        infoColor = '#6fa3cb',
        warningColor = '#f48f3e',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
		</script>
	
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script>	
</body>
</html>
