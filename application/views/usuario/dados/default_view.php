<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
             <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>
            <!-- <div class="layout-app">  -->
            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        <div class="col-lg-9 col-md-8">
                            
                            <?php profileCover(ext($usuario_dados[0], 'foto_capa'),
                                $arrParams = array(
                                    'followers' => ext($followers[0], 'total'),
                                    'name' => ext($usuario_dados[0], 'nome'),
                                    'follow' => ext($follow[0], 'total'),
                                    'tokenME' => $token,
                                    'tokenUSER'=> ext($usuario_dados[0], 'token'),
                                    'userID' => ext($usuario_dados[0], 'usuarioid')
                                    )
                                ); 
                            ?>  



                            <h2>Sobre</h2>
                            <div class="row">
                            <div class="col-sm-6">
                                <div class="innerAll bg-white">
                                    
                                    <div class="media innerB " style="min-height:116px;">
                                        <a href="" class="pull-left">
                                            <img src="<?php echo ext($usuario_dados[0], 'foto_perfil'); ?>" alt="" width="75">
                                        </a>
                                        <div class="media-body" >
                                            <p><?php echo ext($usuario_dados[0], 'sobre'); ?></p>
                                        </div>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="widget">
                                    <div class="widget-head border-bottom bg-gray">
                                        <h5 class="innerAll pull-left margin-none">Dados</h5>
                                        <div class="pull-right">
                                            <a href="<?php echo ext($usuario_dados[0], 'facebook'); ?>" target="_blank"   class="text-muted">
                                                <i class="fa fa-facebook innerL"></i>
                                            </a>
                                            <a href="<?php echo ext($usuario_dados[0], 'twitter'); ?>" target="_blank" class="text-muted">
                                                <i class="fa fa-twitter innerL"></i>
                                            </a>
                                            <a href="<?php echo ext($usuario_dados[0], 'youtube'); ?>" target="_blank" class="text-muted">
                                                <i class="fa fa-youtube innerL"></i>
                                            </a>
                                        </div>
                                    </div>


                                    <div class="widget-body">
                                        <div class="row">
                                            <div class="col-sm-6">Nome:</div>
                                            <div class="col-sm-6 text-LEFT">
                                                <span class="label label-default"><?php echo ext($usuario_dados[0], 'nome'); ?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">Sobrenome:</div>
                                            <div class="col-sm-6 text-LEFT">
                                                <span class="label label-default"><?php echo ext($usuario_dados[0], 'sobrenome'); ?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">Data de nascimento:</div>
                                            <div class="col-sm-6 text-LEFT">
                                                <span class="label label-default"><?php echo (ext($usuario_dados[0], 'aniversario') == '//') ? '??/??/????' : ext($usuario_dados[0], 'aniversario'); ?></span>
                                            </div>
                                        </div>

                                         <div class="row">
                                            <div class="col-sm-6">Sexo:</div>
                                            <div class="col-sm-6 text-LEFT">
                                                <span class="label label-default"><?php echo (ext($usuario_dados[0], 'sexo') == 'F') ? 'Feminino' : 'Masculino'; ?></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            
                            </div>
                        </div>

                        </div>

                        
                        <div class="col-md-4 col-lg-3" id="app-userinfo">
                          <?php 
                                profileInfo($array = array(
                                    'name' => ext($usuario_dados[0], 'nome') .' '. ext($usuario_dados[0], 'sobrenome'), 
                                    'level' =>ext($usuario_dados[0], 'nivel'),
                                    'profile' => ext($usuario_dados[0], 'foto_perfil'),
                                    'cargo' =>  ext($usuario_dados[0], 'cargo')
                                    )
                                ); 
                            ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>  
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo JS; ?>appuser.js"></script>
</body>
</html>
