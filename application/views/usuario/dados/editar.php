<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
     <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
            <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>
            <!-- <div class="layout-app">  -->
            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        <div class="col-lg-9 col-md-8">
                            
                            <?php userCover(
                                $arrParam = array(
                                    'cover' => $cover,
                                    'following' => ext($following_count[0], 'total'),
                                    'followers' => ext($followers_count[0], 'total'),
                                    'userid' => $userid
                                    )
                                 );
                            ?>


                            <h2>Editar Meus Dados</h2>
                            <div class="row">
                            <form method="post" action="<?php echo BASEURL; ?>usuario/editar_meus_dados_process" role="form">
                            <div class="col-sm-6">
                                <div class="widget">
                                    <div class="widget-head border-bottom bg-gray">
                                        <h5 class="innerAll pull-left margin-none">Meus Dados</h5>
                                        <div class="pull-right">
                                            <a href="" class="text-muted">
                                                <i class="fa fa-pencil innerL"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="row">
                                            <div class="col-sm-6">Nome:</div>
                                            <div class="col-sm-6 text-LEFT">
                                                <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">Sobrenome:</div>
                                            <div class="col-sm-6 text-LEFT">
                                                 <input type="text" name="surname" id="surname" value="<?php echo $surname; ?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">Data de nascimento:</div>
                                            <div class="col-sm-6 text-LEFT">
                                                 <input type="text" name="nascimento" value="<?php echo $birth; ?>" id="inputmask-date" class="form-control" placeholder="__/__/____">
                                            </div>
                                        </div>

                                         <div class="row">
                                            <div class="col-sm-6">Sexo: </div>
                                            <div class="col-sm-6 text-LEFT">

                                                <select name="sex" id="sex" class="form-control">
                                                    <option></option>
                                                    <option value="F" <?php echo ($sex == 'F')? 'selected="selected"' : ''; ?>>Feminino</option>
                                                    <option value="M" <?php echo ($sex == 'M')? 'selected="selected"' : ''; ?>>Masculino</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">Cadastro:</div>
                                            <div class="col-sm-6 text-LEFT">
                                                 <input type="text" value="<?php echo $date; ?>" class="form-control" disabled="disabled">
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-6">Email Principal:</div>
                                            <div class="col-sm-6 text-LEFT">
                                                 <input type="text" value="<?php echo $email; ?>" name="email" id="email" class="form-control">
                                            </div>
                                        </div>

                                         <div class="row">
                                            <div class="col-sm-6">Senha:</div>
                                            <div class="col-sm-6 text-LEFT">
                                                <input type="password" name="password" id="password" class="form-control" style="display:none">
                                                 <input type="password" name="senha" id="senha" class="form-control">
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-sm-6">Cargo:</div>
                                            <div class="col-sm-6 text-LEFT">
                                                <input type="password" name="password" id="password" class="form-control" style="display:none">
                                                 <input type="text" name="cargo" id="cargo" value="<?php echo $cargo; ?>" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>                                



                                <div class="widget">
                                <div class="innerAll bg-white" style="height:125px">
                                    <div class="media innerB ">
                                        <a href="" class="pull-left"><img src="<?php echo $profile; ?>" alt="" width="75"></a>
                                        <div class="media-body">
                                            <textarea  name="about" id="about" class="form-control" rows="5"><?php echo $about; ?></textarea>
                                        </div>
                                    </div>                                   
                                </div>
                                </div>
                                </div>

                                <div class="col-sm-6">
                                <div class="widget">
                                    <div class="widget-head border-bottom bg-gray">
                                        <h5 class="innerAll pull-left margin-none">Dados de Contato</h5>
                                    </div>
                                    <div class="widget-body padding-none">
                                       
                                        <div class="innerAll">
                                            <p class=" margin-none">
                                                <i class="fa fa-phone fa-fw text-muted"></i> Telefone
                                                <input type="text" name="phone" id="inputmask-phone" value="<?php echo $phone; ?>" class="form-control"> 
                                            </p>
                                        </div>

                                        <div class="border-top innerAll">
                                            <p class=" margin-none">
                                                <i class="fa fa-envelope fa-fw text-muted"></i> Email Secundário
                                                <input type="text" name="email2" id="email2" value="<?php echo $email2; ?>" class="form-control"> 
                                            </p>
                                        </div>


                                        <div class="border-top innerAll">
                                            <p class=" margin-none"> <i class="fa fa-facebook text-muted"></i> Facebook
                                                <input type="text" name="facebook" id="facebook" value="<?php echo $facebook; ?>" class="form-control" placeholder="Ex.: https://www.facebook.com/usuario"> 
                                            </p>
                                        </div>

                                        <div class="border-top innerAll">
                                            <p class=" margin-none"><i class="fa fa-twitter text-muted"></i> Twitter
                                                <input type="text" name="twitter" id="twitter" value="<?php echo $twitter; ?>" class="form-control" placeholder="Ex.: https://www.twitter.com/channel/usuario"> 
                                            </p>
                                        </div>

                                        <div class="border-top innerAll">
                                            <p class=" margin-none"><i class="fa fa-youtube text-muted"></i> Youtube
                                                <input type="text" name="youtube" id="youtube" value="<?php echo $youtube; ?>" class="form-control" placeholder="Ex.: https://www.youtube.com/channel/usuario"> 
                                            </p>
                                        </div>

                                        <div class="border-top innerAll">
                                            <p class=" margin-none"><i class="fa fa-link fa-fw text-muted"></i> Site
                                                <input type="text" name="site" id="site" value="<?php echo $site; ?>" class="form-control" placeholder="Ex.: https://www.meusite.com.br"> 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <input type="submit" value="Alterar" class="btn btn-primary" style="width:100%">
                            </div>
                        </form>
                        </div>

                        </div>

                        
                        <div class="col-md-4 col-lg-3" id="app-userinfo">
                            <?php userInfo($array = array('name' => $name . ' ' . $surname, 'account' => $account, 'profile' => $profile)); ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script> 
<script src="<?php echo frontTheme; ?>assets/plugins/forms_elements_inputmask/jquery.inputmask.bundle.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/forms_elements_inputmask/inputmask.init.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/forms_elements_jasny-fileupload/js/bootstrap-fileupload.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>

<script src="<?php echo JS; ?>appuser.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $("#upload").mouseover(function() { $("#view").css('visibility','visible'); });
        $("#upload").mouseout(function() { $("#view").css('visibility','hidden'); });
        $("#upload_profile").mouseover(function() { $("#upload_profile_update").css('visibility','visible'); });
        $("#upload_profile").mouseout(function() { $("#upload_profile_update").css('visibility','hidden'); });
    })
    </script>

    <script src="<?php echo JS; ?>jquery.form.js"></script>
    <script>
        $(document).ready(function(){
            if (navigator.userAgent.toLowerCase().indexOf('chrome') >= 0) {
                setTimeout(function () {
                    document.getElementById('password').value = 'off';
                }, 1);
            }
            var options1 = { 
                beforeSend: function() {$("#bar").width('0%');},
                uploadProgress: function(event, position, total, percentComplete) {$("#bar").width(percentComplete+'%');},
                success: function() {$("#bar").width('100%');},
                complete: function(response) {$("#retorno").html(response.responseText);},
                error: function(){$("#retorno").html("Erro ao realizar o upload :(");} 
            }; 
             var options2 = { 
                beforeSend: function() {$("#bar1").width('0%');},
                uploadProgress: function(event, position, total, percentComplete) {$("#bar1").width(percentComplete+'%');},
                success: function() {$("#bar1").width('100%');},
                complete: function(response) {$("#retorno1").html(response.responseText);},
                error: function(){$("#retorno1").html("Erro ao realizar o upload :(");} 
            }; 
            $("#myForm1").ajaxForm(options1);
            $("#myForm2").ajaxForm(options2);

        });
    </script>
</body>
</html>
<?php modal_cover_photo($this->session->userdata('token')) ?>
<?php modal_profile_photo($this->session->userdata('token')) ?>