<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
             <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>
            <!-- <div class="layout-app">  -->
            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        <div class="col-lg-9 col-md-8">
                        <?php userCover(
                                $arrParam = array(
                                    'cover' => $cover,
                                    'following' => ext($following_count[0], 'total'),
                                    'followers' => ext($followers_count[0], 'total'),
                                    'userid' => $userid
                                    )
                                 );
                            ?>


<?php if(count($following_info ) > 0){ ?>
<br /><br /><br />
<div class="row row-merge">
<?php foreach ($following_info as $data): ?>
        <div class="col-md-12 col-lg-6 bg-white border-bottom" id="<?php echo encrypt($data['token'], TOKENHASH); ?>">
        <div class="row">
            <div class="col-sm-7">
                <div class="media">
                    <a class="pull-left margin-none" href="#">
                        <img class="img-clean" src="<?php echo $data['foto_perfil']; ?>" alt="usuário">
                    </a>
                    <div class="media-body innerAll inner-2x padding-right-none padding-bottom-none">
                         <h4 class="media-heading"><a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $data['usuarioid']; ?>" class="text-inverse"><?php echo $data['nome'] . ' ' . $data['sobrenome']; ?></a></h4>
                         <p>
                    </div>
                </div>
            </div><br/>
            <div class="col-sm-2">
                <div class="innerAll">
                    <div class="btn-group-vertical btn-group-sm">
                        <a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $data['usuarioid']; ?>" class="btn btn-primary"><i class="fa fa-fw fa-link"></i>Perfil</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="innerAll text-right">
                    <div class="btn-group-vertical btn-group-sm">
                        <a href="javascript:void(0)" onclick="unset_follower('<?php echo encrypt($this->session->userdata("token"), TOKENHASH); ?>', '<?php echo encrypt($data['token'], TOKENHASH); ?>')" class="btn btn-primary"><i class="fa fa-fw fa-chain-broken"></i>Deixar</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php endforeach; ?>
</div>
<?php } else { 

    message('info', 'Não segue ninguém até o momento!', 'margin-top:20px;');
 } ?>


                        </div>

                        
                        <div class="col-md-4 col-lg-3" id="app-userinfo">
                         <?php userInfo($array = array('name' => $name . ' ' . $surname, 'account' => $account, 'profile' => $profile)); ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/common/forms/elements/jasny-fileupload/assets/js/bootstrap-fileupload.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
<script src="<?php echo JS; ?>appuser.js"></script>
</body>
</html>
