<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?&v=v0.0.1.2"></script>
    <script src="<?php echo JS; ?>appuser.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
        <?php 
            userProfileHeader(
                $usuario = array(
                                'name' => $name . ' ' . $surname, 
                                'profile' => $profile, 
                                'usertype' => $type, 
                                'account' => $account,
                                'token' => $this->session->userdata('token')
                            )
            ); 
        ?>
        <div class="container">
        <div class="innerAll">
        <div class="row">
        <div class="col-lg-9 col-md-8">
                            
        <!--            
        <div class="widget border-bottom" style="margin-top:3%">
        <div class="widget-body border-bottom">
            <div class="media">
                <div class="pull-left innerAll">
                    <img src="<?php echo ext($partner[0], 'foto_perfil'); ?>" alt="" class="img-circle">
                </div>
                <div class="media-body">
                    <h4><a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo ext($partner[0], 'usuarioid'); ?>"><?php echo ext($partner[0], 'nome') . ' ' . ext($partner[0], 'sobrenome'); ?></a>   <small>Parceiro sugerido*</small></h4>
                    <div class="clearfix"></div>
                    <p><?php echo ext($partner[0], 'sobre'); ?></p>
                    <div id="button_follow_0" style="margin-left:10px">
                    <a href="javascript:void(0)" onclick="set_follower(0, 0, '<?php echo generate_token_unique($token) . $token; ?>', '<?php echo ext($partner[0], 'token') . generate_token_unique('partner'); ?>')" class="btn btn-primary btn-sm">
                        <i class="fa fa-thumbs-up"></i> Seguir
                    </a>
                    </div>
                </div>
            </div>
        </div>
        </div>
        -->

        <?php 
        if(count($result_not_follow) > 0){ 
        ?>
        <div class="col-md-12 col-lg-12 bg-white border-bottom" style="margin-top:26px">
                <h3> Resultados para: "<?php echo $search; ?>" <small> Pessoas que você não segue ainda.</small> </h3>       
        </div>
        
        <?php
            $x = 1;
            
            foreach($result_not_follow as $data):
        ?>


        <div class="col-md-12 col-lg-6 bg-white border-bottom">
        <div class="row">

            <div class="col-sm-9">
                <div class="media" >
                    <a class="pull-left margin-none" href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $data['usuarioid']; ?>">
                        <img class="img-clean" src="<?php echo $data['foto_perfil']; ?>" alt="usuário do simule e passe">
                    </a>
                    <div class="media-body innerAll inner-2x padding-right-none padding-bottom-none"style="margin-right:3px;">
                         <h5 class="media-heading"><a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $data['usuarioid']; ?>" class="text-inverse"><?php echo $data['nome'] . ' ' . $data['sobrenome']; ?></a></h5>
                         <p>
                            <?php if($data['facebook'] != ''){?>
                            <i class="fa fa-fw fa-facebook text-muted"></i> <a href="<?php echo (!strstr('http', $data['facebook'])) ? 'http://'. $data['facebook'] : $data['facebook']; ?> " alt="<?php echo $data['facebook']; ?>" target="_blank"><?php echo substr($data['facebook'], 0, 28); ?>...</a>
                        </p> 
                            <?php }?>                             
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="innerAll text-right">
                    <div class="btn-group-vertical btn-group-sm">
                        <br/>
                        <div id="button_follow_<?php echo $x; ?>">
                        <a href="javascript:void(0)" onclick="set_follower(0, <?php echo $x; ?>, '<?php echo generate_token_unique($data['token']) . $token; ?>', '<?php echo $data['token'] . generate_token_unique($data['token']); ?>')" class="btn btn-primary"><i class="fa fa-thumbs-up"></i> Seguir</a>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div id="result"></div>

        <?php 
            $x++;
            endforeach;
        } else {
        ?>
        <div>
        <h3> Nenhum resultado encontrado para "<b><?php echo $search; ?></b>"! </h3>
        </div>
        <?php } ?>



        <?php 
        if(count($result_follow) > 0){ 
        ?>
          <div class="col-md-12 col-lg-12 bg-white border-bottom" style="margin-top:20px">
                <h3> Resultados para: "<?php echo $search; ?>" <small> Pessoas que você segue.</small> </h3>       
        </div>
        
        <?php
            $x = 1;
            
            foreach($result_follow as $data):
        ?>

        <div class="col-md-12 col-lg-6 bg-white border-bottom">
        <div class="row">

            <div class="col-sm-9">
                <div class="media" >
                    <a class="pull-left margin-none" href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $data['usuarioid']; ?>">
                        <img class="img-clean" src="<?php echo $data['foto_perfil']; ?>" alt="usuário do simule e passe">
                    </a>
                    <div class="media-body innerAll inner-2x padding-right-none padding-bottom-none"style="margin-right:3px;"   >
                         <h4 class="media-heading"><a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $data['usuarioid']; ?>" class="text-inverse"><?php echo $data['nome'] . ' ' . $data['sobrenome']; ?></a></h4>
                         <p>
                            <?php if($data['facebook'] != ''){?>
                            <i class="fa fa-fw fa-facebook text-muted"></i> <a href="<?php echo (!strstr('http', $data['facebook'])) ? 'http://'. $data['facebook'] : $data['facebook']; ?> " alt="<?php echo $data['facebook']; ?>" target="_blank"><?php echo substr($data['facebook'], 0, 28); ?>...</a>
                        </p> 
                            <?php }?>                             
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="innerAll text-right">
                    <div class="btn-group-vertical btn-group-sm">
                        <br />
                        <div id="button_follow_<?php echo $x; ?>">
                        <a href="javascript:void(0)"class="btn btn-default" disabled="disabled"><i class="fa fa-check"></i></a>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div id="result"></div>
         <?php 
            $x++;
            endforeach;
        } else {
        ?>        
        <?php } ?>


        </div><!-- col-lg-9-->

        <div class="col-md-4 col-lg-3" id="app-userinfo">
            <?php userInfo($array = array('name' => $name . ' ' . $surname, 'account' => $account, 'profile' => $profile)); ?>
        </div>
                        
    </div>
    </div>
    </div>
    </div>
    <div class="clearfix"></div>
    </div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo JS; ?>appuser.js"></script>
</body>
</html>