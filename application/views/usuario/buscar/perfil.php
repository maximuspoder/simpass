<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?&v=v0.0.1.2"></script>
    <script src="<?php echo JS; ?>appuser.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
             <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>

            <div class="container">
                <div class="innerAll">
                    <div class="row">


                        <div class="col-lg-9 col-md-8">
                        <?php profileCover(ext($user[0], 'foto_capa'),
                                $arrParams = array(
                                    'followers' => ext($followers[0], 'total'),
                                    'name' => ext($user[0], 'nome'),
                                    'follow' => ext($follow[0], 'total'),
                                    'tokenME' => $token,
                                    'tokenUSER'=> ext($user[0], 'token'),
                                    'userID' => ext($user[0], 'usuarioid')
                                    )
                                ); 
                        ?>  

                        
                            <?php if(ext($follow[0], 'total') > 0){ ?>
                                <ul class="timeline-activity list-unstyled" id="timeline">
                                    <?php foreach($cursor as $timeline): ?>
                                    <li>
                                    <span class="marker"></span>
                                    <div class="block block-inline" style="width:100%">
                                        <div class="caret"></div>
                                        <div class="box-generic">
                                            <div class="timeline-top-info">
                                            <a class="pull-left" href="javascript:void()">
                                            <img src="<?php echo ext($user[0], 'foto_perfil'); ?>" class="img-circle" width="35">
                                            </a>
                                            <div style="margin-left:45px; border:0px solid red">
                                            <a href="javascript:void()" target="_blank" class="text-inverse" id=""><?php echo ext($user[0], 'nome') . ' ' . ext($user[0], 'sobrenome'); ?></a></div>
                                            
                                            </div>


                                            <div class="media margin-none"  style=" border:0px solid red">
                                                <div class="row innerLR innerB">
                                                    <div class="col-sm-12 col-lg-9">
                                                        <div class="innerT">
                                                            <p><?php echo $timeline['post']; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>      
                                        </div>
                                        <div class="timeline-bottom innerT half">
                                            <span class="innerL"><i class="fa fa-calendar fa-fw"></i> 
                                                <?php echo date('d/m/Y', $timeline['date']->sec); ?>
                                            </span>
                                        </div>
                                    </div>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>


                            <?php } else { ?>
                                <div class="alert alert-info">
                                    <strong>Hey!</strong> Você ainda não segue <b><?php echo  ext($user[0], 'nome') .' '. ext($user[0], 'sobrenome'); ?></b>! Siga para poder receber suas publicações.
                                    <br />
                                </div>
                            <?php } ?>
                        
                        </div>

                        
                        <div class="col-md-4 col-lg-3" id="app-userinfo">
                            <?php 
                                profileInfo($array = array(
                                    'name' => ext($user[0], 'nome') .' '. ext($user[0], 'sobrenome'), 
                                    'level' =>ext($user[0], 'nivel'),
                                    'profile' => ext($user[0], 'foto_perfil'),
                                    'cargo' =>  ext($user[0], 'cargo')
                                    )
                                ); 
                            ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var App = {};
    var basePath = '',
        commonPath = '../assets/',
        rootPath = '../',
        DEV = false,
        componentsPath = '../assets/components/';
    var primaryColor = '#5dc94b',
        dangerColor = '#ff553e',
        successColor = '#80cf47',
        infoColor = '#6fa3cb',
        warningColor = '#f48f3e',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <script>
    $(document).ready(function(){

      //$('#conteudo').load('http://localhost:8080/updater/1')

    })

    
    </script>
    
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_gridalicious/jquery.gridalicious.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/media_gridalicious/gridalicious.init.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/components/core/core.init.js?v=v2.0.0-rc8"></script> 
<script src="<?php echo frontTheme; ?>assets/components/common/forms/elements/jasny-fileupload/assets/js/bootstrap-fileupload.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
<script src="<?php echo JS; ?>appuser.js"></script>
</body>
</html>