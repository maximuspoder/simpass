<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo frontTheme; ?>assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
    <script src="<?php echo frontTheme; ?>assets/plugins/core_browser/ie/ie.prototype.polyfill.js?&v=v0.0.1.2"></script>
    <script src="<?php echo JS; ?>appuser.js"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
             <?php 
                userProfileHeader(
                    $usuario = array(
                                    'name' => $name . ' ' . $surname, 
                                    'profile' => $profile, 
                                    'usertype' => $type, 
                                    'account' => $account,
                                    'token' => $this->session->userdata('token')
                                )
                ); 
            ?>
            <!-- <div class="layout-app">  -->
            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        <div class="col-lg-9 col-md-8">
                        <?php profileCover(ext($usuario_dados[0], 'foto_capa'),
                            $arrParams = array(
                                'followers' => ext($followers[0], 'total'),
                                'name' => ext($usuario_dados[0], 'nome'),
                                'follow' => ext($follow[0], 'total'),
                                'tokenME' => $token,
                                'tokenUSER'=> ext($usuario_dados[0], 'token'),
                                'userID' => ext($usuario_dados[0], 'usuarioid')
                                )
                            ); 
                        ?>  

                        <!--
                        <form action="<?php echo BASEURL; ?>/usuario/buscar" method="post" >
                        <div class="input-group hidden-xs pull-left">    
                            <span class="input-group-addon"><i class="icon-search"></i></span>
                            <input type="text" class="form-control" placeholder="Encontrar"/>
                        </div>
                        </form>
                        -->


<?php if(count($followers_info ) > 0){ ?>

<div class="row row-merge">
<?php foreach ($followers_info as $data): ?>
        <div class="col-md-12 col-lg-6 bg-white border-bottom">
        <div class="row">
            <div class="col-sm-9">
                <div class="media">
                    <a class="pull-left margin-none" href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $data['usuarioid']; ?>">
                        <img class="img-clean" src="<?php echo $data['foto_perfil']; ?>" alt="usuário">
                    </a>
                    <div class="media-body innerAll inner-2x padding-right-none padding-bottom-none">
                         <h4 class="media-heading"><a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $data['usuarioid']; ?>" class="text-inverse"><?php echo $data['nome'] . ' ' . $data['sobrenome']; ?></a></h4>
                         <p>
                    </div>
                </div>
            </div>
            <div class="col-sm-3"><br/>
                <div class="innerAll text-right">
                    <div class="btn-group-vertical btn-group-sm">
                        <a href="<?php echo BASEURL; ?>usuario/perfil/<?php echo $data['usuarioid']; ?>" class="btn btn-primary"><i class="fa fa-fw fa-link"></i>Perfil</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
</div>
<?php } else { 
    message('info', 'Sem seguidores até o momento!', 'margin-top:20px;');
 } ?>


                        </div>

                        
                        <div class="col-md-4 col-lg-3" id="app-userinfo">
                          <?php 
                                profileInfo($array = array(
                                    'name' => ext($usuario_dados[0], 'nome') .' '. ext($usuario_dados[0], 'sobrenome'), 
                                    'level' =>ext($usuario_dados[0], 'nivel'),
                                    'profile' => ext($usuario_dados[0], 'foto_perfil'),
                                    'cargo' =>  ext($usuario_dados[0], 'cargo')
                                    )
                                ); 
                            ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>  
<script src="<?php echo frontTheme; ?>assets/library/bootstrap/js/bootstrap.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_nicescroll/jquery.nicescroll.min.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8"></script>
<script src="<?php echo frontTheme; ?>assets/plugins/media_holder/holder.js?v=v2.0.0-rc8&sv=v0.0.1.2"></script>
<script src="<?php echo JS; ?>appuser.js"></script>
</body>
</html>
