<!DOCTYPE html>
<html class="footer-sticky" lang="pt-br">
<head>
    <title><?php echo TITLE ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?php echo frontTheme; ?>assets/css/admin/estilo1.css" />
    <link rel="stylesheet" href="<?php echo CSS; ?>app.css" />
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/jquery/jquery-migrate.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/modernizr/modernizr.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/less-js/less.min.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v1.0.1-rc2&sv=v0.0.1.1"></script>
</head>
<body class=" menu-right-hidden">
    <div class="container-fluid ">
        <div id="content">
            <?php userProfileHeader($usuario = array('nome' => $this->session->userdata('nome') )); ?>


            
            <!-- <div class="layout-app">  -->
            <div class="container">
                <div class="innerAll">
                    <div class="row">
                        <div class="col-lg-9 col-md-8">
                            
                            <?php userCover($this->session->userdata('nivel')); ?>


                          


                  





                          
                        </div>

                        
                        <div class="col-md-4 col-lg-3">
                            <?php userInfo(''); ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script data-id="App.Config">
    var primaryColor = '#25ad9f',
        dangerColor = '#b55151',
        successColor = '#609450',
        infoColor = '#4a8bc2',
        warningColor = '#ab7a4b',
        inverseColor = '#45484d';
    var themerPrimaryColor = primaryColor;
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo frontTheme; ?>assets/components/library/bootstrap/js/bootstrap.min.js?v=v1.0.1-rc2&sv=v0.0.1.1?data=<?php echo date('Y-m-d'); ?>"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v1.0.1-rc2&sv=v0.0.1.1?data=<?php echo date('Y-m-d'); ?>"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/breakpoints/breakpoints.js?v=v1.0.1-rc2&sv=v0.0.1.1?data=<?php echo date('Y-m-d'); ?>"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/menus/sidr/jquery.sidr.js?v=v1.0.1-rc2?data=<?php echo date('Y-m-d'); ?>"></script>
    <script src="<?php echo frontTheme; ?>assets/components/plugins/holder/holder.js?v=v1.0.1-rc2&sv=v0.0.1.1?data=<?php echo date('Y-m-d'); ?>"></script>
    <script src="<?php echo frontTheme; ?>assets/components/modules/admin/modals/assets/js/bootbox.min.js?v=v1.0.1-rc2&sv=v0.0.1.1?data=<?php echo date('Y-m-d'); ?>"></script>
    <script src="<?php echo frontTheme; ?>assets/components/core/js/core.init.js?v=v1.0.1-rc2?data=<?php echo date('Y-m-d'); ?>"></script>


    <script src="<?php echo JS; ?>appuser.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){

    })
    </script>
</body>
</html>